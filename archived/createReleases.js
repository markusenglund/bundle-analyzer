require("dotenv").config();
const { promisify } = require("util");
const { db, connect } = require("../db");
const logger = require("../logger");

const delay = promisify(setTimeout);

const createReleases = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  const aggregationCursor = db.tokens.aggregate([
    {
      $project: {
        tokens: {
          $reduce: {
            input: "$tokensByFile",
            initialValue: [],
            in: { $concatArrays: ["$$value", "$$this.tokens"] }
          }
        },
        files: {
          $reduce: {
            input: "$tokensByFile",
            initialValue: [],
            in: {
              $concatArrays: [
                "$$value",
                {
                  $let: {
                    vars: { lastArrayItem: { $arrayElemAt: ["$$value", -1] } },
                    in: [
                      {
                        name: "$$this.fileName",
                        startIndex: {
                          $ifNull: ["$$lastArrayItem.endIndex", 0]
                        },
                        endIndex: {
                          $add: [
                            { $ifNull: ["$$lastArrayItem.endIndex", 0] },
                            { $size: "$$this.tokens" }
                          ]
                        }
                      }
                    ]
                  }
                }
              ]
            }
          }
        },
        numTokens: 1,
        size: 1,
        libraryName: 1,
        version: 1,
        error: 1
      }
    },
    { $out: "releases" }
  ]);
  await aggregationCursor.toArray();

  logger.info("Created new collection");

  await delay(2000);

  const asdf = await db.releases.createIndex({ tokens: 1 });

  console.log("asdf", asdf);

  logger.info("Done");

  process.exit(0);
};
/*
Structure of document
{
  _id: "jsdom@1.2.3",
  tokens: ["undefined", "yolo"],
  files: [{name: "/yolo.js", startIndex: 8, endIndex: 16}],
  numTokens: 20,
  size: 124567,
  libraryName: "jsdom"
  version: "1.2.3"
}

*/

createReleases();
