const fuzzyMatch = (needle, haystack) => {
  const matrix = [];

  for (let row = 0; row <= needle.length; row += 1) {
    matrix[row] = [row];
  }

  for (let col = 0; col <= haystack.length; col += 1) {
    matrix[0][col] = 0;
  }

  for (let row = 1; row <= needle.length; row += 1) {
    for (let col = 1; col <= haystack.length; col += 1) {
      if (needle[row - 1] === haystack[col - 1]) {
        matrix[row][col] = matrix[row - 1][col - 1];
      } else {
        matrix[row][col] =
          Math.min(
            matrix[row - 1][col - 1], // substitution
            matrix[row][col - 1], // insertion
            matrix[row - 1][col] // deletion
          ) + 1;
      }
    }
  }

  const editDistance = Math.min(...matrix[needle.length]);
  const endCol = matrix[needle.length].indexOf(editDistance);
  const startCol = findStartCol(matrix, endCol);
  const matchedSubArray = haystack.slice(startCol - 1, endCol);

  const similarity = 1 - editDistance / needle.length;

  return { editDistance, subArray: matchedSubArray, similarity };
};

const findStartCol = (matrix, endCol) => {
  if (endCol === 0) return 0;
  let curRow = matrix.length - 1;
  let curCol = endCol;
  while (curRow > 0) {
    const minNearbyValue = Math.min(
      matrix[curRow - 1][curCol - 1], // substitution
      matrix[curRow][curCol - 1], // insertion
      matrix[curRow - 1][curCol] // deletion
    );

    [curRow, curCol] = [
      [curRow, curCol - 1],
      [curRow - 1, curCol],
      [curRow - 1, curCol - 1]
    ].find(([row, col]) => matrix[row][col] === minNearbyValue);
  }

  return curCol;
};

module.exports = fuzzyMatch;
