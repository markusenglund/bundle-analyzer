const semver = require("semver");
const logger = require("../logger");
const { getLibraryReleaseManifests } = require("../npm");
const tokenizeReleases = require("../tokenizeReleases");
const { connect, db } = require("../db");
const getEstDownloadsByVersion = require("../getEstDownloadsByVersion");

const MAX_RELEASES = 300;

const tokenizeLibrary = async libraryId => {
  logger.info("Starting up...");
  await connect();
  logger.info(`Connected to db, fetching library ${libraryId}`);
  const { releaseManifests, releaseDates } = await getLibraryReleaseManifests(
    libraryId
  );
  logger.info(`Fetched ${releaseManifests.length} manifests for ${libraryId}`);

  const { downloads } = await db.libraries.findOne({
    _id: libraryId
  });
  const versions = releaseManifests.map(({ version }) => version);

  const estDownloadsByVersion = getEstDownloadsByVersion({
    releaseDates,
    monthlyDownloads: downloads,
    versions
  });

  const enhancedReleaseManifests = releaseManifests
    // ! For testing
    .filter(({ version }) => version === "2.0.6")
    .map(manifest => ({
      ...manifest,
      releaseDate: releaseDates?.[manifest.version]
        ? new Date(releaseDates[manifest.version])
        : null,
      estDownloads: estDownloadsByVersion[manifest.version]
    }));

  // Get all releases above a downloads threshold - capped at MAX_RELEASES per library
  // Then sort again by version to enable most efficient caching
  const filteredReleaseManifests = enhancedReleaseManifests
    .sort((a, b) => b.estDownloads - a.estDownloads)
    .slice(0, MAX_RELEASES)
    .filter(manifest => manifest.estDownloads > 1000)
    .filter(manifest => {
      const size = manifest.dist.unpackedSize;
      if (!size) return true;
      // Filter out releases bigger than 100MB
      if (size > 1024 * 1024 * 100) return false;
      // Filter out releases with low downloads and big size
      return manifest.estDownloads / Math.sqrt(size) > 3;
    })
    .sort((a, b) => semver.rcompare(a.version, b.version));

  logger.info(
    `Filtered out ${
      enhancedReleaseManifests.length - filteredReleaseManifests.length
    } out of ${
      enhancedReleaseManifests.length
    } releases due to having to few estimated downloads or being too big`
  );

  const results = await tokenizeReleases({
    releaseManifests: filteredReleaseManifests,
    libraryId
  });
  const resultsWithErrors = results.filter(({ error }) => error);
  logger.info(`Finished ${libraryId} analysis - ${results.length} versions`);
  if (resultsWithErrors.length) {
    logger.warn(
      `${resultsWithErrors.length} versions had errors: ${resultsWithErrors
        .map(({ version }) => version)
        .join(", ")}`
    );
  }
  process.exit(0);
};

tokenizeLibrary("asap");
