const { uniqBy, partition } = require("lodash");
const { db } = require("../db");
const getBundlePositionInterval = require("./getBundlePositionInterval");

const mediumStage2 = async ({
  secondStageReleases,
  overlapMap,
  bundledLibraries
}) => {
  // Stage 2
  // Get release-tokens, release-files & release-index
  const releases = (
    await Promise.all(
      secondStageReleases.map(async ({ _id: releaseId }) => {
        const release = await db.releases.findOne({ _id: releaseId });
        const { tokens } = release;
        // Token->indices map for one specific release
        const releaseIndex = new Map();
        for (let j = 0; j < tokens.length; j++) {
          const token = tokens[j];
          releaseIndex.set(
            token,
            releaseIndex.get(token) ? [...releaseIndex.get(token), j] : [j]
          );
        }

        // TODO: Get full overlap between the set and the
        const tokenSet = new Set(tokens);

        // ?? Should we limit use of the lowest signal tokens (high df) to save on compute power?
        // const df30Tokens = [];
        // for (const token of tokenSet) {
        //   const df = global.dfMap.get(token) || 1;
        //   if (df < 30) {
        //     df30Tokens.push({
        //       token,
        //       releasePositions: releaseIndex.get(token),
        //       bundlePositions: global.haystackIndex.get(token)
        //     });
        //   }
        // }

        const numTokens = tokens.length;

        // Step 1 - Figure out in which interval the library is most likely to be
        // TODO: Also weight by number of positions in the bundle
        const sortedBundlePositions = [...tokenSet]
          .flatMap(token => {
            const bundlePositions = global.haystackIndex.get(token) || [];
            const df = global.dfMap.get(token) || 1;
            const tf = bundlePositions.length;
            // TODO: Big tf and df should be penalized more
            const weight = Math.sqrt(1 / (df * tf));
            return bundlePositions.map(value => ({
              value,
              weight,
              token,
              df,
              tf
            }));
          })
          .sort((a, b) => a.value - b.value);

        const {
          positionInterval,
          indexInterval,
          intervalSize,
          intervalScore
        } = getBundlePositionInterval(sortedBundlePositions, numTokens);

        // TODO: Figure out the two different types of intervalSize
        if (
          releaseId ===
          // "date-fns@1.30.1"
          // "history@4.9.0"
          "@emotion/core@10.0.16"
        ) {
          const interval = sortedBundlePositions.slice(
            indexInterval[0],
            indexInterval[1] + 1
          );
          console.log(
            `Interval (${positionInterval[1] - positionInterval[0]} tokens)`
            // { numTokens },
            // interval
          );
          // console.log("\n\n\n\n\n yolo");
          console.dir(
            sortedBundlePositions.filter(({ df }) => df < 10),
            { maxArrayLength: 300 }
          );
        }

        const maxIntervalScore = tokens.reduce((acc, token) => {
          const df = global.dfMap.get(token) || 1;
          const bundlePositions = global.haystackIndex.get(token);
          const tf = bundlePositions ? bundlePositions.length : 1;
          const weight = Math.sqrt(1 / (df * tf));
          return weight + acc;
        }, 0);

        const intervalScoreRatio = intervalScore / maxIntervalScore;
        // console.log({
        //   // positionInterval,
        //   maxIntervalScore,
        //   intervalScore
        // });
        // if (bundledReleases.includes(releaseId)) {
        //   console.log("bundle-release", {
        //     releaseId,
        //     intervalScoreRatio,
        //     maxIntervalScore
        //   });
        // }
        const library =
          releaseId.split("@")[0] || `@${releaseId.split("@")[1]}`;
        return {
          releaseId,
          intervalScoreRatio,
          intervalScore,
          maxIntervalScore,
          library,
          isBundled: bundledLibraries.includes(library)
        };
      })
    )
  ).sort((a, b) => b.intervalScoreRatio - a.intervalScoreRatio);
  // console.log(releases);

  logStats({ releases });

  return releases;
};

const logStats = ({ releases }) => {
  // Get best match of each library for better visibility
  const libraries = uniqBy(releases, "library");

  // Get average bundled and non-bundled scores

  const [bundledLibs2, nonBundledLibs2] = partition(
    libraries,
    ({ isBundled }) => isBundled
  );

  const avgBundledScore2 =
    bundledLibs2.reduce(
      (acc, { intervalScoreRatio }) => acc + intervalScoreRatio,
      0
    ) / bundledLibs2.length;

  const avgNonBundledScore2 =
    nonBundledLibs2.reduce(
      (acc, { intervalScoreRatio }) => acc + intervalScoreRatio,
      0
    ) / nonBundledLibs2.length;

  console.log({ avgBundledScore2, avgNonBundledScore2 });

  const nonBundledLibraryReleases2 = releases.filter(
    ({ releaseId, isBundled }) =>
      !isBundled || global.PARTIAL_MATCH_BASKET.includes(releaseId)
  );
  const basketRanks2 = {};
  for (let j = 0; j < nonBundledLibraryReleases2.length; j += 1) {
    const {
      releaseId,
      intervalScoreRatio,
      maxIntervalScore
    } = nonBundledLibraryReleases2[j];
    if (global.PARTIAL_MATCH_BASKET.includes(releaseId)) {
      basketRanks2[releaseId] = {
        rank: j,
        intervalScoreRatio,
        maxIntervalScore
      };
    }
  }
  console.log({ basketRanks2 });
};

module.exports = mediumStage2;
