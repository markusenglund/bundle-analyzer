require("dotenv").config();
const { db, connect } = require("../db");
const logger = require("../logger");

const getReverseLibraryIndex = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  // TODO: Index tokens for all 76k releases
  const cursor = db.tokens.find({}); //.limit(1000);

  let j = 0;
  // TODO: Split up into chunks to save memory
  let reverseIndex = new Map();
  for await (const { tokensByFile, _id, libraryName, numTokens } of cursor) {
    const release = _id.replace(/\./g, "-");
    if (!tokensByFile) continue;
    for (const { tokens, fileName } of tokensByFile) {
      const parsedFileName = fileName.replace(/\./g, "-");
      for (let i = 0; i < tokens.length; i += 1) {
        let token = tokens[i];
        if (token instanceof RegExp) {
          token = token.toString();
        }
        let tokenData;
        if (!reverseIndex.has(token)) {
          tokenData = {
            releases: [],
            dataByRelease: {}
          };
        } else {
          tokenData = reverseIndex.get(token);
        }
        if (!tokenData.dataByRelease[release]) {
          tokenData.releases.push(release);
          tokenData.dataByRelease[release] = {
            libraryName,
            files: [],
            dataByFile: {}
          };
        }

        if (!tokenData.dataByRelease[release].dataByFile[parsedFileName]) {
          tokenData.dataByRelease[release].files.push(parsedFileName);
          tokenData.dataByRelease[release].dataByFile[parsedFileName] = {
            positions: []
          };
        }

        tokenData.dataByRelease[release].dataByFile[
          parsedFileName
        ].positions.push(i);

        reverseIndex.set(token, tokenData);
      }
    }
    console.log(j);
    if (j % 100 === 0) {
      const { result } = await db.reverseIndex.bulkWrite(
        [...reverseIndex.entries()].map(
          ([token, { releases, dataByRelease }]) => ({
            updateOne: {
              filter: { token },
              update: {
                $set: Object.fromEntries(
                  Object.entries(dataByRelease).map(([release, data]) => [
                    `dataByRelease.${release}`,
                    data
                  ])
                ),
                $addToSet: { releases: { $each: releases } }
              },
              upsert: true
            }
          })
        )
      );
      console.log("Put 100 into db", j, _id);

      reverseIndex = new Map();
    }
    j += 1;
  }

  logger.info("Inserted into db");

  // TODO: Figure out how to handle updates of existing tokens in db
  // console.log(arr);
  // await db.libraryIndex
  process.exit(0);
};
/*
Structure of index

{
  [token]: {
    releases: [],
    dataByRelease: {
      [release]: {
        files: [],
        dataByFile: {
          [fileName]: {
            positions: [1, 2, 3],
          }
        }
      }

    }

  }
}

V2 for mongo

token: "yolo",
releases: [{
  release: "react@1.1.1"
  positions
  files: [{
    fileName: "",
    positions: [1, 2, 3]
  }],
  libraryName: "react"
}]

*/

getReverseLibraryIndex();

// Abandoned attempt at aggregating the index
// const aggregationCursor = db.tokens.aggregate([
//   { $limit: 10 },
//   { $unwind: { path: "$tokensByFile" } },
//   {
//     $unwind: {
//       path: "$tokensByFile.tokens",
//       includeArrayIndex: "tokensByFile.position"
//     }
//   },
//   // { $group: { _id: "$tokensByFile.tokens", releases: { $push: "$_id" },
//   // TODO: Push all data to array
//   {$group: {_id: "$tokensByFile.tokens", releases: {$push: { _id: "$_id", files: {  } }}}}
//   // TODO: Turn into object using arrayToObject
//   dataByRelease: {

//   } } }
// ]);
// const next = await aggregationCursor.next();
// console.log({ next });
