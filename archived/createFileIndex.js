require("dotenv").config();
const { promisify } = require("util");
const { db, connect } = require("../db");
const logger = require("../logger");

const sleep = promisify(setTimeout);

const k = 4;

// TODO: Consider logic for not storing duplicate files multiple times (when a new release doesn't change every file)
const createFileIndex = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  const cursor = db.releases.find({}).project({ tokens: 1, files: 1 });

  let newFiles = [];

  for await (const { tokens, files, _id } of cursor) {
    const tokens15 = tokens.map(token => String(token).slice(0, 15));
    for (const file of files) {
      const { name } = file;
      const kGrams = getFileKGrams({ file, tokens: tokens15 });

      newFiles.push({
        fileName: name,
        releaseId: _id,
        fourGrams: kGrams
      });
    }
    if (newFiles.length > 2000) {
      console.log("Bulk updating...", _id);
      await db.files2.insertMany(newFiles);
      newFiles = [];
      console.log("finished bulk update");
    }
  }
  await db.files2.insertMany(newFiles);
  logger.info("Done creating files");
  await sleep(2000);

  await db.files2.createIndex({ fourGrams: 1 });
  logger.info("Created fourgram index");
  await db.files2.createIndex({ fileName: 1, releaseId: 1 });
  logger.info("Done!");
};

const getFileKGrams = ({ file, tokens }) => {
  const { startIndex, endIndex } = file;
  // const stepSize = getStepSize(endIndex - startIndex);

  const kGrams = [];
  for (let i = startIndex; i + k - 1 < endIndex; i += 1) {
    let kGram = tokens[i];
    for (let j = i + 1; j < i + k; j++) {
      kGram = `${kGram}·${tokens[j]}`;
    }
    kGrams.push(kGram);
  }
  return kGrams;
};

// const getStepSize = numTokens => {
//   if (numTokens < 100) {
//     return 1;
//   }
//   if (numTokens < 300) {
//     return 2;
//   }
//   return 3;
// };

createFileIndex();
