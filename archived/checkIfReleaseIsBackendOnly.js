const checkIfReleaseIsBackendOnly = ({ modules, packageJson }) => {
  if (packageJson.browser) return false;
  if (modules.builtIn.size > 0) return true;
  return false;
};

module.exports = checkIfReleaseIsBackendOnly;
