const {
  promises: { readFile }
} = require("fs");
const {
  getEntryFileFromVolume,
  getFileContentMap,
  minify
} = require("./utils");
const streamTarballToVolume = require("./streamTarballToVolume");
const { getLibraryTarball } = require("./npm");
const logger = require("./logger");
const extractTokens = require("./libraryFns/extractTokens");
const fuzzyMatch = require("./utils/fuzzyMatch");

const evaluateSpeedtyping = async () => {
  // const emotionCore = await readFile(
  //   "./codeSamples/node_modules/@emotion/core/dist/core.esm.js",
  //   "utf8"
  // );
  const fileContentMap = await getBundledLibrary("date-fns@1.30.1");
  // const fileContentMap = await getBundledLibrary("@emotion/cache@10.0.15");
  // const fileContentMap = await getBundledLibrary("dequal@1.0.0");
  // const fileContentMap = await getBundledLibrary("extract-files@5.0.1");
  // const fileContentMap = await getBundledLibrary("graphql-hooks@3.6.1");
  // const fileContentMap = await getBundledLibrary(
  //   "graphql-hooks-memcache@1.2.2"
  // );
  // const fileContentMap = await getBundledLibrary("tiny-lru@6.0.1");
  // const fileContentMap = await getBundledLibrary("history@4.9.0");

  // const fileContentMap = await getBundledLibrary("loose-envify@1.4.0");
  // const fileContentMap = await getBundledLibrary("object-assign@4.1.1");
  // const fileContentMap = await getBundledLibrary("prop-types@15.7.2");
  // const fileContentMap = await getBundledLibrary("react@16.9.0");
  // const fileContentMap = await getBundledLibrary("react-dom@16.9.0");
  // const fileContentMap = await getBundledLibrary("react-head@3.1.1");
  // const fileContentMap = await getBundledLibrary("react@0.11.0");

  // const history = await readFile(
  //   "./__data__/libs/history/cjs/history.min.js",
  //   "utf8"
  // );
  // const emotionCache = await readFile(
  //   "./codeSamples/node_modules/@emotion/cache/dist/cache.cjs.prod.js"
  // );
  // const dateFnsFormat = await readFile(
  //   "./codeSamples/node_modules/date-fns/format/index.js"
  // );
  // const graphqlHooks = await readFile(
  //   "./codeSamples/node_modules/graphql-hooks/lib/graphql-hooks.js"
  // );
  // const dequal = await readFile(
  //   "./codeSamples/node_modules/dequal/dist/dequal.js"
  // );
  // const extractFiles = await readFile(
  //   "./codeSamples/node_modules/extract-files/lib/extractFiles.js"
  // );

  // const { code: minifiedEmotionCore } = await minify(emotionCore, {
  //   mangle: false,
  //   compress: {
  //     global_defs: {
  //       "process.env.NODE_ENV": "production"
  //     }
  //   }
  // });

  // const minifiedEmotion = await minify(emotionCore);
  const bundleJs = await readFile(
    // "./codeSamples/bundle.leastminified.js",
    "./codeSamples/bundle-with-lodash.js",
    "utf8"
  );

  const { tokens: haystack } = extractTokens(bundleJs);

  const fileStats = (
    await Promise.all(
      Array.from(fileContentMap).map(async ([filePath, content]) => {
        const minifiedContent = await minify(content);
        const { tokens: needle } = extractTokens(minifiedContent);
        const {
          editDistance,
          // subArray,
          similarity
        } = fuzzyMatch(needle, haystack);

        // console.dir(needle, { maxArrayLength: null });
        // console.dir(subArray, { maxArrayLength: null });
        if (needle.length >= 8) {
          // console.log({ filePath, needleLength: needle.length });
          // console.log({
          //   editDistance,
          //   similarity,
          //   length: subArray.length
          // });
          const score = similarity * Math.log10(needle.length);
          return {
            filePath,
            similarity,
            score,
            editDistance,
            length: needle.length
          };
        }
        return null;
      })
    )
  ).filter(Boolean);
  const filesInBundle = fileStats.filter(data => data.score > 0.8);
  console.log(fileStats);
  logger.info(
    `${filesInBundle.length} out of ${fileStats.length} files (${Math.round(
      (filesInBundle.length * 100) / fileStats.length
    )}%) matched the bundle:
${filesInBundle.map(({ filePath }) => filePath).join("\n")}`
  );
};
// evaluateSpeedtyping();

const getBundledLibrary = async libName => {
  // TODO: DOESN'T WORK!
  const tarballStream = await getLibraryTarball(libName);
  logger.info("Fetched tarball from npm registry");
  const volume = await streamTarballToVolume(tarballStream);
  logger.info("Extracted volume from tarball");
  const entryFile = await getEntryFileFromVolume(volume);
  logger.info(`Calculated the entry file: ${entryFile}`);
  const fileContentMap = await getFileContentMap(volume, entryFile);
  logger.info("Extracted contents of all files");
  return fileContentMap;
};

// getBundledLibrary("react-router");
evaluateSpeedtyping();
