const semver = require("semver");
// const { groupBy } = require("lodash");
const { differenceInHours, addDays } = require("date-fns");

const getEstDownloadsByVersion = ({
  releaseDates,
  monthlyDownloads,
  versions
}) => {
  // If releaseDates don't exist - give each release equal share of total downloads which we arbitrarily assume to be 24x monthlyDownloads
  if (!releaseDates) {
    const numVersions = versions.length;
    const estDownloads = (monthlyDownloads * 24) / numVersions;
    return Object.fromEntries(versions.map(version => [version, estDownloads]));
  }

  const {
    modified,
    created: libraryCreated,
    ...versionReleaseDates
  } = releaseDates;

  // Filter out all prerelease versions
  const nonPrereleaseReleases = Object.entries(versionReleaseDates)
    .map(([version, releaseDate]) => ({ version, releaseDate }))
    .filter(
      ({ version }) => semver.valid(version) && !semver.prerelease(version)
    );
  // .sort(semver.compare);

  // TODO: Maybe go back to this or in some other way implement some downloads for libraries that are not latest
  // Group releases by major version
  // const releasesGroupedByMajorVersion = groupBy(
  //   nonPrereleaseReleases,
  //   release => semver.major(release.version)
  // );

  // console.log(releasesGroupedByMajorVersion);

  // // Get a date interval where a major version was the latest available
  // const majorReleaseIntervals = Object.fromEntries(
  //   Object.entries(releasesGroupedByMajorVersion).map(
  //     ([majorVersion, [firstRelease]], i, arr) => {
  //       const nextMajorVersion = arr[i + 1];
  //       return [
  //         majorVersion,
  //         {
  //           start: firstRelease.releaseDate,
  //           end: nextMajorVersion?.releaseDate
  //         }
  //       ];
  //     }
  //   )
  // );
  // console.log(majorReleaseIntervals);

  // Get releases that were both latest and greatest (meaning we filter out updates for older versions)
  const latestReleases = nonPrereleaseReleases.filter((release, i) => {
    return (
      i === 0 ||
      semver.gt(release.version, nonPrereleaseReleases[i - 1].version)
    );
  });

  // Calculate interval where a specific version was the latest
  const downloadsByVersion = Object.fromEntries(
    latestReleases.map((release, i) => {
      const nextRelease = latestReleases[i + 1];
      const interval = {
        start: new Date(release.releaseDate),
        end: nextRelease ? new Date(nextRelease.releaseDate) : new Date()
      };

      // console.log(release.version);
      // Give a specific release 100% of estimated downloads when it was the latest version
      const estDownloads = calculateEstDownloads({
        monthlyDownloads,
        libraryCreated,
        interval
      });

      return [release.version, estDownloads];
    })
  );

  return downloadsByVersion;
};

const differenceInDays = (d1, d2) =>
  Math.floor(differenceInHours(d1, d2) || 0) / 24;

// TODO: Stop assuming linear growth and implement a mf integral
const calculateEstDownloads = ({
  monthlyDownloads,
  libraryCreated,
  interval
}) => {
  const libraryCreatedDate = new Date(libraryCreated);
  const curDate = new Date();
  const estCurrentDailyDownloads = monthlyDownloads / 30;

  const daysSinceLibraryCreation = differenceInDays(
    curDate,
    libraryCreatedDate
  );

  const daysAsLatestRelease = differenceInDays(interval.end, interval.start);

  const middleOfReleaseInterval = addDays(
    interval.start,
    daysAsLatestRelease / 2
  );

  // Calculate estimated downloads for the middle day of the release interval and multiply with interval length
  const averageDailyDownloads =
    (estCurrentDailyDownloads *
      differenceInDays(middleOfReleaseInterval, libraryCreatedDate)) /
    daysSinceLibraryCreation;

  const estDownloads = Math.round(averageDailyDownloads * daysAsLatestRelease);

  return estDownloads;
};

module.exports = getEstDownloadsByVersion;
