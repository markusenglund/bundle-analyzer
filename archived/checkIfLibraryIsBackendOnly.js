const { getLibraryTarball } = require("../npm");
const {
  getEntryFileFromVolume,
  getImportedModules,
  getPackageJson
} = require("../utils");
const streamTarballToVolume = require("../streamTarballToVolume");
const logger = require("../logger");

const checkIfLibraryIsBackendOnly = async library => {
  try {
    // TODO: THIS DOESN'T WORK!
    const tarballStream = await getLibraryTarball(library._id);
    const volume = await streamTarballToVolume(tarballStream);

    const packageJson = getPackageJson(volume);

    const { browser } = packageJson;

    if (browser) {
      logger.info(
        `Package ${library._id} has browser field in package.json - presumed to work in front-end.`
      );
      return { result: false };
    }

    const entryFilePath = await getEntryFileFromVolume(volume);
    const modules = await getImportedModules({
      volume,
      entryFilePath,
      packageJson
    });

    if (modules.builtIn.size > 0) {
      // It's a backend only package
      logger.info(
        `Package ${library._id} uses built in node modules: ${[
          ...modules.builtIn
        ].join(", ")}`
      );
      return { result: true };
    }

    // If library doesn't import any built-in modules, it's assumed to work in the browser
    return { result: false };
  } catch (err) {
    if (err.name === "EnhancedResolveError") {
      logger.warn(`Could not resolve file for ${library._id}: ${err.message}`);
      return { error: err.message, result: false };
    }
    throw err;
  }
};

module.exports = checkIfLibraryIsBackendOnly;
