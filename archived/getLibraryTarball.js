const fetch = require("node-fetch");
const pacote = require("pacote");

// TODO: Maybe just use got instead of pacote in case we don't need it
const getLibraryTarball = async libSpecifier => {
  const tarballUrl = await pacote.resolve(libSpecifier);
  const stream = await fetch(tarballUrl).then(res => res.body);
  return stream;
};

module.exports = getLibraryTarball;
