require("dotenv").config();
const { promisify } = require("util");
const { db, connect } = require("../db");
const logger = require("../logger");

const sleep = promisify(setTimeout);

const createKGramIndex = async (k = 4) => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  const cursor = db.releases.find({}).project({ tokens: 1, files: 1 });

  let newReleases = [];
  for await (const release of cursor) {
    const { tokens, files, _id } = release;

    const step = Math.min(Math.ceil(tokens.length / 200), 10);
    // Add a max length for each token
    const tokensMaxLength = tokens.map(token => String(token).slice(0, 15));

    const kGrams = [];
    for (const { startIndex, endIndex } of files) {
      for (let i = startIndex; i + k - 1 < endIndex; i += step) {
        let kGram = tokensMaxLength[i];
        for (let j = i + 1; j < i + k; j++) {
          kGram = `${kGram}·${tokensMaxLength[j]}`;
        }

        kGrams.push(kGram);
      }
    }
    newReleases.push({ _id, [`${k}Grams`]: kGrams });
    if (newReleases.length > 100) {
      console.log("Bulk updating...", _id);
      await db.releases4.insertMany(newReleases);
      newReleases = [];
      console.log("finished bulk update");
    }
  }
  await db.releases4.insertMany(newReleases);

  logger.info("Created Everything");

  await sleep(2000);

  const asdf = await db.releases4.createIndex({ [`${k}Grams`]: 1 });

  console.log("asdf", asdf);

  logger.info("Done");

  process.exit(0);
};

createKGramIndex();

// ! Takes a minute / 1000 releases
// await db.releases
//   .aggregate([
//     // { $limit: 1000 },
//     {
//       $addFields: {
//         result: {
//           $reduce: {
//             input: "$tokens",
//             initialValue: {
//               prevValue: null,
//               biwords: []
//             },
//             in: {
//               $cond: {
//                 if: "$$value.prevValue",
//                 then: {
//                   prevValue: "$$this",
//                   biwords: {
//                     $concatArrays: [
//                       "$$value.biwords",
//                       [
//                         {
//                           a: "$$value.prevValue",
//                           b: "$$this"
//                         }
//                       ]
//                     ]
//                   }
//                 },
//                 else: {
//                   prevValue: "$$this",
//                   biwords: "$$value.biwords"
//                 }
//               }
//             }
//           }
//         }
//       }
//     },
//     {
//       $project: { biwords: "$result.biwords" }
//     },
//     { $project: { result: 0 } },
//     { $out: "releases2" }
//   ])
//   .toArray();
