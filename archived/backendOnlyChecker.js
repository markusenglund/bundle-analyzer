const path = require("path");
require("dotenv").config();
const { cpus } = require("os");
const Piscina = require("piscina");
const { default: PQueue } = require("p-queue");
const logger = require("../logger");
const { db, connect } = require("../db");

const cursorQueue = new PQueue({ concurrency: 1 });

const maxThreads = Math.min(4, cpus().length);

const workerPool = new Piscina({
  filename: path.join(
    __dirname,
    "../libraryFns/checkIfLibraryIsBackendOnly.js"
  ),
  maxThreads,
  idleTimeout: 1000
});

const markLibrariesAsBackendOnly = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db, fetching libraries sorted by downloads");

  const cursor = db.libraries
    .find({
      isBackendOnly: { $exists: false },
      tokenIndexingError: { $exists: false }
    })
    .sort({ downloads: -1 })
    .addCursorFlag("noCursorTimeout", true);

  const runIndefinitely = async () => {
    while (true) {
      // Prevent "cursor id is already in use" error by calling cursor.next one at a time.
      const library = await cursorQueue.add(() => cursor.next());
      if (library._id.startsWith("@types/")) {
        logger.info(
          `Skipping ${library._id} due to typescript types being useless.`
        );
        continue;
      }
      console.log(library._id);
      const { result: isBackendOnly, error } = await workerPool.runTask(
        library
      );
      if (error) {
        await db.libraries.updateOne(
          { _id: library._id },
          { $set: { tokenIndexingError: error } }
        );
      } else {
        await db.libraries.updateOne(
          { _id: library._id },
          { $set: { isBackendOnly } }
        );
      }

      logger.info(`Package ${library._id} set isBackendOnly: ${isBackendOnly}`);
    }
  };

  await Promise.all(
    Array.from({ length: maxThreads + 2 }).map(runIndefinitely)
  );
  await cursor.close();
};

markLibrariesAsBackendOnly();
