const path = require("path");
require("dotenv").config();
const {
  promises: { readFile }
} = require("fs");
const { db, connect } = require("../db");
const logger = require("../logger");
const extractTokens = require("../libraryFns/extractTokens");
const fuzzyMatch = require("../utils/fuzzyMatch");

const getMatchingLibraries = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  const bundleJs = await readFile(
    path.resolve(__dirname, "../codeSamples/bundle-with-lodash.js"),
    "utf8"
  );

  const { tokens: haystack } = extractTokens(bundleJs);

  // Get all tokens from 'react' from the DB
  const cursor = db.tokens.find({
    libraryName: "react",
    error: { $exists: false }
  });

  const numVersions = await cursor.count();

  logger.info(`Got ${numVersions} versions`);

  console.time("yolo");
  // TODO: Put all the versions in an array sorted by score
  const versions = [];

  // Load document frequencies into a map
  const dfs = await db.df.find({}).toArray();
  const dfMap = new Map(dfs.map(({ token, count }) => [token, count]));

  // TODO: Pick infrequent tokens from tokensByFile and see how many matches we get in haystack

  for await (const { tokensByFile, _id } of cursor) {
    const results = tokensByFile.map(({ fileName, tokens: needle }) => {
      const { editDistance, similarity } = fuzzyMatch(needle, haystack);

      return {
        fileName,
        similarity,
        // score,
        editDistance,
        needleLength: needle.length
      };
    });

    // const { totalEditDistance, totalNeedleLength} =
    const totalEditDistance = results.reduce((acc, result) => {
      return acc + result.editDistance;
    }, 0);
    const totalNeedleLength = results.reduce((acc, result) => {
      return acc + result.needleLength;
    }, 0);

    const similarity = 1 - totalEditDistance / totalNeedleLength;
    const score = similarity * Math.log10(totalNeedleLength);

    versions.push({ similarity, score, _id });

    // return { similarity, score, _id };
    logger.info(`${_id}: ${similarity}`);
  }

  // TODO: Output a list of library versions by likelyhood of matching
  const versionsSortedBySimilarity = [...versions].sort(
    (a, b) => b.similarity - a.similarity
  );

  console.log(
    versionsSortedBySimilarity.map(
      ({ _id, similarity }) => `${_id}: ${similarity}`
    )
  );

  console.timeEnd("yolo");
  process.exit(0);
};

getMatchingLibraries();
