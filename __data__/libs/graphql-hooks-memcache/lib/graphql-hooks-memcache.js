'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var LRU = _interopDefault(require('tiny-lru'));
var fnv1a = _interopDefault(require('@sindresorhus/fnv1a'));

function generateKey(keyObj) {
  return fnv1a(JSON.stringify(keyObj)).toString(36);
}

function memCache({
  size = 100,
  ttl = 0,
  initialState
} = {}) {
  const lru = LRU(size, ttl);

  if (initialState) {
    Object.keys(initialState).map(k => {
      lru.set(k, initialState[k]);
    });
  }

  return {
    get: keyObj => lru.get(generateKey(keyObj)),
    set: (keyObj, data) => lru.set(generateKey(keyObj), data),
    delete: keyObj => lru.delete(generateKey(keyObj)),
    clear: () => lru.clear(),
    keys: () => lru.keys(),
    getInitialState: () => lru.keys().reduce((initialState, key) => ({ ...initialState,
      [key]: lru.get(key)
    }), {})
  };
}

module.exports = memCache;
