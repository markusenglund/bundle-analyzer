(function (global, factory) {
typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
typeof define === 'function' && define.amd ? define(factory) :
(global = global || self, global.GraphQLHooksMemcache = factory());
}(this, function () { 'use strict';

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var tinyLru = createCommonjsModule(function (module, exports) {

(function (global) {

	class LRU {
		constructor (max = 0, ttl = 0) {
			this.first = null;
			this.items = {};
			this.last = null;
			this.max = max;
			this.size = 0;
			this.ttl = ttl;
		}

		has (key) {
			return key in this.items;
		}

		clear () {
			this.first = null;
			this.items = {};
			this.last = null;
			this.size = 0;

			return this;
		}

		delete (key) {
			if (this.has(key)) {
				const item = this.items[key];

				delete this.items[key];
				this.size--;

				if (item.prev !== null) {
					item.prev.next = item.next;
				}

				if (item.next !== null) {
					item.next.prev = item.prev;
				}

				if (this.first === item) {
					this.first = item.next;
				}

				if (this.last === item) {
					this.last = item.prev;
				}
			}

			return this;
		}

		evict () {
			const item = this.first;

			delete this.items[item.key];
			this.first = item.next;
			this.first.prev = null;
			this.size--;

			return this;
		}

		get (key) {
			let result;

			if (this.has(key)) {
				const item = this.items[key];

				if (this.ttl > 0 && item.expiry <= new Date().getTime()) {
					this.delete(key);
				} else {
					result = item.value;
					this.set(key, result, true);
				}
			}

			return result;
		}

		keys () {
			return Object.keys(this.items);
		}

		set (key, value, bypass = false) {
			let item;

			if (bypass || this.has(key)) {
				item = this.items[key];
				item.value = value;

				if (this.last !== item) {
					const last = this.last,
						next = item.next,
						prev = item.prev;

					if (this.first === item) {
						this.first = item.next;
					}

					item.next = null;
					item.prev = this.last;
					last.next = item;

					if (prev !== null) {
						prev.next = next;
					}

					if (next !== null) {
						next.prev = prev;
					}
				}
			} else {
				if (this.max > 0 && this.size === this.max) {
					this.evict();
				}

				item = this.items[key] = {
					expiry: this.ttl > 0 ? new Date().getTime() + this.ttl : this.ttl,
					key: key,
					prev: this.last,
					next: null,
					value
				};

				if (++this.size === 1) {
					this.first = item;
				} else {
					this.last.next = item;
				}
			}

			this.last = item;

			return this;
		}
	}

	function factory (max = 1000, ttl = 0) {
		if (isNaN(max) || max < 0) {
			throw new TypeError("Invalid max value");
		}

		if (isNaN(ttl) || ttl < 0) {
			throw new TypeError("Invalid ttl value");
		}

		return new LRU(max, ttl);
	}

	// Node, AMD & window supported
	{
		module.exports = factory;
	}
}());
});

const OFFSET_BASIS_32 = 2166136261;

const fnv1a = string => {
	let hash = OFFSET_BASIS_32;

	for (let i = 0; i < string.length; i++) {
		hash ^= string.charCodeAt(i);

		// 32-bit FNV prime: 2**24 + 2**8 + 0x93 = 16777619
		// Using bitshift for accuracy and performance. Numbers in JS suck.
		hash += (hash << 1) + (hash << 4) + (hash << 7) + (hash << 8) + (hash << 24);
	}

	return hash >>> 0;
};

var fnv1a_1 = fnv1a;
// TODO: remove this in the next major version, refactor the whole definition to:
var default_1 = fnv1a;
fnv1a_1.default = default_1;

function generateKey(keyObj) {
  return fnv1a_1(JSON.stringify(keyObj)).toString(36);
}

function memCache({
  size = 100,
  ttl = 0,
  initialState
} = {}) {
  const lru = tinyLru(size, ttl);

  if (initialState) {
    Object.keys(initialState).map(k => {
      lru.set(k, initialState[k]);
    });
  }

  return {
    get: keyObj => lru.get(generateKey(keyObj)),
    set: (keyObj, data) => lru.set(generateKey(keyObj), data),
    delete: keyObj => lru.delete(generateKey(keyObj)),
    clear: () => lru.clear(),
    keys: () => lru.keys(),
    getInitialState: () => lru.keys().reduce((initialState, key) => ({ ...initialState,
      [key]: lru.get(key)
    }), {})
  };
}

return memCache;

}));
