'use strict';

if (process.env.NODE_ENV === "production") {
  module.exports = require("./babel-preset-css-prop.cjs.prod.js");
} else {
  module.exports = require("./babel-preset-css-prop.cjs.dev.js");
}
