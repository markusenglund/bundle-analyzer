'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var _extends = _interopDefault(require('@babel/runtime/helpers/extends'));
var _objectWithoutPropertiesLoose = _interopDefault(require('@babel/runtime/helpers/objectWithoutPropertiesLoose'));
var jsx = _interopDefault(require('@babel/plugin-transform-react-jsx'));
var pragmatic = _interopDefault(require('@emotion/babel-plugin-jsx-pragmatic'));
var emotion = _interopDefault(require('babel-plugin-emotion'));

var pragmaName = '___EmotionJSX'; // pull out the emotion options and pass everything else to the jsx transformer
// this means if @babel/plugin-transform-react-jsx adds more options, it'll just work
// and if babel-plugin-emotion adds more options we can add them since this lives in
// the same repo as babel-plugin-emotion

var index = (function (api, _temp) {
  var _ref = _temp === void 0 ? {} : _temp,
      pragma = _ref.pragma,
      sourceMap = _ref.sourceMap,
      autoLabel = _ref.autoLabel,
      labelFormat = _ref.labelFormat,
      instances = _ref.instances,
      options = _objectWithoutPropertiesLoose(_ref, ["pragma", "sourceMap", "autoLabel", "labelFormat", "instances"]);

  return {
    plugins: [[pragmatic, {
      export: 'jsx',
      module: '@emotion/core',
      import: pragmaName
    }], [jsx, _extends({
      pragma: pragmaName,
      pragmaFrag: 'React.Fragment'
    }, options)], [emotion, {
      sourceMap: sourceMap,
      autoLabel: autoLabel,
      labelFormat: labelFormat,
      instances: instances,
      cssPropOptimization: true
    }]]
  };
});

exports.default = index;
