"use strict";

function _interopDefault(ex) {
  return ex && "object" == typeof ex && "default" in ex ? ex.default : ex;
}

Object.defineProperty(exports, "__esModule", {
  value: !0
});

var _extends = _interopDefault(require("@babel/runtime/helpers/extends")), _objectWithoutPropertiesLoose = _interopDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose")), jsx = _interopDefault(require("@babel/plugin-transform-react-jsx")), pragmatic = _interopDefault(require("@emotion/babel-plugin-jsx-pragmatic")), emotion = _interopDefault(require("babel-plugin-emotion")), pragmaName = "___EmotionJSX", index = function(api, _temp) {
  var _ref = void 0 === _temp ? {} : _temp, sourceMap = (_ref.pragma, _ref.sourceMap), autoLabel = _ref.autoLabel, labelFormat = _ref.labelFormat, instances = _ref.instances, options = _objectWithoutPropertiesLoose(_ref, [ "pragma", "sourceMap", "autoLabel", "labelFormat", "instances" ]);
  return {
    plugins: [ [ pragmatic, {
      export: "jsx",
      module: "@emotion/core",
      import: pragmaName
    } ], [ jsx, _extends({
      pragma: pragmaName,
      pragmaFrag: "React.Fragment"
    }, options) ], [ emotion, {
      sourceMap: sourceMap,
      autoLabel: autoLabel,
      labelFormat: labelFormat,
      instances: instances,
      cssPropOptimization: !0
    } ] ]
  };
};

exports.default = index;
