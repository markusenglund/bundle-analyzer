"use strict";

function _interopDefault(ex) {
  return ex && "object" == typeof ex && "default" in ex ? ex.default : ex;
}

Object.defineProperty(exports, "__esModule", {
  value: !0
});

var syntaxJsx = _interopDefault(require("@babel/plugin-syntax-jsx"));

function jsxPragmatic(babel) {
  var t = babel.types;
  return {
    inherits: syntaxJsx,
    pre: function() {
      if (!this.opts.module || !this.opts.import) throw new Error("@emotion/babel-plugin-jsx-pragmatic: You must specify `module` and `import`");
    },
    visitor: {
      Program: {
        exit: function(path, state) {
          state.get("jsxDetected") && path.unshiftContainer("body", function(state) {
            return t.importDeclaration([ t.importSpecifier(t.identifier(state.opts.import), t.identifier(state.opts.export || "default")) ], t.stringLiteral(state.opts.module));
          }(state));
        }
      },
      JSXElement: function(path, state) {
        state.set("jsxDetected", !0);
      },
      JSXFragment: function(path, state) {
        state.set("jsxDetected", !0);
      }
    }
  };
}

exports.default = jsxPragmatic;
