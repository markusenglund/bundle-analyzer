import syntaxJsx from '@babel/plugin-syntax-jsx';

function jsxPragmatic(babel) {
  var t = babel.types;

  function getPragmaImport(state) {
    return t.importDeclaration([t.importSpecifier(t.identifier(state.opts.import), t.identifier(state.opts.export || 'default'))], t.stringLiteral(state.opts.module));
  }

  return {
    inherits: syntaxJsx,
    pre: function pre() {
      if (!(this.opts.module && this.opts.import)) {
        throw new Error('@emotion/babel-plugin-jsx-pragmatic: You must specify `module` and `import`');
      }
    },
    visitor: {
      Program: {
        exit: function exit(path, state) {
          if (!state.get('jsxDetected')) return; // Apparently it's now safe to do this even if Program begins with
          // directives.

          path.unshiftContainer('body', getPragmaImport(state));
        }
      },
      JSXElement: function JSXElement(path, state) {
        state.set('jsxDetected', true);
      },
      JSXFragment: function JSXFragment(path, state) {
        state.set('jsxDetected', true);
      }
    }
  };
}

export default jsxPragmatic;
