"use strict";

exports.__esModule = true;
exports.default = void 0;

var _prism = _interopRequireDefault(require("./vendor/prism"));

var _duotoneDark = _interopRequireDefault(require("../themes/duotoneDark"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var defaultProps = {
  // $FlowFixMe
  Prism: _prism.default,
  theme: _duotoneDark.default
};
var _default = defaultProps;
exports.default = _default;