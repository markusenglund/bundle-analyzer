"use strict";

exports.__esModule = true;
exports.default = void 0;

var _prism = _interopRequireDefault(require("./vendor/prism"));

exports.Prism = _prism.default;

var _defaultProps = _interopRequireDefault(require("./defaultProps"));

exports.defaultProps = _defaultProps.default;

var _Highlight = _interopRequireDefault(require("./components/Highlight"));

var _normalizeTokens = _interopRequireDefault(
  require("./utils/normalizeTokens")
);

exports.normalizeTokens = _normalizeTokens.default;

var _themeToDict = _interopRequireDefault(require("./utils/themeToDict"));

exports.themeToDict = _themeToDict.default;

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var _default = _Highlight.default;
exports.default = _default;