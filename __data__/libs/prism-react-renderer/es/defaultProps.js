import Prism from './vendor/prism';
import theme from '../themes/duotoneDark';
var defaultProps = {
  // $FlowFixMe
  Prism: Prism,
  theme: theme
};
export default defaultProps;