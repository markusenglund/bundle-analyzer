import Prism from "./vendor/prism";
import defaultProps from "./defaultProps";
import Highlight from "./components/Highlight";
import normalizeTokens from "./utils/normalizeTokens";
import themeToDict from "./utils/themeToDict";
export { Prism, defaultProps, normalizeTokens, themeToDict };
export default Highlight;