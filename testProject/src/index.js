// import "@angular/google-maps";
// import "@angular/material";
// import "@angular/router";
// import "@angular/youtube-player";
// import { NgModule } from "@angular/core";
// import { BrowserModule } from "@angular/platform-browser";
// import { Routes, RouterModule, PreloadAllModules } from "@angular/router";
// import { ActivatedRoute, Router } from "@angular/router";
// import { HttpClient } from "@angular/common/http";
// import "ngx-bootstrap";
// import "ngx-toastr";

import "@auth0/auth0-react";
import { ChakraProvider } from "@chakra-ui/react";
import * as Core from "@emotion/core";
import "@emotion/styled";
import * as Sentry from "@sentry/react";
import algolia from "algoliasearch";
import "axios";
import "bezier-easing";
import "bootstrap";
import "browser-cookies";
import "chart.js";
import "codemirror";
import "core-js";
import * as d3 from "d3";
import "dayjs";
import "draft-js";
import * as draftImportHtml from "draft-js-import-html";
import "draftjs-to-html";
import "fast-deep-equal";
import "file-saver";
import "final-form";
import "firebase";
import "formik";
import "i18next";
import "immutable";
import "joi";
import "jquery";
import "lodash";
import "marked";
import "material-ui";
import "moment";
import "nanoid";
import normalizr from "normalizr";
import "prismjs";
import "prop-types";
import "qs";
import "query-string";
import "react";
import * as Admin from "react-admin";
import "react-animated-number";
import "react-aria-menubutton";
import BeautifulDnd from "react-beautiful-dnd";
import Colorful from "react-colorful";
import "react-copy-to-clipboard";
import "react-day-picker";
import * as Dnd from "react-dnd";
import * as HtmlBackend from "react-dnd-html5-backend";
import "react-dom";
import "react-draft-wysiwyg";
import "react-draggable";
import "react-dropbox-chooser";
import "react-dropzone";
import "react-final-form";
import "react-google-picker";
import "react-helmet";
import "react-icons";
import * as InstantSearch from "react-instantsearch-dom";
import "react-modal";
import "react-modern-calendar-datepicker";
import "react-pdf";
import * as Popper from "react-popper";
import "react-redux";
import "react-reflex";
import Router from "react-router";
import Select from "react-select";
import "react-sidebar";
import Spinners from "react-spinners";
import "react-sticky";
import "react-switch";
import "react-tap-event-plugin";
import TextareaAutosize from "react-textarea-autosize";
import Toastify from "react-toastify";
import "react-tooltip";
import "react-trumbowyg";
import "react-youtube";
import * as Strap from "reactstrap";
import * as Recharts from "recharts";
import "redux";
import * as Rxjs from "rxjs";
import "shortid";
import "url-join";
import Vue from "vue";
import "zone.js";
import * as ApolloHooks from "@apollo/react-hooks";
import "airtable";
import Boost from "apollo-boost";
import LinkBatch from "apollo-link-batch-http";
import * as LinkContext from "apollo-link-context";
import "cropperjs";
import Downshift from "downshift";
import "escope";
import * as Framer from "framer-motion";
import "fuse.js";
import "glamor";
import immer from "immer";
import "jszip";
import "keyboardjs";
import "localforage";
import mobx from "mobx";
import mobxReact from "mobx-react";
import mobxStateTree from "mobx-state-tree";
import "react-color";
import "react-markdown";
import "react-motion";
import "react-virtualized";
import Window from "react-window";
import "i18next-browser-languagedetector";
import "mediasoup-client";
import Zustand from "zustand";

function component() {
  const element = document.createElement("div");
  console.log(
    ChakraProvider,
    Core,
    Sentry,
    algolia,
    d3,
    draftImportHtml,
    normalizr,
    Admin,
    BeautifulDnd,
    Colorful,
    Dnd,
    HtmlBackend,
    InstantSearch,
    Popper,
    Router,
    Select,
    Spinners,
    TextareaAutosize,
    Toastify,
    Strap,
    Rxjs,
    Vue,
    ApolloHooks,
    Boost,
    LinkBatch,
    LinkContext,
    Downshift,
    Framer,
    immer,
    mobx,
    mobxReact,
    mobxStateTree,
    Window,
    Zustand,
    Recharts
  );
  // const days = moment.unix();
  // console.log({ days });
  // Lodash, currently included via a script, is required for this line to work
  // element.innerHTML = _.join(["Hello", "webpack"], " ");

  return element;
}

document.body.appendChild(component());
