import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import { terser } from "rollup-plugin-terser";
import analyze from "rollup-plugin-analyzer";

const getPlugins = () => [
  resolve({
    extensions: [".js", ".jsx"]
  }),
  commonjs(),
  ...(process.env.NODE_ENV === "production"
    ? [terser(), analyze({ summaryOnly: true })]
    : [])
];
//
const client = {
  input: "src/index.js",
  output: {
    dir: "dist",
    entryFileNames: "bundle.js",
    format: "cjs"
  },
  plugins: getPlugins()
};

export default [client];
