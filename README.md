# Setup

## Pre-requisites

- Node 16
- Yarn 1.22
- MongoDB

## Steps

- Run `yarn install` in root folder
- You need to seed the db with the library index data to get results. Follow these steps:
  - Run MongoDB on your local machine
  - Download db archive
    - [https://drive.google.com/file/d/12cNEvKpWGr25Dq557ienJsc9ruosYjtg/view?usp=share_link](https://drive.google.com/file/d/12cNEvKpWGr25Dq557ienJsc9ruosYjtg/view?usp=share_link)
    - In the folder where the archive file was downloaded, run `mongorestore --archive=bundle_scanner_analysis_db.agz` to restore it to your local db
- Go to `packages/analysis-server` and `packages/api-server` and copy the contents of .env.dist files into new files with the name `.env`. Fill in the `MONGO_URL_DEV` variables with something that looks like this:
  - Analysis db: `mongodb://127.0.0.1:27017/bundle_scanner_analysis_db`
  - API db: `mongodb://127.0.0.1:27017/bundle_scanner_api_db`
- Build the app
  - Run `yarn build` to build the JS files for each package (parts of the code base still have type errors, so you'll see a lot of warning messages.)
  - When you’re coding, go to the specific package you’re changing and run `yarn build:dev` to run that part in watch.
- Start the servers in separate terminals
  - `cd packages/api-server && yarn start:dev`
  - `cd packages/analysis-server && yarn start:dev`
- Go to `http://localhost:1337` - you should see the Bundle scanner homepage. Querying a URL should give results. If not, most likely the .env varible for the analysis DB is not correct.
