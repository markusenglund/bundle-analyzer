import { config } from "dotenv";
config();
// import fs from "fs";
import { logger, mongo } from "@bundle-scanner/common";

const { db, connect } = mongo;

const addSourceMapFrequencyToLibraries = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  const librariesCursor = db.libraries.find({}).project({ _id: 1 });
  const allLibraryIds = new Set();

  for await (const { _id: libraryId } of librariesCursor) {
    allLibraryIds.add(libraryId);
  }
  logger.info("Fetched all library IDs");

  const librariesWithSourceMapFrequencies = await db.sourceMappedBundles
    .aggregate([
      {
        $match: {
          "sourceMapData.libraryIds.0": {
            $exists: true,
          },
        },
      },
      {
        $project: {
          "sourceMapData.libraryIds": 1,
          bundleDomain: 1,
          domain: {
            $arrayElemAt: ["$domains", 0],
          },
        },
      },
      {
        $group: {
          _id: "$sourceMapData.libraryIds",
          count: {
            $sum: 1,
          },
          domain: {
            $first: "$domain",
          },
        },
      },
      {
        $addFields: {
          libraryIds: "$_id",
        },
      },
      {
        $unwind: {
          path: "$libraryIds",
        },
      },
      {
        $group: {
          _id: "$domain",
          libraryIds: {
            $addToSet: "$libraryIds",
          },
        },
      },
      {
        $unwind: {
          path: "$libraryIds",
        },
      },
      {
        $group: {
          _id: "$libraryIds",
          sourceMapFrequency: {
            $sum: 1,
          },
        },
      },
      {
        $sort: {
          sourceMapFrequency: -1,
        },
      },
    ])
    .toArray();

  logger.info("Fetched all source map frequencies");
  const untrackedLibraryIds = [];
  for (const { _id: libraryId, sourceMapFrequency } of librariesWithSourceMapFrequencies) {
    if (allLibraryIds.has(libraryId)) {
      await db.libraries.updateOne({ _id: libraryId }, { $set: { sourceMapFrequency } });
    } else {
      untrackedLibraryIds.push(libraryId);
    }
  }

  // fs.writeFileSync("./untracked-libraries.json", JSON.stringify(untrackedLibraryIds));

  // console.log({ untrackedLibraryIds });
  logger.info("Done!");
  process.exit(0);
};

addSourceMapFrequencyToLibraries();
