import { readFile } from "fs/promises";
import chalk from "chalk";
import {
  differenceBy,
  orderBy,
  groupBy,
  sumBy,
  round,
  partition,
  truncate,
  keyBy,
} from "lodash-es";
import { logger, mongo, tokenUtils } from "@bundle-scanner/common";
import scanBundle from "@bundle-scanner/scanner";

const { getFourGrams } = tokenUtils;
const { connect, db } = mongo;

const getMatchingReleases = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  const BUNDLE_URL = "video-api.wsj.com/api-video/player/v3/js/video.min.js";
  const benchmarkBundle = await db.benchmarkBundles.findOne({
    _id: BUNDLE_URL,
  });

  // const { scriptUrl, libraryIds, codeId, sourceMapId } = benchmarkBundle
  const {
    code: bundleJs,
    sourceMapData: { libraryIds, intervals: actualIntervals },
  } = benchmarkBundle;

  logger.info(`Analyzing script '${BUNDLE_URL}'`);

  const bundledReleases = [
    ...new Set(
      libraryIds.map((libraryId) => {
        if (libraryId.startsWith("lodash")) return "lodash";
        if (libraryId === "rxjs-es") return "rxjs";
        if (libraryId === "core-js-pure") return "core-js";
        if (libraryId === "apollo-client") return "@apollo/client";
        if (["jquery-min", "jquery-slim"].includes(libraryId)) return "jquery";
        return libraryId;
      })
    ),
  ].map((libraryId) => ({ libraryId }));

  console.time("Scan bundle total");
  const { matchedReleases, sortedIntervals, fourGramPositions } = await scanBundle({
    code: bundleJs,
    bundleUrl: BUNDLE_URL,
    bundledReleases,
  });
  console.timeEnd("Scan bundle total");

  const releasesBundledInOtherLibrary = matchedReleases.filter(
    ({ isBundledDependency }) => isBundledDependency
  );
  // Convert sortedIntervals to charIndexedIntervals
  const calculatedCharIntervals = sortedIntervals.map(
    ({ fileName, libraryId, start, end, version, intervalMatchRatio }) => ({
      fileName,
      libraryId,
      version,
      start: fourGramPositions[start].start,
      end: fourGramPositions[end - 1].end,
      intervalMatchRatio,
    })
  );

  const calculatedReleasesByLibraryId = keyBy(matchedReleases, "libraryId");
  const releasesBundledInOtherLibraryByLibraryId = keyBy(
    releasesBundledInOtherLibrary,
    "libraryId"
  );

  const falsePositives = matchedReleases
    .filter(({ isBundledDependency }) => !isBundledDependency)
    .filter(({ isBundled }) => !isBundled)
    .map(({ libraryId }) => libraryId);
  const falseNegatives = differenceBy(bundledReleases, matchedReleases, "libraryId").map(
    ({ libraryId }) => libraryId
  );

  visualizeIntervalDiscrepancies({
    actualIntervals: actualIntervals.filter(({ fileName }) => !fileName.endsWith(".html")),
    releasesBundledInOtherLibrary,
    releasesBundledInOtherLibraryByLibraryId,
    calculatedCharIntervals,
    calculatedReleasesByLibraryId,
    bundleJs,
    falseNegatives,
  });

  const numPossiblePositives = bundledReleases.length;

  // const [bundledReleases, normalReleases] = partition(
  //   matchedReleases,
  //
  // );
  const matchedLibraryIds = matchedReleases.map(({ libraryId }) => libraryId);
  const truePositiveLibraryIds = matchedReleases.filter(({ isBundled }) => isBundled);
  logger.info(
    `Matched ${matchedLibraryIds.length} releases out of which ${truePositiveLibraryIds.length} are listed in source maps and ${releasesBundledInOtherLibrary.length} are presumed bundled in other libraries`
  );
  logger.info(
    `Matched ${truePositiveLibraryIds.length} out of ${numPossiblePositives} bundled libraries`
  );

  logger.info(`False positives: ${falsePositives.join(", ")}`);
  logger.info(`False negatives: ${falseNegatives.join(", ")}`);
  logger.info(
    `Bundled in other libraries: ${releasesBundledInOtherLibrary
      .map(({ libraryId }) => libraryId)
      .join(", ")}`
  );

  process.exit(0);
};

function visualizeIntervalDiscrepancies({
  calculatedCharIntervals,
  releasesBundledInOtherLibraryByLibraryId,
  releasesBundledInOtherLibrary,
  calculatedReleasesByLibraryId,
  actualIntervals,
  falseNegatives,
  bundleJs,
}) {
  const falseNegativeSet = new Set(falseNegatives);

  const humanReadableCalculatedIntervals = calculatedCharIntervals.map(
    ({ fileName, start, end, libraryId }) =>
      `${start}->${end}: ${libraryId} ${fileName.slice(0, 65)}`
  );
  const humanReadableActualIntervals = orderBy(actualIntervals, "start").map(
    ({ fileName, start, end, libraryId }) =>
      `${start}->${end}: ${libraryId ?? "[N/A]"} ${fileName.slice(0, 65)}`
  );

  const completelyMissedIntervals = [];
  // TODO: Figure out what to do with incorrectly identified intervals that were not part of a library - we simply get to this later in the false positives table
  const incorrectlyIdentifiedIntervals = [];
  const correctlyIdentifiedIntervals = [];
  for (const actualInterval of actualIntervals) {
    const intersectingCalculatedIntervals = calculatedCharIntervals
      .map((calculatedInterval) => ({
        fileName: calculatedInterval.fileName,
        libraryId: calculatedInterval.libraryId,
        intersection: calculateIntervalIntersection(actualInterval, calculatedInterval),
      }))
      .filter(({ intersection }) => intersection);

    const [correctlyIntersectingCalculatedIntervals, incorrectlyIntersectingCalculatedIntervals] =
      partition(
        intersectingCalculatedIntervals,
        ({ libraryId }) =>
          libraryId === actualInterval.libraryId ||
          releasesBundledInOtherLibraryByLibraryId[libraryId]
      );
    if (incorrectlyIntersectingCalculatedIntervals.length > 0) {
      incorrectlyIdentifiedIntervals.push({
        ...actualInterval,
        incorrectlyIntersectingCalculatedIntervals,
        incorrectlyIdentifiedLibraries: incorrectlyIntersectingCalculatedIntervals.map(
          ({ libraryId }) => libraryId
        ),
      });
    }
    if (correctlyIntersectingCalculatedIntervals.length > 0) {
      correctlyIdentifiedIntervals.push({
        ...actualInterval,
        correctlyIntersectingCalculatedIntervals,
      });
    }

    if (!intersectingCalculatedIntervals.length) {
      completelyMissedIntervals.push(actualInterval);
    }
  }

  console.log(chalk.bold("Actual libraries"));
  const calculatedIntervalsByLibraryId = groupBy(calculatedCharIntervals, "libraryId");
  const missedIntervalsByLibraryId = groupBy(completelyMissedIntervals, "libraryId");
  const actualIntervalsByLibraryId = groupBy(actualIntervals, "libraryId");
  const actualLibraryIdSet = new Set(Object.keys(actualIntervalsByLibraryId));
  const incorrectlyIdentifiedIntervalsByLibraryId = groupBy(
    incorrectlyIdentifiedIntervals,
    "libraryId"
  );
  const correctlyIdentifiedIntervalsByLibraryId = groupBy(
    correctlyIdentifiedIntervals,
    "libraryId"
  );

  // console.log(
  //   "asdf",
  //   actualIntervalsByLibraryId["@angularclass/hmr"],
  //   calculatedIntervalsByLibraryId["@angularclass/hmr"]
  // );
  // process.exit(0);

  const actualLibraryStats = orderBy(
    Object.entries(actualIntervalsByLibraryId).map(([libraryId, intervals]) => {
      const missedIntervals = missedIntervalsByLibraryId[libraryId] ?? [];
      const numMissedIntervals = missedIntervals.length;
      const incorrectlyIdentifiedIntervals =
        incorrectlyIdentifiedIntervalsByLibraryId[libraryId] ?? [];
      const correctlyIdentifiedIntervals = correctlyIdentifiedIntervalsByLibraryId[libraryId] ?? [];
      const calculatedIntervals = calculatedIntervalsByLibraryId[libraryId] ?? [];
      const numFalsePositives = incorrectlyIdentifiedIntervals.length;
      const numActualIntervals = intervals.length;
      const falsePositiveIntervalsPct = round(100 * (numFalsePositives / numActualIntervals));
      const falselyIdentifiedLibs =
        truncate(
          [
            ...new Set(
              incorrectlyIdentifiedIntervals.flatMap(
                ({ incorrectlyIdentifiedLibraries }) => incorrectlyIdentifiedLibraries
              )
            ),
          ].join(", ")
        ) || undefined;
      const numCorrectlyIdentifiedIntervals = correctlyIdentifiedIntervals.length;
      const correctlyIdentifiedIntervalsPct = round(
        100 * (numCorrectlyIdentifiedIntervals / numActualIntervals)
      );
      const pctMissedIntervals = round((numMissedIntervals / numActualIntervals) * 100);
      const numActualIntervalChars = sumBy(intervals, (interval) => interval.end - interval.start);
      const largestCalculatedInterval = orderBy(
        calculatedIntervals,
        (interval) => interval.end - interval.start,
        "desc"
      )[0];
      const largestActualInterval = orderBy(
        intervals,
        (interval) => interval.end - interval.start,
        "desc"
      )[0];
      const correctlyIdentifiedIntervalIntersections = correctlyIdentifiedIntervals.flatMap(
        ({ correctlyIntersectingCalculatedIntervals }) =>
          correctlyIntersectingCalculatedIntervals.map(({ intersection, fileName }) => {
            const numIntersectionChars = intersection.end - intersection.start;
            return { numIntersectionChars, fileName, intersection };
          })
      );
      const numCorrectlyIdentifiedChars = sumBy(
        correctlyIdentifiedIntervalIntersections,
        "numIntersectionChars"
      );

      const incorrectlyIdentifiedIntervalIntersections = incorrectlyIdentifiedIntervals.flatMap(
        ({ incorrectlyIntersectingCalculatedIntervals }) =>
          incorrectlyIntersectingCalculatedIntervals.map(({ intersection, fileName }) => {
            const numIntersectionChars = intersection.end - intersection.start;
            return { numIntersectionChars, fileName, intersection };
          })
      );
      const numIncorrectlyIdentifiedChars = sumBy(
        incorrectlyIdentifiedIntervalIntersections,
        "numIntersectionChars"
      );

      const correctlyIdentifiedCharsPct = round(
        (numCorrectlyIdentifiedChars / numActualIntervalChars) * 100
      );
      const incorrectlyIdentifiedCharsPct = round(
        (numIncorrectlyIdentifiedChars / numActualIntervalChars) * 100
      );
      // const numMissedChars = sumBy(missedIntervals, (interval) => interval.end - interval.start);
      // const pctMissedChars = `${round((numMissedChars / numActualIntervalChars) * 100)}%`;
      const isPositivelyIdentified = !falseNegativeSet.has(libraryId);
      // if (libraryId === "@nguniversal/express-engine" && !isPositivelyIdentified) {
      //   for (const missedInterval of intervals) {
      //     logger.info(
      //       `Interval from ${libraryId} ${missedInterval.fileName}:\n${chalk.white(
      //         bundleJs.slice(missedInterval.start - 300, missedInterval.start)
      //       )}${chalk.green(bundleJs.slice(missedInterval.start, missedInterval.end))}${chalk.white(
      //         bundleJs.slice(missedInterval.end, missedInterval.end + 300)
      //       )}\n`
      //     );
      //   }
      // }

      const { sourceMapFrequency, version, score, idfScoreRatio, maxIdfScore } =
        calculatedReleasesByLibraryId[libraryId] ?? {};

      return {
        libraryId: libraryId !== "undefined" ? libraryId : "[NO LIBRARY]",
        "ID'd": isPositivelyIdentified,
        Files: numActualIntervals,
        "Main interv": `${largestActualInterval.start}-${largestActualInterval.end}`,
        "Main calc interv":
          largestCalculatedInterval &&
          `${largestCalculatedInterval.start}-${largestCalculatedInterval.end}`,
        "Main interv match %":
          largestCalculatedInterval && round(100 * largestCalculatedInterval.intervalMatchRatio),
        "Size chars": numActualIntervalChars,
        "Found chars %": correctlyIdentifiedCharsPct,
        "Incorrect chars %": incorrectlyIdentifiedCharsPct,
        "Found files %": correctlyIdentifiedIntervalsPct,
        // "Missed %": pctMissedIntervals,
        falselyIdentifiedLibs,
        // score: score && round(score, 1),
        idfRatio: idfScoreRatio && round(idfScoreRatio, 2),
      };
    }),
    ["Found chars %"],
    ["desc"]
  );

  console.table(actualLibraryStats);

  console.log(chalk.bold("Incorrect libraries"));

  const incorrectFileCalculatedIntervals = [];
  const incorrectLibraryCalculatedIntervals = [];
  // const incorrectFalsePositiveLibraryIntervals = [];
  // const incorrectNonLibraryIntervals = [];

  const falsePositiveIntervals = calculatedCharIntervals
    .map((calculatedInterval) => {
      const intersectingActualIntervals = actualIntervals
        .map((actualInterval) => ({
          ...actualInterval,
          intersection: calculateIntervalIntersection(actualInterval, calculatedInterval),
        }))
        .filter(({ intersection }) => intersection);

      if (
        intersectingActualIntervals.some(
          ({ intersection, libraryId }) =>
            libraryId === calculatedInterval.libraryId &&
            intersection.start === calculatedInterval.start &&
            intersection.end === calculatedInterval.end
        )
      ) {
        // A real file from the correct library contains the calculated interval, success
        return;
      }
      return { ...calculatedInterval, intersectingActualIntervals };
    })
    .filter(Boolean)
    .filter(({ libraryId }) => !releasesBundledInOtherLibraryByLibraryId[libraryId]);

  // const incorrectFileIntervalsByLibraryId = groupBy(incorrectFileCalculatedIntervals, "libraryId");
  // const incorrectLibraryIntervalsByLibraryId = groupBy(incorrectLibraryCalculatedIntervals, "libraryId");
  const falsePositiveIntervalsByLibraryId = groupBy(falsePositiveIntervals, "libraryId");

  let weightedFalsePositivesSum = 0;

  const falsePositiveStats = orderBy(
    Object.entries(falsePositiveIntervalsByLibraryId).map(
      ([libraryId, incorrectlyCalculatedIntervals]) => {
        const isTruePositive = actualLibraryIdSet.has(libraryId);
        const incorrectFileCalculatedIntervals = [];
        const incorrectLibraryCalculatedIntervals = [];

        for (const calculatedInterval of incorrectlyCalculatedIntervals) {
          const { intersectingActualIntervals } = calculatedInterval;
          const numIntersectingFiles = intersectingActualIntervals.length;
          const matchedLibraries = new Set(
            intersectingActualIntervals.map(({ libraryId }) => libraryId).filter(Boolean)
          );
          if (numIntersectingFiles < 1) {
            // Calculated interval corresponds to nomansland
            logger.warn(
              `${calculatedInterval.libraryId} ${calculatedInterval.fileName} is a false positive in no mans land, ${calculatedInterval.start}-${calculatedInterval.end}`
            );
          }
          if (matchedLibraries.size === 1 && matchedLibraries.has(calculatedInterval.libraryId)) {
            incorrectFileCalculatedIntervals.push({
              ...calculatedInterval,
              intersectingActualIntervals,
            });
            continue;
          }

          incorrectLibraryCalculatedIntervals.push({
            ...calculatedInterval,
            intersectingActualIntervals,
          });
        }

        const incorrectLibs = incorrectLibraryCalculatedIntervals
          .flatMap(({ intersectingActualIntervals }) =>
            intersectingActualIntervals.map(({ libraryId }) => libraryId)
          )
          .filter((id) => id !== libraryId);

        const incorrectLibraryActualIntervals = incorrectLibraryCalculatedIntervals
          .flatMap(({ intersectingActualIntervals }) => intersectingActualIntervals)
          .filter((interval) => libraryId !== interval.libraryId);

        const mainIncorrectInterval = orderBy(
          incorrectLibraryCalculatedIntervals,
          ({ start, end }) => end - start,
          "desc"
        )[0];

        const sumIncorrectLibraryActualIntervals = sumBy(
          incorrectLibraryActualIntervals,
          ({ intersection }) => intersection.end - intersection.start
        );

        weightedFalsePositivesSum += !isTruePositive
          ? Math.sqrt(sumIncorrectLibraryActualIntervals)
          : sumIncorrectLibraryActualIntervals ** (1 / 3);

        const { sourceMapFrequency, version, score, idfScoreRatio, maxIdfScore } =
          calculatedReleasesByLibraryId[libraryId];

        // TODO: We should remove the part of the interval that is from the correct library
        // if (libraryId === "prebid.js") {
        for (const incorrectInterval of incorrectLibraryCalculatedIntervals) {
          if (
            !incorrectInterval.intersectingActualIntervals.some(
              ({ libraryId }) => libraryId === incorrectInterval.libraryId
            )
          ) {
            logger.info(
              `Interval(s) from ${incorrectInterval.intersectingActualIntervals
                .map(({ libraryId, fileName }) => (libraryId ?? "") + fileName)
                .join(", ")} incorrectly ID'd as '${libraryId}${
                incorrectInterval.fileName
              }':\n${chalk.white(
                bundleJs.slice(incorrectInterval.start - 300, incorrectInterval.start)
              )}${chalk.green(
                bundleJs.slice(incorrectInterval.start, incorrectInterval.end)
              )}${chalk.white(
                bundleJs.slice(incorrectInterval.end, incorrectInterval.end + 300)
              )}\n`
            );
          }
        }
        // }
        return {
          libraryId,
          Actual: isTruePositive,
          idfRatio: round(idfScoreRatio, 2),
          score: round(score, 1),
          "Sum interv": sumIncorrectLibraryActualIntervals,
          Files: incorrectlyCalculatedIntervals.length,
          "Wrong lib": incorrectLibraryCalculatedIntervals.length,
          // "Correct lib, wrong file": incorrectFileCalculatedIntervals.length,
          Libs: truncate([...new Set(incorrectLibs)].join(", ")),
          // "Main file": mainIncorrectInterval && truncate(mainIncorrectInterval.fileName),
          "Main interval":
            mainIncorrectInterval && `${mainIncorrectInterval.start}-${mainIncorrectInterval.end}`,
          "Main inter match %":
            mainIncorrectInterval && round(100 * mainIncorrectInterval.intervalMatchRatio),
          SMF: round(sourceMapFrequency, 1),
          maxIdf: round(maxIdfScore, 1),
          version,
        };
      }
    ),
    ["Actual", "Sum interv"],
    ["asc", "desc"]
  );

  console.table(falsePositiveStats);
  logger.info(`Weighted false positive interval sum: '${round(weightedFalsePositivesSum)}'`);

  const bundledDepsStats = releasesBundledInOtherLibrary.map((release) => {
    const {
      libraryId,
      version,
      idfScoreRatio,
      isBundled: isInSourceMap,
      bundledByLibraryId,
      maxIdfScore,
    } = release;

    return {
      libraryId,
      version,
      "idf %": round(100 * idfScoreRatio),
      maxIdfScore,
      "In Sourcemap?": isInSourceMap,
      "Bundled by": bundledByLibraryId,
    };
  });
  console.log(chalk.bold("Libraries bundled inside other dependencies (not in sourcemap)"));
  console.table(bundledDepsStats);

  // TODO: Visualize intervals where only a small subset was matched
  // TODO: Visualize partial false positives (transpositions)
  // console.dir(humanReadableCalculatedIntervals, { maxArrayLength: null });
  // console.dir(humanReadableActualIntervals, { maxArrayLength: null });
  // process.exit(0);
}

function calculateIntervalIntersection(a, b) {
  const firstInterval = a.start <= b.start ? a : b;
  const secondInterval = firstInterval === a ? b : a;

  if (firstInterval.end <= secondInterval.start) {
    return null;
  }
  const intersection = {
    start: secondInterval.start,
    end: Math.min(firstInterval.end, secondInterval.end),
  };

  return intersection;
}

getMatchingReleases();
