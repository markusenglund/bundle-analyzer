import { logger, mongo } from "@bundle-scanner/common";
const { db, connect } = mongo;

const addIndices = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info(`Connected to db, setting indices`);

  await db.urls.createIndexes([{ key: { redirectedUrls: 1 } }]);
  logger.info("Added url indexes");

  await db.urlScans.createIndexes([{ key: { url: 1 } }, { key: { source: 1 } }]);
  logger.info("Added bundle indexes");

  await db.bundleScans.createIndexes([
    { key: { bundleUrl: 1 } },
    { key: { date: 1 } },
    { key: { checksum: 1 } },
    { key: { "sourceMapData.libraryIds": 1 } },
  ]);

  await db.files.createIndexes([
    { key: { fileName: 1 } },
    { key: { fourGramHash: 1 } },
    { key: { libraryId: 1 } },
    { key: { releaseIds: 1 } },
    { key: { maxIdfScore: 1 } },
  ]);
  logger.info("Added file indexes");
  await db.fourGrams.createIndexes([{ key: { fileIds: 1 } }]);
  logger.info("Added fourgram indexes");

  await db.releases.createIndexes([
    { key: { bundledDependencies: 1 } },
    { key: { dependencies: 1 } },
    { key: { error: 1 } },
    { key: { libraryId: 1 } },
    { key: { sourceMapFrequency: 1 } },
  ]);
  logger.info("Added release indexes");
  await db.libraries.createIndexes([{ key: { downloads: 1 } }]);
  logger.info("Added library indexes");
  await db.sourceMappedWebsites.createIndexes([
    { key: { failedScrape: 1 } },
    { key: { rank: 1 } },
    { key: { url: 1 } },
    { key: { "scripts.libraryIds": 1 } },
  ]);
  logger.info("Added sourcemapped website indexes");
  await db.sourceMapFiles.createIndexes([
    { key: { libraryId: 1 } },
    { key: { sourceMapFrequency: 1 } },
    { key: { sourceMapRatio: 1 } },
  ]);
  logger.info("Added sourcemapfile indexes");

  logger.info("Finished setting indices");
  process.exit(0);
};

addIndices();
