import { db, connect } from "../packages/common/dist/mongo/index.js";
import logger from "../packages/common/dist/logger/index.js";

const addDependenciesToLibraries = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  const releases = await db.releases.find({ "bundledDependencies.0": { $exists: true } }).toArray();
  const selfBundlingReleases = releases.filter(({ libraryId, bundledDependencies }) =>
    bundledDependencies.some((dep) => dep.libraryId === libraryId)
  );
  for (const { _id, bundledDependencies, libraryId } of selfBundlingReleases) {
    const newBundledDependencies = bundledDependencies.filter((dep) => dep.libraryId !== libraryId);
    console.log("Updating ", _id, newBundledDependencies);
    await db.releases.updateOne({ _id }, { $set: { bundledDependencies: newBundledDependencies } });
  }
  process.exit(0);
};

addDependenciesToLibraries();
