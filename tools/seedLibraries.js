import querystring from "querystring";
import got from "got";
import { partition, round } from "lodash-es";
import pMap from "p-map";
import { logger, mongo } from "@bundle-scanner/common";
const { connect, db } = mongo;

// PAGE_SIZE can not be higher than 128
const PAGE_SIZE = 128;
const CONCURRENCY = 20;

async function seedLibraries() {
  logger.info("Starting package seed");
  await connect();
  logger.info("Connected to db");
  const baseUrl = "https://replicate.npmjs.com/_all_docs";

  let done = false;
  let startKey = process.env.START_KEY || "@alifd/theme-9894";
  let i = 0;
  while (!done) {
    const query = {
      include_docs: false,
      limit: PAGE_SIZE + 1,
      startkey_docid: startKey,
    };

    const queryString = querystring.stringify(query);

    const url = `${baseUrl}?${queryString}`;

    logger.info(`Fetching library batch no ${i}`);
    const {
      body: { rows, total_rows: numTotalLibraries, offset },
    } = await got(url, { responseType: "json" });

    const [scopedLibraries, unscopedLibraries] = partition(
      rows.slice(0, rows.length - 1),
      ({ key }) => key.startsWith("@")
    );

    const unscopedDownloadsMap = await bulkFetchDownloadsMap(unscopedLibraries);
    // For scoped packages we can only fetch downloads one package at a time
    const scopedDownloadsMap = await parallelFetchDownloadsMap(scopedLibraries);

    const downloadsMap = { ...unscopedDownloadsMap, ...scopedDownloadsMap };

    const libraries = rows.slice(0, rows.length - 1).map((row) => {
      const { key: _id } = row;
      const data = downloadsMap[_id];
      if (!data) {
        return { _id };
      }

      const { downloads } = data;
      return { _id, downloads };
    });

    // Bulk insert into mongodb
    const { result } = await db.libraries2.bulkWrite(
      libraries.map((library) => ({
        updateOne: {
          filter: { _id: library._id },
          update: { $set: library },
          upsert: true,
        },
      }))
    );

    if (!result.ok) {
      logger.error(`Writing to database received error`, result.writeErrors[0]);
    }

    if (rows.length < PAGE_SIZE + 1) {
      logger.info("Finished downloading all packages");
      done = true;
    }

    startKey = rows[rows.length - 1].key;

    logger.info(`Done with page ${i}, setting new startkey to ${startKey}`);
    if (i % 5 === 0) {
      const numIndexedLibraries = offset + 128;
      logger.info(
        `${numIndexedLibraries} out of ${numTotalLibraries} (${round(
          (100 * numIndexedLibraries) / numTotalLibraries,
          1
        )}%) have been indexed.`
      );
    }
    i += 1;
  }

  process.exit(0);
}

async function bulkFetchDownloadsMap(libraries) {
  const commaSeparatedLibraryNames = libraries.map(({ key }) => key).join(",");
  const npmDownloadsUrl = `https://api.npmjs.org/downloads/point/last-month/${commaSeparatedLibraryNames}`;
  const { body: downloadsMap } = await got(npmDownloadsUrl, { responseType: "json" });
  // await pRetry(() => fetch(npmDownloadsUrl).then((res) => res.json()), {
  //   retries: 3,
  //   onFailedAttempt: () => logger.warn(`Fetching ${npmDownloadsUrl} failed, trying again...`),
  // });
  return downloadsMap;
}

async function parallelFetchDownloadsMap(libraries) {
  const libraryDownloadsArray = await pMap(libraries, fetchDownloads, {
    concurrency: CONCURRENCY,
  });

  const downloadsMap = Object.fromEntries(
    libraryDownloadsArray.filter(Boolean).map((data) => [data.package, data])
  );

  return downloadsMap;
}

// Fetch downloads for a single library
async function fetchDownloads(library, i) {
  const { key } = library;
  const npmDownloadsUrl = `https://api.npmjs.org/downloads/point/last-month/${key}`;
  if (i % 10 === 0) {
    logger.info(`Fetching downloads for library no ${i} - ${key}`);
  }

  try {
    const { body: results } = await got(npmDownloadsUrl, { responseType: "json" });
    return results;
  } catch (err) {
    logger.error(`Could not get downloads for ${key}`);
  }
}

seedLibraries();
