import { logger, mongo, sourceMapUtils } from "@bundle-scanner/common";
import { scrapeUrl, getSourceMap } from "@bundle-scanner/scraper";
const { extractLibrariesFromSourceMap, extractIntervalsFromSourceMap } = sourceMapUtils;
import tldts from "tldts";

const { db, connect } = mongo;

const BENCHMARK_DOMAINS = [
  "reddit.com",
  "github.com",
  "roblox.com",
  "calendly.com",
  "theguardian.com",
  "behance.net",
  "dribbble.com",
  "weather.com",
  "fiverr.com",
  "sberbank.ru",
  "bukalapak.com",
  "xfiniti.com",
  "nba.com",
  "buzzfeed.com",
  "freecodecamp.org",
  "ahrefs.com",
  "codecademy.com",
  "kaggle.com",
  "codesandbox.io",
  "ecosia.org",
  // Angular
  "freelancer.com",
  "softwareadvice.com",
  "2ssa.ir",
  "rappi.com",
  "usana.com",
  "speedyshare.com",
  "hooksounds.com",
  // Preact
  "substack.com",
  "wsj.com",
];

const fetchBenchmarkFiles = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  for (const url of BENCHMARK_DOMAINS) {
    const { bundles, finalUrl, faviconUrl, host, domain, redirectedUrls } = await scrapeUrl(url);

    for (const bundle of bundles) {
      const data = await getSourceMap(bundle);
      if (!data) continue;
      const { sourceMap, sourceMapUrl } = data;
      if (Number(sourceMap.version) !== 3) continue;

      const { fullUrl, sizeBytes, sizeBytesGzip, numChars, _id, code } = bundle;
      const { libraryIds, modulePaths } = extractLibrariesFromSourceMap(sourceMap);
      const intervals = await extractIntervalsFromSourceMap(sourceMap, code);

      const { domain: bundleDomain } = tldts.parse(_id);

      logger.info(`Inserting bundle data for ${_id}`);
      await db.benchmarkBundles.insertOne({
        _id,
        fullUrl,
        bundleDomain,
        domain,
        sizeBytes,
        sizeBytesGzip,
        createdDate: new Date(),
        code,
        sourceMapData: {
          url: sourceMapUrl,
          libraryIds,
          modulePaths,
          intervals,
        },
      });
    }
  }
  logger.info("Done!");
  process.exit(0);
};

fetchBenchmarkFiles();
