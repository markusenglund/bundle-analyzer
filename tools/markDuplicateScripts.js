import { logger, mongo } from "@bundle-scanner/common";
const { db, connect } = mongo;

const checkForDuplicateSourceMaps = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  const commonestLibraryIdArrays = await db.sourceMappedWebsites
    .aggregate([
      { $match: { scripts: { $ne: [] }, failedScrape: { $ne: true } } },
      { $unwind: { path: "$scripts" } },
      { $group: { _id: "$scripts.libraryIds", count: { $sum: 1 } } },
      { $match: { count: { $gte: 2 } } },
      { $sort: { count: -1 } },
    ])
    .toArray();

  const libraryIdsMap = new Map();
  for (const { _id: libraryIds } of commonestLibraryIdArrays) {
    const libraryIdsString = JSON.stringify(libraryIds);
    libraryIdsMap.set(libraryIdsString, "available");
  }

  const websitesCursor = db.sourceMappedWebsites.find();
  const numWebsites = await websitesCursor.count();
  let i = -1;
  for await (const website of websitesCursor) {
    i++;
    const { scripts, _id, url } = website;
    let websiteHasDuplicates = false;
    if (!scripts) continue;
    for (const script of scripts) {
      const libraryIdsString = JSON.stringify(script.libraryIds);
      const duplicateStatus = libraryIdsMap.get(libraryIdsString);
      if (!duplicateStatus) continue;
      if (duplicateStatus === "available") {
        libraryIdsMap.set(libraryIdsString, "taken");
      } else if (duplicateStatus === "taken") {
        websiteHasDuplicates = true;
        script.isDuplicate = true;
      }
    }
    if (websiteHasDuplicates) {
      if (i % 20 === 0) {
        logger.info(`${url} has duplicate script(s), replacing... (${i} of ${numWebsites})`);
      }
      await db.sourceMappedWebsites.replaceOne({ _id }, website);
    }
  }
  process.exit(0);
};

checkForDuplicateSourceMaps();
