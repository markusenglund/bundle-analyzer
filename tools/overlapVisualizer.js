import { readFile } from "fs/promises";
import { logger, mongo, tokenUtils } from "@bundle-scanner/common";
import { orderBy } from "lodash-es";

const { db, connect } = mongo;
const { extractTokens } = tokenUtils;
const RELEASE = "@ng-bootstrap/ng-bootstrap@6.0.0";

const k = 4;

const visualizeOverlap = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  // const bundleJs = await readFile(
  //   "/home/markus/Downloads/byzantine-module-0.1.1/package/dist/index.es.js",
  // path.resolve(
  //   __dirname,
  // "../server/__data__/bundles/speedtyper.dev/vendors~main.51d75e74.js"
  // "../server/__data__/bundles/speedtyper.dev/play.tsx.afade24e.js"
  // "../server/__data__/bundles/speedtyper.dev/main.36476f66.js"
  // "../server/__data__/bundles/speedtyping.dev/bundle.js"
  //   "../server/__data__/bundles/spectrum.chat/main.3002bcb6.js"
  // ),
  //   "utf8"
  // );

  const BUNDLE_URL = "usana.com/ux/dotcom/vendor-es2015.e21de39dfb77633d6c79.js";
  const benchmarkBundle = await db.benchmarkBundles.findOne({
    _id: BUNDLE_URL,
  });

  const bundleJs = benchmarkBundle.code;

  const { tokens: haystack } = extractTokens(bundleJs, 150000);

  const kGramSet = new Set();
  for (let i = 0; i + k - 1 < haystack.length; i += 1) {
    let kGram = haystack[i];
    for (let j = i + 1; j < i + k; j++) {
      kGram = `${kGram}·${haystack[j]}`;
    }
    kGramSet.add(kGram);
  }

  const files = await db.files.find({ releaseIds: RELEASE }).toArray();
  logger.info(`${files.length} files matched`);
  await calculateOverlap({ files, haystackSet: kGramSet });
  process.exit(0);
};

const dfMap = new Map();
const calculateOverlap = async ({ files, haystackSet }) => {
  let numOverlap = 0;
  let cumulativeIdfScore = 0;
  let totalMaxIdfScore = 0;

  for (const file of files) {
    const { fileName, maxIdfScore } = file;

    const fourGramsData = await db.fourGrams
      .find({ fileIds: file._id })
      .project({ _id: 1, df: 1 })
      .toArray();

    totalMaxIdfScore += maxIdfScore;
    console.log(`\n\n\n${fileName}\n`);
    for (const { _id: fourGram, df = 1 } of fourGramsData) {
      if (haystackSet.has(fourGram)) {
        logger.info(`${fourGram}: ${df}`);
      } else {
        logger.error(`${fourGram}: ${df}`);
      }
      dfMap.set(fourGram, df);
    }
  }

  for (const [fourGram, df] of dfMap) {
    if (haystackSet.has(fourGram)) {
      const idf = (1 / df) ** (1 / 2.5);
      cumulativeIdfScore += idf;
      numOverlap += 1;
    }
  }

  logger.info(
    `${numOverlap} out of ${dfMap.size} overlapped (${Math.round(
      (numOverlap * 100) / dfMap.size
    )}%)`
  );
  logger.info(
    `${cumulativeIdfScore} idf score out max ${totalMaxIdfScore} (${
      Math.round((cumulativeIdfScore * 1000) / totalMaxIdfScore) / 10
    }%)`
  );
};

visualizeOverlap();
