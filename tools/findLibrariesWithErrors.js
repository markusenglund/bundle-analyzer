require("dotenv").config();
const { connect, db } = require("../db");
const logger = require("../logger");

const tokenizeErroredReleases = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info(`Connected to db, fetching errored releases`);

  const erroredLibraries = await db.releases7
    .aggregate([
      {
        $match: { error: { $exists: true } }
      },
      {
        $group: {
          _id: "$libraryId",
          errors: { $addToSet: "$error" }
        }
      }
    ])
    .toArray();

  console.log(erroredLibraries);
  process.exit(0);
};

tokenizeErroredReleases();
