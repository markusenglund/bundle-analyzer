import { db, connect } from "../packages/common/dist/mongo/index.js";
import logger from "../packages/common/dist/logger/index.js";

const addDependenciesToLibraries = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  // Put sourceMapData.matchedReleases in matchedReleases
  const faultyOldBundles = await db.bundles
    .find({
      "sourceMapData.matchedReleases": { $exists: true },
    })
    .toArray();

  for (const bundle of faultyOldBundles) {
    await db.bundles.updateOne(
      { _id: bundle._id },
      {
        $unset: { "sourceMapData.matchedReleases": 1 },
        $set: { matchedReleases: bundle.sourceMapData.matchedReleases },
      }
    );
    console.log("Updated", bundle._id);
  }

  // Delete newly created bundles and urls with incorrect data
  const faultyNewBundles = await db.bundles.find({ meta: { $exists: true } }).toArray();
  const faultyNewUrls = await db.urls.find({ bundlesById: { $exists: true } }).toArray();

  for (const bundle of faultyNewBundles) {
    await db.bundles.deleteOne({ _id: bundle._id });
    console.log("Delete bundle", bundle._id);
  }
  for (const url of faultyNewUrls) {
    await db.urls.deleteOne({ _id: url._id });
    console.log("Deleted url", url._id);
  }
  console.log("Faulty new", faultyNewBundles.length, faultyNewUrls.length);
  process.exit(0);
};

addDependenciesToLibraries();
