import { logger, mongo } from "@bundle-scanner/common";
import addMaxIdfScore from "../packages/tokenizer/dist/addMaxIdfScore.js";

const { connect } = mongo;

(async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  await addMaxIdfScore();
  process.exit(0);
})();
