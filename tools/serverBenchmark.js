import { logger } from "@bundle-scanner/common";
import pMap from "p-map";
import { sumBy } from "lodash-es";
import got from "got";

const websites = [
  "reddit.com",
  "foxnews.com",
  "geeksforgeeks.org",
  "scribd.com",
  "wikihow.com",
  "jawapos.com",
  "mailchimp.com",
  "wildberries.ru",
  "google.com.ar",
  "merdeka.com",
  "gismeteo.ru",
  "rokna.net",
  "moneycontrol.com",
  "squarespace.com",
  "basecamp.com",
  "hotmart.com",
  "zol.com.cn",
  "secureserver.net",
  "worldometers.info",
  "donya-e-eqtesad.com",
  "blogger.com",
  "hotstar.com",
  "marca.com",
  "hbomax.com",
  "padlet.com",
  "zendesk.com",
  "tripadvisor.com",
  "setn.com",
  "in.gr",
  "discord.com",

  // "chinaz.com",
  // "namnak.com",
  // "indiamart.com",
  // "japanpost.jp",
  // "hootsuite.com",
  // "ndtv.com",
  // "binance.com",
  // "spankbang.com",
  // "allegro.pl",
  // "docusign.net",
  // "tudou.com",
  // "shaparak.ir",
  // "hp.com",
  // "line.biz",
  // "shopee.tw",
  // "wayfair.com",
  // "fedex.com",
  // "sindonews.com",
  // "coupang.com",
  // "onlyfans.com",
  // "sberbank.ru",
  // "reuters.com",
];

const fetchWebsiteAnalysis = async (websiteUrl) => {
  logger.info(`Request for ${websiteUrl}`);
  const {
    body: { data: website },
  } = await got(`http://localhost:1337/api/website/${encodeURIComponent(websiteUrl)}`, {
    responseType: "json",
    timeout: 20_000,
    retry: 0,
  });
  const totalSize = sumBy(Object.values(website.bundlesById), "sizeBytes");
  const numBundles = website.firstPartyBundleIds.length + website.thirdPartyBundleIds.length;

  const { timeSpent } = website.meta;
  return { timeSpent, numBundles, totalSize };
};

const benchmarkServer = async () => {
  // Start by warming up the threads by fetching some website
  logger.info(`Making a single request to warm up threads`);
  const result = await fetchWebsiteAnalysis("uber.com");
  logger.info(`Finished warm-up request in ${result.timeSpent / 1000} seconds`);

  const timeStart = Date.now();
  const results = await pMap(
    websites,
    async (websiteUrl) => {
      try {
        const result = await fetchWebsiteAnalysis(websiteUrl);
        logger.info(`[${websiteUrl}]: Finished`);
        return { ...result, websiteUrl };
      } catch (err) {
        logger.warn(`Reqest to '${websiteUrl}' failed with message: '${err.message}'`);
      }
    },
    { concurrency: 10 }
  );

  const benchmarkTime = Date.now() - timeStart;
  const numTotalBundles = sumBy(results, "numBundles");
  logger.info(`Bundles scanned: ${numTotalBundles}`);
  logger.info(
    `${results.length} websites analyzed in ${benchmarkTime / 1000} seconds (${
      (results.length * 1000) / benchmarkTime
    } requests per second)`
  );

  process.exit(0);
};

benchmarkServer();
