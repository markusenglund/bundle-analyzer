require("dotenv").config();
const { db, connect } = require("../db");
const logger = require("../logger");

const createTokenIndex = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");
  console.time("yolo");
  const cursor = db.files7.find({}).project({ fourGrams: 1 });
  // const hej = await cursor.toArray();
  const fourGramFileIdMap = new Map();
  let i = 0;
  for await (const file of cursor) {
    const { _id, fourGrams } = file;
    for (const fourGram of fourGrams) {
      const fileIds = fourGramFileIdMap.get(fourGram) || [];
      fileIds.push(_id);
      fourGramFileIdMap.set(fourGram, fileIds);
    }
    if (fourGramFileIdMap.size > 100000) {
      logger.info(`Bulk updating... ${i}`);
      await bulkUpdate(fourGramFileIdMap);
      fourGramFileIdMap.clear();
    }
    i += 1;
  }
  await bulkUpdate(fourGramFileIdMap);
  console.timeEnd("yolo");

  console.log("Done");
  process.exit(0);
};

async function bulkUpdate(map) {
  const updates = [...map.entries()].map(([fourGram, fileIds]) => ({
    updateOne: {
      filter: { _id: fourGram },
      update: { $addToSet: { fileIds: { $each: fileIds } } },
      upsert: true
    }
  }));
  await db.fourGrams.bulkWrite(updates);
}

createTokenIndex();
