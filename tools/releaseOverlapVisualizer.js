import { logger, mongo, tokenUtils, sourceMapUtils } from "@bundle-scanner/common";
const { db, connect } = mongo;
// ! If step is large for either release, this can be very misleading
const RELEASE_A = "react-persist@1.0.2";
const RELEASE_B = "lodash.isequal@4.5.0";

const visualizeOverlap = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  // const dfs = await db.dfs7.find({}).toArray();
  // const dfMap = new Map(dfs.map(({ fourGram, count }) => [fourGram, count]));

  const filesA = await db.files.find({ releaseIds: RELEASE_A }).toArray();
  const filesB = await db.files.find({ releaseIds: RELEASE_B }).toArray();
  let maxIdfScoreA = 0;
  let maxIdfScoreB = 0;

  const fourGramsA = [];
  const fourGramsB = [];
  for (const file of filesA) {
    const { fileName, maxIdfScore } = file;
    maxIdfScoreA += maxIdfScore;

    const fourGrams = (
      await db.fourGrams.find({ fileIds: file._id }).project({ _id: 1 }).toArray()
    ).map(({ _id }) => _id);

    fourGramsA.push(...fourGrams);
  }

  for (const file of filesB) {
    const fourGrams = (
      await db.fourGrams.find({ fileIds: file._id }).project({ _id: 1 }).toArray()
    ).map(({ _id }) => _id);

    fourGramsB.push(...fourGrams);
  }

  const fourGramSetA = new Set(fourGramsA);
  const fourGramSetB = new Set(fourGramsB);

  let overlappingFourGrams = [];
  for (const fourGram of fourGramsA) {
    if (fourGramSetB.has(fourGram)) {
      overlappingFourGrams.push(fourGram);
      logger.info(fourGram);
    } else {
      logger.error(fourGram);
    }
  }

  logger.info(
    `[${RELEASE_A}]: ${RELEASE_B} matches ${overlappingFourGrams.length} out of ${fourGramsA.length} fourGrams`
  );
  // logger.info(overlappingFourGrams);
  process.exit(0);
};

visualizeOverlap();
