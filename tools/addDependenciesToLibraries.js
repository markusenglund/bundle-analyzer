import { db, connect } from "../packages/common/dist/mongo/index.js";
import logger from "../packages/common/dist/logger/index.js";

const addDependenciesToLibraries = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  const libraryDependencies = await db.releases
    .aggregate([
      {
        $group: {
          _id: "$libraryId",
          dependencies: { $first: "$dependencies" },
          version: { $first: "$version" },
        },
      },
    ])
    .toArray();

  let i = 0;
  for (const { _id: libraryId, dependencies } of libraryDependencies) {
    console.log(i);
    i += 1;
    await db.libraries.updateOne({ _id: libraryId }, { $set: { dependencies } });
  }
  process.exit(0);
};

addDependenciesToLibraries();
