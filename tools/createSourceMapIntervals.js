import { logger, mongo, sourceMapUtils } from "@bundle-scanner/common";
const { extractIntervalsFromSourceMap } = sourceMapUtils;

const { db, connect } = mongo;
(async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  const website = await db.benchmarkWebsites4.findOne({
    url: "allrecipes.com",
  });
  const { sourceMap, code } = website.scripts[0];

  const intervals = await extractIntervalsFromSourceMap(sourceMap, code);

  const formattedIntervals = Object.entries(intervals).map(
    ([fileName, { start, end }]) => `${start}-${end}: ${fileName}`
  );
  console.dir(formattedIntervals, { maxArrayLength: null });
  process.exit(0);
})();
