import { cpus } from "os";
import { differenceBy, countBy, sortBy, sumBy, groupBy, partition } from "lodash-es";
import { logger, mongo } from "@bundle-scanner/common";
import Piscina from "piscina";
import pMap from "p-map";

const { connect, db } = mongo;

const maxThreads = 2;
const workerPool = new Piscina({
  filename: new URL("../packages/scanner/dist/scanBundleWorker.js", import.meta.url).href,
  maxThreads,
  idleTimeout: 1000,
});

const benchmark = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  const benchmarkBundles = await db.benchmarkBundles
    .find({ sizeBytes: { $gt: 100000 } })
    .sort({ sizeBytes: -1 })
    .toArray();
  const groupedBundles = groupBy(benchmarkBundles, "domain");
  const snippedBenchmarkBundles = Object.values(groupedBundles).flatMap((bundles) =>
    bundles.slice(0, 3)
  );

  logger.info(`Analyzing ${snippedBenchmarkBundles.length} bundles`);
  console.log(countBy(snippedBenchmarkBundles, "domain"));

  const librariesCursor = db.libraries
    .find({})
    .project({ _id: 1, sourceMapFrequency: 1, numErrors: 1, numVersions: 1 });
  const allLibraryIds = new Set();
  const allLibraryIdsWithSourceMapFrequency = new Set();
  const allLibrariesThatFailedTokenization = new Set();

  for await (const {
    _id: libraryId,
    sourceMapFrequency,
    numErrors,
    numVersions,
  } of librariesCursor) {
    allLibraryIds.add(libraryId);
    if (sourceMapFrequency) {
      allLibraryIdsWithSourceMapFrequency.add(libraryId);
      if (numVersions === numErrors) {
        allLibrariesThatFailedTokenization.add(libraryId);
      }
    }
  }
  logger.info("Got all library IDs");

  console.time("Time spent: ");
  const results = [];

  await pMap(
    snippedBenchmarkBundles,
    async (bundle) => {
      logger.info(`Calculating benchmark for '${bundle._id}'`);

      const {
        _id: bundleUrl,
        sourceMapData: { libraryIds },
        domain,
        code,
      } = bundle;
      const [knownLibraryIds, unknownLibraryIds] = partition(libraryIds, (libraryId) =>
        allLibraryIds.has(libraryId)
      );
      const [sourceMappedLibraryIds, nonSourceMappedLibraryIds] = partition(
        knownLibraryIds,
        (libraryId) => allLibraryIdsWithSourceMapFrequency.has(libraryId)
      );

      const [tokenizedLibraryIds, nonTokenizedLibraryIds] = partition(
        sourceMappedLibraryIds,
        (libraryId) => !allLibrariesThatFailedTokenization.has(libraryId)
      );

      const bundledReleases = [
        ...new Set(
          knownLibraryIds
            .map((libraryId) => {
              if (libraryId.startsWith("lodash")) return "lodash";
              if (libraryId === "rxjs-es") return "rxjs";
              if (libraryId === "core-js-pure") return "core-js";
              if (libraryId === "apollo-client") return "@apollo/client";
              if (["jquery-min", "jquery-slim"].includes(libraryId)) return "jquery";
              return libraryId;
            })
            .map((libraryId) => ({ libraryId }))
        ),
      ];

      const { matchedReleases } = await workerPool.runTask({ code, bundleUrl, bundledReleases });

      const numActualBundledLibraries = knownLibraryIds.length;
      const matchedLibraryIds = matchedReleases.map(({ libraryId }) => libraryId);
      const libraryIdsBundledInOtherLibraries = matchedReleases
        .filter(({ isBundledDependency }) => isBundledDependency)
        .map(({ libraryId }) => libraryId);
      const truePositiveLibraryIds = matchedReleases.filter(({ isBundled }) => isBundled);
      logger.info(
        `Matched ${matchedLibraryIds.length} releases out of which ${truePositiveLibraryIds.length} are actually in the bundle and ${libraryIdsBundledInOtherLibraries.length} are bundled in other libs`
      );
      logger.info(
        `Matched ${truePositiveLibraryIds.length} out of ${numActualBundledLibraries} bundled libraries`
      );

      const falsePositives = matchedReleases
        .filter(({ isBundled }) => !isBundled)
        .filter(({ isBundledDependency }) => !isBundledDependency)
        .map(({ libraryId }) => libraryId);
      logger.info(`False positives: ${falsePositives.join(", ")}`);
      const falseNegatives = differenceBy(bundledReleases, matchedReleases, "libraryId").map(
        ({ libraryId }) => libraryId
      );

      logger.info(`False negatives: ${falseNegatives.join(", ")}`);
      logger.info(`Bundled in other libraries: ${libraryIdsBundledInOtherLibraries.join(", ")}`);
      const falseNegativityRate = falseNegatives.length / knownLibraryIds.length;
      const falsePositivityRate = falsePositives.length / matchedLibraryIds.length;

      console.log(
        `False negatives: ${falseNegatives.length} should be equal to ${
          knownLibraryIds.length -
          matchedReleases.length +
          falsePositives.length +
          libraryIdsBundledInOtherLibraries.length
        }`
      );

      const result = {
        domain,
        bundleUrl,
        // matchedLibraryIds,
        // truePositiveLibraryIds,
        numMatchedLibraryIds: matchedLibraryIds.length,
        falsePositiveLibraryIds: falsePositives,
        falseNegativeLibraryIds: falseNegatives,
        sourceMappedLibraryIds,
        tokenizedLibraryIds,
        nonSourceMappedLibraryIds,
        nonTokenizedLibraryIds,
        knownLibraryIds,
        unknownLibraryIds,
        libraryIdsBundledInOtherLibraries,
        falseNegativityRate,
        falsePositivityRate,
      };
      results.push(result);
    },
    { concurrency: maxThreads }
  );
  const statsByDomain = results.reduce((acc, result) => {
    return {
      ...acc,
      [result.domain]: { ...acc[result.domain], [result.bundleUrl]: result },
    };
  }, {});

  console.dir(statsByDomain, { depth: null });
  const totalFalseNegatives = results.flatMap(
    ({ falseNegativeLibraryIds }) => falseNegativeLibraryIds
  );
  const totalFalsePositives = results.flatMap(
    ({ falsePositiveLibraryIds }) => falsePositiveLibraryIds
  );

  const totalNonSourceMappedLibraryIds = results.flatMap(
    ({ nonSourceMappedLibraryIds }) => nonSourceMappedLibraryIds
  );

  const totalNonTokenizedLibraryIds = results.flatMap(
    ({ nonTokenizedLibraryIds }) => nonTokenizedLibraryIds
  );

  const totalTokenizedLibraryIds = results.flatMap(
    ({ tokenizedLibraryIds }) => tokenizedLibraryIds
  );

  const totalActualLibraryIds = results.flatMap(({ knownLibraryIds }) => knownLibraryIds);

  const totalBundledDependenciesInOtherLibs = results.flatMap(
    ({ libraryIdsBundledInOtherLibraries }) => libraryIdsBundledInOtherLibraries
  );
  const numBundledDependenciesInOtherLibs = totalBundledDependenciesInOtherLibs.length;

  const falseNegativityRate = totalFalseNegatives.length / totalActualLibraryIds.length;

  const numTotalMatchedLibraries = sumBy(results, "numMatchedLibraryIds");
  const falsePositivityRate = totalFalsePositives.length / numTotalMatchedLibraries;
  const nonSourceMappedRate = totalNonSourceMappedLibraryIds.length / totalActualLibraryIds.length;

  const nonTokenizedRate = totalNonTokenizedLibraryIds.length / totalActualLibraryIds.length;

  const falseNegativesByFrequency = countBy(totalFalseNegatives);
  const falsePositivesByFrequency = countBy(totalFalsePositives);
  const nonSourceMappedByFrequency = countBy(totalNonSourceMappedLibraryIds);
  const nonTokenizedByFrequency = countBy(totalNonTokenizedLibraryIds);

  const sortedFalseNegatives = Object.fromEntries(
    sortBy(Object.entries(falseNegativesByFrequency), "1")
  );

  const sortedFalsePositives = Object.fromEntries(
    sortBy(Object.entries(falsePositivesByFrequency), "1")
  );

  const sortedNonSourceMapped = Object.fromEntries(
    sortBy(Object.entries(nonSourceMappedByFrequency), "1")
  );

  const sortedNonTokenized = Object.fromEntries(
    sortBy(Object.entries(nonTokenizedByFrequency), "1")
  );

  console.log({
    totalActualLibraryIds: totalActualLibraryIds.length,
    numTotalMatchedLibraries,
    totalTokenizedLibraryIds: totalTokenizedLibraryIds.length,
    totalFalseNegatives: totalFalseNegatives.length,
    totalFalsePositives: totalFalsePositives.length,
    totalNonTokenizedLibraries: totalNonTokenizedLibraryIds.length,
    totalNonSourceMappedLibraryIds: totalNonSourceMappedLibraryIds.length,
  });

  // console.dir(
  //   { libs: [...new Set(totalActualLibraryIds), ...new Set(totalFalsePositives)] },
  //   { maxArrayLength: null }
  // );
  console.log({
    sortedNonSourceMapped,
    sortedNonTokenized,
    sortedFalseNegatives,
    sortedFalsePositives,
  });
  console.log({ falseNegativityRate, falsePositivityRate, nonSourceMappedRate, nonTokenizedRate });
  console.log({ numBundledDependenciesInOtherLibs });
  console.log("Done");
  console.timeEnd("Time spent: ");

  process.exit(0);
};

benchmark();
