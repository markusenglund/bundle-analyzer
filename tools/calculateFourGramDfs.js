import { round } from "lodash-es";
import { logger, mongo } from "@bundle-scanner/common";
const { db, connect } = mongo;

const calculateFourGramDfs = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  // Fetch all files to gather a map with this structure : Map<fileId, libraryId>
  const fileIdLibraryIdMap = new Map();
  const filesCursor = db.files.find().project({ _id: 1, libraryId: 1 });

  for await (const { _id, libraryId } of filesCursor) {
    fileIdLibraryIdMap.set(_id, libraryId);
  }

  logger.info(`Created fileIdLibraryIdMap`);

  // Loop through every fourGram; for each fourgram, get the number of libraries that contain it
  const fourGramsCursor = db.fourGrams.find();
  const numFourGrams = await fourGramsCursor.count();
  logger.info(`Calculating df for ${numFourGrams} fourGrams`);
  let updates = [];
  let numUpdatedFourGrams = 0;
  for await (const { _id: fourGram, fileIds } of fourGramsCursor) {
    const numLibraries = new Set(fileIds.map((fileId) => fileIdLibraryIdMap.get(fileId))).size;
    const df = numLibraries;
    updates.push({
      updateOne: {
        filter: { _id: fourGram },
        update: { $set: { df } },
      },
    });

    if (updates.length >= 1000) {
      await bulkUpdate(updates);
      numUpdatedFourGrams += updates.length;
      updates = [];
      logger.info(
        `Number of fourGrams completed: ${numUpdatedFourGrams} (${round(
          (100 * numUpdatedFourGrams) / numFourGrams,
          1
        )}%)`
      );
    }
  }
  await bulkUpdate(updates);

  process.exit(0);
};

function bulkUpdate(updates) {
  return db.fourGrams.bulkWrite(updates);
}

calculateFourGramDfs();
