import { logger, mongo } from "@bundle-scanner/common";
import { round } from "lodash-es";

const { db, connect } = mongo;

const createFileFrequencies = async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db");

  const filePathsWithSourceMapFrequencies = await db.sourceMappedBundles
    .aggregate(
      [
        {
          $match: {
            "sourceMapData.libraryIds.0": { $exists: true },
          },
        },
        {
          $project: {
            "sourceMapData.modulePaths": 1,
            "sourceMapData.libraryIds": 1,
            domain: { $arrayElemAt: ["$domains", 0] },
          },
        },
        {
          $group: {
            _id: "$sourceMapData.libraryIds",
            modulePaths: { $first: "$sourceMapData.modulePaths" },
            domain: { $first: "$domain" },
          },
        },
        { $unwind: { path: "$modulePaths" } },
        {
          $group: {
            _id: "$domain",
            modulePaths: { $addToSet: "$modulePaths" },
          },
        },
        { $unwind: { path: "$modulePaths" } },
        { $group: { _id: "$modulePaths", sourceMapFrequency: { $sum: 1 } } },
        {
          $sort: {
            sourceMapFrequency: -1,
          },
        },
      ],
      { allowDiskUse: true }
    )
    .toArray();

  logger.info("Fetched module paths frequencies");

  const librariesWithSourceMapFrequencies = await db.libraries
    .find({
      sourceMapFrequency: { $exists: true },
    })
    .project({ _id: 1, sourceMapFrequency: 1 })
    .toArray();

  logger.info("Fetched library frequencies");

  const librarySourceMapFrequencyMap = new Map();
  for (const library of librariesWithSourceMapFrequencies) {
    librarySourceMapFrequencyMap.set(library._id, library.sourceMapFrequency);
  }

  const filePaths = filePathsWithSourceMapFrequencies.map(({ _id, sourceMapFrequency }) => {
    let libraryId;
    const parts = _id.split("/");
    if (_id.startsWith("@")) {
      libraryId = `${parts[0]}/${parts[1]}`;
    } else {
      libraryId = parts[0];
    }
    const sourceMapRatio = round(
      sourceMapFrequency / librarySourceMapFrequencyMap.get(libraryId),
      3
    );

    return { _id, sourceMapFrequency, libraryId, sourceMapRatio };
  });

  await db.sourceMapFiles.insertMany(filePaths);
  logger.info("Done!");
  process.exit(0);
};

createFileFrequencies();
