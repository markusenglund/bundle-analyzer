FROM node:16

WORKDIR /usr/app

COPY package.json ./
COPY yarn.lock ./
COPY packages/common ./packages/common
COPY packages/scanner ./packages/scanner
COPY packages/scraper ./packages/scraper
COPY packages/tokenizer ./packages/tokenizer
COPY packages/client ./packages/client

RUN yarn install --frozen-lockfile

COPY . .

RUN yarn build

ENV NODE_ENV=production

CMD [ "yarn", "tokenize-libraries" ]