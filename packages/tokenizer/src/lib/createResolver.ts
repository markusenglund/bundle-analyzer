import enhancedResolve, { FileSystem } from "enhanced-resolve";
import { utils } from "@bundle-scanner/common";

export type Resolver = (arg: {
  dir: string;
  filePath: string;
}) => Promise<string>;

const createResolver = (fs: FileSystem): Resolver => {
  const resolver = enhancedResolve.ResolverFactory.createResolver({
    fileSystem: fs,
    mainFields: ["browser", "module", "main"],
    aliasFields: ["browser"]
  });
  return ({ dir, filePath }) =>
    new Promise((resolve, reject) => {
      resolver.resolve({}, dir, filePath, {}, (err, absolutePath) => {
        if (err) {
          return reject(
            new utils.CustomError("EnhancedResolveError", err.message)
          );
        }
        if (!absolutePath) {
          return reject(
            new utils.CustomError(
              "EnhancedResolveError",
              "Resolver failed to produce path"
            )
          );
        }
        resolve(absolutePath);
      });
    });
};

export default createResolver;
