import { createFsFromVolume, vol } from "memfs";
import createResolver from "./createResolver.js";

const getEntryFileFromVolume = async (volume: typeof vol): Promise<string> => {
  const memoryFileSystem = createFsFromVolume(volume);
  const resolve = createResolver(memoryFileSystem);
  // All npm tarballs have a folder in root which includes the actual package, usually called 'package' but sometimes a random hash
  const packageDir = memoryFileSystem.readdirSync("/")[0];
  const absolutePath = await resolve({ dir: "/", filePath: `./${packageDir}` });

  return absolutePath;
};

export default getEntryFileFromVolume;
