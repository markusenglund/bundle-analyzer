import crypto from "crypto";
import { dataToEsm } from "@rollup/pluginutils";
import { logger, tokenUtils } from "@bundle-scanner/common";
import minifySnippet from "./minifySnippet.js";

const { extractTokens } = tokenUtils;
const MAX_TOKENS = 20000;
export type FileTokenMap = Map<string, string[]>;
type TokensCache = Map<string, string[]>;
let tokensCache: TokensCache = new Map();

const getFileTokenMap = async (fileContentMap: Map<string, string>): Promise<FileTokenMap> => {
  const nextTokensCache: TokensCache = new Map();
  const fileTokenMap: FileTokenMap = new Map();
  let numTokens = 0;
  for (const [filePath, content] of Array.from(fileContentMap)) {
    // console.time("minifySnippet");
    const contentHash = crypto.createHash("md5").update(content).digest("base64");

    let tokens = tokensCache.get(contentHash);
    if (!tokens) {
      const parsedContent = await parseContent({ filePath, content });

      // console.timeEnd("minifySnippet");
      // console.time("extract tokens");
      ({ tokens } = extractTokens(parsedContent));
      nextTokensCache.set(contentHash, tokens);
      // console.timeEnd("extract tokens");
    }

    if (tokens.length >= 4) {
      fileTokenMap.set(filePath, tokens);
      numTokens += tokens.length;
      // If the tally exceeds MAX_TOKENS - bail out and return the map
      if (numTokens >= MAX_TOKENS) {
        return fileTokenMap;
      }
    }
  }
  tokensCache = nextTokensCache;
  return fileTokenMap;
};

type ParseContent = (arg: { filePath: string; content: string }) => Promise<string>;

const parseContent: ParseContent = async ({ filePath, content }) => {
  if (filePath.endsWith(".json")) {
    // TODO: Consider not using rollup pluginutils for this
    return dataToEsm(JSON.parse(content));
  }

  try {
    const minifiedContent = await minifySnippet(content);
    return minifiedContent;
  } catch (err) {
    logger.warn(`Terser failed to minify '${filePath}', proceding with unminified content...`);
    return content;
  }
};

export default getFileTokenMap;
