// @ts-nocheck
import path from "path";
import * as acorn from "acorn";
import * as acornLoose from "acorn-loose";
import { simple } from "acorn-walk";
import { escapeRegExp } from "lodash-es";

type FindImportedModules = (
  content: string,
  parentDir: string
) => { importedModules: Set<string>; regExps: Set<RegExp> };

const findImportedModules: FindImportedModules = (content, parentDir) => {
  const importedModules: Set<string> = new Set();
  const regExps: Set<RegExp> = new Set();

  const ast = getAst(content);

  const requireAliases: Set<string> = new Set();

  simple(ast, {
    CallExpression(node) {
      // CommonJS
      if (
        node.callee?.name === "require" ||
        // @ts-ignore
        requireAliases.has(node.callee?.name)
      ) {
        const argNode = node.arguments?.[0];
        if (argNode?.type === "Literal") {
          importedModules.add(argNode.value);
        } else if (argNode?.type === "BinaryExpression") {
          // console.dir(node.arguments, { depth: null });
          const regExp = evaluateBinaryExpression(argNode, parentDir);
          regExps.add(regExp);
        } else if (argNode?.type === "TemplateLiteral") {
          if (argNode.expressions.length === 0) {
            importedModules.add(argNode.quasis[0].value.cooked);
          } else {
            const regExpPattern = argNode.quasis
              .map(({ value }, i) =>
                i === 0 && value.cooked.startsWith(".")
                  ? escapeRegExp(path.join(parentDir, value.cooked))
                  : escapeRegExp(value.cooked)
              )
              .join(".*");
            const regExp = new RegExp(regExpPattern);
            regExps.add(regExp);
          }
        }

        return;
      }
      // AMD
      if (node.callee?.name === "define") {
        const importsArg = (node.arguments ?? []).find(({ type }) => type === "ArrayExpression");
        for (const node of importsArg?.elements ?? []) {
          importedModules.add(node.value);
        }
        return;
      }
    },
    ImportDeclaration(node) {
      if (node.source) {
        importedModules.add(node.source.value);
      }
    },
    ExportNamedDeclaration(node) {
      if (node.source) {
        importedModules.add(node.source.value);
      }
    },
    AssignmentExpression(node) {
      if (node.right?.name === "require" && node.left) {
        requireAliases.add(node.left.name);
      }
    },
    VariableDeclarator(node) {
      if (node.init?.name === "require" && node.id?.name) {
        requireAliases.add(node.id.name);
      }
    },
  });
  return { importedModules, regExps };
};

// TODO: Dedupe this function
function getAst(snippet: string): acorn.Node {
  const options: acorn.Options = {
    sourceType: "module",
    ecmaVersion: 2021,
  };

  // If the acorn parser throws (usually due to invalid syntax), try with the error resistant loose version
  try {
    const ast = acorn.parse(snippet, options);
    return ast;
  } catch (err) {
    const ast: acorn.Node = acornLoose.parse(snippet, options);
    return ast;
  }
}

function evaluateBinaryExpression(node: Node, parentDir: string) {
  let regExpPattern = "";
  function recursivelyEvaluate({ type, left, right, operator, value }: Node) {
    if (type === "Literal") {
      if (regExpPattern === "" && value.startsWith(".")) {
        regExpPattern += escapeRegExp(path.join(parentDir, value));
      } else {
        regExpPattern += escapeRegExp(value);
      }
    } else if (type === "BinaryExpression") {
      if (operator !== "+") {
        throw new Error(`Unsupported operator: '${operator}'`);
      }
      if (left) {
        recursivelyEvaluate(left);
      }
      if (right) {
        recursivelyEvaluate(right);
      }
      return;
    } else {
      regExpPattern += ".*";
    }
  }

  recursivelyEvaluate(node);
  const regExp = new RegExp(regExpPattern);
  return regExp;
}

export default findImportedModules;
