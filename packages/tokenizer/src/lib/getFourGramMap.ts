import { PackageJson } from "type-fest";
import { uniqBy } from "lodash-es";
import { FileTokenMap } from "./getFileTokenMap.js";
import partialBundleScan from "../partialScanner/index.js";
import tokensToKGrams from "./tokensToKGrams.js";
import calculateStepSize from "./calculateStepSize.js";

type FileStepMap = Map<string, number>;
type BundledDependencies = { libraryId: string; version: string }[];

const getFourGramMap = async (
  fileTokenMap: FileTokenMap,
  packageJson: PackageJson
): Promise<{
  fileStepMap: FileStepMap;
  fileFourGramMap: FileTokenMap;
  bundledDependencies?: BundledDependencies;
}> => {
  const fileFourGramMap: FileTokenMap = new Map();
  const fileStepMap: FileStepMap = new Map();
  const bundledDependencies = [];

  const numReleaseTokens = [...fileTokenMap.values()].flat().length;

  // Skip bundle if fileTokenMap has a size > 1 since that means it's probably not been bundled
  for (const [filePath, tokens] of fileTokenMap.entries()) {
    if (
      fileTokenMap.size <= 2 ||
      filePath.endsWith(".min.js") ||
      filePath.endsWith(".esm.js") ||
      filePath.includes("vendor") ||
      filePath.includes(`${packageJson.name}.`)
    ) {
      const { bundledDependencies: fileBundledDependencies, remainingFourGrams } =
        await partialBundleScan(tokens, packageJson, filePath);
      // Remove (step-1)/step k-grams from the scanned releases
      const step = calculateStepSize(remainingFourGrams.length, remainingFourGrams.length);
      const winnowedFourGrams = remainingFourGrams.filter(
        (fourGram, i) => i % step === 0 || i === remainingFourGrams.length - 1
      );

      for (const dependency of fileBundledDependencies) {
        bundledDependencies.push(dependency);
      }
      fileFourGramMap.set(filePath, winnowedFourGrams);
      fileStepMap.set(filePath, step);
    } else {
      const step = calculateStepSize(tokens.length, numReleaseTokens);
      const fourGrams = tokensToKGrams(tokens, { k: 4, step });
      fileFourGramMap.set(filePath, fourGrams);
      fileStepMap.set(filePath, step);
    }
  }
  return {
    fileFourGramMap,
    fileStepMap,
    bundledDependencies: uniqBy(bundledDependencies, "libraryId"),
  };
};

export default getFourGramMap;
