import path from "path";
import crypto from "crypto";
import { createFsFromVolume } from "memfs";
import LruCache from "lru-cache";
import { partition } from "lodash-es";
import { PackageJson } from "type-fest";
import { logger, utils } from "@bundle-scanner/common";
import findImportedModules from "./findImportedModules.js";
import createResolver from "./createResolver.js";
import { VolumeType } from "../npm/streamTarballToVolume";

const { CustomError, isBuiltInModule } = utils;

interface ImportedModules {
  internal: Set<string>;
  external: Set<string>;
  builtIn: Set<string>;
}

const filePathsCache: LruCache<string, ImportedModules> = new LruCache({ max: 10 });

// Create a cache that stores only modules for the latest release (is reset at the end of the function)
type FileModulesCache = Map<string, { importedModules: Set<string>; regExps: Set<RegExp> }>;
let fileModulesCache: FileModulesCache = new Map();

type GetImportedModules = (arg: {
  volume: VolumeType;
  entryFilePaths: string[];
  packageJson: PackageJson;
}) => Promise<ImportedModules>;

const getImportedModules: GetImportedModules = async ({ volume, entryFilePaths, packageJson }) => {
  // Get an array of the filepaths of the vol
  const filePaths = recursivelyGetDirFiles(volume, "/");

  const filePathsKey = JSON.stringify(filePaths);

  // Check if cache contains result filePaths
  const cachedFilePaths = filePathsCache.get(filePathsKey);
  if (cachedFilePaths) return cachedFilePaths;

  const nextFileModulesCache: FileModulesCache = new Map();

  const memoryFileSystem = createFsFromVolume(volume);
  const resolve = createResolver(memoryFileSystem);

  const dependencies = Object.keys(packageJson.dependencies || {});

  const internalModuleSet: Set<string> = new Set(entryFilePaths);
  const externalModuleSet: Set<string> = new Set();
  const builtInModuleSet: Set<string> = new Set();

  const recursivelyAddModules = async (parentFilePath: string) => {
    const content: string = memoryFileSystem
      .readFileSync(parentFilePath, "utf8")
      // @ts-ignore
      .replace("delete define;", "");
    const contentHash = crypto.createHash("md5").update(content).digest("base64");

    const parentDirectory = path.dirname(parentFilePath);
    let moduleData = fileModulesCache.get(contentHash);
    if (!moduleData) {
      moduleData = findImportedModules(content, parentDirectory);
      nextFileModulesCache.set(contentHash, moduleData);
    }

    const { importedModules: moduleSet, regExps } = moduleData;
    const modules = [...moduleSet].filter(Boolean);

    const regExpModules = getModulesMatchingRegExps(regExps, filePaths);

    const [internalModules, nonInternalModules] = partition(modules, (module) =>
      module.startsWith(".")
    );

    const settledPromises = await Promise.allSettled([
      ...internalModules.map((module) => resolve({ dir: parentDirectory, filePath: module })),
      ...Array.from(regExpModules).map((module) => resolve({ dir: "", filePath: module })),
    ]);

    if (
      settledPromises.length &&
      settledPromises.filter(({ status }) => status === "rejected").length ===
        settledPromises.length
    ) {
      logger.warn(
        `All ${settledPromises.length} imports in ${parentFilePath} failed for ${packageJson.version}`
      );
    }

    // @ts-ignore
    const newInternalModules: string[] = settledPromises
      .map((result) => {
        if (result.status !== "fulfilled") {
          logger.debug(`File could not be resolved in ${parentFilePath}: ${result.reason.message}`);
          return null;
        }
        return result.value;
      })
      .filter((module) => {
        if (!module) return false;
        const isNovel = !internalModuleSet.has(module);
        const hasSupportedExtension = [".js", ".mjs", ".cjs", ".es", ".json"].some((extension) =>
          module.endsWith(extension)
        );
        const isNodeModule = module.includes("node_modules/");
        return isNovel && hasSupportedExtension && !isNodeModule;
      });

    const [builtInModules, externalModules] = partition(
      nonInternalModules,
      (module) => isBuiltInModule(module) && !dependencies.includes(module)
    );

    for (const module of newInternalModules) {
      internalModuleSet.add(module);
    }
    for (const module of builtInModules) {
      builtInModuleSet.add(module);
    }
    for (const module of externalModules) {
      externalModuleSet.add(module);
    }

    await Promise.all(newInternalModules.map(recursivelyAddModules));
  };

  try {
    for (const entryFilePath of entryFilePaths) {
      await recursivelyAddModules(entryFilePath);
    }
  } catch (err) {
    console.log(err);
    throw new CustomError("ModuleError", err);
  }

  const importedModules = {
    internal: internalModuleSet,
    external: externalModuleSet,
    builtIn: builtInModuleSet,
  };

  filePathsCache.set(filePathsKey, importedModules);
  fileModulesCache = nextFileModulesCache;

  return importedModules;
};

function getModulesMatchingRegExps(regExps: Set<RegExp>, filePaths: string[]) {
  const matchedFilePaths = new Set(
    Array.from(regExps).flatMap((regExp) => filePaths.filter((filePath) => regExp.test(filePath)))
  );

  return matchedFilePaths;
}

// TODO: This needs to be used for filepaths from sourceMapFiles collection
const recursivelyGetDirFiles = (volume: VolumeType, dir: string): string[] => {
  // @ts-ignore
  const filePaths = volume.readdirSync(dir).flatMap((dirEntry: string) => {
    const absolutePath = path.resolve(dir, dirEntry);
    if (volume.lstatSync(absolutePath).isDirectory()) {
      return recursivelyGetDirFiles(volume, absolutePath);
    }
    return absolutePath;
  });

  return filePaths;
};

export default getImportedModules;
