const calculateStepSize = (numFileTokens: number, numReleaseTokens: number): number => {
  const numRemainingReleaseTokens = numReleaseTokens - numFileTokens;
  const maxNumKGrams = (20 * numFileTokens ** (1 / 3)) / Math.log10(10 + numRemainingReleaseTokens);

  const step = Math.ceil(numFileTokens / maxNumKGrams);

  return step;
};

export default calculateStepSize;
