const tokensToKGrams = (
  tokens: string[],
  { k = 4, step = 1 }: { k?: number; step?: number }
): string[] => {
  const kGrams: string[] = [];
  for (let i = 0; i + k - 1 < tokens.length; i += step) {
    let kGram = tokens[i];
    for (let j = i + 1; j < i + k; j++) {
      kGram = `${kGram}·${tokens[j]}`;
    }
    kGrams.push(kGram);
  }

  let lastKGram = tokens[tokens.length - k];
  for (let j = tokens.length - k + 1; j < tokens.length; j++) {
    lastKGram = `${lastKGram}·${tokens[j]}`;
  }

  if (lastKGram !== kGrams[kGrams.length - 1]) {
    kGrams.push(lastKGram);
  }

  return kGrams;
};

export default tokensToKGrams;
