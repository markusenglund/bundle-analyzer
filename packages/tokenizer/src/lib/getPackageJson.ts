import path from "path";
import { utils } from "@bundle-scanner/common";
import { PackageJson } from "type-fest";
import { VolumeType } from "../npm/streamTarballToVolume";

const getPackageJson = (volume: VolumeType): PackageJson => {
  // TODO: Figure out how to fix types returned by memfs
  // @ts-ignore
  const packageDir: string = volume.readdirSync("/")[0];
  try {
    // @ts-ignore
    const packageJsonFile: string = volume.readFileSync(
      path.resolve("/", packageDir, "package.json"),
      "utf8"
    );

    const packageJson: PackageJson = JSON.parse(packageJsonFile);

    return packageJson;
  } catch (err) {
    throw new utils.CustomError("PackageJsonError", err.message);
  }
};

export default getPackageJson;
