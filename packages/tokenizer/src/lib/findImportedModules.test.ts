import findImportedModules from "./findImportedModules.js";

const testSnippet = `
const a = require("./test1")
require("./test2")
import b from "./test3"
import './test4';
import c as d from './test5';
import {
  LanguageDetector as i18nextLanguageDetector,
  handle as i18nextHandle
} from './test6';
export { default as function1 } from './test7';
define(["./test8", "./test9"])
define('myModule', ['./test10'])

// Aliased require
const aliasedRequire = require
aliasedRequire("./test11")

let aliasedRequire2
aliasedRequire2 = require
aliasedRequire2("./test12")

// Dynamic imports
const locales = ["sv", "en"];
for (const locale of locales) {
  require("./folder1/" + locale + ".js")
}
require(\`./folder2\${locale[0]}.js\`)
require(\`./test13\`)
const yoloDeclaration = "./yolo"
require(\`\${yoloDeclaration}/test14.js\`)
`;

describe("findImportedModules", () => {
  const { importedModules, regExps } = findImportedModules(testSnippet, "/parent-folder");
  it("Correctly identifies all ESM, CJS and AMD modules", () => {
    expect(importedModules).toEqual(
      new Set([
        "./test1",
        "./test2",
        "./test3",
        "./test4",
        "./test5",
        "./test6",
        "./test7",
        "./test8",
        "./test9",
        "./test10",
        "./test11",
        "./test12",
        "./test13",
      ])
    );
  });
  it("Correctly identifies dynamic wildcard regexes", () => {
    expect(regExps).toEqual(
      new Set([
        /\/parent-folder\/folder1\/.*\.js/,
        /\/parent-folder\/folder2.*\.js/,
        /.*\/test14\.js/,
      ])
    );
  });
});
