import { round } from "lodash-es";
import { DownloadsByVersion } from "../npm/getWeeklyDownloadsByVersion";

interface SourceMapFrequencyByVersion {
  [key: string]: number;
}

export default function calculateSourceMapFrequencyByVersion(
  weeklyDownloadsByVersion: DownloadsByVersion,
  librarySourceMapFrequency: number
): SourceMapFrequencyByVersion {
  const totalWeeklyDownloads = Object.values(weeklyDownloadsByVersion).reduce(
    (acc, val) => acc + val,
    0
  );

  const sourceMapFrequencyByVersion = Object.fromEntries(
    Object.entries(weeklyDownloadsByVersion).map(
      ([version, weeklyDownloads]) => {
        const shareOfTotalWeeklyDownloads =
          weeklyDownloads / totalWeeklyDownloads;
        const sourceMapFrequency =
          librarySourceMapFrequency * shareOfTotalWeeklyDownloads;
        const roundedSourceMapFrequency = round(sourceMapFrequency, 2);
        return [version, roundedSourceMapFrequency];
      }
    )
  );

  return sourceMapFrequencyByVersion;
}
