import { minify } from "terser";
import { utils } from "@bundle-scanner/common";

const minifySnippet = async (snippet: string): Promise<string> => {
  try {
    const { code } = await minify(snippet, {
      mangle: false,
      compress: {
        global_defs: {
          "process.env.NODE_ENV": "production",
          define: undefined,
        },
      },
    });

    if (!code) {
      throw new Error("Terser returned undefined");
    }

    return code;
  } catch (err) {
    throw new utils.CustomError("TerserError", err);
  }
};

export default minifySnippet;
