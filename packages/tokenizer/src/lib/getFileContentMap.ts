import { PackageJson } from "type-fest";
import { utils } from "@bundle-scanner/common";
import getImportedModules from "./getImportedModules.js";
import { VolumeType } from "../npm/streamTarballToVolume";

type FileContentMap = Map<string, string>;

async function getFileContentMap(
  volume: VolumeType,
  entryFilePaths: string[],
  packageJson: PackageJson
): Promise<FileContentMap> {
  const modules = await getImportedModules({
    volume,
    entryFilePaths,
    packageJson,
  });

  const fileContentMap: FileContentMap = new Map();
  for (const filePath of modules.internal) {
    const shortenedFilePath = filePath.replace(/^\/package/, "");
    // @ts-ignore
    const fileContent: string = volume.readFileSync(filePath, "utf8");
    fileContentMap.set(shortenedFilePath, fileContent);
  }
  return fileContentMap;
}

export default getFileContentMap;
