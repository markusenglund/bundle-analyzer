import { threadId } from "worker_threads";
import { logger, mongo } from "@bundle-scanner/common";
import tokenizeReleases from "./tokenizeReleases.js";
const { connect } = mongo;

// Initialize the db connection and return the function
// so we can pass it directly to piscina
const tokenizeReleasesWorker = async () => {
  await connect();
  logger.info(`Connected to db in thread ${threadId}`);

  return tokenizeReleases;
};

export default tokenizeReleasesWorker();
