import semver from "semver";
import { uniqBy } from "lodash-es";
import { logger, mongo, Release } from "@bundle-scanner/common";
const { db } = mongo;

type Dependencies = { libraryId: string; versionRange: string; isDevDependency?: boolean }[];

async function getDependencyReleases(
  topLevelDependencies: Dependencies,
  originalLibraryId: string | undefined
): Promise<Release[]> {
  const dependencyReleases: Release[] = [];
  const dependencySet: Set<string> = new Set();

  const recursivelyGetDependencyReleases = async (dependencies: Dependencies) => {
    for (const { libraryId, versionRange, isDevDependency } of dependencies) {
      // Prevent package from thinking it is bundled into itself
      if (libraryId === originalLibraryId) continue;

      dependencySet.add(`${libraryId}/${versionRange}`);
      const releases = await db.PREV_releases.find({
        libraryId,
        error: { $exists: false },
      }).toArray();

      const releasesInVersionRange = releases.filter(({ version }) =>
        semver.satisfies(version, versionRange)
      );
      dependencyReleases.push(...releasesInVersionRange);

      if (!isDevDependency && libraryId !== "webpack") {
        const secondOrderDependencies = releasesInVersionRange.flatMap(
          ({ dependencies }) => dependencies
        );

        const uniqueSecondOrderDependencies = uniqBy(
          secondOrderDependencies,
          ({ libraryId, versionRange }) => `${libraryId}/${versionRange}`
        );

        const newSecondOrderDependencies = uniqueSecondOrderDependencies.filter(
          ({ libraryId, versionRange }) => {
            return !dependencySet.has(`${libraryId}/${versionRange}`);
          }
        );

        logger.debug(
          `${libraryId} ${versionRange} has ${
            newSecondOrderDependencies.length
          } new dependencies: ${newSecondOrderDependencies
            .map(({ libraryId, versionRange }) => libraryId + " " + versionRange)
            .join(", ")}`
        );

        await recursivelyGetDependencyReleases(newSecondOrderDependencies);
      }
    }
  };

  await recursivelyGetDependencyReleases(topLevelDependencies);

  const uniqueDependencyReleases = uniqBy(dependencyReleases, "_id");
  return uniqueDependencyReleases;
}

export default getDependencyReleases;
