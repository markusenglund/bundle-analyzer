import { logger, mongo } from "@bundle-scanner/common";
import { memoize } from "lodash-es";
const { db } = mongo;

// Ignore all libraries that have a smf / downloads ratio that is too low
const getTopLibraryIds = memoize(async () => {
  logger.info("Fetching top libraries (you should only see this log once)");
  const topLibraries = await db.libraries
    .aggregate([
      { $match: { sourceMapFrequency: { $gte: 10 } } },
      { $addFields: { smfDownloadsRatio: { $divide: ["$sourceMapFrequency", "$downloads"] } } },
      { $match: { smfDownloadsRatio: { $gte: 2 / 100000 } } },
      { $sort: { smfDownloadsRatio: -1 } },
    ])
    .toArray();
  const topLibraryIds = new Set(topLibraries.map(({ _id }) => _id));

  return topLibraryIds;
});

export default getTopLibraryIds;
