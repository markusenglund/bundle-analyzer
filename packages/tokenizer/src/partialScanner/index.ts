import { logger, mongo } from "@bundle-scanner/common";
import { PackageJson } from "type-fest";
import { sumBy, uniqBy, orderBy, range, round, inRange, xor } from "lodash-es";
import LruCache from "lru-cache";
import { getBundlePositionInterval } from "@bundle-scanner/scanner";
import { tokensToKGrams } from "../lib/index.js";
import getDependencyReleases from "./getDependencyReleases.js";
import getTopLibraryIds from "./getTopLibraryIds.js";
// TODO: Manage to import the db release type
const { db } = mongo;

const cache = new LruCache({ max: 10 });

const partialBundleScan = async (tokens: string[], packageJson: PackageJson, filePath: string) => {
  logger.info(
    `[${packageJson.name}@${packageJson.version}]: Scanning '${filePath}' for bundled fourgrams`
  );

  const bundleFourGrams = tokensToKGrams(tokens, { step: 1 });

  const cacheKey = `${packageJson.name}${filePath}`;
  const cachedResult = cache.get(cacheKey);
  if (cachedResult && cachedResult.bundledDependencies.length === 0) {
    const xorSize = xor(tokens, cachedResult.tokens).length;
    if (xorSize < 10) {
      logger.info(
        `'${cacheKey}' is very similar to previously scanned file, returning same result...`
      );
      return { bundledDependencies: [], remainingFourGrams: bundleFourGrams };
    }
  }
  const bundleFourGramIndex: Map<string, number[]> = new Map();
  for (let i = 0; i < bundleFourGrams.length; i += 1) {
    const fourGram = bundleFourGrams[i];
    bundleFourGramIndex.set(
      fourGram,
      bundleFourGramIndex.has(fourGram) ? [...bundleFourGramIndex.get(fourGram), i] : [i]
    );
  }

  const topLibraryIds = await getTopLibraryIds();

  const devAndPeerDependencies = Object.entries({
    ...packageJson.devDependencies,
    ...packageJson.peerDependencies,
  })
    .filter(([libraryId]) => topLibraryIds.has(libraryId))
    .map(([libraryId, versionRange]) => ({ libraryId, versionRange, isDevDependency: true }));

  const dependencies = Object.entries({
    ...packageJson.dependencies,
  }).map(([libraryId, versionRange]) => ({ libraryId, versionRange, isDevDependency: false }));

  const found = filePath.match(/(?<libraryId>[^/]+)(\.min\.|.esm.)/);
  const fileNameLibraryId = found?.groups?.libraryId;
  const fileNameDependencies =
    fileNameLibraryId && fileNameLibraryId !== packageJson.name
      ? [{ libraryId: fileNameLibraryId, versionRange: "*", isDevDependency: false }]
      : [];

  const allDependencies = uniqBy(
    [...dependencies, ...devAndPeerDependencies, ...fileNameDependencies],
    "libraryId"
  );

  if (allDependencies.length < 1) {
    return { bundledDependencies: [], remainingFourGrams: bundleFourGrams };
  }
  // Recursively get dependencies of the package that fulfil the semver criteria
  const dependencyReleases = await getDependencyReleases(allDependencies, packageJson.name);

  const files = await db.PREV_files.find({
    releaseIds: { $in: dependencyReleases.map(({ _id }) => _id) },
  }).toArray();

  const fileIdsGroupedByRelease: { [key: string]: string[] } = {};
  const fileMap: { [key: string]: typeof files[0] } = {};
  for (const file of files) {
    fileMap[file._id] = file;
    for (const releaseId of file.releaseIds) {
      const fileIds = fileIdsGroupedByRelease[releaseId];
      if (fileIds) {
        fileIds.push(file._id);
      } else {
        fileIdsGroupedByRelease[releaseId] = [file._id];
      }
    }
  }

  const fileIds = files.map(({ _id }) => _id);

  // Fetch the fourGrams in the bundle that belong to one of the files connected to releases
  const fourGramsData = await db.PREV_fourGrams.find({
    fileIds: { $in: fileIds },
    _id: { $in: bundleFourGrams },
  }).toArray();

  const dfMap = new Map(fourGramsData.map(({ _id: fourGram, df = 1 }) => [fourGram, df]));
  // TODO: Get dfMap (won't be necessary later when dfs and fourGrams are combined)
  // const dfMap = await getDfMap();

  const { fileIdfScoreMap, fileMatchingFourGramsMap } = createFileIdfScoreMap({
    fourGramsData,
    dfMap,
  });

  const releasesWithScores = dependencyReleases.map((release) => {
    const { _id: releaseId, numFiles } = release;
    const fileIds = fileIdsGroupedByRelease[releaseId] ?? [];
    if (fileIds.length < 1) {
      logger.error(`${releaseId} unexpectedly did not have any files in DB`);
    }
    const filesWithScores = fileIds.map((fileId) => {
      const file = fileMap[fileId];
      const { maxIdfScore } = file;
      const fileIdfScore = fileIdfScoreMap[fileId] ?? 0;
      const fileIdfScoreRatio = fileIdfScore / maxIdfScore;
      return { ...file, idfScore: fileIdfScore, idfScoreRatio: fileIdfScoreRatio };
    });
    const releaseScore =
      filesWithScores.reduce((sum, { idfScoreRatio, maxIdfScore }) => {
        if (!maxIdfScore) return sum;
        return sum + idfScoreRatio ** 1.5 * maxIdfScore ** (1 / 2.5);
      }, 0) /
      (numFiles ** (1 / 2.5) + numFiles / 50);

    const releaseIdfScore = sumBy(filesWithScores, "idfScore");
    const releaseMaxIdfScore = sumBy(filesWithScores, "maxIdfScore");
    const releaseIdfScoreRatio = releaseIdfScore / releaseMaxIdfScore;
    return {
      ...release,
      score: releaseScore,
      idfScore: releaseIdfScore,
      idfScoreRatio: releaseIdfScoreRatio,
      fileIds,
    };
  });

  const topScoringReleases = releasesWithScores.filter(({ score, libraryId }) =>
    [...dependencies, ...fileNameDependencies].some((dep) => dep.libraryId === libraryId)
      ? score > 0.5
      : score > 1
  );

  const topScoringReleasesPerLibrary = uniqBy(
    orderBy(topScoringReleases, "idfScoreRatio", "desc"),
    "libraryId"
  );

  const excludeIntervals: { start: number; end: number }[] = [];
  for (const release of topScoringReleasesPerLibrary) {
    const { fileIds } = release;
    for (const fileId of fileIds) {
      const matchingFourGrams = fileMatchingFourGramsMap[fileId];
      if (!matchingFourGrams) continue;
      const sortedBundlePositions = matchingFourGrams
        .flatMap((fourGram) => {
          const bundlePositions = bundleFourGramIndex.get(fourGram) || [];
          const df = dfMap.get(fourGram) || 1;
          const tf = bundlePositions.length;
          const idfScore = (1 / df) ** (1 / 2.5);
          return bundlePositions.map((position) => ({
            position,
            // weight,
            idfScore,
            fourGram,
            df,
            tf,
          }));
        })
        .filter(
          ({ position }) =>
            !excludeIntervals.some((interval) => inRange(position, interval.start, interval.end))
        )
        .sort((a, b) => a.position - b.position);

      if (sortedBundlePositions.length < 1) continue;
      const file = fileMap[fileId];
      const { numFourGrams, step } = file;
      // TODO: Consider adding a claimedIntervals arg to this function
      // which prevents matching of already matched intervals
      const positionInterval = getBundlePositionInterval({
        sortedBundlePositions,
        numFourGrams,
        step,
      });

      excludeIntervals.push(positionInterval);
    }
  }

  const bundledDependencies = topScoringReleasesPerLibrary.map(({ libraryId, version }) => ({
    libraryId,
    version,
  }));

  // console.log(
  //   { intervals: excludeIntervals },
  //   excludeIntervals.reduce((sum, [a, b]) => sum + b - a, 0)
  // );
  const excludeIndices = new Set(
    excludeIntervals.flatMap(({ start, end }) => range(start, end + 1))
  );
  const remainingFourGrams = bundleFourGrams.filter((fourGram, i) => !excludeIndices.has(i));
  const filteredOutFourGrams = bundleFourGrams.filter((fourGram, i) => excludeIndices.has(i));

  // console.dir(
  //   { remainingFourGrams, filteredOutFourGrams, excludeIntervals },
  //   { depth: null, maxArrayLength: null }
  // );

  if (topScoringReleasesPerLibrary.length > 0) {
    const releaseIds = topScoringReleasesPerLibrary.map(({ _id }) => _id);
    const numRemovedFourGrams = bundleFourGrams.length - remainingFourGrams.length;
    const percentageRemovedFourGrams = numRemovedFourGrams / bundleFourGrams.length;
    logger.warn(
      `[${packageJson.name}@${packageJson.version}]: Found ${
        topScoringReleasesPerLibrary.length
      } bundled dependencies: ${releaseIds.join(
        ", "
      )} - filtered out ${numRemovedFourGrams} out of ${bundleFourGrams.length} (${round(
        percentageRemovedFourGrams * 100
      )}%) four-grams.`
    );
  }

  cache.set(cacheKey, { tokens, bundledDependencies });
  return { bundledDependencies, remainingFourGrams };
};

const createFileIdfScoreMap = ({ fourGramsData, dfMap }) => {
  const fileIdfScoreMap: { [key: string]: number } = {};
  const fileMatchingFourGramsMap: { [key: string]: string[] } = {};
  for (const { _id: fourGram, fileIds } of fourGramsData) {
    const df = dfMap.get(fourGram) ?? 1;
    const idf = (1 / df) ** (1 / 2.5);
    for (const fileId of fileIds) {
      fileIdfScoreMap[fileId] = (fileIdfScoreMap[fileId] ?? 0) + idf;
      fileMatchingFourGramsMap[fileId] = fileMatchingFourGramsMap[fileId] || [];
      fileMatchingFourGramsMap[fileId].push(fourGram);
    }
  }
  return { fileIdfScoreMap, fileMatchingFourGramsMap };
};

export default partialBundleScan;
