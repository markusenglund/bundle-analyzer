import tokenizeLibraries from "./tokenizeLibraries.js";
import { logger, mongo } from "@bundle-scanner/common";
import { db } from "../../common/dist/mongo/index.js";
import addMaxIdfScore from "./addMaxIdfScore.js";
const { connect } = mongo;

const LIBRARY_IDS = ["wouter"];
(async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db, fetching libraries sorted by downloads");

  // const alreadyTokenizedLibraries = await db.libraries.find({ hasIndexedTokens11: true }).toArray();
  // const alreadyTokenizedLibraryIds = new Set(alreadyTokenizedLibraries.map(({ _id }) => _id));

  // const libraryIdsToTokenize = LIBRARY_IDS.filter((_id) => !alreadyTokenizedLibraryIds.has(_id));

  const libraryIdsToTokenize = LIBRARY_IDS;

  const fileIds = (
    await db.files
      .find({ libraryId: { $in: libraryIdsToTokenize } })
      .project({ _id: 1 })
      .toArray()
  ).map(({ _id }) => _id);

  logger.info(`Removing fileIds from fourGrams for ${fileIds.length} files`);
  for (const fileId of fileIds) {
    await db.fourGrams.updateMany({ fileIds: fileId }, { $pull: { fileIds: fileId } });
  }

  logger.info(`Deleting files...`);
  await db.files.deleteMany({ libraryId: { $in: libraryIdsToTokenize } });

  logger.info(`Starting retokenization`);
  await tokenizeLibraries({
    // hasIndexedTokens11: { $ne: true },
    _id: {
      $in: libraryIdsToTokenize,
    },
    sourceMapFrequency: { $exists: true },
  });
  await addMaxIdfScore({ libraryId: { $in: libraryIdsToTokenize } });

  // TODO: Remove orphaned fourGrams (fourGrams with empty fileIds array)
  process.exit(0);
})();
