import { logger } from "@bundle-scanner/common";
import pRetry from "p-retry";
import {
  getEntryFileFromVolume,
  getFileContentMap,
  getFileTokenMap,
  getFourGramMap,
  getPackageJson,
} from "./lib/index.js";
import { streamTarballToVolume } from "./npm/index.js";
import { mongo, utils } from "@bundle-scanner/common";

const { db } = mongo;

global.fileMappingTime = 0;
global.tokenizationTime = 0;
global.partialScanTime = 0;
global.tarballTime = 0;

const getTokensFromTarballUrl = async (
  tarballUrl: string,
  libraryId: string
): ReturnType<typeof getFourGramMap> =>
  // Promise<Map<string, string[]>>
  {
    // console.time("streamTarballToVolume");
    const tarballStart = Date.now();

    const volume = await pRetry(() => streamTarballToVolume(tarballUrl), {
      retries: 3,
    });
    const tarballEnd = Date.now();
    global.tarballTime += tarballEnd - tarballStart;
    // console.timeEnd("streamTarballToVolume");

    const packageJson = getPackageJson(volume);
    // console.time("getEntryFileFromVolume");

    // Get 'entryFiles' by looking at the sourceMapFiles collection
    // TODO: Cache this db call
    const sourceMapFiles = await db.sourceMapFiles
      .find({ libraryId, sourceMapRatio: { $gte: 0.05 } })
      .sort({ sourceMapRatio: -1 })
      .toArray();

    const packageDir = volume.readdirSync("/")[0];
    const sourceMapFilePaths = sourceMapFiles
      .map(({ _id: filePath }) => {
        const absolutePath = filePath.replace(libraryId, `/${packageDir}`);
        return absolutePath;
      })
      .filter((absolutePath) => volume.existsSync(absolutePath))
      .filter((absolutePath) =>
        [".js", ".mjs", ".cjs", ".es", ".json"].some((extension) =>
          absolutePath.endsWith(extension)
        )
      );
    let entryFilePaths;
    if (sourceMapFilePaths.length > 0) {
      entryFilePaths = sourceMapFilePaths;
    } else {
      const entryFilePath = await getEntryFileFromVolume(volume);
      entryFilePaths = [entryFilePath];
    }

    // console.timeEnd("getEntryFileFromVolume");
    // console.time("file mapping");
    const fileMappingStart = Date.now();
    const fileContentMap = await getFileContentMap(volume, entryFilePaths, packageJson);
    const fileMappingEnd = Date.now();
    // console.timeEnd("file mapping");
    // console.time("getFileTokenMap");
    const tokenizationStart = Date.now();
    const fileTokenMap = await getFileTokenMap(fileContentMap);
    const tokenizationEnd = Date.now();
    // console.timeEnd("getFileTokenMap");

    const partialScanStart = Date.now();
    const { fileFourGramMap, fileStepMap, bundledDependencies } = await getFourGramMap(
      fileTokenMap,
      packageJson
    );
    const partialScanEnd = Date.now();

    global.fileMappingTime += fileMappingEnd - fileMappingStart;
    global.tokenizationTime += tokenizationEnd - tokenizationStart;
    global.partialScanTime += partialScanEnd - partialScanStart;
    return { fileFourGramMap, fileStepMap, bundledDependencies };
    // return fileFourGramMap;
  };

export default getTokensFromTarballUrl;
