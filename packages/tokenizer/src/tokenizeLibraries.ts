import { cpus } from "os";
import PQueue from "p-queue";
import semver from "semver";
import Piscina from "piscina";
import { logger, mongo } from "@bundle-scanner/common";
import { getLibraryReleaseManifests, getWeeklyDownloadsByVersion } from "./npm/index.js";
import { calculateSourceMapFrequencyByVersion } from "./lib/index.js";
import { TokenizeReleasesReturnType } from "./tokenizeReleases";

const { db } = mongo;

const cursorQueue = new PQueue({ concurrency: 1 });

const maxThreads = Math.min(4, cpus().length);

const workerPool = new Piscina({
  filename: new URL("./tokenizeReleasesWorker.js", import.meta.url).href,
  maxThreads,
  idleTimeout: 1000,
});

const createLibraryCursor = (libraryQuery) =>
  db.libraries
    .find(libraryQuery)
    .sort({ sourceMapFrequency: -1 })
    .addCursorFlag("noCursorTimeout", true);

const tokenizeLibraries = async (libraryQuery) => {
  // Fetch library names from db sorted by most downloads
  let cursor = createLibraryCursor(libraryQuery);
  const numLibraries = await cursor.count();

  logger.info(`Tokenizing ${numLibraries} libraries`);
  let index = -1;
  const indefinitelyCreateLibraryTokens = async () => {
    while (true) {
      index += 1;
      if (index >= numLibraries) break;
      const startTime = Date.now();
      let library;
      try {
        // eslint-disable-next-line no-loop-func
        library = await cursorQueue.add(() => cursor.next());
        if (!library) {
          logger.warn("Cursor returned null, bailing out of loop");
          break;
        }
      } catch (err) {
        logger.error(`Cursor failed with error '${err.message}', creating new cursor...`);
        cursor = createLibraryCursor(libraryQuery);
        continue;
      }

      // TODO: Check if this is still necessary when using source map frequency
      if (library._id.startsWith("@types/")) {
        logger.info(`Skipping ${library._id} due to typescript types being useless.`);
        continue;
      }

      const { releaseManifests, releaseDates, metadata } = await getLibraryReleaseManifests(
        library._id
      );

      const { sourceMapFrequency } = library;

      let weeklyDownloadsByVersion;
      try {
        weeklyDownloadsByVersion = await getWeeklyDownloadsByVersion(library._id);
      } catch (err) {
        logger.error(`[${library._id}]: Failed to fetch weekly downloads, skipping...`);
        continue;
      }

      const sourceMapFrequencyByVersion = calculateSourceMapFrequencyByVersion(
        weeklyDownloadsByVersion,
        sourceMapFrequency
      );

      const enhancedReleaseManifests = releaseManifests.map((manifest) => ({
        ...manifest,
        releaseDate: releaseDates?.[manifest.version]
          ? new Date(releaseDates[manifest.version])
          : undefined,
        sourceMapFrequency: sourceMapFrequencyByVersion[manifest.version],
        weeklyDownloads: weeklyDownloadsByVersion[manifest.version],
      }));

      const filteredReleaseManifests = enhancedReleaseManifests
        .sort((a, b) => b.sourceMapFrequency - a.sourceMapFrequency)
        .filter((manifest, i) => manifest.sourceMapFrequency > 0.3 || i === 0)
        // Sort again by version to enable most efficient caching
        .sort((a, b) => semver.rcompare(a.version, b.version));

      const numSkippedReleases = enhancedReleaseManifests.length - filteredReleaseManifests.length;

      logger.info(
        `[${library._id}]: ${filteredReleaseManifests.length} out of ${enhancedReleaseManifests.length} releases will be tokenized.`
      );

      const results: TokenizeReleasesReturnType = await workerPool.run({
        releaseManifests: filteredReleaseManifests,
        libraryId: library._id,
      });

      const resultsWithErrors = results.filter(({ error }) => error);

      const completedTime = Date.now();
      const estimatedTime = completedTime - startTime;
      logger.info(
        `[${library._id}]: Finished tokenization (i: ${index}, ${
          results.length
        } versions) in ${Math.round(estimatedTime / 1000)} seconds`
      );
      if (resultsWithErrors.length) {
        logger.warn(
          `[${library._id}]: ${resultsWithErrors.length} out of ${results.length} (${Math.round(
            (100 * resultsWithErrors.length) / results.length
          )}%) versions had errors: ${resultsWithErrors.map(({ version }) => version).join(", ")}`
        );
      }
      await db.libraries.updateOne(
        { _id: library._id },
        {
          $set: {
            ...metadata,
            isTokenized8: true,
            numVersions: results.length,
            numErrors: resultsWithErrors.length,
            numSkippedReleases,
            tokenizingTimeMs: estimatedTime,
          },
          $currentDate: {
            lastModified: true,
          },
        }
      );
    }
  };
  // Queue 2 extra libraries to make sure there's no dead time on any worker
  await Promise.all(Array.from({ length: maxThreads + 2 }).map(indefinitelyCreateLibraryTokens));
  await cursor.close();
  logger.info(`Tokenized all ${numLibraries} libraries, closing down...`);
};

export default tokenizeLibraries;
