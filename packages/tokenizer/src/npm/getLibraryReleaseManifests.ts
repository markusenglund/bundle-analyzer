import pacote, { packument } from "pacote";
import pRetry from "p-retry";
import hostedGitInfo from "hosted-git-info";
import spdxCorrect from "spdx-correct";
import { logger } from "@bundle-scanner/common";

interface ReturnType {
  releaseManifests: pacote.Manifest[];
  releaseDates: pacote.Packument["time"] | Record<string, never>;
  metadata: {
    description?: string;
    keywords?: string[];
    repositoryUrl?: string;
    license?: string;
  };
}

const normalizeLicense = (license: unknown): string | undefined => {
  if (!license) return;
  if (typeof license !== "string") return;
  return spdxCorrect(license) ?? undefined;
};

const getLibraryReleaseManifests = async (libraryId: string): Promise<ReturnType> => {
  let packument;
  try {
    packument = await pRetry(() => pacote.packument(libraryId, { fullMetadata: true }), {
      retries: 3,
    });
  } catch (err) {
    if (err.statusCode === 404) {
      return { releaseManifests: [], releaseDates: {}, metadata: {} };
    }
    throw err;
  }

  if (!packument.versions) {
    logger.warn(
      `Library ${libraryId} has no versions - it has likely been unpublished, skipping...`
    );
    return {
      releaseManifests: [],
      releaseDates: {},
      metadata: {},
    };
  }

  const releaseManifests = Object.values(packument.versions).reverse();

  const license = normalizeLicense(packument.license);
  const repositoryUrl =
    packument.repository?.url && hostedGitInfo.fromUrl(packument.repository.url)?.browse();

  const dependenciesObj = releaseManifests[0]?.dependencies ?? {};
  const dependencies = Object.entries(dependenciesObj).map((entries) => ({
    libraryId: entries[0],
    versionRange: entries[1],
  }));

  const metadata = {
    description: packument.description as string,
    keywords: packument.keywords,
    repositoryUrl,
    license,
    dependencies,
  };

  return {
    releaseManifests: releaseManifests,
    releaseDates: packument.time,
    metadata,
  };
};

export default getLibraryReleaseManifests;
