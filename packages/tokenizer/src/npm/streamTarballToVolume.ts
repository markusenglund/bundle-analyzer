import { pipeline } from "stream/promises";
import path from "path";
import { Volume, vol } from "memfs";
import tar from "tar-stream";
import gunzip from "gunzip-maybe";
import got from "got";
import { utils } from "@bundle-scanner/common";

const { CustomError } = utils;

export type VolumeType = typeof vol;

const streamTarballToVolume = async (tarballUrl: string): Promise<VolumeType> => {
  const volume = new Volume();

  const extract = tar.extract();
  extract.on("entry", async (header, stream, next) => {
    if (header.type === "file") {
      const chunks = [];
      for await (const chunk of stream) {
        chunks.push(chunk);
      }
      const buffer = Buffer.concat(chunks);
      const filePath = `/${header.name}`;
      const dirName = path.dirname(filePath);
      if (!volume.existsSync(dirName)) {
        volume.mkdirSync(dirName, { recursive: true });
      }
      volume.writeFileSync(filePath, buffer);
    }

    return next();
  });

  try {
    await pipeline(got.stream(tarballUrl), gunzip(), extract);
    return volume;
  } catch (err) {
    throw new CustomError("TarballError", err);
  }
};

export default streamTarballToVolume;
