import got, { Response } from "got";

export interface DownloadsByVersion {
  [key: string]: number;
}

interface ApiResponse extends Response {
  body: {
    package: string;
    downloads: DownloadsByVersion;
  };
}

const getWeeklyDownloadsByVersion = async (
  libraryId: string
): Promise<DownloadsByVersion> => {
  const encodedLibraryId = encodeURIComponent(libraryId);
  const url = `https://api.npmjs.org/versions/${encodedLibraryId}/last-week`;
  const { body }: ApiResponse = await got(url, { responseType: "json" });
  const downloadsByVersion = body.downloads;
  return downloadsByVersion;
};

export default getWeeklyDownloadsByVersion;
