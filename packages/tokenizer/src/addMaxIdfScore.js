import { logger, mongo } from "@bundle-scanner/common";
const { db } = mongo;

const addMaxIdfScore = async (query = {}) => {
  const filesCursor = db.files.find({
    ...query,
    error: { $exists: false },
  });

  let maxIdfScores = [];
  for await (const file of filesCursor) {
    const fourGrams = await db.fourGrams
      .find({ fileIds: file._id })
      .project({ _id: 1, df: 1 })
      .toArray();

    let maxIdfScore = 0;
    for (const { _id: fourGram, df = 1 } of fourGrams) {
      const idf = (1 / df) ** (1 / 2.5);
      maxIdfScore += idf;
    }
    maxIdfScores.push({
      maxIdfScore: Math.ceil(maxIdfScore * 100) / 100,
      fileId: file._id,
    });
    if (maxIdfScores.length > 1000) {
      console.log("Bulk updating...", file._id);
      await bulkUpdate(maxIdfScores);
      maxIdfScores = [];
      console.log("finished bulk update");
    }
  }
  await bulkUpdate(maxIdfScores);
  logger.info("Added max match score");
};

const bulkUpdate = async (files) => {
  await db.files.bulkWrite(
    files.map(({ fileId, maxIdfScore }) => ({
      updateOne: {
        filter: { _id: fileId },
        update: { $set: { maxIdfScore } },
      },
    }))
  );
};

export default addMaxIdfScore;
