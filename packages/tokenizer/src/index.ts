import tokenizeLibraries from "./tokenizeLibraries.js";
import { logger, mongo } from "@bundle-scanner/common";
const { connect } = mongo;

(async () => {
  logger.info("Starting up...");
  await connect();
  logger.info("Connected to db, fetching libraries sorted by downloads");

  tokenizeLibraries({
    sourceMapFrequency: { $exists: true },
    dependencies: { $exists: false },
  }).then(() => {
    process.exit(0);
  });
})();
