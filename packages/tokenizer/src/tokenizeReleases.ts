import crypto from "crypto";
import pMap from "p-map";
import { Manifest } from "pacote";
import { logger, mongo } from "@bundle-scanner/common";
import getTokensFromTarballUrl from "./getTokensFromTarballUrl.js";

const { db } = mongo;

interface EnhancedManifest extends Manifest {
  releaseDate?: Date;
  sourceMapFrequency: number;
}

// @ts-ignore
global.dbTime = 0;
let latestReleaseUpdate: { releaseId?: string; time: number } = {
  time: Date.now(),
};

setInterval(() => {
  logger.info(
    `Last updated release: ${latestReleaseUpdate.releaseId}, ${Math.round(
      (Date.now() - latestReleaseUpdate.time) / 1000
    )} seconds ago`
  );

  logger.info(`Time spent mapping files: ${Math.round(global.fileMappingTime / 1000)}s`);
  logger.info(`Time spent tokenizing: ${Math.round(global.tokenizationTime / 1000)}s`);
  logger.info(`Time spent partial scanning: ${Math.round(global.partialScanTime / 1000)}s`);
  logger.info(`Time spent updating db: ${Math.round(global.dbTime / 1000)}s`);
  logger.info(`Time spent fetching tarballs: ${Math.round(global.tarballTime / 1000)}s`);

  const { rss, heapUsed } = process.memoryUsage();
  logger.info(
    `MEMORY USAGE - heapUsed: ${Math.round(heapUsed / (1024 * 1024))}MB, rss: ${Math.round(
      rss / (1024 * 1024)
    )}MB`
  );
}, 100 * 1000);

export type TokenizeReleasesReturnType = {
  _id: string;
  version: string;
  error?: string;
  success: boolean;
}[];

const tokenizeRelease = async (releaseManifest: EnhancedManifest, i: number) => {
  const {
    name: libraryId,
    version,
    dependencies: dependenciesObj = {},
    dist,
    releaseDate,
    sourceMapFrequency,
    weeklyDownloads,
  } = releaseManifest;
  const { tarball: tarballUrl, unpackedSize: size } = dist;

  // Some releases from 2014 lack _id field (!)
  const releaseId = releaseManifest._id ?? `${libraryId}@${version}`;

  let error;
  const files: {
    _id: string;
    fileName: string;
    numFourGrams: number;
    fourGramHash: string;
    libraryId: string;
    step: number;
  }[] = [];
  const fourGramFileIdMap: Map<string, string[]> = new Map();

  let bundledDependencies;
  try {
    // console.time(`getTokensFromTarball complete - ${version}`);
    const result = await getTokensFromTarballUrl(tarballUrl, libraryId);
    const { fileFourGramMap, fileStepMap } = result;
    ({ bundledDependencies } = result);
    // console.timeEnd(`getTokensFromTarball complete - ${version}`);

    // const numReleaseTokens = [...tokenMap.values()].flat().length;

    for (const [fileName, fourGrams] of fileFourGramMap.entries()) {
      // const step = calculateStepSize(fileTokens.length, numReleaseTokens);
      // const fourGrams = tokensToKGrams(fileTokens, { k: 4, step });

      if (fourGrams.length < 1) continue;

      const fourGramHash = crypto
        .createHash("md5")
        .update(JSON.stringify(fourGrams))
        .digest("base64");

      const fileId = `${libraryId}-${fourGramHash}`;

      const step = fileStepMap.get(fileName) as number;
      files.push({
        _id: fileId,
        fileName,
        numFourGrams: fourGrams.length,
        fourGramHash,
        libraryId,
        step,
      });

      for (const fourGram of fourGrams) {
        const fileIds = fourGramFileIdMap.get(fourGram) || [];
        fileIds.push(fileId);
        fourGramFileIdMap.set(fourGram, fileIds);
      }
    }
    if (files.length === 0) {
      error = "No files were indexed";
    }
  } catch (err) {
    if (err instanceof SyntaxError) {
      logger.error(`Syntax error in ${libraryId} ${version}: ${err.message}`);
      error = err.message;
    } else if (err.name === "EnhancedResolveError") {
      logger.error(`[${libraryId}@${version}]: Could not resolve file: ${err.message}`);
      error = err.message;
    } else if (err.name === "TerserError") {
      logger.error(`[${libraryId}@${version}]: Terser could not parse code: ${err.message}`);
      error = err.message;
    } else if (err.name === "ModuleError") {
      logger.error(`[${libraryId}@${version}]: File mapping failed: ${err.message}`);
      error = err.message;
    } else if (err.name === "PackageJsonError") {
      logger.error(`[${libraryId}@${version}]: Could not get package.json file from volume`);
      error = err.message;
    } else if (err.name === "TarballError") {
      logger.error(`[${libraryId}@${version}]: Tarball fetch failed`);
    } else {
      logger.error(`Unhandled error tokenizing '${releaseId}'`);
      throw err;
    }
  }

  const numFourGrams = [...fourGramFileIdMap.values()].flat().length;
  const numUniqueFourGrams = [...fourGramFileIdMap.keys()].length;
  const numFiles = files.length;

  const dependencies = Object.entries(dependenciesObj).map((entries) => ({
    libraryId: entries[0],
    versionRange: entries[1],
  }));

  const release = {
    _id: releaseId,
    libraryId,
    version,
    numFourGrams,
    numUniqueFourGrams,
    numFiles,
    size,
    dependencies,
    bundledDependencies,
    error,
    sourceMapFrequency,
    weeklyDownloads,
    releaseDate,
    tokenizedDate: new Date(),
  };

  // console.time("release db");
  const dbUpdatesStart = Date.now();
  await db.releases.replaceOne({ _id: release._id }, release, {
    upsert: true,
  });
  // console.timeEnd("release db");

  if (files.length > 0) {
    await db.files.bulkWrite(
      files.map((file) => {
        return {
          updateOne: {
            filter: { _id: file._id },
            update: { $set: file, $addToSet: { releaseIds: releaseId } },
            upsert: true,
          },
        };
      })
    );

    await db.fourGrams.bulkWrite(
      [...fourGramFileIdMap.entries()].map(([fourGram, fileIds]) => ({
        updateOne: {
          filter: { _id: fourGram },
          update: { $addToSet: { fileIds: { $each: fileIds } } },
          upsert: true,
        },
      }))
    );
  }

  const dbUpdatesEnd = Date.now();
  global.dbTime += dbUpdatesEnd - dbUpdatesStart;

  latestReleaseUpdate = {
    releaseId,
    time: Date.now(),
  };

  if (i % 20 === 0) {
    logger.info(
      `[${libraryId}@${version}] Finished tokenizing release. (${numFourGrams} 4-grams, ${numFiles} files indexed)`
    );
  }
  if (numFourGrams === 0) {
    logger.warn(`[${libraryId}@${version}] - Zero 4-grams indexed!`);
  }
  return { _id: releaseId, version, error, success: !!error };
};

export type TokenizeReleases = (arg: {
  releaseManifests: EnhancedManifest[];
  libraryId: string;
}) => Promise<TokenizeReleasesReturnType>;

const tokenizeReleases: TokenizeReleases = async ({ releaseManifests, libraryId }) => {
  // logger.warn(
  //   `Deleting previously existing files belonging to library ${libraryId}`
  // );
  // await db.files7.deleteMany({ libraryId });

  const results = await pMap(releaseManifests, tokenizeRelease, {
    concurrency: 1,
  });

  return results;
};

export default tokenizeReleases;
