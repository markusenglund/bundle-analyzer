import Piscina from "piscina";

const maxThreads = Number(process.env.MAX_THREADS) || 3;

const workerPool = new Piscina({
  filename: new URL("../../scanner/dist/scanBundleWorker.js", import.meta.url).href,
  maxThreads,
  minThreads: maxThreads,
  idleTimeout: 1000,
});

export default workerPool;
