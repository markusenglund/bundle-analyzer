import workerPool from "../workerPool.js";
import { mongo } from "@bundle-scanner/common";
import getMatchedReleasesWithIntervals from "../getMatchedReleasesWithIntervals.js";

const { db } = mongo;

async function getMatchesFromAnalysis(code, bundleUrl) {
  const { matchedReleases, sortedIntervals, fourGramPositions } = await workerPool.run({
    code,
    bundleUrl,
  });

  const matchedReleasesWithIntervals = getMatchedReleasesWithIntervals({
    matchedReleases,
    matchedIntervals: sortedIntervals,
    fourGramPositions,
  });

  const libraries = await db.libraries
    .find({
      _id: { $in: matchedReleases.map(({ libraryId }) => libraryId) },
    })
    .project({
      sourceMapFrequency: 1,
      description: 1,
      license: 1,
      repositoryUrl: 1,
      dependencies: 1,
    })
    .toArray();

  const librariesById = new Map(libraries.map(({ _id, ...library }) => [_id, library]));

  const releasesWithLibraryInfo = matchedReleasesWithIntervals.map((release) => ({
    ...librariesById.get(release.libraryId),
    ...release,
    sourceMapFrequency: librariesById.get(release.libraryId)?.sourceMapFrequency,
  }));

  return releasesWithLibraryInfo;
}

export default getMatchesFromAnalysis;
