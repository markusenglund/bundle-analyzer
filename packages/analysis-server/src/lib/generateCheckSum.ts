import crypto from "crypto";

function generateCheckSum(string: string): string {
  const checkSum = crypto.createHash("md5").update(string).digest("base64");
  return checkSum;
}

export default generateCheckSum;
