import { sumBy, groupBy } from "lodash-es";
import { mongo, SourceMap, sourceMapUtils, bundleTypes } from "@bundle-scanner/common";

const { extractLibrariesFromSourceMap, extractIntervalsFromSourceMap } = sourceMapUtils;
const { db } = mongo;

async function getMatchesFromSourceMap(
  sourceMapResults: { sourceMap: SourceMap; sourceMapUrl: string | undefined },
  code: string
): Promise<{
  sourceMapData: { url: string | undefined; libraryIds: string[]; modulePaths: string[] };
  matchedReleases: bundleTypes.SourceMapMatchedReleasesType;
}> {
  const { sourceMap, sourceMapUrl } = sourceMapResults;
  const { libraryIds, modulePaths } = extractLibrariesFromSourceMap(sourceMap);
  const intervals = await extractIntervalsFromSourceMap(sourceMap, code);

  const libraryIntervals = intervals.filter(
    (interval: typeof intervals[0]): interval is bundleTypes.SourceMapLibraryIntervalType =>
      typeof interval.libraryId === "string"
  );

  const intervalsByLibraryId = groupBy(libraryIntervals, "libraryId");

  const libraries: {
    _id: string;
    sourceMapFrequency?: number;
    description?: string;
    license?: string;
    repositoryUrl?: string;
    dependencies?: { libraryId: string; versionRange: string }[];
  }[] = await db.libraries
    .find({
      _id: { $in: libraryIds },
    })
    .project({
      sourceMapFrequency: 1,
      description: 1,
      license: 1,
      repositoryUrl: 1,
      dependencies: 1,
    })
    .toArray();

  const librariesById = new Map(libraries.map(({ _id, ...library }) => [_id, library]));

  const matchedReleasesWithIntervals = libraryIds
    .filter((libraryId) => librariesById.has(libraryId))
    .map((libraryId) => {
      const intervals = intervalsByLibraryId[libraryId];

      const cumulativeIntervalSize =
        (intervals && sumBy(intervals, ({ start, end }) => (end ? end - start : 0))) ?? 0;

      const libraryData = librariesById.get(libraryId)!;

      return {
        _id: libraryId,
        libraryId,
        intervals,
        cumulativeIntervalSize,
        ...libraryData,
      };
    });

  const sourceMapData = {
    url: sourceMapUrl,
    libraryIds,
    modulePaths,
  };

  return { matchedReleases: matchedReleasesWithIntervals, sourceMapData };
}

export default getMatchesFromSourceMap;
