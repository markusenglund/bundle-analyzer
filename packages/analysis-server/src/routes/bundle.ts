import got from "got";
import { IncomingHttpHeaders } from "http";
import { logger, utils, bundleTypes } from "@bundle-scanner/common";
import gzipSize from "gzip-size";
import { FastifyInstance } from "fastify";
import { getSourceMapFromBundle, getSourceMapFromUrl } from "@bundle-scanner/scraper";
import thirdPartyWeb from "third-party-web";
import { Type, Static } from "@sinclair/typebox";
import normalizeUrl from "normalize-url";
import getMatchesFromAnalysis from "../lib/getMatchesFromAnalysis.js";
import getMatchesFromSourceMap from "../lib/getMatchesFromSourceMap.js";
import generateCheckSum from "../lib/generateCheckSum.js";
import { SCANNER_VERSION } from "../constants.js";

const { standardizeBundleUrl } = utils;

const BundleParams = Type.Object({
  bundleUrl: Type.String(),
});

type BundleParamsType = Static<typeof BundleParams>;

async function routes(fastify: FastifyInstance): Promise<void> {
  fastify.get<{ Params: BundleParamsType; Response: bundleTypes.BundleResponseType }>(
    "/api/bundle/:bundleUrl",
    {
      schema: {
        params: BundleParams,
        response: {
          200: bundleTypes.BundleResponse,
        },
      },
    },
    async (request, reply) => {
      const { bundleUrl } = request.params;
      const normalizedBundleUrl = normalizeUrl(bundleUrl, { stripHash: true, stripWWW: false });

      logger.info(`Handling request to scan individual bundle: '${normalizedBundleUrl}'`);

      let code: string, headers: IncomingHttpHeaders;
      try {
        ({ body: code, headers } = await got(normalizedBundleUrl, {
          retry: 0,
        }));
      } catch (err) {
        reply.notFound(`Request to '${normalizedBundleUrl}' failed`);
        return;
      }

      const contentType = headers["content-type"];
      const isJsMimeType = [
        "application/javascript",
        "application/x-javascript",
        "text/javascript",
        "application/ecmascript",
        "text/ecmascript",
      ].some((mimeType) => contentType?.startsWith(mimeType));

      if (!isJsMimeType) {
        reply.unsupportedMediaType(
          `URL could not be handled as Javascript due to having content-type '${contentType}'`
        );
        return;
      }

      const bundleId = standardizeBundleUrl(normalizedBundleUrl);

      const bundle = {
        code,
        _id: bundleId,
        fullUrl: normalizedBundleUrl,
      };

      const checkSum = generateCheckSum(code);

      const sizeBytes = Buffer.byteLength(code);
      const sizeBytesGzip = await gzipSize(code);
      const numChars = code.length;

      let matchedReleases: bundleTypes.MatchedReleasesType;
      let sourceMapData: bundleTypes.SourceMapDataType;
      let analysisMode: bundleTypes.AnalysisModeType;

      const sourceMapResults = await getSourceMapFromBundle(bundle);
      if (sourceMapResults) {
        try {
          ({ matchedReleases, sourceMapData } = await getMatchesFromSourceMap(
            sourceMapResults,
            bundle.code
          ));
          analysisMode = "sourcemap";
        } catch (error) {
          logger.error(error);
          matchedReleases = await getMatchesFromAnalysis(bundle.code, bundle._id);
          analysisMode = "scan";
        }
      } else {
        matchedReleases = await getMatchesFromAnalysis(bundle.code, bundle._id);
        analysisMode = "scan";
      }

      const bundleData: {
        _id: string;
        fullUrl: string;
        thirdPartyWebData?: { name: string; categories: string[] };
      } = {
        _id: bundleId,
        fullUrl: normalizedBundleUrl,
      };
      const thirdPartyWebEntity = thirdPartyWeb.getEntity(bundle._id);
      if (thirdPartyWebEntity) {
        const { name, categories } = thirdPartyWebEntity;
        bundleData.thirdPartyWebData = { name, categories };
      }

      const bundleScanData = {
        bundleUrl: bundleId,
        scannerVersion: SCANNER_VERSION,
        date: new Date(),
        numChars,
        sizeBytes,
        sizeBytesGzip,
        analysisMode,
        sourceMapData,
        matchedReleases,
        checkSum,
      };

      // TODO: Type system did not catch incorrect data in this return object!
      reply.send({ data: { ...bundleData, bundleScan: bundleScanData } });
    }
  );
  const WebsiteBundlesBody = Type.Object({
    bundle: Type.Object({
      url: Type.String(),
      sourceMapUrl: Type.Optional(Type.String()),
      numChars: Type.Number(),
      code: Type.String(),
    }),
  });

  type WebsiteBundlesBodyType = Static<typeof WebsiteBundlesBody>;

  fastify.post<{ Body: WebsiteBundlesBodyType; Reply: bundleTypes.BundleResponseType }>(
    "/api/bundle",
    {
      schema: {
        body: WebsiteBundlesBody,
        response: { 200: bundleTypes.BundleResponse },
      },
    },
    async (request, reply) => {
      const {
        bundle: { url: bundleUrl, code, numChars, sourceMapUrl },
      } = request.body;

      const bundleId = standardizeBundleUrl(bundleUrl);

      const sizeBytes = Buffer.byteLength(code);
      const sizeBytesGzip = await gzipSize(code);

      let matchedReleases: bundleTypes.MatchedReleasesType;
      let sourceMapData: bundleTypes.SourceMapDataType;
      let analysisMode: bundleTypes.AnalysisModeType;

      const sourceMapResults =
        !!sourceMapUrl && (await getSourceMapFromUrl(sourceMapUrl, bundleUrl));
      if (sourceMapResults) {
        try {
          ({ matchedReleases, sourceMapData } = await getMatchesFromSourceMap(
            sourceMapResults,
            code
          ));
          analysisMode = "sourcemap";
        } catch (error) {
          logger.error(error);
          matchedReleases = await getMatchesFromAnalysis(code, bundleId);
          analysisMode = "scan";
        }
      } else {
        matchedReleases = await getMatchesFromAnalysis(code, bundleId);
        analysisMode = "scan";
      }
      const analyzedBundle: bundleTypes.BundleWithScanBaseType = {
        _id: bundleId,
        fullUrl: bundleUrl,
        bundleScan: {
          bundleUrl: bundleId,
          scannerVersion: SCANNER_VERSION,
          date: new Date().toISOString(),
          analysisMode,
          matchedReleases,
          sourceMapData,
          numChars,
          sizeBytes,
          sizeBytesGzip,
        },
      };

      const thirdPartyWebEntity = thirdPartyWeb.getEntity(bundleId);
      if (thirdPartyWebEntity) {
        const { name, categories } = thirdPartyWebEntity;
        analyzedBundle.thirdPartyWebData = { name, categories };
      }

      reply.send({ data: analyzedBundle });
    }
  );
}

export default routes;
