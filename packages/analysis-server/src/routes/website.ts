import { logger, bundleTypes } from "@bundle-scanner/common";
import {
  initializeCluster,
  scrapeUrl,
  categorizeBundlesByParty,
  getSourceMapFromBundle,
  ClusterType,
} from "@bundle-scanner/scraper";
import thirdPartyWeb from "third-party-web";
import got from "got";
import { sumBy } from "lodash-es";
import { Server, IncomingMessage, ServerResponse } from "http";
import { FastifyInstance } from "fastify";
import pMap from "p-map";
import { Type, Static } from "@sinclair/typebox";
import getMatchesFromAnalysis from "../lib/getMatchesFromAnalysis.js";
import getMatchesFromSourceMap from "../lib/getMatchesFromSourceMap.js";
import { SCANNER_VERSION } from "../constants.js";
import generateCheckSum from "../lib/generateCheckSum.js";

const maxThreads = Number(process.env.MAX_THREADS) || 3;
const puppeteerConcurrency = Number(process.env.PUPPETEER_CONCURRENCY) || 4;
const puppeteerTimeoutSeconds = Number(process.env.PUPPETEER_TIMEOUT_SECONDS) || 10;

let initializedCluster: ClusterType;
const getInitializedCluster = async () => {
  if (!initializedCluster) {
    initializedCluster = await initializeCluster({
      concurrency: puppeteerConcurrency,
      timeout: puppeteerTimeoutSeconds * 1000,
      includeFaviconUrl: true,
      includeThirdPartyBundles: true,
    });
  }
  return initializedCluster;
};

const WebsiteParams = Type.Object({
  standardizedWebsiteUrl: Type.String(),
});

type WebsiteParamsType = Static<typeof WebsiteParams>;

async function routes(
  fastify: FastifyInstance<Server, IncomingMessage, ServerResponse>
): Promise<void> {
  fastify.get<{ Params: WebsiteParamsType; Reply: bundleTypes.WebsiteReplyType }>(
    "/api/website/:standardizedWebsiteUrl",
    {
      schema: {
        params: WebsiteParams,
        response: {
          200: bundleTypes.WebsiteReply,
        },
      },
    },
    async (request, reply) => {
      const cluster = await getInitializedCluster();
      const { standardizedWebsiteUrl } = request.params;

      logger.info(`Fetching scripts from '${standardizedWebsiteUrl}'`);

      try {
        var { bundles, finalUrl, faviconUrl, host, domain, redirectedUrls } = await scrapeUrl(
          cluster,
          standardizedWebsiteUrl
        );
      } catch (err) {
        logger.warn(err.message);
        let message: string | undefined;
        if (err.message.includes("ERR_NAME_NOT_RESOLVED")) {
          message = `Could not resolve URL '${standardizedWebsiteUrl}'`;
        }
        reply.notFound(message ?? err.message);
        return;
      }

      logger.info(`[${standardizedWebsiteUrl}] Found ${bundles.length} bundles`);

      const totalSizeBytes = sumBy(bundles, "sizeBytes");
      const totalSizeBytesGzip = sumBy(bundles, "sizeBytesGzip");

      const { firstPartyBundles, thirdPartyBundles } = categorizeBundlesByParty(
        bundles,
        finalUrl,
        domain
      );

      const firstPartyBundleIds = firstPartyBundles.map((bundle) => bundle._id);
      const thirdPartyBundleIds = thirdPartyBundles.map((bundle) => bundle._id);

      interface FullAnalyzedBundle extends bundleTypes.BundleWithScanType {
        meta: {
          wasCached: boolean;
        };
      }

      const analyzedBundlesById: Record<string, FullAnalyzedBundle> = {};
      const failedBundleIds: string[] = [];
      if (bundles.length > 0) {
        const bundleIds = bundles.map(({ _id }) => _id);
        // @ts-ignore
        const queryParams = new URLSearchParams({ bundleIds }).toString();

        try {
          const {
            body: { data: alreadyAnalyzedBundles },
          } = await got<{ data: bundleTypes.BundleWithScanType[] }>(
            `${process.env.API_SERVER_URL}/api/bundle/analyzed?${queryParams}`,
            {
              responseType: "json",
            }
          );

          for (const analyzedBundle of alreadyAnalyzedBundles) {
            analyzedBundlesById[analyzedBundle._id] = {
              ...analyzedBundle,
              meta: { wasCached: true },
            };
          }
        } catch (err) {
          logger.warn(
            `[${standardizedWebsiteUrl}] Request to API server for analyzed bundle failed`
          );
        }
      }

      await pMap(
        bundles,
        async (bundle) => {
          if (analyzedBundlesById[bundle._id]) return;

          let matchedReleases;
          let sourceMapData;

          const checkSum = generateCheckSum(bundle.code);

          const sourceMapResults = await getSourceMapFromBundle(bundle);
          let analysisMode: "sourcemap" | "scan";
          if (sourceMapResults) {
            try {
              ({ matchedReleases, sourceMapData } = await getMatchesFromSourceMap(
                sourceMapResults,
                bundle.code
              ));
              analysisMode = "sourcemap";
            } catch (error) {
              logger.error(error);
              try {
                matchedReleases = await getMatchesFromAnalysis(bundle.code, bundle._id);
                analysisMode = "scan";
              } catch (err) {
                logger.error(`'${bundle._id}' scan failed'`);
                logger.error(err);
                failedBundleIds.push(bundle._id);
                return;
              }
            }
          } else {
            try {
              matchedReleases = await getMatchesFromAnalysis(bundle.code, bundle._id);
              analysisMode = "scan";
            } catch (err) {
              logger.error(`'${bundle._id}' scan failed'`);
              logger.error(err);
              failedBundleIds.push(bundle._id);
              return;
            }
          }
          const { sizeBytes, sizeBytesGzip, numChars, fullUrl } = bundle;

          const analyzedBundle: FullAnalyzedBundle = {
            _id: bundle._id,
            domains: [domain],
            hosts: [host],
            pageUrls: [finalUrl],
            fullUrl,
            bundleScan: {
              bundleUrl: bundle._id,
              numChars,
              sizeBytes,
              sizeBytesGzip,
              matchedReleases,
              sourceMapData,
              analysisMode,
              date: new Date().toISOString(),
              scannerVersion: SCANNER_VERSION,
              checkSum,
            },
            meta: {
              wasCached: false,
            },
          };

          if (matchedReleases.find(({ sourceMapFrequency }) => !sourceMapFrequency)) {
            console.log({ matchedReleases });
          }
          const thirdPartyWebEntity = thirdPartyWeb.getEntity(bundle._id);
          if (thirdPartyWebEntity) {
            const { name, categories } = thirdPartyWebEntity;
            analyzedBundle.thirdPartyWebData = { name, categories };
          }

          analyzedBundlesById[bundle._id] = analyzedBundle;
        },
        { concurrency: maxThreads + 4 }
      );

      reply.send({
        data: {
          _id: finalUrl,
          faviconUrl,
          host,
          domain,
          redirectedUrls,
          createdDate: new Date().toISOString(),
          urlScan: {
            url: finalUrl,
            date: new Date().toISOString(),
            source: "puppeteer",
            firstPartyBundleIds,
            thirdPartyBundleIds,
            totalSizeBytes,
            totalSizeBytesGzip,
            bundlesById: analyzedBundlesById,
            failedBundleIds,
          },
        },
      });

      logger.info(`[${standardizedWebsiteUrl}]: Successfully finished request`);
    }
  );
}

export default routes;
