import { groupBy, sumBy } from "lodash-es";

function getMatchedReleasesWithIntervals({ matchedReleases, matchedIntervals, fourGramPositions }) {
  const charIntervalsByLibraryId = groupBy(
    matchedIntervals.map(
      ({
        fileName,
        libraryId,
        start,
        end,
        version,
        intervalMatchRatio,
        intervalSize,
        maxIntervalSize,
      }) => ({
        fileName,
        libraryId,
        start: fourGramPositions[start].start,
        end: fourGramPositions[end - 1].end,
        intervalSizeFourGrams: intervalSize,
        maxIntervalSizeFourGrams: maxIntervalSize,
        intervalMatchRatio,
      })
    ),
    "libraryId"
  );
  const matchedReleasesWithIntervals = matchedReleases.map((release) => {
    const intervals = charIntervalsByLibraryId[release.libraryId] ?? [];
    const cumulativeIntervalSize =
      (intervals && sumBy(intervals, ({ start, end }) => end - start)) ?? 0;
    return {
      ...release,
      intervals,
      cumulativeIntervalSize,
    };
  });

  return matchedReleasesWithIntervals;
}

export default getMatchedReleasesWithIntervals;
