import Fastify from "fastify";
import sensible from "fastify-sensible";
import { truncate } from "lodash-es";
import website from "./routes/website.js";
import bundle from "./routes/bundle.js";
import { mongo, logger } from "@bundle-scanner/common";

const fastify = Fastify({
  maxParamLength: 10000,
  disableRequestLogging: true,
  connectionTimeout: 60_000,
  bodyLimit: 30_000_000,
  ajv: {
    customOptions: {
      removeAdditional: true,
      useDefaults: true,
      coerceTypes: true,
      allErrors: false,
      nullable: false,
    },
    plugins: [],
  },
});

fastify.addHook("onResponse", (request, reply, done) => {
  const { statusCode } = reply.raw;
  if (statusCode < 400) {
    const message = `${statusCode} - ${request.method} ${truncate(request.raw.url, {
      length: 200,
    })}`;
    logger.info(message);
  }

  done();
});

fastify.addHook("onTimeout", (request, reply, done) => {
  logger.error(
    `${request.method} ${truncate(request.raw.url, {
      length: 200,
    })}: Request timed out!`
  );
  done();
});

fastify.setErrorHandler((error, request, reply) => {
  const { statusCode } = reply.raw;
  const message = `${statusCode} - ${request.method} ${truncate(request.raw.url, {
    length: 200,
  })}: ${error.message}`;
  if (statusCode >= 500) {
    logger.error(message);
    logger.error(error);
  } else if (statusCode >= 400) {
    logger.warn(message);
  } else {
    logger.error(message);
    logger.error(error);
  }

  reply.send(error);
});

fastify.register(sensible, { errorHandler: false });

fastify.register(website);
fastify.register(bundle);

mongo.connect().then(() => {
  const port = process.env.PORT || "1338";
  const host = process.env.HOST || "0.0.0.0";
  fastify.listen(port, host).catch((err) => {
    fastify.log.error(err);
    process.exit(1);
  });
});
