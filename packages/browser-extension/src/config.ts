const env = process.env.NODE_ENV ?? "development";

export const SERVER_URL_BASE =
  env === "production" ? "https://bundlescanner.com" : "http://localhost:1337";
