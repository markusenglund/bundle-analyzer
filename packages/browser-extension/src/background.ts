function isValidUrl(url) {
  try {
    new URL(url);
    return true;
  } catch {
    return false;
  }
}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.type === "openPopup") {
    chrome.tabs.query({ active: true, currentWindow: true }).then(async ([tab]) => {
      console.log("Tab", { tab });
      const { id: tabId, url: tabUrl, favIconUrl: faviconUrl } = tab;

      const debuggee = { tabId };

      const bundles: {
        url: string;
        sourceMapUrl?: string;
        numChars: number;
        code: string;
      }[] = [];

      let errorMessage;
      try {
        await chrome.debugger.attach(debuggee, "1.3");
      } catch (err) {
        errorMessage = err.message;
      }
      if (errorMessage) {
        sendResponse({ error: errorMessage });
        return;
      }

      const scriptSourcePromises: Promise<void>[] = [];
      chrome.debugger.onEvent.addListener(async (source, method, params) => {
        if (method === "Debugger.scriptParsed") {
          const { url, sourceMapURL: sourceMapUrl, length: numChars, scriptId } = params;
          if (
            !url ||
            !isValidUrl(url) ||
            !["http:", "https:"].some((protocol) => url.startsWith(protocol)) ||
            url === tabUrl
          ) {
            return;
          }
          // source.tabId
          const promise = chrome.debugger
            .sendCommand(debuggee, "Debugger.getScriptSource", { scriptId })
            .then(({ scriptSource: code }) => {
              bundles.push({ url, sourceMapUrl, numChars, code });
            });
          scriptSourcePromises.push(promise);
        }
        console.log("Debugger event handler", { source, method, params });
      });

      // await chrome.debugger.sendCommand(debuggee, "Network.enable");
      const { debuggerId } = await chrome.debugger.sendCommand(debuggee, "Debugger.enable");
      console.log(`Attached debugger with ID '${debuggerId}' to URL ${tabUrl}`);
      await new Promise((resolve) => {
        setTimeout(resolve, 50);
      });
      await Promise.all(scriptSourcePromises);
      await chrome.debugger.detach(debuggee);
      console.log(`Bundle gathering finished, got ${bundles.length} bundles`, bundles);

      sendResponse({ tabUrl, faviconUrl, bundles });
    });

    return true;
  }
  sendResponse("???");
});
