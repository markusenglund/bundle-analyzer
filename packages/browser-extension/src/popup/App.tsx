import React from "react";
import GlobalStyles from "./GlobalStyles";
import { QueryClient, QueryClientProvider } from "react-query";
import { withProfiler, ErrorBoundary } from "@sentry/react";
import { NavigationProvider } from "./NavigationContext";
import Main from "./Main";
import ErrorFallback from "./ErrorFallback";

const App = () => {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        staleTime: Infinity,
        retry: false,
        refetchOnWindowFocus: false,
      },
    },
  });

  return (
    <ErrorBoundary fallback={ErrorFallback}>
      <QueryClientProvider client={queryClient}>
        <NavigationProvider>
          <GlobalStyles />
          <Main />
        </NavigationProvider>
      </QueryClientProvider>
    </ErrorBoundary>
  );
};

export default withProfiler(App);
