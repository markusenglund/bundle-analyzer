import React, { useMemo, useContext } from "react";
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import styled from "@emotion/styled";
import { useTable, useSortBy } from "react-table";
import prettyBytes from "pretty-bytes";
import { sumBy, round, truncate } from "lodash-es";
import { formatDistanceToNow } from "date-fns";
import { NavigationContext } from "../NavigationContext";
import { bundlePropType } from "../propTypes";

const Cell = styled("div")`
  padding: 6px 12px;
`;

const propTypes = {
  bundleIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  bundlesById: PropTypes.objectOf(bundlePropType).isRequired,
  isThirdParty: PropTypes.bool,
  websiteUrl: PropTypes.string.isRequired,
};

const WebsiteTable = ({ bundleIds, bundlesById, isThirdParty }) => {
  const bundles = useMemo(
    () =>
      bundleIds.map((url) => {
        const bundle = bundlesById[url];
        const { host, fileName } = getUrlParts(bundle._id);
        return { ...bundle, host, fileName };
      }),
    [bundlesById, bundleIds]
  );

  const { dispatch, state } = useContext(NavigationContext);

  const columns = useMemo(
    () => [
      {
        Header: "",
        Cell: ({
          value: _id,
          row: {
            original: { matchedReleases },
          },
        }) =>
          matchedReleases.length > 0 ? (
            <button
              css={css`
                text-decoration: none;
                background: #161;
                appearance: button;
                color: inherit;
                :hover {
                  background: #050;
                }
                height: calc(100% - 8px);
                padding: 4px;
                width: calc(100% - 8px);
                display: flex;
                justify-content: center;
                align-items: center;
                text-align: center;
              `}
              onClick={() => dispatch({ type: "VIEW_BUNDLE", payload: { bundleUrl: _id } })}
            >
              View libraries
            </button>
          ) : null,
        accessor: "_id",
        disableSortBy: true,
      },
      {
        Header: "Bundle",
        Cell: ({ value: fileName }) => (
          <Cell>
            <span
              css={css`
                overflow-wrap: break-word;
              `}
            >
              {fileName}
            </span>
          </Cell>
        ),
        accessor: "fileName",
      },
      ...(isThirdParty
        ? [
            {
              Header: "Third party",
              accessor: "thirdPartyWebData.name",
              Cell: ({ value: thirdParty }) => <Cell>{thirdParty}</Cell>,
            },
          ]
        : []),
      {
        Header: "Host",
        accessor: "host",
        Cell: ({ value: host }) => <Cell>{host}</Cell>,
      },
      {
        Header: "Size",
        Cell: ({
          value: sizeBytes,
          row: {
            original: { sizeBytesGzip },
          },
        }) => (
          <Cell>
            {prettyBytes(sizeBytes)} ({prettyBytes(sizeBytesGzip)} gz)
          </Cell>
        ),
        accessor: "sizeBytes",
        sortDescFirst: true,
      },
      {
        Header: "Matched libraries",
        Cell: ({ value: length }) => <Cell>{length}</Cell>,
        accessor: "matchedReleases.length",
        sortDescFirst: true,
      },
      {
        Header: "Size of identified libraries",
        Cell: ({
          value: intervalSize,
          row: {
            original: { numChars },
          },
        }) => {
          if (intervalSize) {
            return (
              <Cell>
                {prettyBytes(intervalSize)} ({round((100 * intervalSize) / numChars, 1)}% of bundle)
              </Cell>
            );
          }
          return null;
        },
        accessor: (bundle) =>
          bundle.matchedReleases && sumBy(bundle.matchedReleases, "cumulativeIntervalSize"),
        id: "identifiedIntervals",
        sortDescFirst: true,
      },
      {
        Header: "Analyzed at",
        Cell: ({
          value: lastScannedDate,
          row: {
            original: {
              meta: { wasCached },
            },
          },
        }) =>
          lastScannedDate && (
            <Cell>
              {wasCached
                ? formatDistanceToNow(new Date(lastScannedDate), { addSuffix: true })
                : "Just now"}
            </Cell>
          ),
        accessor: "lastScannedDate",
        sortDescFirst: true,
      },
    ],
    [bundles]
  );
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable(
    {
      columns,
      data: bundles,
      initialState: {
        sortBy: [{ id: "matchedReleases.length", desc: true }],
      },
    },
    useSortBy
  );

  return (
    <div
      css={css`
        display: flex;
        justify-content: center;
      `}
    >
      <table
        {...getTableProps()}
        css={css`
          border-collapse: collapse;
          box-shadow: 0px 1px 6px #111;
          flex: 1;
          overflow-x: auto;

          @media (max-width: 1400px) {
            display: block;
          }
        `}
      >
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th
                  css={css`
                    border: 1px solid rgb(58, 62, 63);
                    padding: 6px 12px;
                  `}
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                >
                  {column.render("Header")}
                  <span>
                    {column.canSort &&
                      (column.isSorted ? (column.isSortedDesc ? " ▼" : " ▲") : "  ↕")}
                  </span>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => (
                  <td
                    css={css`
                      max-width: 500px;
                      min-width: 120px;
                      height: 50px;
                      text-align: left;
                      border: 1px solid rgb(58, 62, 63);
                      background: ${i % 2 === 0 ? "rgb(23, 27, 28);" : "inherit"};
                      box-sizing: border-box;
                    `}
                    {...cell.getCellProps()}
                  >
                    {cell.render("Cell")}
                  </td>
                ))}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

WebsiteTable.propTypes = propTypes;

function getUrlParts(route) {
  const { pathname: pathName, host } = new URL(`https://${route}`);
  return { fileName: truncate(pathName, { length: 250 }), host };
}

export default WebsiteTable;
