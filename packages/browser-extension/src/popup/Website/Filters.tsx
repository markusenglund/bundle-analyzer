import React, { useState } from "react";
import PropTypes from "prop-types";
import { orderBy, round, groupBy, sumBy } from "lodash-es";
import { Range, getTrackBackground } from "react-range";
import { css } from "@emotion/react";
import { websitePropType } from "../propTypes";

const Filters = ({ website, filter, setFilter, maxFootprint }) => {
  const [footprintRange, setFootprintRange] = useState([
    filter.footprintInterval.min,
    filter.footprintInterval.max,
  ]);
  const [showAllLibraries, setShowAllLibraries] = useState(false);

  const [minScore, setMinScore] = useState(filter.minScore);

  const thirdPartyBundleIds = new Set(website.urlScan.thirdPartyBundleIds);
  const bundles = Object.values(website.urlScan.bundlesById)
    .filter((bundle) => showAllLibraries || bundle.bundleScan.matchedReleases.length > 0)
    .map((bundle) => {
      const url = new URL(bundle.fullUrl);
      const { host, pathname, search } = url;

      const isThirdParty = thirdPartyBundleIds.has(bundle._id);
      return { ...bundle, host, path: `${pathname}${search}`, isThirdParty };
    });

  const numBundlesWithNoLibraries =
    Object.keys(website.urlScan.bundlesById).length - bundles.length;

  const bundlesByHost = groupBy(bundles, "host");

  const orderedHosts = orderBy(
    Object.values(bundlesByHost),
    ["[0].isThirdParty", (hostBundles) => sumBy(hostBundles, "bundleScan.matchedReleases.length")],
    ["asc", "desc"]
  ).map(([bundle]) => bundle.host);

  const handleBundleFilterChange = (event) => {
    const bundleId = event.target.name;
    const newBundles = new Map(filter.bundles);
    newBundles.set(bundleId, event.target.checked);
    setFilter({ ...filter, bundles: newBundles });
  };

  const handleHostFilterChange = (event) => {
    const host = event.target.name;
    const newBundles = new Map(filter.bundles);
    const hostBundles = bundlesByHost[host];
    for (const bundle of hostBundles) {
      newBundles.set(bundle._id, event.target.checked);
    }
    setFilter({ ...filter, bundles: newBundles });
  };

  const handleFilterChange = (event) => {
    const onlyIncludeTopLevelDependencies = event.target.checked;
    setFilter({ ...filter, onlyIncludeTopLevelDependencies });
  };
  return (
    <div>
      <h3>Filters</h3>
      <label
        htmlFor="top-level-deps"
        css={css`
          display: flex;
          font-size: 12px;
          overflow-wrap: break-word;
          word-break: break-all;
          margin-bottom: 8px;
          @media (max-width: 800px) {
            font-size: 15px;
          }
        `}
      >
        <input
          name="top-level-deps"
          type="checkbox"
          checked={filter.onlyIncludeTopLevelDependencies}
          onChange={handleFilterChange}
        />
        Only top-level dependencies
      </label>
      <label
        css={css`
          font-size: 12px;
          @media (max-width: 800px) {
            font-size: 15px;
          }
        `}
      >
        Footprint:{" "}
        <div
          css={css`
            display: inline-block;
            min-width: 44px;
            text-align: right;
          `}
        >
          {round(footprintRange[0] / 1000, 1)} kB
        </div>{" "}
        - {round(footprintRange[1] / 1000, 1)} kB
        <div
          css={css`
            margin: 6px 10px 12px 10px;
          `}
        >
          <Range
            step={1000}
            min={0}
            max={maxFootprint}
            values={footprintRange}
            onChange={(values) => setFootprintRange(values)}
            onFinalChange={(values) =>
              setFilter({ ...filter, footprintInterval: { min: values[0], max: values[1] } })
            }
            renderTrack={({ props, children }) => (
              <div
                {...props}
                style={{
                  ...props.style,
                  height: "6px",
                  width: "140px",
                  borderRadius: "2px",
                  background: getTrackBackground({
                    values: footprintRange,
                    colors: ["#ccc", "#104fa1", "#ccc"],
                    min: 0,
                    max: maxFootprint,
                  }),
                }}
              >
                {children}
              </div>
            )}
            renderThumb={({ props }) => (
              <div
                {...props}
                style={{
                  ...props.style,
                  height: "18px",
                  width: "18px",
                  borderRadius: "50%",
                  backgroundColor: "#ccc",
                  border: "1px solid #104FA1",
                }}
              />
            )}
          />
        </div>
      </label>
      <label
        css={css`
          font-size: 12px;
          @media (max-width: 800px) {
            font-size: 15px;
          }
        `}
      >
        Min match score: {minScore}
        <div
          css={css`
            margin: 6px 10px 12px 10px;
          `}
        >
          <Range
            step={1}
            min={3}
            max={50}
            values={[minScore]}
            onChange={(values) => setMinScore(values[0])}
            onFinalChange={(values) => setFilter({ ...filter, minScore: values[0] })}
            renderTrack={({ props, children }) => (
              <div
                {...props}
                style={{
                  ...props.style,
                  height: "6px",
                  width: "140px",
                  borderRadius: "2px",
                  background: getTrackBackground({
                    values: [minScore],
                    colors: ["#ccc", "#104fa1"],
                    min: 3,
                    max: 50,
                  }),
                }}
              >
                {children}
              </div>
            )}
            renderThumb={({ props }) => (
              <div
                {...props}
                style={{
                  ...props.style,
                  height: "18px",
                  width: "18px",
                  borderRadius: "50%",
                  backgroundColor: "#ccc",
                  border: "1px solid #104FA1",
                }}
              />
            )}
          />
        </div>
      </label>
      <h3>Bundles</h3>
      {orderedHosts.map((host) =>
        bundlesByHost[host].length < 2 ? (
          <div key={host}>
            <label
              htmlFor={bundlesByHost[host][0]._id}
              css={css`
                display: flex;
                font-size: 12px;
                overflow-wrap: break-word;
                word-break: break-all;
              `}
            >
              <input
                name={bundlesByHost[host][0]._id}
                type="checkbox"
                checked={filter.bundles.get(bundlesByHost[host][0]._id)}
                onChange={handleBundleFilterChange}
                disabled={bundlesByHost[host][0].bundleScan.matchedReleases.length === 0}
              />
              {bundlesByHost[host][0]._id} (
              {bundlesByHost[host][0].bundleScan.matchedReleases.length} libraries)
            </label>
          </div>
        ) : (
          <div key={host}>
            <div>
              <label
                htmlFor={host}
                css={css`
                  display: flex;
                  font-size: 12px;
                  font-weight: bold;
                  overflow-wrap: break-word;
                  word-break: break-all;
                `}
              >
                <input
                  name={host}
                  type="checkbox"
                  checked={bundlesByHost[host].some(({ _id }) => filter.bundles.get(_id))}
                  onChange={handleHostFilterChange}
                  disabled={bundlesByHost[host].every(
                    (bundle) => bundle.bundleScan.matchedReleases.length === 0
                  )}
                />
                {host} (
                {
                  new Set(
                    bundlesByHost[host]
                      .flatMap((bundle) => bundle.bundleScan.matchedReleases)
                      .map((release) => release.libraryId)
                  ).size
                }{" "}
                libraries)
              </label>
            </div>
            <div
              css={css`
                margin-left: 15px;
                margin-bottom: 10px;
              `}
            >
              {orderBy(bundlesByHost[host], "bundleScan.matchedReleases.length", "desc").map(
                (bundle) => (
                  <div
                    key={bundle._id}
                    css={css`
                      display: flex;
                    `}
                  >
                    <input
                      name={bundle._id}
                      type="checkbox"
                      checked={filter.bundles.get(bundle._id)}
                      disabled={bundle.bundleScan.matchedReleases.length === 0}
                      onChange={handleBundleFilterChange}
                    />
                    <label
                      htmlFor={bundle._id}
                      css={css`
                        display: flex;
                        flex-direction: column;
                        font-size: 12px;
                        overflow-wrap: break-word;
                        word-break: break-all;
                      `}
                    >
                      <div
                        css={css`
                          -webkit-line-clamp: 3;
                          -webkit-box-orient: vertical;
                          display: -webkit-box;
                          overflow: hidden;
                        `}
                      >
                        {bundle.path}
                      </div>
                      <div
                        css={css`
                          font-weight: bold;
                        `}
                      >
                        ({bundle.bundleScan.matchedReleases.length} libraries)
                      </div>
                    </label>
                  </div>
                )
              )}
            </div>
          </div>
        )
      )}
      {numBundlesWithNoLibraries > 0 && (
        <button
          css={css`
            font-size: 12px;
            border: 0px;
            background: inherit;
            color: inherit;
            cursor: pointer;
            :hover {
              text-decoration: underline;
            }
          `}
          onClick={() => setShowAllLibraries(true)}
        >
          ... {numBundlesWithNoLibraries} more bundle
          {numBundlesWithNoLibraries > 1 && "s"} with no libraries
        </button>
      )}
    </div>
  );
};

Filters.propTypes = {
  website: websitePropType,
  filter: PropTypes.exact({
    bundles: PropTypes.instanceOf(Map).isRequired,
    onlyIncludeTopLevelDependencies: PropTypes.bool.isRequired,
    footprintInterval: PropTypes.exact({
      min: PropTypes.number,
      max: PropTypes.number,
    }).isRequired,
    minIdfScoreRatio: PropTypes.number.isRequired,
    minScore: PropTypes.number.isRequired,
  }).isRequired,
  setFilter: PropTypes.func.isRequired,
  maxFootprint: PropTypes.number.isRequired,
};

export default Filters;
