import React from "react";
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import formatSize from "../../utils/formatSize";

const Overview = ({ totalSize, numMatchedLibraries, numBundles }) => {
  const { unit, roundedSize } = formatSize(totalSize);
  return (
    <div
      css={css`
        display: flex;
        justify-content: center;
        text-align: center;
      `}
    >
      <div
        css={css`
          display: flex;
          justify-content: space-around;
          padding: 20px;
          margin: 20px 0 10px 0;
          width: 600px;
          background: #342323;
          border-radius: 10px;
          box-shadow: 0px 1px 6px #111;
        `}
      >
        <div>
          <div>Libraries</div>
          <div
            css={css`
              font-size: 40px;
              margin: 8px;
              margin-bottom: 0;
              font-weight: bold;
            `}
          >
            {numMatchedLibraries}
          </div>
        </div>
        <div>
          <div>Bundles</div>
          <div
            css={css`
              font-size: 40px;
              margin: 8px;
              margin-bottom: 0;
              font-weight: bold;
            `}
          >
            {numBundles}
          </div>
        </div>
        <div>
          <div>Total size</div>
          <div
            css={css`
              font-size: 40px;
              margin: 8px;
              margin-bottom: 0;
              font-weight: bold;
            `}
          >
            {roundedSize}
            <span
              css={css`
                font-size: 18px;
              `}
            >
              {" "}
              {unit}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

Overview.propTypes = {
  totalSize: PropTypes.number.isRequired,
  numMatchedLibraries: PropTypes.number.isRequired,
  numBundles: PropTypes.number.isRequired,
};

export default Overview;
