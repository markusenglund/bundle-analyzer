import React from "react";
import { css } from "@emotion/react";
import PropTypes from "prop-types";
import WebsiteTable from "./WebsiteTable";
import { websitePropType } from "../propTypes";

const WebsiteTablesWrapper = ({ website }) => {
  const { firstPartyBundleIds, thirdPartyBundleIds, bundlesById } = website;
  const showPartyCategorization = firstPartyBundleIds.length > 0 && thirdPartyBundleIds.length > 0;
  return (
    <div
      css={css`
        padding-bottom: 50px;
        display: flex;
        flex-direction: column;
        max-width: 1900px;
        margin-left: auto;
        margin-right: auto;
      `}
    >
      {showPartyCategorization && (
        <div>
          <h2
            css={css`
              font-size: 24px;
              font-weight: normal;
              margin-top: 30px;
              margin-bottom: 10px;
              @media (max-width: 800px) {
                text-align: center;
              }
            `}
          >
            First party bundles
          </h2>
        </div>
      )}
      {firstPartyBundleIds.length > 0 && (
        <WebsiteTable
          bundleIds={firstPartyBundleIds}
          bundlesById={bundlesById}
          websiteUrl={website.meta.standardizedWebsiteUrl}
        />
      )}
      {showPartyCategorization && (
        <h2
          css={css`
            font-size: 24px;
            font-weight: normal;
            margin-top: 30px;
            margin-bottom: 10px;
            @media (max-width: 800px) {
              text-align: center;
            }
          `}
        >
          Third party bundles
        </h2>
      )}
      {thirdPartyBundleIds.length > 0 && (
        <WebsiteTable
          isThirdParty
          bundleIds={thirdPartyBundleIds}
          bundlesById={bundlesById}
          websiteUrl={website.meta.standardizedWebsiteUrl}
        />
      )}
    </div>
  );
};

WebsiteTablesWrapper.propTypes = {
  website: websitePropType,
};

export default WebsiteTablesWrapper;
