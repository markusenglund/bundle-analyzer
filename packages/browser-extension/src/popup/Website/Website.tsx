import React, { useMemo } from "react";
import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { formatDistanceToNow } from "date-fns";
import WebsiteResults from "./WebsiteResults";
import logoSvg from "../../images/bundle-scanner-logo-3.svg";
import useWebsite from "../hooks/useWebsite";

const ErrorMessage = styled("div")`
  padding-top: 40px;
  white-space: pre-line;
  font-size: 28px;
  text-align: center;
`;

const Website = () => {
  const { status, data, error } = useWebsite();

  // Memoize to prevent recalculation every time this component rerenders
  const scannedDateDistance = useMemo(
    () =>
      status === "success"
        ? formatDistanceToNow(new Date(data.website.urlScan.date), { addSuffix: true })
        : null,
    [data?.website?.lastScannedDate]
  );

  return (
    <div
      css={css`
        padding-top: 20px;
        padding-bottom: 20px;
      `}
    >
      {status === "loading" && (
        <div
          css={css`
            display: flex;
            flex-direction: column;
            align-items: center;
          `}
        >
          <div
            css={css`
              margin-bottom: 80px;
              display: flex;
              align-items: center;
            `}
          >
            <img
              css={css`
                width: 26px;
                margin: 8px;
              `}
              src={logoSvg}
              alt="Bundle Scanner logo"
            />
            <div
              css={css`
                font-size: 24px;
                margin: 0;
              `}
            >
              Bundle Scanner
              <span
                css={css`
                  font-size: 12px;
                  margin-left: 10px;
                `}
              >
                BETA
              </span>
            </div>
          </div>
          <div
            className="lds-dual-ring"
            css={css`
              margin-right: 12px;
              margin-bottom: 22px;
            `}
          />
          <div>Scanning bundles...</div>
        </div>
      )}
      {status === "error" && <ErrorMessage>{error.message}</ErrorMessage>}
      {status === "success" && <WebsiteResults website={data.website} />}
    </div>
  );
};

export default Website;
