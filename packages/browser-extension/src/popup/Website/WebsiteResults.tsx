import React, { useEffect, useState, useMemo } from "react";
import { css } from "@emotion/react";
import { sumBy, groupBy, orderBy, truncate, ceil } from "lodash-es";
import { websitePropType } from "../propTypes";
import Overview from "./Overview";
import LibraryList from "./LibraryList";
import Sidebar from "./Sidebar";

const createDefaultFilter = (website, maxFootprint, numMatchedLibraries) => {
  if (numMatchedLibraries > 0) {
    let bundles;
    const firstPartyBundleIdsWithLibraries = website.urlScan.firstPartyBundleIds.filter(
      (bundleId) => website.urlScan.bundlesById[bundleId].bundleScan.matchedReleases.length > 0
    );
    const thirdPartyBundleIdsWithLibraries = website.urlScan.thirdPartyBundleIds.filter(
      (bundleId) => website.urlScan.bundlesById[bundleId].bundleScan.matchedReleases.length > 0
    );
    if (firstPartyBundleIdsWithLibraries.length > 0) {
      bundles = new Map([
        ...firstPartyBundleIdsWithLibraries.map((id) => [id, true]),
        ...thirdPartyBundleIdsWithLibraries.map((id) => [id, false]),
      ]);
    } else {
      bundles = new Map(thirdPartyBundleIdsWithLibraries.map((id) => [id, true]));
    }

    return {
      bundles,
      onlyIncludeTopLevelDependencies: false,
      footprintInterval: { min: 0, max: maxFootprint },
      minIdfScoreRatio: 0,
      minScore: 3,
    };
  }
};

const WebsiteResults = ({ website }) => {
  const [faviconFailed, setFaviconFailed] = useState(false);
  useEffect(() => {
    setFaviconFailed(false);
  }, [website?._id]);

  const [prevWebsiteId, setPrevWebsiteId] = useState(null);
  const [filter, setFilter] = useState(null);

  const maxFootprint = ceil(
    Math.max(
      0,
      ...Object.values(website.urlScan.bundlesById).flatMap(({ bundleScan: { matchedReleases } }) =>
        matchedReleases.map((release) => release.cumulativeIntervalSize)
      )
    ),
    -3
  );
  const bundles = Object.values(website.urlScan.bundlesById);
  const totalSize = sumBy(bundles, "bundleScan.sizeBytes");
  const totalSizeGzip = sumBy(bundles, "bundleScan.sizeBytesGzip");
  const uniqueLibraries = new Set(
    bundles
      .flatMap((bundle) => bundle.bundleScan.matchedReleases)
      .map((release) => release.libraryId)
  );

  const numMatchedLibraries = uniqueLibraries.size;

  const [sorting, setSorting] = useState("cumulative-interval-size");

  const libraries = useMemo(
    () => calculateLibraries(website, filter, sorting),
    [website, filter, sorting]
  );

  const hasReceivedNewWebsiteData = website._id !== prevWebsiteId;

  if (hasReceivedNewWebsiteData) {
    setFilter(createDefaultFilter(website, maxFootprint, numMatchedLibraries));
    setPrevWebsiteId(website._id);
    return null;
  }

  const numBundles =
    filter &&
    [...filter.bundles].filter(
      ([id, isSelected]) =>
        isSelected && website.urlScan.bundlesById[id].bundleScan.matchedReleases.length > 0
    ).length;
  return (
    <div>
      <h2
        css={css`
          margin-bottom: 30px;
          display: flex;
          justify-content: center;
          align-items: center;
        `}
      >
        {website.faviconUrl && !faviconFailed && (
          <img
            css={css`
              max-width: 32px;
              margin-right: 10px;
            `}
            onError={() => setFaviconFailed(true)}
            src={website.faviconUrl}
            alt={`${website._id} favicon`}
          />
        )}{" "}
        <div
          css={css`
            overflow: hidden;
            text-overflow: ellipsis;
          `}
        >
          {truncate(website._id, { length: 56 })}
        </div>
      </h2>
      {/* <Overview
        totalSize={totalSize}
        totalSizeGzip={totalSizeGzip}
        numMatchedLibraries={numMatchedLibraries}
        numBundles={bundles.length}
      /> */}
      {numMatchedLibraries === 0 && (
        <p
          css={css`
            text-align: center;
            font-style: italic;
          `}
        >
          No libraries were found
        </p>
      )}

      {numMatchedLibraries > 0 && (
        <div
          css={css`
            display: flex;
            justify-content: center;
          `}
        >
          <LibraryList libraries={libraries} numBundles={numBundles} />
          {filter ? (
            <Sidebar
              website={website}
              maxFootprint={maxFootprint}
              filter={filter}
              setFilter={setFilter}
              sorting={sorting}
              setSorting={setSorting}
            />
          ) : null}
        </div>
      )}
    </div>
  );
};
WebsiteResults.propTypes = {
  website: websitePropType,
};

function addDependentsChains(releases) {
  const librarySet = new Set(releases.map(({ libraryId }) => libraryId));
  const dependencyDependentsMap = new Map();

  for (const release of releases) {
    const { dependencies = [], libraryId } = release;
    for (const dependency of dependencies) {
      if (librarySet.has(dependency.libraryId)) {
        const dependents = dependencyDependentsMap.get(dependency.libraryId);
        if (dependents) {
          dependents.push(libraryId);
        } else {
          dependencyDependentsMap.set(dependency.libraryId, [libraryId]);
        }
      }
    }
  }

  const releasesWithDependentsChain = releases.map((release) => {
    // Recursively get one chain of dependents
    const dependentsChain = [];
    let curLibraryId = release.libraryId;
    while (curLibraryId != null) {
      curLibraryId = dependencyDependentsMap.get(curLibraryId)?.[0];
      if (curLibraryId) {
        dependentsChain.push(curLibraryId);
      }
    }

    return { ...release, dependentsChain };
  });

  return releasesWithDependentsChain;
}

function calculateLibraries(website, filter, sorting) {
  if (!website || !filter) {
    return [];
  }
  const {
    bundles,
    onlyIncludeTopLevelDependencies,
    footprintInterval,
    minIdfScoreRatio,
    minScore,
  } = filter;

  const pickedBundles = [
    ...website.urlScan.firstPartyBundleIds,
    ...website.urlScan.thirdPartyBundleIds,
  ]
    .filter((id) => bundles.get(id))
    .map((id) => website.urlScan.bundlesById[id]);

  const pickedReleases = [];

  //  Get libraries but exclude ones that don't pass filter
  for (const bundle of pickedBundles) {
    const matchedReleasesWithDependents = addDependentsChains(bundle.bundleScan.matchedReleases);
    for (const release of matchedReleasesWithDependents) {
      if (onlyIncludeTopLevelDependencies && release.dependentsChain.length > 0) {
        continue;
      }
      if (
        release.cumulativeIntervalSize < footprintInterval.min ||
        release.cumulativeIntervalSize > footprintInterval.max
      ) {
        continue;
      }
      if (release.idfScoreRatio < minIdfScoreRatio) {
        continue;
      }
      if (release.score < minScore) {
        continue;
      }
      pickedReleases.push({
        ...release,
        bundleId: bundle._id,
        analysisMode: bundle.bundleScan.analysisMode,
      });
    }
  }

  // Group releases by library and get the right data structure
  const releasesByLibraryId = groupBy(pickedReleases, "libraryId");
  const pickedLibraries = Object.entries(releasesByLibraryId).map(([libraryId, releases]) => ({
    _id: libraryId,
    description: releases[0].description,
    repositoryUrl: releases[0].repositoryUrl,
    license: releases[0].license,
    bundles: releases.map((release) => ({
      _id: release.bundleId,
      version: release.version,
      dependentsChain: release.dependentsChain,
      analysisMode: release.analysisMode,
      cumulativeIntervalSize: release.cumulativeIntervalSize,
      score: release.score,
      idfScoreRatio: release.idfScoreRatio,
    })),
  }));

  /* Properties to enable sorting by:
  alphabetic libraryId,
  footprint (of one or cumulative?),
  match score,
  match%
  
  Keep in mind that match score and match% doesn't exist in source maps
  
  How do we do sorting of properties that are different for different bundles?
  a) Just take the 0th bundle - let's do this for now
  b) Sort bundles by the same property and take the first one (the highest)
  */

  let sortingKey;
  let order;
  switch (sorting) {
    case "alphabetical": {
      sortingKey = "_id";
      order = "asc";
      break;
    }
    case "cumulative-interval-size": {
      sortingKey = "bundles[0].cumulativeIntervalSize";
      order = "desc";
      break;
    }
    case "score": {
      sortingKey = "bundles[0].score";
      order = "desc";
      break;
    }
    case "idf-score-ratio": {
      sortingKey = "bundles[0].idfScoreRatio";
      order = "desc";
      break;
    }
    default: {
      throw new Error(`Unknown sorting string: '${sorting}'`);
    }
  }

  const sortedLibraries = orderBy(pickedLibraries, sortingKey, order);

  return sortedLibraries;
}

export default React.memo(WebsiteResults);
