import React from "react";
import { render } from "react-dom";
import App from "./App";

console.log("Popup is starting");

const rootElement = window.document.querySelector("#root");
render(<App />, rootElement);
