import React, { useReducer } from "react";

const NavigationContext = React.createContext();

function navigationReducer(state, action) {
  switch (action.type) {
    case "VIEW_WEBSITE": {
      return { ...state, page: "website", interval: null };
    }
    case "VIEW_BUNDLE": {
      return { page: "bundle", bundleUrl: action.payload.bundleUrl };
    }
    case "VIEW_INTERVAL_INSPECTION": {
      return {
        ...state,
        page: "interval-inspection",
        interval: {
          bundleUrl: action.payload.bundleUrl,
          libraryId: action.payload.libraryId,
        },
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

function NavigationProvider({ children }) {
  const [state, dispatch] = useReducer(navigationReducer, {
    page: "website",
    tabId: null,
    interval: null,
  });

  // TODO: Memoize for perf
  const value = { state, dispatch };

  return <NavigationContext.Provider value={value}>{children}</NavigationContext.Provider>;
}

export { NavigationContext, NavigationProvider };
