import React, { useContext, useLayoutEffect } from "react";
import Website from "./Website/Website";
import IntervalInspection from "./IntervalInspection/IntervalInspection";
import { NavigationContext } from "./NavigationContext";

const Main = () => {
  const {
    state: { page },
  } = useContext(NavigationContext);

  // Scroll to top on route change
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
  }, [page]);

  switch (page) {
    case "website": {
      return <Website />;
    }
    case "interval-inspection": {
      return <IntervalInspection />;
    }
    default: {
      throw new Error(`Unknown page type: '${page}''`);
    }
  }
};

export default Main;
