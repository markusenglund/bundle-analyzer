import React from "react";
import PropTypes from "prop-types";

import { css } from "@emotion/react";

const ErrorFallback = ({ error, resetError }) => (
  <div
    css={css`
      text-align: center;
      margin-top: 100px;
    `}
  >
    <h2>Something went wrong!</h2>
    <p>Error: &apos;{error.message}&apos;</p>
    <button onClick={resetError}>Try again</button>
  </div>
);
ErrorFallback.propTypes = {
  error: PropTypes.object.isRequired,
  resetError: PropTypes.func.isRequired,
};

export default ErrorFallback;
