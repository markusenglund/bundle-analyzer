import React, { useMemo, useRef } from "react";
import PropTypes from "prop-types";
import { useInView } from "react-intersection-observer";
import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { inRange, truncate } from "lodash-es";
import { intervalPropType } from "../propTypes";
import useLibraryFileCode from "../hooks/useLibraryFileCode";
import useTokenPositions from "../hooks/useTokenPositions";

const Code = styled("code")`
  /* Both word-break and overflow-wrap are necessary */
  word-break: break-all;
  overflow-wrap: break-word;
  display: block;
  white-space: pre-wrap;
  overflow: auto;
  line-height: 22px;
  border: 1px solid rgb(58, 62, 63);
  box-shadow: 0px 1px 6px #111;
  padding: 20px;
  height: 70vh;
  border-radius: 6px;
  @media (max-width: 800px) {
    padding: 10px 2px;
    font-size: 11px;
    line-height: 18px;
  }
`;

const BorderCode = styled("span")`
  color: grey;
`;

const ComparisonContainer = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: min-content min-content;
  position: relative;
  grid-auto-flow: column;
  @media (min-width: 800px) {
    column-gap: 20px;
  }
`;

const propTypes = {
  bundleCode: PropTypes.string,
  interval: intervalPropType.isRequired,
  releaseId: PropTypes.string.isRequired,
  bundleUrl: PropTypes.string.isRequired,
  bundleTokenPositions: PropTypes.arrayOf(
    PropTypes.shape({
      start: PropTypes.number.isRequired,
      end: PropTypes.number.isRequired,
      token: PropTypes.string.isRequired,
    })
  ),
  bundleTokenPositionsStatus: PropTypes.string.isRequired,
  bundleTokenPositionsError: PropTypes.object,
};

const Interval = ({
  bundleCode,
  interval,
  releaseId,
  bundleUrl,
  bundleTokenPositions,
  bundleTokenPositionsStatus,
  bundleTokenPositionsError,
}) => {
  const { ref, inView } = useInView();

  const {
    status: libraryFileCodeStatus,
    data: libraryFileCode,
    error: libraryFileCodeError,
  } = useLibraryFileCode(releaseId, interval.fileName, inView);

  const {
    status: libraryFileTokenPositionsStatus,
    data: libraryFileTokenPositions,
    error: libraryFileTokenPositionsError,
  } = useTokenPositions(libraryFileCode, `${releaseId}${interval.fileName}`);

  const libraryContainerElement = useRef(null);
  const selectedBundleToken = useRef(null);

  const matchedTokenData = useMemo(() => {
    if (!bundleTokenPositions || !libraryFileTokenPositions) {
      return null;
    }

    const bundleTokenSet = new Set(bundleTokenPositions.map(({ token }) => token));

    const matchedLibraryFileTokenPositions = libraryFileTokenPositions.filter(({ token }) =>
      bundleTokenSet.has(token)
    );
    const libraryFileSnippetsBetweenMatchedTokens = [];
    let libraryFileCharIndex = 0;
    for (const position of matchedLibraryFileTokenPositions) {
      let { start, end } = position;
      if (position.token.length < end - start) {
        start = start + 1;
        end = end - 1;
      }
      const snippet = libraryFileCode.slice(libraryFileCharIndex, start);
      libraryFileSnippetsBetweenMatchedTokens.push(snippet);
      libraryFileCharIndex = end;
    }
    const lastSnippet = libraryFileCode.slice(libraryFileCharIndex);
    libraryFileSnippetsBetweenMatchedTokens.push(lastSnippet);

    const libraryFileTokenPositionsMap = new Map();
    for (let i = 0; i < matchedLibraryFileTokenPositions.length; i += 1) {
      const { token } = matchedLibraryFileTokenPositions[i];
      if (libraryFileTokenPositionsMap.has(token)) {
        libraryFileTokenPositionsMap.get(token).push(i);
      } else {
        libraryFileTokenPositionsMap.set(token, [i]);
      }
    }

    const matchedBundleTokenPositions = bundleTokenPositions.filter(({ token }) =>
      libraryFileTokenPositionsMap.has(token)
    );

    const bundleSnippetsBetweenMatchedTokens = [];
    let bundleCharIndex = interval.start;
    for (const position of matchedBundleTokenPositions) {
      let { start, end } = position;

      if (position.token.length < end - start) {
        start = start + 1;
        end = end - 1;
      }
      const snippet = bundleCode.slice(bundleCharIndex, start);
      bundleSnippetsBetweenMatchedTokens.push(snippet);
      bundleCharIndex = end;
    }

    const bundleTokenPositionsMap = new Map();
    for (let i = 0; i < matchedBundleTokenPositions.length; i += 1) {
      const { token } = matchedBundleTokenPositions[i];
      if (bundleTokenPositionsMap.has(token)) {
        bundleTokenPositionsMap.get(token).push(i);
      } else {
        bundleTokenPositionsMap.set(token, [i]);
      }
    }

    return {
      matchedLibraryFileTokenPositions,
      libraryFileSnippetsBetweenMatchedTokens,
      libraryFileTokenPositionsMap,
      matchedBundleTokenPositions,
      bundleSnippetsBetweenMatchedTokens,
      bundleTokenPositionsMap,
    };
  }, [bundleTokenPositions, libraryFileTokenPositions]);

  const codeInterval = bundleCode?.slice(interval.start, interval.end);

  const { start, end, fileName, intervalSizeFourGrams, maxIntervalSizeFourGrams } = interval;
  const borderCodeSize = Math.min(200, 0.8 * (end - start));

  const intervalPercentageOfFile = intervalSizeFourGrams / maxIntervalSizeFourGrams;

  const markSelectedToken = (newSelectedBundleToken) => {
    const previouslySelectedBundleToken = selectedBundleToken.current;
    if (previouslySelectedBundleToken) {
      const previouslySelectedLibraryTokenPositions =
        matchedTokenData.libraryFileTokenPositionsMap.get(previouslySelectedBundleToken);

      for (const position of previouslySelectedLibraryTokenPositions) {
        const element = document.getElementById(`lib-${fileName}-${position}`);
        if (element) {
          element.style.background = "rgb(58, 64, 66)";
          element.style.outline = "solid #666 1px";
        } else {
          console.log(`Something went wrong, didn't find element lib-${fileName}-${position}`);
        }
      }

      const previouslySelectedBundleTokenPositions = matchedTokenData.bundleTokenPositionsMap.get(
        previouslySelectedBundleToken
      );

      for (const position of previouslySelectedBundleTokenPositions) {
        const element = document.getElementById(`bundle-${fileName}-${position}`);
        if (element) {
          element.style.background = "rgb(58, 64, 66)";
          element.style.outline = "solid #666 1px";
        } else {
          console.log(`Something went wrong, didn't find element bundle-${fileName}-${position}`);
        }
      }
    }

    const libraryTokenPositions =
      matchedTokenData.libraryFileTokenPositionsMap.get(newSelectedBundleToken);

    const containerHeight = libraryContainerElement.current.clientHeight;
    const containerScrollPosTop = libraryContainerElement.current.scrollTop;
    const containerScrollPosBottom = containerScrollPosTop + containerHeight;

    let someElementIsInViewport = false;
    let closestElementToViewport;
    for (const position of libraryTokenPositions) {
      // const cssIndex = position + 1;
      // const element = document.querySelector(`span[data-token]:nth-of-type(${cssIndex})`);
      const element = document.getElementById(`lib-${fileName}-${position}`);

      if (element) {
        element.style.background = "#161";
        element.style.outline = "solid #585 1px";

        if (!someElementIsInViewport) {
          const elementPosInContainer =
            element.offsetTop - libraryContainerElement.current.offsetTop;

          if (inRange(elementPosInContainer, containerScrollPosTop, containerScrollPosBottom)) {
            someElementIsInViewport = true;
          } else {
            const distanceToViewport =
              elementPosInContainer < containerScrollPosTop
                ? containerScrollPosTop - elementPosInContainer
                : elementPosInContainer - containerScrollPosBottom;
            if (
              !closestElementToViewport ||
              closestElementToViewport.distance > distanceToViewport
            ) {
              closestElementToViewport = { element, distance: distanceToViewport };
            }
          }
        }
      }
    }

    if (!someElementIsInViewport) {
      closestElementToViewport.element.scrollIntoView({ behavior: "smooth", block: "nearest" });
    }

    const selectedBundleTokenPositions =
      matchedTokenData.bundleTokenPositionsMap.get(newSelectedBundleToken);
    for (const position of selectedBundleTokenPositions) {
      const element = document.getElementById(`bundle-${fileName}-${position}`);
      if (element) {
        element.style.background = "#161";
        element.style.outline = "solid #585 1px";
      }
    }
    selectedBundleToken.current = newSelectedBundleToken;
  };

  const isLoading =
    libraryFileCodeStatus === "loading" || libraryFileTokenPositionsStatus === "loading";

  return (
    <div ref={ref}>
      <h3
        css={css`
          text-align: center;
          font-size: 20px;
        `}
      >
        {fileName}
      </h3>
      {intervalPercentageOfFile < 0.5 && (
        <div
          css={css`
            font-size: 18px;
            margin-bottom: 20px;
          `}
        >
          The identified bundle interval corresponds to only{" "}
          {Math.round(intervalPercentageOfFile * 100)}% of {fileName} from {releaseId}
        </div>
      )}
      <ComparisonContainer>
        <p
          css={css`
            font-size: 15px;
            margin: 0;
            align-self: end;
          `}
        >
          <span
            css={css`
              font-weight: bold;
              word-break: break-all;
            `}
          >
            {truncate(bundleUrl, { length: 60 })}
          </span>
        </p>
        {bundleTokenPositionsStatus === "error" && <div>{bundleTokenPositionsError.message}</div>}
        {bundleCode ? (
          <Code>
            <BorderCode>{bundleCode.slice(Math.max(start - borderCodeSize, 0), start)}</BorderCode>
            <span>
              {matchedTokenData
                ? matchedTokenData.matchedBundleTokenPositions.map(({ token }, i) => (
                    <>
                      <span>{matchedTokenData.bundleSnippetsBetweenMatchedTokens[i]}</span>
                      <span
                        role="button"
                        id={`bundle-${fileName}-${i}`}
                        tabIndex={0}
                        onClick={() => {
                          markSelectedToken(token);
                        }}
                        onKeyPress={(event) => {
                          if (event.key === "Enter") {
                            markSelectedToken(token);
                          }
                        }}
                        css={css`
                          background: rgb(58, 64, 66);
                          outline: solid #666 1px;
                          border-radius: 2px;
                          cursor: pointer;
                          :hover:not(:focus) {
                            background: rgb(78, 84, 86) !important;
                          }
                          :focus {
                            outline: 5px auto Highlight;
                            outline: 5px auto -webkit-focus-ring-color;
                          }
                        `}
                      >
                        {token}
                      </span>
                    </>
                  ))
                : codeInterval}
            </span>
            <BorderCode>{bundleCode.slice(end, end + borderCodeSize)}</BorderCode>
          </Code>
        ) : null}
        <p
          css={css`
            font-size: 15px;
            margin: 0;
            align-self: end;
          `}
        >
          <span
            css={css`
              font-weight: bold;
              word-break: break-all;
            `}
          >
            {fileName}
          </span>{" "}
          from{" "}
          <span
            css={css`
              font-weight: bold;
              word-break: break-all;
            `}
          >
            {releaseId}
          </span>
        </p>
        {libraryFileCodeStatus === "error" && <div>{libraryFileCodeError.message}</div>}
        {libraryFileTokenPositionsStatus === "error" && (
          <div>{libraryFileTokenPositionsError.message}</div>
        )}
        {libraryFileCode && (
          <Code ref={libraryContainerElement}>
            {matchedTokenData
              ? matchedTokenData.matchedLibraryFileTokenPositions.map(({ token }, i) => (
                  <>
                    <span>{matchedTokenData.libraryFileSnippetsBetweenMatchedTokens[i]}</span>
                    <span
                      id={`lib-${fileName}-${i}`}
                      css={css`
                        outline: solid #666 1px;
                        background: rgb(58, 64, 66);
                        border-radius: 2px;
                      `}
                    >
                      {token}
                    </span>
                    {i === matchedTokenData.matchedLibraryFileTokenPositions.length - 1 && (
                      <span>{matchedTokenData.libraryFileSnippetsBetweenMatchedTokens[i + 1]}</span>
                    )}
                  </>
                ))
              : libraryFileCode}
          </Code>
        )}
        {isLoading ? (
          <div
            css={css`
              position: absolute;
              left: 0px;
              top: 0px;
              bottom: 0px;
              right: 0px;
              background: rgba(0, 0, 0, 0.5);
              display: flex;
              justify-content: center;
              padding-top: 100px;
            `}
          >
            <div className="lds-dual-ring" />
          </div>
        ) : null}
      </ComparisonContainer>
    </div>
  );
};

Interval.propTypes = propTypes;

export default Interval;
