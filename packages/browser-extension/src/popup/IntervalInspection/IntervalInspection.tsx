import React, { useMemo, useContext } from "react";
import { useQueryClient } from "react-query";
import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { BsArrowLeft } from "react-icons/bs";
import { truncate } from "lodash-es";
import Interval from "./Interval";
import LibraryIntervalOverview from "./LibraryIntervalOverview";
import { NavigationContext } from "../NavigationContext";
import useBundle from "../hooks/useBundle";
import useBundleCode from "../hooks/useBundleCode";
import useTokenPositions from "../hooks/useTokenPositions";

const BackButton = styled("button")`
  white-space: nowrap;
  max-width: 400px;
  display: flex;
  align-items: center;
  margin: 20px;
  padding: 10px;
  background: rgb(58, 64, 66);
  color: #eee;
  border: 1px solid #666;
  border-radius: 6px;
  font-size: 20px;
  cursor: pointer;
  font-family: inherit;
  @media (max-width: 800px) {
    max-width: calc(100% - 40px);
    margin: 20px auto;
  }
`;

const IntervalInspection = () => {
  const {
    dispatch,
    state: {
      interval: { bundleUrl, libraryId },
    },
  } = useContext(NavigationContext);

  const queryClient = useQueryClient();
  const websiteUrl = queryClient.getQueryData(["website"]).website._id;

  const bundle = useBundle(bundleUrl);

  const bundleCode = useBundleCode(bundle?.fullUrl);

  const {
    status: tokenPositionsStatus,
    data: tokenPositions,
    error: tokenPositionsError,
  } = useTokenPositions(bundleCode, bundleUrl);

  const release = useMemo(
    () => bundle.bundleScan.matchedReleases.find((release) => release.libraryId === libraryId),
    [bundle]
  );

  const tokensByFileName = useMemo(() => {
    if (!tokenPositions) return {};
    return Object.fromEntries(
      release.intervals.map(({ start, end, fileName }) => {
        const tokensInRange = tokenPositions.filter(
          (tokenData) => tokenData.start >= start && tokenData.end <= end
        );

        return [fileName, tokensInRange];
      })
    );
  }, [tokenPositions]);

  return (
    <>
      <BackButton
        onClick={() =>
          dispatch({
            type: "VIEW_WEBSITE",
          })
        }
      >
        <BsArrowLeft />{" "}
        <div
          css={css`
            margin-left: 5px;
            overflow: hidden;
            text-overflow: ellipsis;
          `}
        >
          {websiteUrl}
        </div>
      </BackButton>

      <div
        css={css`
          padding: 30px;
          padding-top: 0px;
          padding-bottom: 100px;
          @media (max-width: 800px) {
            padding-left: 4px;
            padding-right: 4px;
          }
        `}
      >
        <div
          css={css`
            text-align: center;
            margin-left: auto;
            margin-right: auto;
            max-width: 700px;
          `}
        >
          <h2
            css={css`
              font-size: 22px;
              line-height: 25px;
            `}
          >
            Comparison of segments from{" "}
            <span
              css={css`
                font-weight: bold;
              `}
            >
              {truncate(bundle._id, { length: 70 })}
            </span>{" "}
            that match code from{" "}
            <span
              css={css`
                font-weight: bold;
              `}
            >
              {release.libraryId} {release.version}
            </span>
            .
          </h2>
          <p
            css={css`
              font-size: 16px;
            `}
          >
            The parts of the code that are{" "}
            <span
              css={css`
                background: rgb(58, 64, 66);
                outline: solid #666 1px;
                border-radius: 2px;
              `}
            >
              highlighted
            </span>{" "}
            in grey are tokens that can be found in both places. Click on a token in the bundle code
            (left side) to highlight instances of the same token in the library code (right side).
          </p>
        </div>
        <LibraryIntervalOverview release={release} />
        {release.intervals.map((interval) => (
          <Interval
            bundleCode={bundleCode}
            bundleTokenPositions={tokensByFileName[interval.fileName]}
            bundleTokenPositionsStatus={tokenPositionsStatus}
            bundleTokenPositionsError={tokenPositionsError}
            interval={interval}
            releaseId={release._id}
            bundleUrl={bundle._id}
          />
        ))}
      </div>
    </>
  );
};

export default IntervalInspection;
