import React from "react";
import PropTypes from "prop-types";
import prettyBytes from "pretty-bytes";
import { css } from "@emotion/react";
import { sumBy } from "lodash-es";
import { bundlePropType } from "../../propTypes";

const BundleOverview = ({ bundles }) => {
  const releases = bundles.flatMap((bundle) => bundle.matchedReleases);
  const numMatchedReleases = releases.length;
  const totalBundleSize = sumBy(bundles, "sizeBytes");
  const totalBundleSizeGzip = sumBy(bundles, "sizeBytesGzip");

  return (
    <div
      css={css`
        display: flex;
        justify-content: center;
        text-align: center;
      `}
    >
      <div
        css={css`
          display: flex;
          justify-content: space-between;
          margin: 20px 0;
          width: 600px;
        `}
      >
        <div>
          <div>Bundles:</div>
          <div
            css={css`
              font-size: 40px;
              margin: 20px;
            `}
          >
            {bundles.length}
          </div>
        </div>
        <div>
          <div>Total size of bundles:</div>
          <div
            css={css`
              font-size: 40px;
              margin: 20px;
              margin-bottom: 10px;
            `}
          >
            {prettyBytes(totalBundleSize)}
          </div>
          <div>{prettyBytes(totalBundleSizeGzip)} gzipped</div>
        </div>
        <div>
          <div>Libraries matched:</div>
          <div
            css={css`
              font-size: 40px;
              margin: 20px;
            `}
          >
            {numMatchedReleases}
          </div>
        </div>
      </div>
    </div>
  );
};

BundleOverview.propTypes = { bundles: PropTypes.arrayOf(bundlePropType) };

export default BundleOverview;
