import React, { useMemo } from "react";
import PropTypes from "prop-types";
import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { TiArrowBack } from "react-icons/ti";
import { truncate } from "lodash-es";
import CombinedBundleTable from "./CombinedBundleTable";
import CombinedBundleOverview from "./CombinedBundleOverview";
import useWebsite from "../../hooks/useWebsite";
import ShareWidget from "../../components/ShareWidget";

const BackButton = styled("button")`
  overflow-wrap: break-word;
  max-width: 400px;
  display: block;
  margin: 20px;
  padding: 10px;
  background: #161;
  color: #eee;
  border: 0px;
  font-size: 20px;
  cursor: pointer;
  font-family: inherit;
  @media (max-width: 800px) {
    max-width: calc(100% - 40px);
  }
`;

const Error = styled("div")`
  margin-top: 40px;
  white-space: pre-line;
  font-size: 28px;
  text-align: center;
`;

const propTypes = {
  websiteUrl: PropTypes.string.isRequired,
  grouping: PropTypes.oneOf(["all", "first-party", "third-party"]).isRequired,
};

const BundleCombination = ({ websiteUrl: encodedWebsiteUrl, grouping }) => {
  const { status, data: website, error } = useWebsite(encodedWebsiteUrl);
  const [, setLocation] = useLocation();

  const decodedWebsiteUrl = decodeURIComponent(encodedWebsiteUrl);

  const bundles = useMemo(
    () => (status === "success" ? getBundles(website, grouping) : null),
    [website, grouping]
  );

  if (status === "loading") {
    return (
      <div
        className="lds-dual-ring"
        css={css`
          margin-right: 12px;
          margin-bottom: 22px;
        `}
      />
    );
  }

  if (status === "error") {
    return <Error>Something went wrong</Error>;
  }

  let heading;
  switch (grouping) {
    case "first-party":
      heading = "NPM libraries from all first party bundles found on ";
      break;
    case "third-party":
      heading = "NPM libraries from all third party bundles found on ";
      break;
    case "all":
      heading = "All NPM libraries found on ";
      break;
    default:
      throw new Error("Invalid grouping");
  }

  return (
    <div
      css={css`
        padding-bottom: 50px;
      `}
    >
      <Title>{decodedWebsiteUrl} libraries | Bundle Scanner</Title>
      <BackButton
        onClick={() => setLocation(encodedWebsiteUrl ? `/website/${encodedWebsiteUrl}` : "/")}
      >
        {truncate(decodedWebsiteUrl, { length: 54 })}
        <TiArrowBack />
      </BackButton>
      <h2
        css={css`
          margin-bottom: 40px;
          line-height: 44px;
          font-size: 22px;
          display: block;
          text-align: center;
        `}
      >
        {heading}
        <div
          css={css`
            font-size: 30px;
            overflow-wrap: break-word;
            word-break: break-all;
          `}
        >
          {truncate(decodedWebsiteUrl, { length: 65 })}{" "}
        </div>
      </h2>
      {status === "loading" && (
        <div
          css={css`
            display: flex;
            justify-content: center;
          `}
        >
          <div className="lds-dual-ring" />
        </div>
      )}
      {status === "error" && <Error>{error.message}</Error>}

      {status === "success" ? (
        <>
          <CombinedBundleOverview bundles={bundles} />
          {bundles.some((bundle) => bundle.matchedReleases.length > 0) ? (
            <CombinedBundleTable websiteUrl={encodedWebsiteUrl} bundles={bundles} />
          ) : (
            <p
              css={css`
                text-align: center;
              `}
            >
              No libraries found on {decodedWebsiteUrl}
            </p>
          )}
        </>
      ) : null}
      <ShareWidget pageType="website" websiteUrl={decodedWebsiteUrl} />
    </div>
  );
};

function getBundles(website, grouping) {
  if (grouping === "first-party") {
    return website.firstPartyBundleIds.map((bundleId) => website.bundlesById[bundleId]);
  }
  if (grouping === "third-party") {
    return website.thirdPartyBundleIds.map((bundleId) => website.bundlesById[bundleId]);
  }
  if (grouping === "all") {
    return Object.values(website.bundlesById);
  }
}

BundleCombination.propTypes = propTypes;

export default BundleCombination;
