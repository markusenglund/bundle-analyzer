import React, { useContext } from "react";
import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { TiArrowBack } from "react-icons/ti";
import { BiLinkExternal } from "react-icons/bi";
import { useQueryClient } from "react-query";
import { truncate } from "lodash-es";
import BundleTable from "./BundleTable";
import BundleOverview from "./BundleOverview";
import { NavigationContext } from "../NavigationContext";

const BackButton = styled("button")`
  overflow-wrap: break-word;
  max-width: 400px;
  display: block;
  margin: 20px;
  padding: 10px;
  background: #161;
  color: #eee;
  border: 0px;
  font-size: 20px;
  cursor: pointer;
  font-family: inherit;
  @media (max-width: 800px) {
    max-width: calc(100% - 40px);
  }
`;

const ErrorMessage = styled("div")`
  margin-top: 40px;
  white-space: pre-line;
  font-size: 28px;
  text-align: center;
`;

const useBundle = (bundleUrl) => {
  const queryClient = useQueryClient();
  const data = queryClient.getQueryData(["website"]);

  const bundle = data.website.bundlesById[bundleUrl];

  if (!bundle) {
    throw new Error(`Something went wrong, no data for bundle '${bundleUrl}'`);
  }

  return bundle;
};

const Bundle = () => {
  const {
    dispatch,
    state: { bundleUrl },
  } = useContext(NavigationContext);

  const bundle = useBundle(bundleUrl);

  return (
    <div
      css={css`
        padding-bottom: 50px;
      `}
    >
      <BackButton onClick={() => dispatch("VIEW_WEBSITE")}>
        Back
        <TiArrowBack />
      </BackButton>
      <h2
        css={css`
          margin-bottom: 40px;
          line-height: 44px;
          font-size: 22px;
          display: block;
          text-align: center;
        `}
      >
        NPM libraries bundled in{" "}
        <div
          css={css`
            font-size: 30px;
            overflow-wrap: break-word;
            word-break: break-all;
          `}
        >
          {truncate(bundleUrl, { length: 65 })}{" "}
          <a
            target="_blank"
            rel="noreferrer"
            href={bundle?.fullUrl}
            css={css`
              color: #9ef;
            `}
          >
            <BiLinkExternal />
          </a>
        </div>
      </h2>
      {bundle.sourceMapData ? (
        <p
          css={css`
            text-align: center;
          `}
        >
          Results are based on source map data and should therefore be highly reliable.{" "}
          <a
            target="_blank"
            rel="noreferrer"
            href={bundle.sourceMapData.url}
            css={css`
              color: #9ef;
            `}
          >
            Inspect source map
            <BiLinkExternal />
          </a>
        </p>
      ) : (
        <p
          css={css`
            text-align: center;
          `}
        >
          Click on a library to inspect matching code segments.
        </p>
      )}
      {/* <BundleOverview bundle={bundle} /> */}
      {bundle.matchedReleases.length > 0 ? (
        <BundleTable bundle={bundle} />
      ) : (
        <p
          css={css`
            text-align: center;
          `}
        >
          No libraries found in {bundleUrl}
        </p>
      )}
    </div>
  );
};

export default Bundle;
