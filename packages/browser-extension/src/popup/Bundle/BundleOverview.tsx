import React from "react";
import prettyBytes from "pretty-bytes";
import { css } from "@emotion/react";
import { bundlePropType } from "../../propTypes";

const BundleOverview = ({ bundle }) => {
  return (
    <div
      css={css`
        display: flex;
        justify-content: center;
        text-align: center;
      `}
    >
      <div
        css={css`
          display: flex;
          justify-content: space-between;
          margin: 20px 0;
          width: 600px;
        `}
      >
        <div>
          <div>Libraries matched:</div>
          <div
            css={css`
              font-size: 40px;
              margin: 20px;
            `}
          >
            {bundle.matchedReleases.length}
          </div>
        </div>
        <div>
          <div>Bundle size:</div>
          <div
            css={css`
              font-size: 40px;
              margin: 20px;
              margin-bottom: 10px;
            `}
          >
            {prettyBytes(bundle.sizeBytes)}
          </div>
          <div>{prettyBytes(bundle.sizeBytesGzip)} gzipped</div>
        </div>
      </div>
    </div>
  );
};

BundleOverview.propTypes = { bundle: bundlePropType };

export default BundleOverview;
