import React, { useMemo } from "react";
import PropTypes from "prop-types";
import { useTable, useSortBy } from "react-table";
import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { Link } from "wouter";
import prettyBytes from "pretty-bytes";
import { BsFillInfoCircleFill, BsArrowReturnRight } from "react-icons/bs";
import { IoLogoGithub } from "react-icons/io";
import npmLogo from "../../../images/npm-logo.svg";
import gitlabLogo from "../../../images/gitlab-logo.svg";
import bitbucketLogo from "../../../images/bitbucket-logo.svg";
import { bundlePropType } from "../../propTypes";

const Cell = styled("div")`
  padding: 6px 12px;
`;

const CombinedBundleTable = ({ bundles, websiteUrl }) => {
  const matchedReleases = useMemo(() => getUniqueReleases(bundles), [bundles]);

  const inspectLinkParams = websiteUrl ? `website=${websiteUrl}` : "";

  const columns = useMemo(
    () => [
      {
        Header: "Library",
        Cell: ({ value: libraryId, row: { original: release } }) => (
          <Cell>
            <span>{libraryId}</span>{" "}
            {release.description && (
              <span data-tooltip={release.description}>
                <BsFillInfoCircleFill />
              </span>
            )}
          </Cell>
        ),
        accessor: "libraryId",
      },
      {
        Header: () => (
          <>
            Bundle(s){" "}
            <span data-tooltip="List of files where the library was identified.">
              <BsFillInfoCircleFill />
            </span>
          </>
        ),
        Cell: ({ value: bundles, row: { original: release } }) => (
          <Cell>
            {bundles.map(({ _id: bundleId, hasSourceMap }) => (
              <div
                key={bundleId}
                css={css`
                  display: flex;
                `}
              >
                <Link href={`/bundle/${encodeURIComponent(bundleId)}?website=${websiteUrl}`}>
                  <a
                    css={css`
                      color: #9ef;
                      overflow: hidden;
                      text-overflow: ellipsis;
                      white-space: nowrap;
                      display: block;
                    `}
                  >
                    {bundleId}
                  </a>
                </Link>
                {!hasSourceMap && (
                  <span>
                    <Link
                      href={`/bundle/${encodeURIComponent(bundleId)}/inspect/${encodeURIComponent(
                        release.libraryId
                      )}?${inspectLinkParams}`}
                    >
                      <a
                        css={css`
                          text-decoration: none;
                          background: #161;
                          appearance: button;
                          color: inherit;
                          :hover {
                            background: #050;
                          }
                          padding: 2px 4px;
                          font-size: 11px;
                          display: flex;
                          justify-content: center;
                          align-items: center;
                          text-align: center;
                          margin-left: 6px;
                        `}
                      >
                        Inspect
                      </a>
                    </Link>
                  </span>
                )}
              </div>
            ))}
          </Cell>
        ),
        accessor: "bundles",
        disableSortBy: true,
      },
      {
        Header: () => (
          <>
            Footprint{" "}
            <span data-tooltip="The total size of code segments that are believed to match the library">
              <BsFillInfoCircleFill />
            </span>
          </>
        ),
        Cell: ({ value }) => <Cell>{prettyBytes(value)}</Cell>,
        accessor: "totalFootprint",
        sortDescFirst: true,
      },
      {
        Header: "Dependency of",
        Cell: ({ value: dependentsChain }) =>
          [...dependentsChain].reverse().map((dependent, i) => (
            <Cell key={dependent}>
              <div
                css={css`
                  margin-left: ${((i || 1) - 1) * 10}px;
                `}
              >
                {i > 0 && <BsArrowReturnRight />}
                {dependent}
              </div>
            </Cell>
          )),
        accessor: "dependentsChain",
        disableSortBy: true,
      },
      {
        Header: "Links",
        Cell: ({
          value: repositoryUrl,
          row: {
            original: { libraryId },
          },
        }) => (
          <Cell>
            <div
              css={css`
                display: flex;
                justify-content: center;
                align-items: center;
              `}
            >
              <a target="_blank" rel="noreferrer" href={`https://npmjs.com/package/${libraryId}`}>
                <img src={npmLogo} alt="npm" height="16" />
              </a>
              <RepositoryLink repositoryUrl={repositoryUrl} />
            </div>
          </Cell>
        ),
        accessor: "repositoryUrl",
        id: "links",
        disableSortBy: true,
      },
      {
        Header: "License",
        accessor: "license",
        Cell: ({ value: license }) => <Cell>{license}</Cell>,
      },
    ],
    [matchedReleases]
  );
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable(
    {
      columns,
      data: matchedReleases,
      initialState: {
        sortBy: [{ id: "totalFootprint", desc: true }],
      },
    },
    useSortBy
  );
  return (
    <div
      css={css`
        display: flex;
        justify-content: center;
        padding: 50px;
        padding-top: 10px;
        @media (max-width: 800px) {
          padding-left: 0;
          padding-right: 0;
        }
      `}
    >
      <table
        {...getTableProps()}
        css={css`
          border-collapse: collapse;
          box-shadow: 0px 1px 6px #111;
          overflow-x: auto;
          overflow-y: hidden;
          display: block;
        `}
      >
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th
                  css={css`
                    border: 1px solid rgb(58, 62, 63);
                    padding: 6px 12px;
                  `}
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                >
                  {column.render("Header")}
                  <span>
                    {column.canSort &&
                      (column.isSorted ? (column.isSortedDesc ? " ▼" : " ▲") : "  ↕")}
                  </span>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => (
                  <td
                    css={css`
                      max-width: 600px;
                      min-width: 100px;
                      height: 50px;
                      text-align: left;
                      border: 1px solid rgb(58, 62, 63);
                      background: ${i % 2 === 0 ? "rgb(23, 27, 28);" : "inherit"};
                      box-sizing: border-box;
                    `}
                    {...cell.getCellProps()}
                  >
                    {cell.render("Cell")}
                  </td>
                ))}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
CombinedBundleTable.propTypes = {
  bundles: PropTypes.arrayOf(bundlePropType),
  websiteUrl: PropTypes.string,
};

function RepositoryLink({ repositoryUrl }) {
  if (!repositoryUrl) return null;
  const repositoryHost = new URL(repositoryUrl).host;

  let linkLogo;
  switch (repositoryHost) {
    case "github.com":
      linkLogo = <IoLogoGithub />;
      break;
    case "gitlab.com":
      linkLogo = <img src={gitlabLogo} alt="Gitlab repo" height="30" />;
      break;
    case "bitbucket.org":
      linkLogo = <img src={bitbucketLogo} alt="Bitbucket repo" height="18" />;
      break;
    default:
      linkLogo = "Repo";
  }
  return (
    <a
      target="_blank"
      rel="noreferrer"
      href={repositoryUrl}
      css={css`
        color: inherit;
        margin-left: 6px;
      `}
    >
      {linkLogo}
    </a>
  );
}
RepositoryLink.propTypes = { repositoryUrl: PropTypes.string };

function addDependentsChains(releases) {
  const librarySet = new Set(releases.map(({ libraryId }) => libraryId));
  const dependencyDependentsMap = new Map();

  for (const release of releases) {
    const { dependencies = [], libraryId } = release;
    for (const dependency of dependencies) {
      if (librarySet.has(dependency.libraryId)) {
        const dependents = dependencyDependentsMap.get(dependency.libraryId);
        if (dependents) {
          dependents.push(libraryId);
        } else {
          dependencyDependentsMap.set(dependency.libraryId, [libraryId]);
        }
      }
    }
  }

  const releasesWithDependentsChain = releases.map((release) => {
    // Recursively get one chain of dependents
    const dependentsChain = [];
    let curLibraryId = release.libraryId;
    while (curLibraryId != null) {
      curLibraryId = dependencyDependentsMap.get(curLibraryId)?.[0];
      if (curLibraryId) {
        dependentsChain.push(curLibraryId);
      }
    }

    return { ...release, dependentsChain };
  });

  return releasesWithDependentsChain;
}

function getUniqueReleases(bundles) {
  const bundlesWithDependentsChain = bundles.map((bundle) => ({
    ...bundle,
    matchedReleases: addDependentsChains(bundle.matchedReleases),
  }));
  const librariesMap = new Map();
  for (const bundle of bundlesWithDependentsChain) {
    const matchedReleasesWithDependentsChains = addDependentsChains(bundle.matchedReleases);
    for (const matchedRelease of matchedReleasesWithDependentsChains) {
      const bundleData = { _id: bundle._id, hasSourceMap: !!bundle.sourceMapData };
      if (librariesMap.has(matchedRelease.libraryId)) {
        const library = librariesMap.get(matchedRelease.libraryId);
        library.bundles.push(bundleData);
        library.totalFootprint += matchedRelease.cumulativeIntervalSize;
      } else {
        const {
          _id,
          libraryId,
          license,
          repositoryUrl,
          dependentsChain,
          cumulativeIntervalSize,
          description,
        } = matchedRelease;
        librariesMap.set(matchedRelease.libraryId, {
          releaseId: _id,
          libraryId,
          license,
          repositoryUrl,
          description,
          dependentsChain,
          bundles: [bundleData],
          totalFootprint: cumulativeIntervalSize,
        });
      }
    }
  }
  const uniqueReleases = [...librariesMap.values()];
  return uniqueReleases;
}

export default CombinedBundleTable;
