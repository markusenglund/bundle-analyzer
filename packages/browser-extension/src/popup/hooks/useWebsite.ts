import { useQuery } from "react-query";
import ky from "ky";
import { SERVER_URL_BASE } from "../../config";

const useWebsite = () =>
  useQuery(["website"], async () => {
    const response = await new Promise((resolve) => {
      chrome.runtime.sendMessage({ type: "openPopup" }, resolve);
    });
    if (!response || response.error) {
      console.log(response);
      throw new Error(response.error ?? "Failed to get bundles, try to refresh the tab");
    }

    const { tabUrl, faviconUrl, bundles } = response;

    const apiUrl = `${SERVER_URL_BASE}/api/website/bundles`;
    const { data: website } = await ky
      .post(apiUrl, { timeout: 30_000, json: { bundles, websiteUrl: tabUrl, faviconUrl } })
      .json();

    return { website, tabUrl, bundles };
  });

export default useWebsite;
