import { useQuery } from "react-query";
import * as acorn from "acorn";
import * as acornLoose from "acorn-loose";
import * as walk from "acorn-walk";

// Values which are often removed or added by minification
const literalBlacklist = new Set([true, false, 0, 1, ""]);
const memberExpressionBlacklist = new Set(["exports"]);

const DEFAULT_MAX_TOKENS = 500_000;

function extractTokens({ codeSnippet, maxTokens = DEFAULT_MAX_TOKENS } = {}) {
  if (!codeSnippet) {
    console.log("Tokenization worker unexpectedly received message without a code snippet");
    return;
  }

  const tokenPositions = [];

  const ast = getAst(codeSnippet);

  try {
    walk.ancestor(ast, {
      Literal(node, ancestors) {
        const nextToLastAncestor = ancestors?.[ancestors.length - 2];
        // TODO: This if statement is only necessary for npm packages, not bundles
        if (
          (nextToLastAncestor?.type &&
            ["ImportDeclaration", "ExportNamedDeclaration"].includes(nextToLastAncestor.type)) ||
          nextToLastAncestor?.callee?.name === "require"
        ) {
          return;
        }

        if (
          nextToLastAncestor?.type === "ArrayExpression" &&
          ancestors?.[ancestors.length - 3]?.callee?.name === "define"
        ) {
          return;
        }

        if (literalBlacklist.has(node.value)) {
          return;
        }

        tokenPositions.push({
          start: node.start,
          end: node.end,
          token: String(node.value),
        });
        if (tokenPositions.length > maxTokens) {
          throw new Error(`Number of tokens exceeded maximum of ${maxTokens}`);
        }
      },
      // Add property keys, except computed ones which can be minified
      Property(node) {
        if (node.computed) {
          return;
        }

        if (node.key?.type === "Identifier") {
          tokenPositions.push({
            start: node.key.start,
            end: node.key.end,
            token: String(node.key.name),
          });
        } else if (node.key?.type === "Literal") {
          tokenPositions.push({
            start: node.key.start,
            end: node.key.end,
            token: String(node.key.value),
          });
        }
      },
      // Add properties and methods
      MemberExpression(node) {
        if (node.computed) {
          return;
        }
        if (!node.property || memberExpressionBlacklist.has(node.property.name)) {
          return;
        }
        tokenPositions.push({
          start: node.property.start,
          end: node.property.end,
          token: String(node.property.name),
        });
      },
    });
  } catch (err) {
    if (err.name === "MaxTokensExceeded") {
      console.log("Number of tokens exceeded maximum, cancelling walk...");
    } else {
      throw err;
    }
  }

  tokenPositions.sort((a, b) => a.start - b.start);

  return tokenPositions;
}

function getAst(codeSnippet) {
  const options = {
    sourceType: "module",
    ecmaVersion: 2021,
  };

  // If the acorn parser throws (usually due to invalid syntax), try with the error resistant loose version
  try {
    const ast = acorn.parse(codeSnippet, options);
    return ast;
  } catch (err) {
    const ast = acornLoose.parse(codeSnippet, options);
    return ast;
  }
}

const useTokenPositions = (code, queryId) =>
  useQuery(["tokenPositions", queryId], async () => extractTokens({ codeSnippet: code }), {
    enabled: !!code,
  });

export default useTokenPositions;
