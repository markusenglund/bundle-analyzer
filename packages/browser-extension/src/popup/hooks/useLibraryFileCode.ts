import { useQuery } from "react-query";
import ky from "../../utils/ky";

const useLibraryFileCode = (releaseId, fileName, inView) =>
  useQuery(
    ["bundle", releaseId, fileName],
    async () => {
      const code = await ky(`https://unpkg.com/${releaseId}${fileName}`).text();
      return code;
    },
    { enabled: inView }
  );

export default useLibraryFileCode;
