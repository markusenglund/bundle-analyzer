import { useQueryClient } from "react-query";

const useBundleCode = (fullBundleUrl: string) => {
  const queryClient = useQueryClient();
  const data = queryClient.getQueryData(["website"]);

  const bundle = data.bundles.find(({ url }) => fullBundleUrl === url);
  return bundle.code;
};

export default useBundleCode;
