import { useQueryClient } from "react-query";

const useBundle = (bundleUrl: string) => {
  const queryClient = useQueryClient();
  const data = queryClient.getQueryData(["website"]);

  const bundle = data.website.urlScan.bundlesById[bundleUrl];

  if (!bundle) {
    throw new Error(`Something went wrong, no data for bundle '${bundleUrl}'`);
  }

  return bundle;
};

export default useBundle;
