import React from "react";
import { Global, css } from "@emotion/react";

const GlobalStyles = () => (
  <Global
    styles={css`
      body {
        background: rgb(30, 34, 35);
        color: #eee;
        width: 780px;
        height: 580px;
        margin: 0px;
      }
      .lds-dual-ring {
        margin-top: 40px;
        display: inline-block;
        width: 80px;
        height: 80px;
        animation: fade-in ease 2s;
        animation-iteration-count: 1;
      }
      @keyframes fade-in {
        0% {
          opacity: 0;
        }
        50% {
          opacity: 0;
        }
        100% {
          opacity: 1;
        }
      }
      .lds-dual-ring:after {
        content: " ";
        display: block;
        width: 64px;
        height: 64px;
        margin: 8px;
        border-radius: 50%;
        border: 6px solid #fff;
        border-color: #fff transparent #fff transparent;
        animation: lds-dual-ring 1.2s linear infinite;
      }

      @keyframes lds-dual-ring {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(360deg);
        }
      }
    `}
  />
);

export default GlobalStyles;
