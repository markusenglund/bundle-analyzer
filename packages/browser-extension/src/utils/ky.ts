import ky from "ky";

const customKy = ky.create({ retry: 0 });

export default customKy;
