import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import { babel } from "@rollup/plugin-babel";
import copy from "rollup-plugin-copy";
import replace from "@rollup/plugin-replace";
import image from "@rollup/plugin-image";

const extensions = [".js", ".jsx", ".ts", ".tsx"];

const plugins = [
  babel({ babelHelpers: "bundled", extensions }),
  replace({
    preventAssignment: true,
    "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
  }),
  resolve({
    extensions,
  }),
  commonjs(),
  image(),
  copy({
    targets: [
      { src: "src/popup.html", dest: "dist" },
      { src: "src/manifest.json", dest: "dist" },
      { src: "src/icons", dest: "dist" },
    ],
  }),
];

const popup = {
  input: "src/popup/popup.tsx",
  output: {
    dir: "dist",
    format: "cjs",
  },
  plugins,
};

const background = {
  input: "src/background.ts",
  output: {
    dir: "dist",
    format: "cjs",
  },
  plugins,
};

export default [popup, background];
