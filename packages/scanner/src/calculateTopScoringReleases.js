import sizeof from "object-sizeof";
import { uniqBy, partition } from "lodash-es";
import { logger } from "@bundle-scanner/common";

const SCORE_THRESHOLD = 3;

const calculateTopScoringReleases = async ({
  overlapMap,
  fileMap,
  releasesWithFileIds,
  bundledLibraries = [],
  releaseMap,
  dfMap,
  files,
  bundleUrl,
}) => {
  logger.info(
    `[${bundleUrl}]: ${overlapMap.size} / ${files.length} (${
      Math.round((1000 * overlapMap.size) / files.length) / 10
    }%) files matched at least one k-gram.`
  );

  logger.debug(`Calculating scores`);
  // console.time("Top releases: Get idf score ratio map");
  const idfScoreRatioMap = new Map();
  for (const [fileId, fourGrams] of overlapMap) {
    const file = fileMap.get(fileId);
    // console.log({ fileId, file });
    if (!file) {
      // File exists but has too small maxIdfScore (<0.5) to be useful
      continue;
    }
    const { maxIdfScore } = file;
    // Loop through set to calculate score of individual release

    let cumulativeIdfScore = 0;
    for (const fourGram of fourGrams) {
      const df = dfMap.get(fourGram) || 1;
      // Use sqrt instead of log10, yolo
      const idf = (1 / df) ** (1 / 2.5);
      cumulativeIdfScore += idf;
    }
    const idfScoreRatio = cumulativeIdfScore / maxIdfScore;
    idfScoreRatioMap.set(fileId, {
      idfScoreRatio,
      maxIdfScore,
      cumulativeIdfScore,
    });
  }
  // console.timeEnd("Top releases: Get idf score ratio map");

  logger.debug("Sorting by score");

  // console.time("Top releases: Calculate release scores");

  // TODO: Create a library cache and check similar versions against it
  const releaseScores = releasesWithFileIds
    // Create a filter that removes releases that don't come over a low threshold of a simplified score calculation to increase overall perf
    .filter(({ _id, fileIds }) => {
      if (!releaseMap.has(_id)) {
        return false;
      }
      if (
        fileIds.some((fileId) => {
          const idfScoreData = idfScoreRatioMap.get(fileId);
          return idfScoreData?.idfScoreRatio > 0.1;
        })
      ) {
        return true;
      }
      return false;
    })
    .map(({ fileIds, _id: releaseId }) => {
      const release = releaseMap.get(releaseId);
      const { sourceMapFrequency, bundledDependencies, libraryId, version } = release;
      const numFiles = fileIds.length;

      let releaseIdfScore = 0;
      let modifiedReleaseIdfScore = 0;
      let releaseMaxIdfScore = 0;
      // TODO: START HERE - Get an overview of what the whole function does and figure out ways of bailing out early for files that return zero or belong to hopeless releases
      for (const fileId of fileIds) {
        const file = fileMap.get(fileId);
        if (!file) continue;

        const { maxIdfScore } = file;
        const { idfScoreRatio = 0, cumulativeIdfScore = 0 } = idfScoreRatioMap.get(fileId) ?? {};
        const modifiedMaxIdfScore = maxIdfScore ** (1 / 1.5);

        modifiedReleaseIdfScore += idfScoreRatio ** 1.5 * modifiedMaxIdfScore;
        releaseIdfScore += cumulativeIdfScore ?? 0;
        releaseMaxIdfScore += maxIdfScore ?? 0;
      }

      const bundledDependencyPenalty = bundledDependencies.length > 0 ? 2 : 1;
      const releaseScore =
        (modifiedReleaseIdfScore * Math.log10(1 + 100 * sourceMapFrequency)) /
        (numFiles ** (1 / 2.5) + numFiles / 50) /
        bundledDependencyPenalty;

      const idfScoreRatio = releaseIdfScore / releaseMaxIdfScore;

      return {
        score: releaseScore,
        _id: releaseId,
        numFiles,
        libraryId,
        version,
        idfScoreRatio,
        maxIdfScore: releaseMaxIdfScore,
        fileIds,
        sourceMapFrequency,
        bundledDependencies,
      };
    });

  // console.timeEnd("Top releases: Calculate release scores");

  releaseScores.sort((a, b) => b.score - a.score);

  logger.debug(
    `${
      releasesWithFileIds.length - releaseScores.length
    } releases skipped due to not having data in 'releases' collection`
  );

  const topScoringReleases = releaseScores.filter(({ score }) => score > SCORE_THRESHOLD);

  // console.time("Top releases: Get best release per library");
  const secondStageReleases = uniqBy(
    topScoringReleases
      .map(
        ({
          idfScoreRatio,
          _id: releaseId,
          maxIdfScore,
          score,
          fileIds,
          sourceMapFrequency,
          bundledDependencies,
          version,
          libraryId,
        }) => {
          return {
            _id: releaseId,
            isBundled: bundledLibraries.includes(libraryId),
            libraryId,
            score,
            idfScoreRatio,
            maxIdfScore,
            sourceMapFrequency,
            bundledDependencies,
            fileIds,
            version,
          };
        }
      )
      .sort((a, b) => b.idfScoreRatio - a.idfScoreRatio),
    "libraryId"
  );
  // console.timeEnd("Top releases: Get best release per library");

  // console.log("Top scoring releases", topScoringReleases.map(({releaseId})))

  // logger.info(
  //   `${topScoringReleases.length} / ${releasesWithFileIds.length} (${
  //     Math.round(
  //       (1000 * topScoringReleases.length) / releasesWithFileIds.length
  //     ) / 10
  //   }%) releases scored above the first stage theshold`
  // );

  // logger.info(
  //   `${secondStageReleases.length} / ${releasesWithFileIds.length} (${
  //     Math.round(
  //       (1000 * secondStageReleases.length) / releasesWithFileIds.length
  //     ) / 10
  //   }%) releases on to the next round`
  // );

  // logStats({
  //   topScoringReleases,
  //   secondStageReleases,
  //   bundledLibraries,
  //   releasesSortedByScore: releaseScores
  // });

  return secondStageReleases;
};

const logStats = ({
  topScoringReleases,
  secondStageReleases,
  bundledLibraries,
  releasesSortedByScore,
}) => {
  const avg =
    releasesSortedByScore.reduce((acc, val) => acc + val.score, 0) / releasesSortedByScore.length;
  // Object.values(idfScoreRatioMap).reduce(
  //   (acc, val) => acc + (val.idfScoreRatio || 0),
  //   0
  // ) / Object.values(idfScoreRatioMap).length;
  const mean = releasesSortedByScore[Math.round(releasesSortedByScore.length / 2)].score;
  // console.log(filesSortedByScore);

  console.log({ avg, mean });

  // Get rank indices of the basket libraries
  const nonBundledLibraryReleases = topScoringReleases.filter(({ _id: releaseId }) => {
    const libraryId = releaseId.split("@")[0] || `@${releaseId.split("@")[1]}`;
    const isBundled = bundledLibraries.includes(libraryId);
    return !isBundled || global.PARTIAL_MATCH_BASKET.includes(releaseId);
  });
  const basketRanks = {};
  for (let j = 0; j < nonBundledLibraryReleases.length; j += 1) {
    const {
      _id: releaseId,
      idfScoreRatio,
      maxIdfScore,
      score,
      numFiles,
    } = nonBundledLibraryReleases[j];
    if (global.PARTIAL_MATCH_BASKET.includes(releaseId)) {
      basketRanks[releaseId] = {
        rank: j,
        idfScoreRatio,
        maxIdfScore,
        score,
        numFiles,
      };
    }
  }
  // console.log(basketRanks);

  const [bundledLibs, nonBundledLibs] = partition(
    secondStageReleases,
    ({ isBundled }) => isBundled
  );

  // const fullBundledReleases = releasesSortedByScore
  //   .filter(release => bundledReleases.includes(release._id))
  //   .sort((a, b) => b.idfScoreRatio - a.idfScoreRatio);

  // console.log({ bundledLibs, fullBundledReleases });

  // TODO: Fix this
  logger.info(
    `We have ${bundledLibs.length} bundled libs and ${nonBundledLibs.length} non bundled libs`
  );
  const avgBundledScore =
    bundledLibs.reduce((acc, { score }) => acc + score, 0) / bundledLibs.length;

  const avgNonBundledScore =
    nonBundledLibs.reduce((acc, { score }) => acc + score, 0) / nonBundledLibs.length;

  console.log({ avgBundledScore, avgNonBundledScore });

  // console.log(
  //   "Selected releases: ",
  //   releasesSortedByScore.filter(({ _id }) =>
  //     [
  //       "object-assign@4.1.1",
  //       "history@4.9.0",
  //       "rudy-history@1.0.0",
  //       "@squiz/mercury-storage-adapter-browser@0.18.32",
  //       "vue-scroll@2.1.12",
  //       "lodash-es@4.17.20"
  //     ].includes(_id)
  //   )
  // );

  // TODO: Log out how big max score is for the libraries
  // console.log({ bundledLibs, nonBundledLibs });
};

export default calculateTopScoringReleases;
