import { logger, mongo } from "@bundle-scanner/common";

const { db } = mongo;

let dfMap;
// Load dfMap into memory the first time this function is called
const getDfMap = async () => {
  if (dfMap) return dfMap;
  const dfs = await db.dfs7.find({}).toArray();
  dfMap = new Map(dfs.map(({ fourGram, count }) => [fourGram, count]));
  logger.info("DF map loaded into memory...");
  return dfMap;
};

export default getDfMap;
