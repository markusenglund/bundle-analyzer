import { threadId } from "worker_threads";
import { logger, mongo } from "@bundle-scanner/common";
import scanBundle from "./index.js";
const { connect } = mongo;

// Initialize the db connection and return the function
// so we can pass it directly to piscina
const scanBundleWorker = async () => {
  await connect();
  logger.info(`Connected to db in thread ${threadId}`);

  return scanBundle;
};

export default scanBundleWorker();
