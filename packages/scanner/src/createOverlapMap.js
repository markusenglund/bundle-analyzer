import pMap from "p-map";
import { chunk } from "lodash-es";
import { logger, mongo } from "@bundle-scanner/common";

const { db } = mongo;

const createOverlapMap = async ({ bundleUrl, uniqueBundleFourGrams }) => {
  const overlapMap = new Map();
  const dfMap = new Map();

  // console.time("get-overlap");
  const CHUNK_SIZE = 2000;
  const fourGramChunks = chunk([...uniqueBundleFourGrams], CHUNK_SIZE);
  await pMap(
    fourGramChunks,
    async (fourGramChunk, i) => {
      const fourGramsWithFileIds = await db.fourGrams
        .find({ _id: { $in: fourGramChunk }, df: { $not: { $gte: 300 } } })
        .toArray();
      for (const { _id: fourGram, fileIds, df = 1 } of fourGramsWithFileIds) {
        for (const fileId of fileIds) {
          if (overlapMap.has(fileId)) {
            overlapMap.get(fileId).push(fourGram);
          } else {
            overlapMap.set(fileId, [fourGram]);
          }
        }
        dfMap.set(fourGram, df);
      }
      if (i % 10 === 0) {
        logger.info(
          `[${bundleUrl}]: Handling fourgram no ${i * CHUNK_SIZE}: '${fourGramChunk[0]}'`
        );
      }
    },
    { concurrency: 3 }
  );
  // console.timeEnd("get-overlap");

  return { dfMap, overlapMap };
};

export default createOverlapMap;
