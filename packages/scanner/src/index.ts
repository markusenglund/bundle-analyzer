import { logger, mongo, tokenUtils } from "@bundle-scanner/common";
import createOverlapMap from "./createOverlapMap.js";
import calculateTopScoringReleases from "./calculateTopScoringReleases.js";
import exclusiveIntervalMatch from "./exclusiveIntervalMatch.js";
export { default as getBundlePositionInterval } from "./getBundlePositionInterval.js";
const { getFourGrams } = tokenUtils;

const { db } = mongo;

// TODO: Use bundleFourGramPositions to compare computed interval to actual interval
const scanBundle = async ({ code, bundleUrl, bundledReleases = [] }) => {
  const {
    fourGrams: bundleFourGrams,
    fourGramIndex: bundleFourGramIndex,
    uniqueFourGrams: uniqueBundleFourGrams,
    fourGramPositions: bundleFourGramPositions,
  } = getFourGrams(code);

  logger.info(`'${bundleUrl}' has ${bundleFourGrams.length} tokens, starting bundle scan...`);

  const bundledLibraries = bundledReleases.map(({ libraryId }) => libraryId);
  // TODO: Figure out what we should do with getFourGrams/tokensToKGrams duplicate code
  const { files, fileMap } = await withCache("files", async () => {
    const files = await db.files
      .find({
        maxIdfScore: { $gte: 0.5 },
      })
      .project({
        maxIdfScore: 1,
        fileName: 1,
        step: 1,
        libraryId: 1,
        numFourGrams: 1,
      })
      .toArray();
    const filteredFiles = files
      .filter(
        ({ libraryId }) =>
          !libraryId.startsWith("lodash.") &&
          !libraryId.startsWith("@ramda/") &&
          !libraryId.startsWith("ramda.") &&
          !libraryId.startsWith("underscore.")
      )
      .filter(
        ({ libraryId }) =>
          ![
            "lodash-es",
            "rxjs-es",
            "core-js-pure",
            "apollo-client",
            "jquery-min",
            "jquery-slim",
            "react-layout-effect",
            "positioning",
            "uuid-browser",
            "js-uuid",
            "ts.cryptojs256",
          ].includes(libraryId)
      );

    return {
      files: filteredFiles,
      fileMap: new Map(filteredFiles.map((file) => [file._id.toString(), file])),
    };
  });

  logger.debug("Got files with maxIdfScore");

  const releasesWithFileIds = await withCache("releasesWithFileIds", async () => {
    const filesWithReleaseIds = (
      await db.files
        .find({
          maxIdfScore: { $gte: 0.5 },
        })
        .project({ releaseIds: 1, libraryId: 1 })
        .toArray()
    ).filter(({ libraryId }) => !libraryId.startsWith("lodash.") && libraryId !== "lodash-es");

    const fileIdsGroupedByRelease = {};
    for (const { _id: fileId, releaseIds } of filesWithReleaseIds) {
      for (const releaseId of releaseIds) {
        const val = fileIdsGroupedByRelease[releaseId];
        if (val != null) {
          val.push(fileId);
        } else {
          fileIdsGroupedByRelease[releaseId] = [fileId];
        }
      }
    }

    return Object.entries(fileIdsGroupedByRelease).map(([releaseId, fileIds]) => ({
      _id: releaseId,
      fileIds,
    }));
  });

  logger.debug("Got releases with file IDs");

  const releaseMap = await withCache("releaseMap", async () => {
    const releaseCursor = db.releases.find({});
    const map = new Map();
    for await (const release of releaseCursor) {
      // Removing this code caused big improvements for jetblue and slight improvements in benchmark
      // if (release.bundledDependencies?.length) continue;
      map.set(release._id, release);
    }
    return map;
  });

  logger.debug("Got releaseMap");

  const { overlapMap, dfMap } = await createOverlapMap({ uniqueBundleFourGrams, bundleUrl });
  logger.debug("Created overlap map");
  const secondStageReleases = await calculateTopScoringReleases({
    overlapMap,
    fileMap,
    releaseMap,
    releasesWithFileIds,
    bundledLibraries,
    files,
    dfMap,
    bundleUrl,
  });

  const { matchedReleases, sortedIntervals } = await exclusiveIntervalMatch({
    secondStageReleases,
    releaseMap,
    overlapMap,
    fileMap,
    bundleFourGramIndex,
    bundleFourGrams,
    dfMap,
  });

  // Add dependencies and version
  const enrichedMatchedReleases = matchedReleases.map((release) => {
    const { dependencies, version } = releaseMap.get(release._id);
    return { ...release, dependencies, version };
  });

  return {
    matchedReleases: enrichedMatchedReleases,
    sortedIntervals,
    fourGramPositions: bundleFourGramPositions,
  };
};

const cache = new Map();
const withCache = async (key, fn) => {
  if (cache.has(key)) {
    logger.debug(`${key} cache hit!`);
    return cache.get(key);
  }
  const result = await fn();
  cache.set(key, result);
  return result;
};

// const cacheFile = async (key, fn) => {
//   const cacheFilePath = path.join(__dirname, `../.cache/${key}.json`);
//   if (fs.existsSync(cacheFilePath)) {
//     logger.info(`${key} cache hit!`);
//     return JSON.parse(fs.readFileSync(cacheFilePath, "utf-8"));
//   }
//   const result = await fn();
//   fs.writeFileSync(cacheFilePath, JSON.stringify(result));
//   return result;
// };

export default scanBundle;
