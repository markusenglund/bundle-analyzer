import { logger } from "@bundle-scanner/common";
import { inRange, orderBy } from "lodash-es";
import getBundlePositionInterval from "./getBundlePositionInterval.js";

/*
  1. Get the release with the highest score and recalculate its score by comparing its k-grams from the overlapMap with the bundleIndex clone
  2. Compare the new score with the rest of the scores; if it's now lower than a different lib, start over at 1. with that library
  3. Mark the current release as Definitely Bundled
  4. Loop through each file of the release, if it has an idfScoreRatio > 0.5, remove one position from (or delete) that k-gram from the bundleIndex
  5. When no releases with score > SCORE_THRESHOLD are left we're done
*/

const SCORE_THRESHOLD = 3;

const exclusiveIntervalMatch = async ({
  secondStageReleases,
  overlapMap,
  fileMap,
  bundleFourGramIndex,
  bundleFourGrams,
  dfMap,
  releaseMap,
}) => {
  logger.debug("Starting exclusiveIntervalMatch");
  const bundleFourGramIndexCopy = new Map(bundleFourGramIndex);

  // console.log(secondStageReleases.map(({ score, _id }) => `${_id}: ${score}`));

  const matchedReleases = [];

  const intervals = [];

  // Create clone that we can mutate however we want
  const releases = [...secondStageReleases].sort((a, b) => b.score - a.score);
  let i = 0;
  while (releases.length > 0 && releases[0].score > SCORE_THRESHOLD) {
    if (i > 10_000) {
      throw new Error("Infinite loop detected");
    }
    i += 1;
    // Identify all bundledDependencies of the release which
    // have a score above 3 and have not already been handled, and put them at the top of the stack

    // Check if release has the highest score out of the libraries
    const topRelease = releases[0];
    const { score, fileScores } = calculateScore({
      release: topRelease,
      overlapMap,
      fileMap,
      bundleFourGramIndexCopy,
      dfMap,
    });
    topRelease.score = score;
    const isReleaseNextInLine =
      (score >= SCORE_THRESHOLD && score >= (releases[1]?.score ?? 0)) ||
      (score >= SCORE_THRESHOLD && !!topRelease.isBundledDependency);
    // If not, put it in its place

    if (!isReleaseNextInLine) {
      releases.shift();
      let newIndex = releases.findIndex((release) => release.score < score);
      if (newIndex === -1) {
        newIndex = releases.length;
      }
      releases.splice(newIndex, 0, topRelease);
      continue;
    }
    // If release is next in line, check if it has bundled dependencies and put the bundled dependencies on top of the stack
    const { bundledDependencies } = topRelease;
    if (bundledDependencies?.length) {
      let areBundledDependenciesInTheBundle = false;
      for (const { libraryId } of bundledDependencies) {
        const releaseIndex = releases.findIndex((release) => release.libraryId === libraryId);
        const release = releases[releaseIndex];
        if (releaseIndex !== -1 && release.score > SCORE_THRESHOLD) {
          areBundledDependenciesInTheBundle = true;
          release.isBundledDependency = true;
          release.bundledByLibraryId = topRelease.libraryId;
          releases.splice(releaseIndex, 1);
          releases.unshift(release);
        }
      }
      if (areBundledDependenciesInTheBundle) {
        continue;
      }
    }
    // splice out the top release and calculate interval, push it to the intervals array and delete fourgrams etc

    const { libraryId, version } = topRelease;
    releases.shift();
    matchedReleases.push(topRelease);

    const prevBundleSize = bundleFourGramIndexCopy.size;
    // This is where bundle k-grams are actually deleted
    for (const file of fileScores) {
      const { fileId, idfScoreRatio, step, fileName, fileMaxIdfScore, numFourGrams } = file;
      const fourGrams = overlapMap.get(fileId);
      if (!fourGrams) continue;
      const numOverlappingFourGrams = fourGrams.length;

      // Condition for min absolute matchedFourGram size since matching 2 fourgrams is not important and may add noise
      if (idfScoreRatio > 0.1) {
        // Get sorted bundle k-grams, actually - see if we can get this without calculcating
        const sortedBundlePositions = fourGrams
          .flatMap((fourGram) => {
            const bundlePositions = bundleFourGramIndex.get(fourGram) || [];
            const tf = bundlePositions.length;
            // Since fourgrams that occur more than 100 times in the bundle are unhelpful for calculating the best matching bundle interval - just ignore them to prevent perf-trap
            if (tf > 100) {
              return [];
            }
            const df = dfMap.get(fourGram) || 1;
            // TODO: Big tf and df should be penalized more?
            // const weight = Math.sqrt(1 / (df * tf));
            const idfScore = (1 / df) ** (1 / 2.5);
            return bundlePositions.map((position) => ({
              position,
              // weight,
              idfScore,
              fourGram,
              df,
              tf,
            }));
          })
          .filter(
            ({ position }) =>
              !intervals.some((interval) => inRange(position, interval.start, interval.end))
          )
          .sort((a, b) => a.position - b.position);

        if (sortedBundlePositions.length < 1) continue;

        // Find interval within which almost 1/step positions are matches in the form [positionStart, positionEnd]
        const positionInterval = getBundlePositionInterval({
          sortedBundlePositions,
          numFourGrams,
          step,
          libraryId,
        });

        // { positionInterval, intervalScore, intervalMatchRatio }
        // intervalsByFile.set(`${releaseId}${fileName}`, positionInterval);
        intervals.push({
          libraryId,
          version,
          fileName,
          start: positionInterval.start,
          end: positionInterval.end,
          intervalSize: positionInterval.intervalSize,
          maxIntervalSize: positionInterval.maxIntervalSize,
          intervalMatchRatio: positionInterval.matchRatio,
        });

        // logger.info(`${releaseId} ${fileName}
        // step: ${step}, idfScoreRatio: ${idfScoreRatio}, numFourgrams: ${numFourGrams}, fileMaxScore: ${fileMaxIdfScore}
        // Positions per matched k-gram (avg tf): ${
        //   Math.round((10 * sortedBundlePositions.length) / numOverlappingFourGrams) / 10
        // }
        // Real interval size vs max interval size: ${
        //   positionInterval.end - positionInterval.start
        // } vs ${step * (numFourGrams - 1) + 1} (${Math.round(
        //   (100 * (positionInterval.end - positionInterval.start)) / (step * (numFourGrams - 1) + 1)
        // )}%)
        // Deleting positions ${positionInterval.start} -> ${positionInterval.end}
        // Actual overlap positions: ${sortedBundlePositions
        //   .map(({ position }) => position)
        //   .join(", ")}`);

        // console.log("fourgrams: ", fourGrams);
        // console.log(
        //   "Interval: ",
        //   bundleFourGrams.slice(
        //     positionInterval[0],
        //     positionInterval[1] + 1
        //   )
        // );

        // Loop through bundleFourGrams from positionStart to positionEnd and remove every k-gram encounted from bundleIndexCopy (like in the code below)
        for (let i = positionInterval.start; i <= positionInterval.end; i += 1) {
          const fourGram = bundleFourGrams[i];
          const positions = bundleFourGramIndexCopy.get(fourGram);
          if (positions) {
            // TODO: Splice/delete instead of delete
            // console.log(`Deleting ${fourGram}, ${positions.length}`);
            bundleFourGramIndexCopy.delete(fourGram);
          }
        }
      }
    }
    // logger.info(
    //   `${curRelease._id}: bundle size: ${prevBundleSize} -> ${
    //     bundleIndexCopy.size
    //   } (-${prevBundleSize - bundleIndexCopy.size})`
    // );
  }
  // console.log({
  //   matchedReleases: matchedReleases.map(
  //     ({ _id, score, isBundled }) =>
  //       `${isBundled ? "1" : "0"} ${_id}: ${Math.round(score * 10) / 10}`
  //   ),
  // });
  // console.log({
  //   nonBundledReleases: releases.map(
  //     ({ _id, score, isBundled }) =>
  //       `${isBundled ? "1" : "0"} ${_id}: ${Math.round(score * 10) / 10}`
  //   ),
  // });

  // Mark bundled dependencies as such
  const dependencyDependentMap = new Map();
  for (const release of matchedReleases) {
    for (const bundledDependency of release.bundledDependencies) {
      if (!dependencyDependentMap.get(bundledDependency.libraryId)) {
        dependencyDependentMap.set(bundledDependency.libraryId, release.libraryId);
      }
    }
  }
  for (const release of matchedReleases) {
    const bundledDependent = dependencyDependentMap.get(release.libraryId);
    if (bundledDependent) {
      release.isBundledDependency = true;
      release.bundledByLibraryId = bundledDependent;
    }
  }

  const sortedIntervals = orderBy(intervals, "start");

  return { matchedReleases, sortedIntervals };
};

function calculateScore({ release, overlapMap, fileMap, bundleFourGramIndexCopy, dfMap }) {
  const { fileIds, sourceMapFrequency } = release;
  const numFiles = fileIds.length;

  // Make sure a specific k-gram only adds to a release's score once, preventing duplicated code from gaining too much score
  const usedKGrams = new Set();

  // Recalculate release score
  const fileScores = fileIds.map((fileId) => {
    const fourGrams = overlapMap.get(fileId) ?? [];
    const {
      step,
      fileName,
      numFourGrams,
      maxIdfScore: originalFileMaxIdfScore,
    } = fileMap.get(fileId);
    let cumulativeIdfScore = 0;
    let fileMaxIdfScore = originalFileMaxIdfScore;
    for (const fourGram of fourGrams) {
      if (bundleFourGramIndexCopy.has(fourGram)) {
        const df = dfMap.get(fourGram) || 1;
        const idf = (1 / df) ** (1 / 2.5);

        // Don't add any score for k-grams previously seen for the same release, instead decrease max score to not penalize too much
        if (usedKGrams.has(fourGram)) {
          fileMaxIdfScore -= idf;
        } else {
          cumulativeIdfScore += idf;
          usedKGrams.add(fourGram);
        }
      }
    }
    const idfScoreRatio = cumulativeIdfScore / fileMaxIdfScore;

    return {
      fileId,
      idfScoreRatio,
      fileMaxIdfScore,
      cumulativeIdfScore,
      fileName,
      numFourGrams,
      step,
    };
  });

  const bundledDependencyPenalty = release.bundledDependencies.length > 0 ? 2 : 1;
  const curScore =
    (fileScores.reduce((sum, { idfScoreRatio, fileMaxIdfScore }) => {
      if (!fileMaxIdfScore) return sum;
      const modifiedMaxIdfScore = fileMaxIdfScore ** (1 / 1.5);
      return sum + idfScoreRatio ** 1.5 * modifiedMaxIdfScore;
    }, 0) *
      Math.log10(1 + 100 * sourceMapFrequency)) /
    (numFiles ** (1 / 2.5) + numFiles / 50) /
    bundledDependencyPenalty;
  // logger.info(
  //   `${curRelease._id}: Previous idfScoreRatio: ${curRelease.idfScoreRatio}, new idfScoreRatio: ${newIdfScoreRatio}`
  // );
  // logger.info(
  //   `${curRelease._id} - Previous score: ${curRelease.score}, new score: ${curScore}`
  // );
  return { score: curScore, fileScores };
}

export default exclusiveIntervalMatch;
