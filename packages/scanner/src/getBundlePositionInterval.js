import { sumBy } from "lodash-es";

const getBundlePositionInterval = ({ sortedBundlePositions, numFourGrams, step, libraryId }) => {
  /* Step 1: Find fourGram-interval of length 'numFourGrams * step - step + 1' with the highest
      total cumulative idfScore
  */
  const maxIntervalSize = numFourGrams * step - step + 1;

  // const currentIntervalPositions = sortedBundlePositions;
  let highestScoringInterval;

  for (let i = 0; i < sortedBundlePositions.length; i += 1) {
    const currentKGram = sortedBundlePositions[i];
    const bundlePositionsInInterval = sortedBundlePositions.filter(
      ({ position }) =>
        position >= currentKGram.position && position < currentKGram.position + maxIntervalSize
    );

    const currentInterval = {
      startArrayIndex: i,
      endArrayIndex: i + bundlePositionsInInterval.length,
      cumulativeIdfScore: sumBy(bundlePositionsInInterval, "idfScore"),
    };
    if (currentInterval.cumulativeIdfScore > (highestScoringInterval?.cumulativeIdfScore ?? 0)) {
      highestScoringInterval = currentInterval;
    }
  }

  /* Step 2: Cut off any non-matching part from both the start and the end by removing interval of any size
   that touches the edge of the interval and has a match rate of >25%
   */
  const middleArrayIndex = Math.floor(
    (highestScoringInterval.startArrayIndex + highestScoringInterval.endArrayIndex) / 2
  );
  const getMatchRatio = (startArrayIndex, endArrayIndex) => {
    const subIntervalStart = sortedBundlePositions[startArrayIndex].position;
    const subIntervalEnd = sortedBundlePositions[endArrayIndex - 1].position + 1;
    const subIntervalSize = subIntervalEnd - subIntervalStart;
    const numMatchedFourGrams = endArrayIndex - startArrayIndex;
    const matchRatio = (step * numMatchedFourGrams - step + 1) / subIntervalSize;
    return matchRatio;
  };

  for (let i = highestScoringInterval.startArrayIndex + 1; i <= middleArrayIndex; i += 1) {
    while (getMatchRatio(highestScoringInterval.startArrayIndex, i) < 0.25) {
      highestScoringInterval.startArrayIndex += 1;
    }
  }
  for (let i = highestScoringInterval.endArrayIndex - 1; i >= middleArrayIndex; i -= 1) {
    while (getMatchRatio(i, highestScoringInterval.endArrayIndex) < 0.25) {
      highestScoringInterval.endArrayIndex -= 1;
    }
  }

  highestScoringInterval.start =
    sortedBundlePositions[highestScoringInterval.startArrayIndex].position;
  highestScoringInterval.end =
    sortedBundlePositions[highestScoringInterval.endArrayIndex - 1].position + 1;

  const intervalSize = highestScoringInterval.end - highestScoringInterval.start;
  const numMatchedFourGramsInInterval =
    highestScoringInterval.endArrayIndex - highestScoringInterval.startArrayIndex;
  const matchRatio = (step * numMatchedFourGramsInInterval - step + 1) / intervalSize;

  return { ...highestScoringInterval, matchRatio, maxIntervalSize, intervalSize };
};

export default getBundlePositionInterval;
