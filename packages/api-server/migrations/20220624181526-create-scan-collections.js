export const up = async (db, client) => {
  const urls = db.collection("urls");
  const bundles = db.collection("bundles");
  const urlScans = db.collection("urlScans");
  const bundleScans = db.collection("bundleScans");

  //   Transfer scan data to new urlScans collection
  await urls
    .aggregate([
      {
        $project: {
          _id: 0,
          url: "$_id",
          date: "$lastScannedDate",
          source: "puppeteer",
          firstPartyBundleIds: 1,
          thirdPartyBundleIds: 1,
          failedBundleIds: [],
          totalSizeBytes: 1,
          totalSizeBytesGzip: 1,
        },
      },
      { $out: "urlScans" },
    ])
    .toArray();

  // Remove the transferred properties from urls collection
  await urls.updateMany(
    {},
    {
      $unset: {
        lastScannedDate: "",
        firstPartyBundleIds: "",
        thirdPartyBundleIds: "",
        totalSizeBytes: "",
        totalSizeBytesGzip: "",
        wasScanCompleted: "",
        requestWasCancelled: "",
      },
    }
  );

  const bundleScanResult = await bundles
    .aggregate([
      {
        $project: {
          _id: 0,
          bundleUrl: "$_id",
          scannerVersion: "2021-11-06",
          date: "$lastScannedDate",
          sizeBytes: 1,
          sizeBytesGzip: 1,
          numChars: 1,
          analysisMode: 1,
          sourceMapData: 1,
          matchedReleases: 1,
        },
      },
      {
        $out: "bundleScans",
      },
    ])
    .toArray();

  console.log(bundleScanResult);

  await bundles.updateMany(
    {},
    {
      $unset: {
        lastScannedDate: "",
        sizeBytes: "",
        sizeBytesGzip: "",
        numChars: "",
        analysisMode: "",
        sourceMapData: "",
        matchedReleases: "",
      },
    }
  );

  await urlScans.createIndexes([{ key: { url: 1 } }, { key: { source: 1 } }]);

  await bundleScans.createIndexes([
    { key: { bundleUrl: 1 } },
    { key: { date: 1 } },
    { key: { checksum: 1 } },
    { key: { scannerVersion: 1 } },
  ]);
};

export const down = async (db, client) => {
  const urls = db.collection("urls");

  await urls
    .aggregate([
      {
        $lookup: {
          from: "urlScans",
          localField: "_id",
          foreignField: "url",
          as: "urlScans",
        },
      },
      {
        $addFields: {
          urlScan: { $arrayElemAt: ["$urlScans", 0] },
        },
      },
      {
        $addFields: {
          lastScannedDate: "$urlScan.date",
          firstPartyBundleIds: "$urlScan.firstPartyBundleIds",
          thirdPartyBundleIds: "$urlScan.thirdPartyBundleIds",
          totalSizeBytes: "$urlScan.totalSizeBytes",
          totalSizeBytesGzip: "$urlScan.totalSizeBytesGzip",
          wasScanCompleted: { $literal: true },
        },
      },
      {
        $project: {
          urlScans: 0,
          urlScan: 0,
        },
      },
      {
        $merge: {
          into: "urls",
          on: "_id",
          whenMatched: "replace",
        },
      },
    ])
    .toArray();

  const urlScans = db.collection("urlScans");
  await urlScans.drop();
  const bundles = db.collection("bundles");

  const bundleScanResult = await bundles
    .aggregate([
      {
        $lookup: {
          from: "bundleScans",
          localField: "_id",
          foreignField: "bundleUrl",
          as: "bundleScans",
        },
      },
      {
        $addFields: {
          bundleScan: { $arrayElemAt: ["$bundleScans", 0] },
        },
      },
      {
        $addFields: {
          lastScannedDate: "$bundleScan.date",
          sizeBytes: "$bundleScan.sizeBytes",
          sizeBytesGzip: "$bundleScan.sizeBytesGzip",
          numChars: "$bundleScan.numChars",
          analysisMode: "$bundleScan.analysisMode",
          sourceMapData: "$bundleScan.sourceMapData",
          matchedReleases: "$bundleScan.matchedReleases",
        },
      },
      {
        $project: {
          bundleScans: 0,
          bundleScan: 0,
        },
      },
      {
        $merge: {
          into: "bundles",
          on: "_id",
          whenMatched: "replace",
        },
      },
    ])
    .toArray();
  console.log(bundleScanResult);
  const bundleScans = db.collection("bundleScans");
  await bundleScans.drop();
};
