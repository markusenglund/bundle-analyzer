import "dotenv/config";

console.log({ NODE_ENV: process.env.NODE_ENV, MONGO_URL: process.env.MONGO_URL });
// In this file you can configure migrate-mongo
const MONGO_URL =
  process.env.NODE_ENV === "production" ? process.env.MONGO_URL : process.env.MONGO_URL_DEV;
if (!MONGO_URL) {
  throw new Error("Mongodb URL env variable must be specified");
}
const config = {
  mongodb: {
    url: MONGO_URL,

    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      ignoreUndefined: true,
      serverSelectionTimeoutMS: 10 * 1000,
    },
  },

  // The migrations dir, can be an relative or absolute path. Only edit this when really necessary.
  migrationsDir: new URL("./migrations", import.meta.url).pathname,

  // The mongodb collection where the applied changes are stored. Only edit this when really necessary.
  changelogCollectionName: "changelog",

  // The file extension to create migrations and search for in migration dir
  migrationFileExtension: ".js",

  // Enable the algorithm to create a checksum of the file contents and use that in the comparison to determine
  // if the file should be run.  Requires that scripts are coded to be run multiple times.
  useFileHash: false,

  // Don't change this, unless you know what you're doing
  moduleSystem: "esm",
};

export default config;
