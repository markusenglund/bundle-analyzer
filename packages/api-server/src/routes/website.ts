import { bundleTypes, logger, mongo, utils } from "@bundle-scanner/common";
import { categorizeBundlesByParty } from "@bundle-scanner/scraper";
import { Server, IncomingMessage, ServerResponse } from "http";
import { FastifyInstance } from "fastify";
import { subDays } from "date-fns";
import { Type, Static } from "@sinclair/typebox";
import got from "got";
import pMap from "p-map";
import tldts from "tldts";
import { truncate, sumBy } from "lodash-es";
import {
  getBundleWithLatestScan,
  getBundlesWithLatestScan,
} from "../dbQueries/fetchBundlesWithLatestScan.js";
const { standardizeWebsiteUrl, standardizeBundleUrl, isValidUrl } = utils;

const { db } = mongo;

async function routes(
  fastify: FastifyInstance<Server, IncomingMessage, ServerResponse>
  // options: RouteOptions
): Promise<void> {
  const WebsiteParams = Type.Object({
    websiteUrl: Type.String(),
  });
  type WebsiteParamsType = Static<typeof WebsiteParams>;

  const WebsiteReply = Type.Object({
    data: bundleTypes.WebsiteWithMeta,
  });

  type WebsiteReplyType = Static<typeof WebsiteReply>;

  fastify.get<{ Params: WebsiteParamsType; Reply: WebsiteReplyType }>(
    "/api/website/:websiteUrl",
    {
      config: { rateLimit: { max: 10 } },
      schema: {
        params: WebsiteParams,
        response: {
          200: WebsiteReply,
        },
      },
    },
    async (request, reply) => {
      const { websiteUrl } = request.params;
      const timeStart = Date.now();

      let standardizedWebsiteUrl: string;
      try {
        standardizedWebsiteUrl = standardizeWebsiteUrl(websiteUrl);
      } catch {
        return reply.badRequest(`Invalid URL: '${websiteUrl}'`);
      }

      // Get the most recent URL created as a workaround against getting stale redirects - not really a solution
      const [url] = await db.urls
        .find({
          $or: [{ _id: standardizedWebsiteUrl }, { redirectedUrls: standardizedWebsiteUrl }],
        })
        .sort({ createdDate: -1 })
        .limit(1)
        .toArray();

      const twoWeeksAgoDate = subDays(new Date(), 14);

      const recentUrlScan =
        url &&
        (await db.urlScans.findOne({
          url: url._id,
          date: { $gt: twoWeeksAgoDate },
        }));

      if (recentUrlScan) {
        logger.info(`'${standardizedWebsiteUrl}' already analyzed - fetching cached results`);

        const bundleIds = [
          ...recentUrlScan.firstPartyBundleIds,
          ...recentUrlScan.thirdPartyBundleIds,
        ];

        const bundlesWithLatestScan = await getBundlesWithLatestScan(bundleIds, false);
        const bundlesById = Object.fromEntries(
          bundlesWithLatestScan.map((bundle) => [bundle._id, bundle])
        );

        const wasRedirected = url._id !== standardizedWebsiteUrl;

        return reply.send({
          data: {
            ...url,
            createdDate: url.createdDate.toISOString(),
            urlScan: {
              ...recentUrlScan,
              bundlesById,
              date: recentUrlScan.date.toISOString(),
            },
            meta: {
              standardizedWebsiteUrl,
              timeSpent: Date.now() - timeStart,
              wasCached: true,
              redirectedFrom: wasRedirected ? standardizedWebsiteUrl : undefined,
            },
          },
        });
      }

      const analysisRequestUrl = `${
        process.env.ANALYSIS_SERVER_URL
      }/api/website/${encodeURIComponent(standardizedWebsiteUrl)}`;

      let analyzedWebsite: bundleTypes.WebsiteType;
      try {
        ({
          body: { data: analyzedWebsite },
        } = await got<bundleTypes.WebsiteReplyType>(analysisRequestUrl, {
          responseType: "json",
          retry: 0,
          timeout: 40_000,
        }));
      } catch (err) {
        if (err.response) {
          const { body, statusCode } = err.response;
          throw fastify.httpErrors.createError(statusCode, body);
        } else {
          throw err;
        }
      }
      const wasRedirected = analyzedWebsite._id !== standardizedWebsiteUrl;

      const { urlScan, redirectedUrls = [], ...urlData } = analyzedWebsite;

      reply.send({
        data: {
          ...analyzedWebsite,
          meta: {
            standardizedWebsiteUrl,
            timeSpent: Date.now() - timeStart,
            wasCached: false,
            redirectedFrom: wasRedirected ? standardizedWebsiteUrl : undefined,
          },
        },
      });
      await db.urls.updateOne(
        { _id: analyzedWebsite._id },
        {
          $setOnInsert: { ...urlData, createdDate: new Date() },
          $addToSet: { redirectedUrls: { $each: redirectedUrls } },
        },
        { upsert: true }
      );

      const { bundlesById, ...urlScanData } = urlScan;
      await db.urlScans.insertOne({ ...urlScanData, date: new Date(urlScan.date) });
      const { domain, host, _id: pageUrl } = urlData;
      for (const bundle of Object.values(bundlesById)) {
        const { meta, bundleScan, domains, hosts, pageUrls, ...bundleRest } = bundle;
        if (meta.wasCached) {
          await db.bundles.updateOne(
            { _id: bundle._id },
            {
              $addToSet: { domains: domain, hosts: host, pageUrls: pageUrl },
            }
          );
        } else {
          await db.bundles.updateOne(
            { _id: bundle._id },
            {
              $set: bundleRest,
              $setOnInsert: { createdDate: new Date() },
              $addToSet: {
                domains: domain,
                hosts: host,
                pageUrls: pageUrl,
              },
            },
            { upsert: true }
          );
          await db.bundleScans.insertOne({ ...bundleScan, date: new Date(bundleScan.date) });
        }
      }
    }
  );

  const WebsiteBundlesBody = Type.Object({
    bundles: Type.Array(
      Type.Object({
        url: Type.String(),
        sourceMapUrl: Type.Optional(Type.String()),
        numChars: Type.Number(),
        code: Type.String(),
      })
    ),
    websiteUrl: Type.String(),
    faviconUrl: Type.Optional(Type.String()),
  });

  type WebsiteBundlesBodyType = Static<typeof WebsiteBundlesBody>;

  // Route used by extension (where bundle files are already collected)
  fastify.post<{ Body: WebsiteBundlesBodyType; Reply: WebsiteReplyType }>(
    "/api/website/bundles",
    {
      config: { rateLimit: { max: 10 } },
      schema: {
        body: WebsiteBundlesBody,
        response: {
          200: WebsiteReply,
        },
      },
    },
    async (request, reply) => {
      const timeStart = Date.now();
      const { bundles, websiteUrl, faviconUrl } = request.body;

      let standardizedWebsiteUrl: string;
      try {
        standardizedWebsiteUrl = standardizeWebsiteUrl(websiteUrl);
      } catch {
        return reply.badRequest(`Invalid URL: '${websiteUrl}'`);
      }
      const { domain, hostname: host } = tldts.parse(standardizedWebsiteUrl) as {
        domain: string;
        hostname: string;
      };

      logger.info(`[${websiteUrl}]: Fetching data from ${bundles.length} bundles`);
      const bundlesWithLatestScan = (
        await pMap(
          bundles,
          async (bundle) => {
            if (!isValidUrl(bundle.url)) {
              logger.warn(`URL ${bundle.url} is invalid`);
              return;
            }
            const bundleId = standardizeBundleUrl(bundle.url);

            // Check first if the bundle exists in db
            const alreadyAnalyzedBundleWithLatestScan = await getBundleWithLatestScan(bundleId);

            if (alreadyAnalyzedBundleWithLatestScan) {
              return { ...alreadyAnalyzedBundleWithLatestScan, meta: { wasCached: true } };
            }

            // Send to analysis server for scanning
            const analysisRequestUrl = `${process.env.ANALYSIS_SERVER_URL}/api/bundle`;

            try {
              const {
                body: { data: bundleWithScan },
              } = await got.post<bundleTypes.BundleResponseType>(analysisRequestUrl, {
                json: { bundle: { ...bundle, _id: bundleId } },
                responseType: "json",
                retry: 0,
                timeout: 10_000,
              });

              return {
                ...bundleWithScan,
                domains: [domain],
                hosts: [host],
                pageUrls: [standardizedWebsiteUrl],
                meta: { wasCached: false },
              };
            } catch (err) {
              logger.warn(`Fetching bundle '${truncate(bundleId, { length: 80 })}' failed`);
            }
          },
          { concurrency: 10 }
        )
      ).filter(<B>(bundle: B | undefined): bundle is B => bundle != null);

      const { firstPartyBundles, thirdPartyBundles } = categorizeBundlesByParty(
        bundlesWithLatestScan.map(({ _id, fullUrl }) => ({ _id, initiatorUrlChain: [], fullUrl })),
        standardizedWebsiteUrl,
        domain
      );

      const firstPartyBundleIds = firstPartyBundles.map(({ _id }) => _id);
      const thirdPartyBundleIds = thirdPartyBundles.map(({ _id }) => _id);

      const totalSizeBytes = sumBy(bundlesWithLatestScan, "bundleScan.sizeBytes");
      const totalSizeBytesGzip = sumBy(bundlesWithLatestScan, "bundleScan.sizeBytesGzip");

      const analyzedWebsite: bundleTypes.WebsiteWithMetaType = {
        _id: standardizedWebsiteUrl,
        faviconUrl,
        host,
        domain,
        redirectedUrls: [],
        createdDate: new Date().toISOString(),
        urlScan: {
          url: standardizedWebsiteUrl,
          date: new Date().toISOString(),
          source: "extension",
          bundlesById: Object.fromEntries(
            bundlesWithLatestScan.map((bundle) => [bundle._id, bundle])
          ),
          totalSizeBytes,
          totalSizeBytesGzip,
          firstPartyBundleIds,
          thirdPartyBundleIds,
          failedBundleIds: [],
        },
        meta: {
          timeSpent: Date.now() - timeStart,
          standardizedWebsiteUrl,
          redirectedFrom: undefined,
          wasCached: false,
        },
      };

      console.log(analyzedWebsite.urlScan.bundlesById);

      reply.send({ data: analyzedWebsite });

      const { urlScan, redirectedUrls = [], ...urlData } = analyzedWebsite;

      await db.urls.updateOne(
        { _id: analyzedWebsite._id },
        {
          $setOnInsert: { ...urlData, createdDate: new Date() },
          $addToSet: { redirectedUrls: { $each: redirectedUrls } },
        },
        { upsert: true }
      );

      const { bundlesById, ...urlScanData } = urlScan;
      await db.urlScans.insertOne({ ...urlScanData, date: new Date(urlScan.date) });
      for (const bundle of Object.values(bundlesById)) {
        const { meta, bundleScan, hosts, domains, pageUrls, ...bundleRest } = bundle;
        if (meta.wasCached) {
          await db.bundles.updateOne(
            { _id: bundle._id },
            {
              $addToSet: { domains: domain, hosts: host, pageUrls: urlData._id },
            }
          );
        } else {
          await db.bundles.updateOne(
            { _id: bundle._id },
            {
              $set: bundleRest,
              $setOnInsert: { createdDate: new Date() },
              $addToSet: {
                domains: domain,
                hosts: host,
                pageUrls: urlData._id,
              },
            },
            { upsert: true }
          );
          await db.bundleScans.insertOne({ ...bundleScan, date: new Date(bundleScan.date) });
        }
      }
    }
  );
}

export default routes;
