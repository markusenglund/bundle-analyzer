import got from "got";
import { logger, utils, mongo, bundleTypes } from "@bundle-scanner/common";
import { subDays } from "date-fns";
import { Type, Static } from "@sinclair/typebox";
import { FastifyInstance } from "fastify";
import { getBundlesWithLatestScan } from "../dbQueries/fetchBundlesWithLatestScan.js";

const { standardizeBundleUrl } = utils;

const { db } = mongo;

async function routes(fastify: FastifyInstance): Promise<void> {
  const BundleParams = Type.Object({
    bundleUrl: Type.String(),
  });
  type BundleParamsType = Static<typeof BundleParams>;

  const BundleResponse = Type.Object({
    data: Type.Object({
      bundleScan: bundleTypes.BundleScan,
      _id: Type.String(),
      fullUrl: Type.String(),
      thirdPartyWebData: Type.Optional(
        Type.Object({
          name: Type.String(),
          categories: Type.Array(Type.String()),
        })
      ),
      domains: Type.Array(Type.String()),
      hosts: Type.Array(Type.String()),
      pageUrls: Type.Array(Type.String()),
      meta: Type.Object({
        wasCached: Type.Boolean(),
      }),
    }),
  });
  type BundleResponseType = Static<typeof BundleResponse>;

  fastify.get<{ Params: BundleParamsType; Response: BundleResponseType }>(
    "/api/bundle/:bundleUrl",
    {
      schema: {
        params: BundleParams,
        response: {
          200: BundleResponse,
        },
      },
      config: { rateLimit: { max: 20 } },
    },
    async (request) => {
      const bundleUrl = standardizeBundleUrl(request.params.bundleUrl);
      logger.info(`Handling request for individual bundle: '${bundleUrl}'`);

      // Check if bundle already exists in db
      const sixWeeksAgoDate = subDays(new Date(), 42);
      const savedBundleScan = await db.bundleScans.findOne({
        bundleUrl,
        date: { $gt: sixWeeksAgoDate },
      });

      if (savedBundleScan) {
        const bundle = await db.bundles.findOne({ _id: bundleUrl });
        if (!bundle) {
          throw new Error(
            `Bundlescan for bundle URL '${bundleUrl}' unexpectedly didn't have a bundle associated with it!`
          );
        }
        return { data: { ...bundle, bundleScan: savedBundleScan, meta: { wasCached: true } } };
      }

      const analysisRequestUrl = `${
        process.env.ANALYSIS_SERVER_URL
      }/api/bundle/${encodeURIComponent(bundleUrl)}`;

      try {
        const {
          body: {
            data: { bundleScan, ...partialBundle },
          },
        } = await got<bundleTypes.BundleResponseType>(analysisRequestUrl, {
          responseType: "json",
          retry: 0,
          timeout: 30_000,
        });

        db.bundleScans.insertOne({ ...bundleScan, date: new Date(bundleScan.date) });
        let bundle = await db.bundles.findOne({
          _id: partialBundle._id,
        });
        if (!bundle) {
          bundle = {
            ...partialBundle,
            domains: [],
            hosts: [],
            pageUrls: [],
            createdDate: new Date(),
          };
          db.bundles.insertOne(bundle);
        }

        return {
          data: {
            ...bundle,
            bundleScan,
            meta: { wasCached: false },
          },
        };
      } catch (err) {
        if (err.response) {
          const { body, statusCode } = err.response;
          throw fastify.httpErrors.createError(statusCode, body);
        } else {
          throw err;
        }
      }
    }
  );

  fastify.get<{ Params: { fullBundleUrl: string } }>(
    "/api/bundle/:fullBundleUrl/code",
    { config: { rateLimit: { max: 10 } } },
    async (request, reply) => {
      const { fullBundleUrl } = request.params;

      const codeStream = got.stream(fullBundleUrl, { decompress: false });
      reply.send(codeStream);
    }
  );

  const AnalyzedBundlesQuery = Type.Object({
    bundleIds: Type.String(),
  });
  type AnalyzedBundlesQueryType = Static<typeof AnalyzedBundlesQuery>;
  const AnalyzedBundlesResponse = Type.Object({
    data: Type.Array(bundleTypes.BundleWithScan),
  });
  type AnalyzedBundlesResponseType = Static<typeof AnalyzedBundlesResponse>;

  fastify.get<{
    Querystring: AnalyzedBundlesQueryType;
    Response: AnalyzedBundlesResponseType;
  }>(
    "/api/bundle/analyzed",
    {
      schema: {
        querystring: AnalyzedBundlesQuery,
        response: {
          200: AnalyzedBundlesResponse,
        },
      },
      config: { rateLimit: { max: 20 } },
    },
    async (request) => {
      const { bundleIds: bundleIdsString } = request.query;

      const bundleIds = bundleIdsString.split(",");

      const bundlesWithScan = await getBundlesWithLatestScan(bundleIds);

      return { data: bundlesWithScan };
    }
  );
}

export default routes;
