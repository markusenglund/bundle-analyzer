import { logger, mongo } from "@bundle-scanner/common";
import Cache from "node-cache";
import { Static, Type } from "@sinclair/typebox";

const { db } = mongo;

const cache = new Cache({ stdTTL: 30 });

const RecentSearchesResponse = Type.Object({
  data: Type.Array(Type.String()),
});
const ErrorResponse = Type.Object({
  error: Type.String(),
  message: Type.String(),
  statusCode: Type.String(),
});

async function routes(fastify, options): void {
  fastify.get(
    "/api/recentSearches",
    {
      config: {
        rateLimit: { max: 100 },
      },
      schema: {
        response: {
          200: RecentSearchesResponse,
          500: ErrorResponse,
        },
      },
    },
    async (request, reply) => {
      if (cache.get("recentSearches")) {
        return { data: cache.get("recentSearches") };
      }
      const urls = await db.urls
        .find()
        .sort({ lastScannedDate: -1 })
        .limit(5)
        .project({ _id: 1 })
        .toArray();
      const recentSearches = urls.map(({ _id }) => _id);

      cache.set("recentSearches", recentSearches);

      return { data: recentSearches };
    }
  );
}

export default routes;
