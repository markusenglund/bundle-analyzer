import { logger, mongo, tokenUtils } from "@bundle-scanner/common";

const { db } = mongo;

async function routes(fastify, options): void {
  fastify.get(
    "/api/library/:libraryId",
    { config: { rateLimit: { max: 100 } } },
    async (request, reply) => {
      const { libraryId } = request.params;

      const library = await db.libraries.findOne({ _id: libraryId });

      const bundles = await db.bundles
        .find({ "sourceMapData.libraryIds": libraryId })
        .project({
          _id: 1,
          domains: 1,
          pageUrls: 1,
          sizeBytes: 1,
          sizeBytesGzip: 1,
          "sourceMapData.url": 1,
        })
        .limit(50)
        .toArray();

      return { ...library, bundles };
    }
  );

  // fastify.get("/api/library/autocomplete", async (request, reply) => {
  //   const { q: query } = request.query;
  //   if (!query) {
  //     // TODO: Handle incorrect request
  //   }

  //   const regExp = new RegExp(`^${escapeRegExp(query)}`);

  //   const libraries = await db.libraries
  //     .find({ _id: { $regex: regExp }, sourceMapFrequency: { $exists: true } })
  //     .sort({ sourceMapFrequency: -1 })
  //     .limit(5)
  //     .toArray();
  //   console.log({ libraries });

  //   return JSON.stringify(libraries);
  // });
}

export default routes;
