import Fastify, { FastifyInstance } from "fastify";
import helmet from "fastify-helmet";
import sensible from "fastify-sensible";
import serveStatic from "fastify-static";
import cors from "fastify-cors";
import rateLimit from "fastify-rate-limit";
import { truncate } from "lodash-es";
import website from "./routes/website.js";
import bundle from "./routes/bundle.js";
import library from "./routes/library.js";
import recentSearches from "./routes/recentSearches.js";
import { mongo, logger } from "@bundle-scanner/common";

const fastify = Fastify({
  maxParamLength: 10000,
  disableRequestLogging: true,
  bodyLimit: 30_000_000,
  connectionTimeout: 60_000,
  ajv: {
    customOptions: {
      removeAdditional: true,
      useDefaults: true,
      coerceTypes: true,
      allErrors: false,
      nullable: false,
    },
    plugins: [],
  },
});

fastify.addHook("onResponse", (request, reply, done) => {
  const { statusCode } = reply.raw;
  if (statusCode < 400) {
    const message = `${statusCode} - ${request.method} ${truncate(request.raw.url, {
      length: 200,
    })}`;
    logger.info(message);
  }

  done();
});

fastify.addHook("onTimeout", (request, reply, done) => {
  logger.error(
    `${request.method} ${truncate(request.raw.url, {
      length: 200,
    })}: Request timed out!`
  );
  done();
});

fastify.setErrorHandler((error, request, reply) => {
  const { statusCode } = reply.raw;
  const message = `${statusCode} - ${request.method} ${truncate(request.raw.url, {
    length: 200,
  })}: ${error.message}`;
  if (statusCode >= 500) {
    logger.error(message);
    logger.error(error);
  } else if (statusCode >= 400) {
    logger.warn(message);
  } else {
    logger.error(message);
    logger.error(error);
  }

  if (statusCode === 500) {
    reply.send(new Error("Something went wrong"));
  } else {
    reply.send(error);
  }
});

fastify.register(helmet, {
  contentSecurityPolicy: {
    useDefaults: true,
    directives: {
      defaultSrc: ["'self'", "unpkg.com", "plausible.io", "api.npms.io", "*.sentry.io"],
      imgSrc: ["*", "data:"],
    },
  },
});

fastify.register(cors);

fastify.register(sensible, { errorHandler: false });

fastify.register(serveStatic, {
  root: new URL("../../client/dist/public", import.meta.url).pathname,
  prefix: "/static/",
});

if (process.env.NODE_ENV === "production") {
  fastify.register(rateLimit, { global: false });
  fastify.get("/about", (req, reply) => reply.sendFile("about.html"));
  fastify.get("/libraries", (req, reply) => reply.sendFile("libraries.html"));
  fastify.get("/", (req, reply) => reply.sendFile("main.html"));
}
fastify.get("*", (req, reply) => reply.sendFile("index.html"));
// TODO: Use fastify-autoload for routes

fastify.register(bundle);
fastify.register(website);
fastify.register(library);
fastify.register(recentSearches);

mongo.connect().then(() => {
  const port = process.env.PORT || "1337";
  const host = process.env.HOST || "0.0.0.0";
  fastify.listen(port, host).catch((err) => {
    fastify.log.error(err);
    process.exit(1);
  });
});
