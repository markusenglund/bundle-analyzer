import { subDays } from "date-fns";
import pMap from "p-map";
import { mongo, bundleTypes } from "@bundle-scanner/common";
const { db } = mongo;

export async function getBundleWithLatestScan(
  bundleId: string,
  onlyRecentBundle = true
): Promise<bundleTypes.BundleWithScanType | void> {
  const sixWeeksAgoDate = subDays(new Date(), 42);

  const findQuery: { bundleUrl: string; date?: { $gt: Date } } = {
    bundleUrl: bundleId,
  };
  if (onlyRecentBundle) {
    findQuery.date = { $gt: sixWeeksAgoDate };
  }

  const latestBundleScan = (
    await db.bundleScans.find(findQuery).sort({ date: -1 }).limit(1).toArray()
  )[0];
  if (!latestBundleScan) return;
  const bundle = await db.bundles.findOne({ _id: bundleId });
  if (!bundle) {
    throw new Error(
      `Bundlescan for bundle URL '${bundleId}' unexpectedly didn't have a bundle associated with it!`
    );
  }

  const bundleWithScan = { ...bundle, bundleScan: latestBundleScan, meta: { wasCached: true } };
  return bundleWithScan;
}

export async function getBundlesWithLatestScan(
  bundleIds: string[],
  onlyRecentBundles = true
): Promise<bundleTypes.BundleWithScanType[]> {
  const bundlesWithScan = (
    await pMap(bundleIds, (id) => getBundleWithLatestScan(id, onlyRecentBundles), {
      concurrency: 10,
    })
  ).filter(Boolean);

  return bundlesWithScan;
}
