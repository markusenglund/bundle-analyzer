declare module "source-map-url" {
  export function getFrom(code: string): string | undefined;
}
