import puppeteer from "puppeteer";
import { Cluster } from "puppeteer-cluster";
import puppeteerExtra from "puppeteer-extra";
import StealthPlugin from "puppeteer-extra-plugin-stealth";
import { isBundleThirdParty } from "./partyCategorizer.js";
import { logger, utils } from "@bundle-scanner/common";

const { standardizeWebsiteUrl } = utils;

puppeteerExtra.use(StealthPlugin());

const noop = () => {};

type TaskReturnData = {
  bundles: { url: string; text: string }[];
  finalUrl: string;
  redirectChain: puppeteer.HTTPRequest[];
  urlInitiatorUrlMap: Map<string, string>;
  faviconUrl?: string;
};
export type ClusterType = Cluster<string, TaskReturnData>;

const ALLOWED_RESOURCE_TYPES = new Set(["document", "script", "other"]);

const initializeCluster = async ({
  concurrency,
  timeout,
  includeThirdPartyBundles,
  includeFaviconUrl,
}: {
  concurrency: number;
  timeout: number;
  includeThirdPartyBundles: boolean;
  includeFaviconUrl: boolean;
}): Promise<Cluster<string, TaskReturnData>> => {
  const cluster: Cluster<string, TaskReturnData> = await Cluster.launch({
    concurrency: Cluster.CONCURRENCY_PAGE,
    maxConcurrency: concurrency,
    puppeteer: puppeteerExtra,
    timeout: timeout + 2 * 1000,
    puppeteerOptions: {
      // @ts-ignore
      args: ["--no-sandbox", "--disable-setuid-sandbox"],
    },
    retryLimit: 1,
  });
  await cluster.task(async ({ page, data: normalizedWebsiteUrl }) => {
    // Disable cache to make sure all js files are handled the same way on subsequent requests
    await page.setCacheEnabled(false);
    await page.setRequestInterception(true);
    const bundles: { url: string; text: string }[] = [];

    page.on("request", (request) => {
      const url = request.url();
      const resourceType = request.resourceType();
      const isThirdParty =
        isBundleThirdParty(url, normalizedWebsiteUrl) || url.includes("/wp-content/");

      if (ALLOWED_RESOURCE_TYPES.has(resourceType) && (includeThirdPartyBundles || !isThirdParty)) {
        request.continue();
      } else {
        request.abort();
      }
    });

    page.on("response", async (response) => {
      const contentType = response.headers()["content-type"];
      const isJsMimeType = [
        "application/javascript",
        "application/x-javascript",
        "text/javascript",
        "application/ecmascript",
        "text/ecmascript",
      ].some((mimeType) => contentType?.startsWith(mimeType));

      const resourceType = response.request().resourceType();

      if (response.ok() && (isJsMimeType || resourceType === "script")) {
        const url = response.url();
        try {
          const content = await response.text();
          bundles.push({ url, text: content });
        } catch (err) {
          logger.warn(`[${url}]: Failed to read response body, skipping...`);
        }
      }
    });

    const cdpClient = await page.target().createCDPSession();
    await cdpClient.send("Network.enable");
    const urlInitiatorUrlMap: Map<string, string> = new Map();
    cdpClient.on("Network.requestWillBeSent", ({ request, type, initiator }) => {
      const requestUrl: string = request.url;
      if (type === "Script" || requestUrl.endsWith(".js")) {
        const initiatorUrl: string | undefined =
          initiator?.url ?? initiator?.stack?.callFrames?.find(({ url }) => !!url)?.url;
        if (initiatorUrl) {
          urlInitiatorUrlMap.set(
            standardizeWebsiteUrl(requestUrl),
            standardizeWebsiteUrl(initiatorUrl)
          );
        }
      }
    });

    // Workaround to prevent popups from opening to solve issue where popups would cause crash in puppeteer-extra
    await page.evaluateOnNewDocument(() => {
      // @ts-ignore
      window.open = noop;
    });

    let response: puppeteer.HTTPResponse | undefined;
    try {
      response = await page.goto(normalizedWebsiteUrl, {
        timeout,
        waitUntil: "networkidle2",
      });
    } catch (err) {
      if (err instanceof puppeteer.errors.TimeoutError) {
        logger.warn(`[${normalizedWebsiteUrl}]: ${err.message}`);
      } else {
        throw err;
      }
    }
    const redirectChain = response?.request().redirectChain() ?? [];
    const faviconSelectors = [
      `link[rel="icon"]`,
      `link[rel="shortcut icon"]`,
      `link[rel="apple-touch-icon"]`,
    ];
    let faviconUrl: string | undefined;
    if (includeFaviconUrl) {
      faviconUrl = await page.evaluate((selectors) => {
        for (const faviconSelector of selectors) {
          const faviconHref: string | undefined = document.querySelector(faviconSelector)?.href;
          if (faviconHref) {
            try {
              if (new URL(faviconHref).protocol === "data:") {
                continue;
              }
            } catch {
              continue;
            }
            return faviconHref;
          }
        }
      }, faviconSelectors);

      if (!faviconUrl) {
        faviconUrl = `${new URL(page.url()).origin}/favicon.ico`;
      }
    }
    const finalUrl = standardizeWebsiteUrl(page.url());
    return { bundles, finalUrl, redirectChain, urlInitiatorUrlMap, faviconUrl };
  });

  return cluster;
};

export default initializeCluster;
