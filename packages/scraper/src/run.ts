import { mongo } from "@bundle-scanner/common";
const { connect } = mongo;

import scrape from "./scrapingJob/scrape.js";

(async () => {
  await connect();
  await scrape();
})();
