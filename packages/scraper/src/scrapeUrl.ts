import normalizeUrl from "normalize-url";
import { uniqBy, uniq } from "lodash-es";
import gzipSize from "gzip-size";
import tldts from "tldts";
import { logger, utils } from "@bundle-scanner/common";
import { ClusterType } from "./initializeCluster.js";

const { standardizeWebsiteUrl, standardizeBundleUrl } = utils;

const scrapeUrl = async (
  cluster: ClusterType,
  websiteUrl: string,
  rank?: number
): Promise<{
  bundles: {
    _id: string;
    code: string;
    sizeBytes: number;
    sizeBytesGzip: number;
    numChars: number;
    initiatorUrlChain: string[];
    fullUrl: string;
  }[];
  finalUrl: string;
  faviconUrl?: string;
  host: string;
  domain: string;
  redirectedUrls: string[];
}> => {
  logger.info(`Scraping ${websiteUrl} ${rank != null ? rank : ""}`);

  const normalizedWebsiteUrl = normalizeUrl(websiteUrl, {
    stripHash: true,
    removeQueryParameters: true,
    stripWWW: false,
  });

  const { bundles, finalUrl, redirectChain, urlInitiatorUrlMap, faviconUrl } =
    await cluster.execute(normalizedWebsiteUrl);

  const redirectedUrls = uniq([
    standardizeWebsiteUrl(normalizedWebsiteUrl),
    ...redirectChain.map((request) => standardizeWebsiteUrl(request.url())),
  ]).filter((url) => url !== finalUrl);

  const refinedBundles = await Promise.all(
    bundles.map(async (bundle) => {
      const sizeBytes = Buffer.byteLength(bundle.text);
      const sizeBytesGzip = await gzipSize(bundle.text);
      const numChars = bundle.text.length;
      const fullUrl = bundle.url;
      const _id = standardizeBundleUrl(bundle.url);

      let initiatorUrl = urlInitiatorUrlMap.get(_id);
      const initiatorUrlChain = [];
      while (initiatorUrl) {
        initiatorUrlChain.push(initiatorUrl);
        const nextInitiatorUrl = urlInitiatorUrlMap.get(initiatorUrl);
        if (nextInitiatorUrl && initiatorUrlChain.includes(nextInitiatorUrl)) {
          break;
        }
        initiatorUrl = nextInitiatorUrl;
      }

      return {
        code: bundle.text,
        _id,
        sizeBytes,
        sizeBytesGzip,
        numChars,
        initiatorUrlChain,
        fullUrl,
      };
    })
  );

  const uniqueFilteredBundles = uniqBy(
    // ! This filter belongs in a different function
    refinedBundles.filter(({ sizeBytes }) => sizeBytes > 10 * 1024),
    "_id"
  );

  const { domain, hostname: host } = tldts.parse(finalUrl) as { domain: string; hostname: string };

  return { bundles: uniqueFilteredBundles, finalUrl, faviconUrl, host, domain, redirectedUrls };
};

export default scrapeUrl;
