import parseDataUrl from "data-urls";

function decodeBase64Url(url: string): string {
  const cleanedUrl = url.split("\\n")[0];
  const data = parseDataUrl(cleanedUrl);
  if (!data || !data.body) {
    throw new Error(`parseDataUrl function returned '${data}'`);
  }
  if (data.mimeType.subtype !== "json") {
    throw new Error(`Mime-type '${data.mimeType.subtype}' was not json`);
  }
  const sourceMapText = data.body.toString();
  return sourceMapText;
}

export default decodeBase64Url;
