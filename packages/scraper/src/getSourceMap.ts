import got from "got";
import sourceMapUrlFns from "source-map-url";
import Piscina from "piscina";
import prettyBytes from "pretty-bytes";
import { logger, SourceMap } from "@bundle-scanner/common";

// Run base64decoder as worker thread to prevent hogging the main thread
const decodeBase64UrlWorker = new Piscina({
  filename: new URL("../dist/decodeBase64Url.js", import.meta.url).href,
  maxThreads: 1,
});

export async function getSourceMapFromBundle(bundle: {
  _id: string;
  code: string;
  fullUrl: string;
}): Promise<{ sourceMap: SourceMap; sourceMapUrl: string | undefined } | undefined> {
  const { fullUrl: fullBundleUrl, code } = bundle;
  const relativeSourceMapUrl = sourceMapUrlFns.getFrom(code);
  if (!relativeSourceMapUrl) return;
  const results = await getSourceMapFromUrl(relativeSourceMapUrl, fullBundleUrl);
  return results;
}

export async function getSourceMapFromUrl(
  relativeSourceMapUrl: string,
  fullBundleUrl: string
): Promise<{ sourceMap: SourceMap; sourceMapUrl: string | undefined } | undefined> {
  let absoluteSourceMapUrl, protocol;
  try {
    ({ href: absoluteSourceMapUrl, protocol } = new URL(relativeSourceMapUrl, fullBundleUrl));
  } catch (err) {
    logger.warn(
      `[${fullBundleUrl}]: Source map URL '${relativeSourceMapUrl.slice(
        0,
        100
      )}' is not a valid URL.`
    );
    return;
  }
  if (!["http:", "https:", "data:"].includes(protocol)) {
    logger.warn(
      `[${fullBundleUrl}]: Source map URL '${absoluteSourceMapUrl.slice(
        0,
        100
      )}' has unsported protocol '${protocol}'`
    );
    return;
  }

  let sourceMapText: string;
  if (protocol === "data:") {
    const sourceMapUrlMaxSize = 4_000_000;
    if (absoluteSourceMapUrl.length > sourceMapUrlMaxSize) {
      logger.warn(
        `Source map data URL for '${fullBundleUrl}' was too large: ${prettyBytes(
          absoluteSourceMapUrl.length
        )}, bailing out...`
      );
      return;
    }
    logger.info(`Decoding data URL source map for ${fullBundleUrl})`);
    try {
      sourceMapText = await decodeBase64UrlWorker.run(absoluteSourceMapUrl);
    } catch (err) {
      logger.warn(
        `[${fullBundleUrl}]: Failed to parse source map as base64 URL, error: '${err.message}'`
      );
      return;
    }
  } else {
    try {
      const res = await got(absoluteSourceMapUrl, {
        timeout: {
          connect: 1_000,
          response: 3_000,
          request: 8_000,
        },
        retry: 0,
      });

      sourceMapText = res.body;
    } catch (err) {
      logger.warn(
        `[${fullBundleUrl}]: Request to source map ${absoluteSourceMapUrl} failed with error '${err.message}'`
      );
      return;
    }
  }

  try {
    const sourceMap: SourceMap = JSON.parse(sourceMapText);
    return {
      sourceMap,
      sourceMapUrl: ["http:", "https:"].includes(protocol) ? absoluteSourceMapUrl : undefined,
    };
  } catch (err) {
    logger.warn(
      `[${fullBundleUrl}]: Could not parse URL ${absoluteSourceMapUrl?.slice(
        0,
        100
      )} as JSON; Text: ${sourceMapText?.slice(0, 100)}...`
    );
  }
}
