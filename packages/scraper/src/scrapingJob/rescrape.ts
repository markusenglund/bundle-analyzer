import { logger, mongo } from "@bundle-scanner/common";
import pMap from "p-map";
import scrapeWebsite from "./scrapeWebsite.js";

const { db, connect } = mongo;

async function rescrape(): Promise<never> {
  logger.info("Starting scraping");
  await connect();

  const urlsToSkip = new Set(
    (await db.sourceMappedWebsites.find().project({ url: 1 }).toArray()).map(({ url }) => url)
  );

  const sourceMappedWebsites = await db.websites
    .find({ scripts: { $ne: [] }, failedScrape: { $ne: true } })
    .project({ url: 1, _id: 0 })
    .toArray();

  const websitesToRescrape = sourceMappedWebsites.filter(({ url }) => !urlsToSkip.has(url));

  logger.info(
    `Preparing to scrape ${websitesToRescrape.length} websites (skipping ${urlsToSkip.size} websites due to already being rescraped)`
  );

  await pMap(
    websitesToRescrape,
    async ({ url }, i) => {
      const rank = urlsToSkip.size + 1 + i;
      const websiteData = await scrapeWebsite(url, i);
      if (websiteData) {
        const { failedScrape, scripts } = websiteData;
        await db.sourceMappedWebsites.insertOne({
          url,
          scripts,
          failedScrape,
          rank,
        });
      }
    },
    { concurrency: 50 }
  );

  logger.info("Done");
  process.exit();
}

rescrape();
