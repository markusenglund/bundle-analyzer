import { pipeline } from "stream/promises";
import through2Concurrent from "through2-concurrent";
import { parseFile } from "@fast-csv/parse";
import { sourceMapUtils, logger, mongo } from "@bundle-scanner/common";
import pMap from "p-map";
import tldts from "tldts";

const { extractLibrariesFromSourceMap, extractIntervalsFromSourceMap } = sourceMapUtils;

const { db, connect } = mongo;

const saveUrlData = async (cluster, websiteUrl: string, rank?: number) => {
  let bundles,
    finalUrl: string,
    faviconUrl: string,
    host: string,
    domain: string,
    redirectedUrls: string[];
  try {
    ({ bundles, finalUrl, faviconUrl, host, domain, redirectedUrls } = await scrapeUrl(
      cluster,
      websiteUrl,
      rank
    ));
  } catch (err) {
    await db.urls.updateOne({ _id: websiteUrl }, { $set: { wasScanCompleted: false } });
    return;
  }

  const { firstPartyBundles, thirdPartyBundles } = categorizeBundlesByParty(
    bundles,
    finalUrl,
    domain
  );
  const firstPartyBundleIds = firstPartyBundles.map(({ _id }) => _id);
  const thirdPartyBundleIds = thirdPartyBundles.map(({ _id }) => _id);
  const sourceMappedBundleUrls = [];
  await pMap(
    bundles,
    async (bundle) => {
      try {
        const data = await getSourceMap(bundle);
        if (!data) return;
        const { sourceMap, sourceMapUrl } = data;
        if (Number(sourceMap.version) !== 3) return;

        const { libraryIds, modulePaths } = extractLibrariesFromSourceMap(sourceMap);
        const intervals = await extractIntervalsFromSourceMap(sourceMap, bundle.code);

        sourceMappedBundleUrls.push(bundle._id);

        const { sizeBytes, sizeBytesGzip, numChars, _id } = bundle;

        const { domain: bundleDomain } = tldts.parse(_id);

        await db.bundles.updateOne(
          { _id },
          {
            $set: {
              _id,
              bundleDomain,
              numChars,
              sizeBytes,
              sizeBytesGzip,
              sourceMapData: {
                url: sourceMapUrl,
                libraryIds,
                modulePaths,
                intervals,
              },
              // matchedReleases,
              // matchedIntervals,
              // lastScannedDate: new Date(),
            },
            $addToSet: { domains: domain, hosts: host, pageUrls: finalUrl },
            $setOnInsert: { createdDate: new Date() },
          },
          { upsert: true }
        );
      } catch (err) {
        logger.warn(
          `[${websiteUrl}]: Something went wrong when reading sourcemaps from ${bundle._id} - '${err.message}'`
        );
      }
    },
    { concurrency: 4 }
  );

  if (sourceMappedBundleUrls.length > 0) {
    logger.info(`[${finalUrl}]: Found ${sourceMappedBundleUrls.length} bundles with source maps.`);
  }

  await db.urls.updateOne(
    { _id: finalUrl },
    {
      $set: {
        _id: finalUrl,
        host,
        domain,
        faviconUrl,
        firstPartyBundleIds,
        thirdPartyBundleIds,
      },
      $setOnInsert: { createdDate: new Date(), openPageRank: rank },
      $addToSet: { redirectedUrls: { $each: redirectedUrls } },
    },
    { upsert: true }
  );
};

async function scrape(): Promise<never> {
  logger.info("Starting scraping");

  const scrapedUrlsCursor = db.urls.find().project({ _id: 1, redirectedUrls: 1 });

  const scrapedUrls: Set<string> = new Set();
  const scrapedDomains: Set<string> = new Set();
  for await (const { _id, redirectedUrls, domain } of scrapedUrlsCursor) {
    scrapedUrls.add(_id);
    scrapedDomains.add(domain);
    for (const url of redirectedUrls) {
      scrapedUrls.add(url);
    }
  }

  const cluster = await initializeCluster({
    concurrency: 18,
    timeout: 20 * 1000,
    includeThirdPartyBundles: false,
    includeFaviconUrl: false,
  });

  logger.info("Added old scraped URLs to set");

  const latestScrapedUrl = (
    await db.urls
      .find({ openPageRank: { $exists: true } })
      .sort({ openPageRank: -1 })
      .limit(1)
      .toArray()
  )[0];

  const startingRank = latestScrapedUrl?.openPageRank ? latestScrapedUrl.openPageRank + 1 : 1;

  logger.info(`Starting scraping at rank ${startingRank}`);

  const csvFilePath = new URL("../top10milliondomains.csv", import.meta.url).pathname;

  const csvParserStream = parseFile(csvFilePath, {
    headers: true,
    skipRows: startingRank - 1,
  });

  const scrapeStream = through2Concurrent(
    { objectMode: true, maxConcurrency: 24 },
    async (row, encoding, cb) => {
      const url = row.Domain;
      const rank = Number(row.Rank);
      if (scrapedUrls.has(url)) {
        cb();
        return;
      }

      const { domain } = tldts.parse(url);
      if (scrapedDomains.has(domain)) {
        logger.info(`'${url}' skipped due to domain already being scraped`);
        cb();
        return;
      } else {
        scrapedDomains.add(domain);
      }

      await saveUrlData(cluster, url, rank);
      cb();
    }
  );

  await pipeline(csvParserStream, scrapeStream);

  logger.info("Done");
  process.exit();
}

export default scrape;
