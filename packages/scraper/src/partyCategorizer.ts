import { partition } from "lodash-es";
import thirdPartyWeb from "third-party-web";
import tldts from "tldts";

export function isBundleThirdParty(bundleUrl: string, websiteUrl: string): boolean {
  const bundleThirdPartyEntity = thirdPartyWeb.getEntity(bundleUrl);
  if (!bundleThirdPartyEntity) return false;
  const websiteThirdPartyWebEntity = thirdPartyWeb.getEntity(websiteUrl);

  if (!websiteThirdPartyWebEntity) return true;

  const bundleThirdPartyEntityHomepageDomain = tldts.parse(bundleThirdPartyEntity.homepage).domain;
  const websiteDomain = tldts.parse(websiteUrl).domain;

  const isSameThirdPartyEntity =
    websiteThirdPartyWebEntity.name === bundleThirdPartyEntity.name ||
    websiteDomain === bundleThirdPartyEntityHomepageDomain;

  return !isSameThirdPartyEntity;
}

interface Bundle {
  _id: string;
  initiatorUrlChain: string[];
  fullUrl: string;
}

export function categorizeBundlesByParty(
  bundles: Bundle[],
  websiteUrl: string,
  websiteDomain: string
): {
  firstPartyBundles: Bundle[];
  thirdPartyBundles: Bundle[];
} {
  const bundleUrlThirdPartyWebEntityMap = new Map();
  for (const bundle of bundles) {
    const entity = thirdPartyWeb.getEntity(bundle._id);
    bundleUrlThirdPartyWebEntityMap.set(bundle._id, entity);
  }

  const websiteThirdPartyWebEntity = thirdPartyWeb.getEntity(websiteUrl);

  const bundleUrlDataMap = new Map();
  for (const bundle of bundles) {
    const { _id, initiatorUrlChain, fullUrl } = bundle;

    const bundlePathName = new URL(fullUrl).pathname;

    const { domainWithoutSuffix: websiteDomainWithoutSuffix } = tldts.parse(websiteUrl);

    const { domain: bundleDomain, domainWithoutSuffix: bundleDomainWithoutSuffix } =
      tldts.parse(_id);
    const isOnSameDomain = websiteDomain === bundleDomain;
    const isOnSimilarDomain =
      bundleDomainWithoutSuffix?.length > 3 &&
      bundleDomainWithoutSuffix?.includes(websiteDomainWithoutSuffix);

    const pathNameIncludesWebsiteDomainWithoutSuffix =
      websiteDomainWithoutSuffix?.length > 3 && bundlePathName.includes(websiteDomainWithoutSuffix);

    const thirdPartyWebEntity = bundleUrlThirdPartyWebEntityMap.get(_id);
    const thirdPartyEntityHomepageDomain =
      !!thirdPartyWebEntity && tldts.parse(thirdPartyWebEntity.homepage).domain;
    const isSameThirdPartyEntity =
      !!thirdPartyWebEntity &&
      (websiteThirdPartyWebEntity?.name === thirdPartyWebEntity.name ||
        websiteDomain === thirdPartyEntityHomepageDomain);
    const isDifferentThirdPartyEntity = !!thirdPartyWebEntity && !isSameThirdPartyEntity;
    // TODO: Figure out how to handle this since we don't save it
    const isInitiatedByThirdPartyScript = initiatorUrlChain.some((url) => {
      const initiatorThisPartyWebEntity = bundleUrlThirdPartyWebEntityMap.get(url);
      return (
        initiatorThisPartyWebEntity &&
        initiatorThisPartyWebEntity.name !== websiteThirdPartyWebEntity?.name
      );
    });

    const thirdParty = isDifferentThirdPartyEntity && {
      name: thirdPartyWebEntity.name,
      url: thirdPartyWebEntity.homepage,
    };

    bundleUrlDataMap.set(bundle._id, {
      thirdParty,
      isOnSameDomain,
      isOnSimilarDomain,
      isSameThirdPartyEntity,
      isDifferentThirdPartyEntity,
      isInitiatedByThirdPartyScript,
      pathNameIncludesWebsiteDomainWithoutSuffix,
    });
  }

  const [firstPartyBundles, thirdPartyBundles] = partition(bundles, (bundle) => {
    const {
      isOnSameDomain,
      isOnSimilarDomain,
      isSameThirdPartyEntity,
      isDifferentThirdPartyEntity,
      isInitiatedByThirdPartyScript,
      pathNameIncludesWebsiteDomainWithoutSuffix,
    } = bundleUrlDataMap.get(bundle._id);
    if (isOnSameDomain || isOnSimilarDomain || isSameThirdPartyEntity) {
      return true;
    }
    if (isDifferentThirdPartyEntity || isInitiatedByThirdPartyScript) {
      return false;
    }
    if (pathNameIncludesWebsiteDomainWithoutSuffix) {
      return true;
    }
    if (
      [...bundleUrlDataMap.values()].some(
        (data) => data.isOnSameDomain || data.isOnSimilarDomain || data.isSameThirdPartyEntity
      )
    ) {
      return false;
    }

    // Scripts that:
    // * are not explicitly identified as first-party or third-party AND
    // * belong to a website that has no other scripts identified as first-party
    // are considered first-party
    return true;
  });

  return { firstPartyBundles, thirdPartyBundles };
}
