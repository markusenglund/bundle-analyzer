import { isBundleThirdParty, categorizeBundlesByParty } from "./partyCategorizer.js";

describe("isBundleThirdParty", () => {
  it("Identifies script listed in third-party-web as third-party", () => {
    expect(isBundleThirdParty("googletagmanager.com/gtm.js", "airbnb.com")).toEqual(true);
  });
  it("Identifies script with same URL as first-party", () => {
    expect(
      isBundleThirdParty(
        "airbnb.com/vZqnl_QyFm_9Dkw1x-vo8ZeBTk8/9XSuQJzL5u/aT9EJ2E8Bg/CGM/cPBc9IUs",
        "airbnb.com"
      )
    ).toEqual(false);
  });
  it("Identifies script with a domain similar to the website domain as first-party", () => {
    expect(
      isBundleThirdParty(
        "redditstatic.com/desktop2x/Chat~Governance~Reddit.e69af3fabda6d6d09076.js",
        "reddit.com"
      )
    ).toEqual(false);
  });
  it("Does not label URL as third-party just because it has a different URL", () => {
    expect(isBundleThirdParty("example.com/bundle.js", "airbnb.com")).toEqual(false);
  });
});

const AIRBNB_BUNDLES = [
  {
    code: "",
    _id: "a0.muscache.com/airbnb/static/packages/common-49800e19.js",
    sizeBytes: 531476,
    sizeBytesGzip: 152894,
    numChars: 531011,
    initiatorUrlChain: [],
    fullUrl: "https://a0.muscache.com/airbnb/static/packages/common-49800e19.js",
  },
  {
    code: "",
    _id: "googletagmanager.com/gtm.js?id=GTM-46MK",
    sizeBytes: 435469,
    sizeBytesGzip: 93070,
    numChars: 435463,
    initiatorUrlChain: [],
    fullUrl: "https://www.googletagmanager.com/gtm.js?id=GTM-46MK",
  },
  {
    code: "",
    _id: "gstatic.com/recaptcha/releases/Q_rrUPkK1sXoHi4wbuDTgcQR/recaptcha__en.js",
    sizeBytes: 347943,
    sizeBytesGzip: 134109,
    numChars: 347943,
    initiatorUrlChain: [],
    fullUrl: "https://www.gstatic.com/recaptcha/releases/Q_rrUPkK1sXoHi4wbuDTgcQR/recaptcha__en.js",
  },
  {
    code: "",
    _id: "a0.muscache.com/airbnb/static/packages/cc69-c07c5369.js",
    sizeBytes: 208783,
    sizeBytesGzip: 10516,
    numChars: 208783,
    initiatorUrlChain: [],
    fullUrl: "https://a0.muscache.com/airbnb/static/packages/cc69-c07c5369.js",
  },
  {
    code: "",
    _id: "airbnb.com/J6-nx6/BNIX/2PBSS/13/nHrWv41/YmVuVr2J/fhE3IRYeBQ/RiQVFlx/FfB8",
    sizeBytes: 207036,
    sizeBytesGzip: 66198,
    numChars: 207036,
    initiatorUrlChain: [],
    fullUrl: "https://airbnb.com/J6-nx6/BNIX/2PBSS/13/nHrWv41/YmVuVr2J/fhE3IRYeBQ/RiQVFlx/FfB8",
  },
];

const SPEEDTYPER_BUNDLES = [
  {
    code: "",
    _id: "static.twitchcdn.net/assets/everywhere-main-4d9eeeb56d98680f6aec.js",
    sizeBytes: 2038578,
    sizeBytesGzip: 460003,
    numChars: 2038460,
    initiatorUrlChain: ["embed.twitch.tv"],
    fullUrl: "https://static.twitchcdn.net/assets/everywhere-main-4d9eeeb56d98680f6aec.js",
  },
  {
    code: "",
    _id: "speedtyper.dev/templates/vendors~main.6f3eebc1.js",
    sizeBytes: 758778,
    sizeBytesGzip: 206762,
    numChars: 724221,
    initiatorUrlChain: ["speedtyper.dev"],
    fullUrl: "https://www.speedtyper.dev/templates/vendors~main.6f3eebc1.js",
  },
  {
    code: "",
    _id: "d2v02itv0y9u9t.cloudfront.net/dist/1.1.2/v6s.js",
    sizeBytes: 376204,
    sizeBytesGzip: 102649,
    numChars: 376204,
    initiatorUrlChain: [
      "static.twitchcdn.net/assets/everywhere-main-4d9eeeb56d98680f6aec.js",
      "embed.twitch.tv",
    ],
    fullUrl: "https://d2v02itv0y9u9t.cloudfront.net/dist/1.1.2/v6s.js",
  },
  {
    code: "",
    _id: "gstatic.com/eureka/clank/92/cast_sender.js",
    sizeBytes: 53262,
    sizeBytesGzip: 15189,
    numChars: 53262,
    initiatorUrlChain: [
      "gstatic.com/cv/js/sender/v1/cast_sender.js",
      "static.twitchcdn.net/assets/player-core-variant-a-b08a896d72048fb1dbfe.js",
      "embed.twitch.tv",
    ],
    fullUrl: "https://www.gstatic.com/eureka/clank/92/cast_sender.js",
  },
  {
    code: "",
    _id: "gstatic.com/cast/sdk/libs/sender/1.0/cast_framework.js",
    sizeBytes: 36519,
    sizeBytesGzip: 12304,
    numChars: 36519,
    initiatorUrlChain: [
      "gstatic.com/cv/js/sender/v1/cast_sender.js",
      "static.twitchcdn.net/assets/player-core-variant-a-b08a896d72048fb1dbfe.js",
      "embed.twitch.tv",
    ],
    fullUrl: "https://www.gstatic.com/cast/sdk/libs/sender/1.0/cast_framework.js",
  },
  {
    code: "",
    _id: "speedtyper.dev/main.acc880d4.js",
    sizeBytes: 25688,
    sizeBytesGzip: 8409,
    numChars: 25687,
    initiatorUrlChain: ["speedtyper.dev"],
    fullUrl: "https://www.speedtyper.dev/main.acc880d4.js",
  },
  {
    code: "",
    _id: "embed.twitch.tv/embed/v1.js",
    sizeBytes: 17182,
    sizeBytesGzip: 5698,
    numChars: 17182,
    initiatorUrlChain: [
      "speedtyper.dev/templates/__react_static_root__/src/pages/index.tsx.bd4f3f5f.js",
      "speedtyper.dev/main.acc880d4.js",
      "speedtyper.dev",
    ],
    fullUrl: "https://embed.twitch.tv/embed/v1.js",
  },
  {
    code: "",
    _id: "speedtyper.dev/templates/__react_static_root__/src/pages/index.tsx.bd4f3f5f.js",
    sizeBytes: 14791,
    sizeBytesGzip: 3970,
    numChars: 14791,
    initiatorUrlChain: ["speedtyper.dev/main.acc880d4.js", "speedtyper.dev"],
    fullUrl:
      "https://www.speedtyper.dev/templates/__react_static_root__/src/pages/index.tsx.bd4f3f5f.js",
  },
];

describe("categorizeBundlesByParty", () => {
  it("Correctly categorizes airbnb bundles", () => {
    const actualAirbnbFirstPartyBundleIds = [
      "a0.muscache.com/airbnb/static/packages/common-49800e19.js",
      "a0.muscache.com/airbnb/static/packages/cc69-c07c5369.js",
      "airbnb.com/J6-nx6/BNIX/2PBSS/13/nHrWv41/YmVuVr2J/fhE3IRYeBQ/RiQVFlx/FfB8",
    ];
    const actualAirbnbThirdPartyBundleIds = [
      "googletagmanager.com/gtm.js?id=GTM-46MK",
      "gstatic.com/recaptcha/releases/Q_rrUPkK1sXoHi4wbuDTgcQR/recaptcha__en.js",
    ];

    const { firstPartyBundles, thirdPartyBundles } = categorizeBundlesByParty(
      AIRBNB_BUNDLES,
      "airbnb.com",
      "airbnb.com"
    );
    expect(firstPartyBundles.map(({ _id }) => _id)).toEqual(actualAirbnbFirstPartyBundleIds);
    expect(thirdPartyBundles.map(({ _id }) => _id)).toEqual(actualAirbnbThirdPartyBundleIds);
  });
  it("Correctly categorizes speedtyper.dev bundles", () => {
    const actualFirstPartyBundleIds = [
      "speedtyper.dev/templates/vendors~main.6f3eebc1.js",
      "speedtyper.dev/main.acc880d4.js",
      "speedtyper.dev/templates/__react_static_root__/src/pages/index.tsx.bd4f3f5f.js",
    ];
    const actualThirdPartyBundleIds = [
      "static.twitchcdn.net/assets/everywhere-main-4d9eeeb56d98680f6aec.js",
      "d2v02itv0y9u9t.cloudfront.net/dist/1.1.2/v6s.js",
      "gstatic.com/eureka/clank/92/cast_sender.js",
      "gstatic.com/cast/sdk/libs/sender/1.0/cast_framework.js",
      "embed.twitch.tv/embed/v1.js",
    ];

    const { firstPartyBundles, thirdPartyBundles } = categorizeBundlesByParty(
      SPEEDTYPER_BUNDLES,
      "speedtyper.dev",
      "speedtyper.dev"
    );
    expect(firstPartyBundles.map(({ _id }) => _id)).toEqual(actualFirstPartyBundleIds);
    expect(thirdPartyBundles.map(({ _id }) => _id)).toEqual(actualThirdPartyBundleIds);
  });
});
