import htmlParser, { HTMLElement } from "node-html-parser";
import got from "got";
import PQueue from "p-queue";
import sourceMapUrlFns from "source-map-url";
import { logger } from "@bundle-scanner/common";
import { sourceMapUtils, SourceMap } from "@bundle-scanner/common";

const { extractLibrariesFromSourceMap } = sourceMapUtils;

const fetchQueue = new PQueue({ concurrency: 10 });

const blacklist = ["google.", "amazon."];

async function scrapeWebsite(
  websiteUrl: string,
  rank: number,
  { saveCode }: { saveCode?: boolean } = {}
): Promise<{
  url: string;
  scripts?: {
    scriptUrl: string;
    libraryIds: string[];
    modulePaths: string[];
    sourceMapUrl?: string;
  }[];
  failedScrape?: boolean;
} | void> {
  if (blacklist.some((str) => websiteUrl.includes(str))) return;
  try {
    logger.info(`Scraping ${websiteUrl} (${rank})`);

    const absoluteUrl = `http://${websiteUrl}`;

    let html;
    try {
      const res = await fetchQueue.add(() => got(absoluteUrl, { timeout: 20 * 1000, retry: 1 }));
      html = res.body;
    } catch (err) {
      logger.warn(`[${absoluteUrl}]: Fetching script failed: '${err.message}' (${rank})`);
      return {
        url: websiteUrl,
        failedScrape: true,
      };
    }

    // @ts-ignore
    const document: HTMLElement = htmlParser.parse(html);
    const scriptElements = document.querySelectorAll("script[src]").slice(0, 30);

    // logger.info(`Found ${scriptElements.length} scripts`);

    const scriptsWithLibraries = (
      await Promise.all(
        scriptElements.map(async (scriptElement) => {
          const scriptUrl = scriptElement.attrs.src;
          // ! Hm, I think this will fail when considering scripts that do not belong to the same URL
          const absoluteScriptUrl = new URL(scriptUrl, absoluteUrl).href;
          if (!absoluteScriptUrl.includes(absoluteUrl)) return;

          let code;
          try {
            const res = await fetchQueue.add(() =>
              got(absoluteScriptUrl, {
                timeout: 20 * 1000,
                retry: 1,
              })
            );
            code = res.body;
          } catch (err) {
            logger.warn(`Fetching '${absoluteScriptUrl}' failed with message: '${err.message}'`);
            return;
          }

          const sourceMapUrl = sourceMapUrlFns.getFrom(code);
          if (!sourceMapUrl) return;

          const { href: absoluteSourceMapUrl, protocol } = new URL(sourceMapUrl, absoluteScriptUrl);

          const sourceMap = await getSourceMap(absoluteSourceMapUrl, absoluteScriptUrl, protocol);
          if (!sourceMap) return;

          const { libraryIds, modulePaths } = extractLibrariesFromSourceMap(sourceMap);
          if (libraryIds.length < 1) return;

          const script: {
            scriptUrl: string;
            libraryIds: string[];
            modulePaths: string[];
            sourceMapUrl?: string;
            code?: string;
            sourceMap: any;
          } = { scriptUrl, libraryIds, modulePaths };
          if (protocol !== "data:") {
            script.sourceMapUrl = absoluteSourceMapUrl;
          }
          if (saveCode) {
            script.code = code;
          }
          return script;
        })
      )
    ).filter(<S>(script: S | undefined): script is S => script != null);

    if (scriptsWithLibraries.length > 0) {
      logger.info(
        `[${absoluteUrl}]: Found ${scriptsWithLibraries.length} scripts with libraries in source maps (${rank})`
      );
    }
    return { url: websiteUrl, scripts: scriptsWithLibraries };
  } catch (err) {
    logger.error(`Unexpected err: ${err.message} for ${rank}: ${websiteUrl}\n${err.stack}`);
    return {
      url: websiteUrl,
      failedScrape: true,
    };
  }
}

async function getSourceMap(
  absoluteSourceMapUrl: string,
  absoluteScriptUrl: string,
  protocol: string
): Promise<SourceMap | undefined> {
  if (!["http:", "https:", "data:"].includes(protocol)) {
    logger.warn(
      `[${absoluteScriptUrl}]: Source map URL '${absoluteSourceMapUrl.slice(
        0,
        100
      )}' has unsported protocol '${protocol}'`
    );
    return;
  }

  let sourceMapText;
  if (protocol === "data:") {
    logger.info(`Decoding data URL source map for ${absoluteScriptUrl}`);
    try {
      sourceMapText = decodeBase64Url(absoluteSourceMapUrl);
    } catch (err) {
      logger.warn(
        `[${absoluteScriptUrl}]: Failed to parse source map as base64 URL, error: '${err.message}'`
      );
      return;
    }
  } else {
    logger.info(`Found source map: ${absoluteSourceMapUrl} for ${absoluteScriptUrl}`);

    try {
      const res = await fetchQueue.add(() =>
        got(absoluteSourceMapUrl, {
          timeout: 20 * 1000,
          retry: 1,
        })
      );
      sourceMapText = res.body;
    } catch (err) {
      logger.warn(
        `[${absoluteScriptUrl}]: Request to source map ${absoluteSourceMapUrl} failed with error '${err.message}'`
      );
      return;
    }
  }

  let sourceMap: SourceMap;
  try {
    sourceMap = JSON.parse(sourceMapText);
    return sourceMap;
  } catch (err) {
    logger.warn(
      `[${absoluteScriptUrl}]: Could not parse URL ${absoluteSourceMapUrl?.slice(
        0,
        100
      )} as JSON; Text: ${sourceMapText?.slice(0, 100)}...`
    );
    return;
  }
}

export default scrapeWebsite;
