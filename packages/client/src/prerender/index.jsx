import React from "react";
import { renderToString } from "react-dom/server";
import { Router } from "wouter";
import staticLocationHook from "wouter/static-location";
import { HeadProvider } from "react-head";
import App from "../app/App";

function prerender({ bundleUrl, route }) {
  const headTags = [];
  const app = renderToString(
    <HeadProvider headTags={headTags}>
      <Router hook={staticLocationHook(route)}>
        <App />
      </Router>
    </HeadProvider>
  );

  const html = /* html */ `
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <meta property="og:image" content="https://bundlescanner.com/static/images/bundle-scanner-screenshot.png" />
  <meta property="og:image:type" content="image/png" />
  <meta property="og:image:width" content="1200"/>
  <meta property="og:image:height" content="630" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="Bundle Scanner" />
  <meta property="og:description" content="Bundle Scanner identifies which npm libraries are used on any website. It downloads every Javascript file from a URL and searches through the files for code that matches one of the 35,000 most popular npm libraries." />
  <meta property="description" content="Bundle Scanner identifies which npm libraries are used on any website. It downloads every Javascript file from a URL and searches through the files for code that matches one of the 35,000 most popular npm libraries." />
  <link rel="apple-touch-icon" sizes="180x180" href="/static/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/static/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/static/favicons/favicon-16x16.png">
  <link rel="manifest" href="/static/favicons/site.webmanifest">
  ${renderToString(headTags)}
</head>
<body>
<div id="root">${app}</div>
</body>
<script src=${bundleUrl}></script>
</html>
  `;

  return html;
}

export default prerender;
