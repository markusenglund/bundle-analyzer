import React from "react";
import { css } from "@emotion/react";
import { withProfiler, ErrorBoundary } from "@sentry/react";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import Main from "./Main";
import Header from "./components/Header";
import GlobalStyles from "./GlobalStyles";
import ErrorFallback from "./ErrorFallback";

const App = () => {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        staleTime: Infinity,
        retry: false,
        refetchOnWindowFocus: false,
      },
    },
  });
  return (
    <div
      css={css`
        background: rgb(30, 34, 35);
        color: #eee;
        min-height: 100vh;
        margin: 0;
      `}
    >
      <GlobalStyles />
      <Header />
      <ErrorBoundary fallback={ErrorFallback}>
        <QueryClientProvider client={queryClient}>
          <Main />
          <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
      </ErrorBoundary>
    </div>
  );
};

export default withProfiler(App);
