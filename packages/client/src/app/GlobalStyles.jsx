import React from "react";
import { Global, css } from "@emotion/react";

const GlobalStyles = () => (
  <Global
    styles={css`
      body {
        margin: 0;
        input:focus {
          outline: none;
        }
        font-family: system-ui, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue,
          Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol,
          Noto Color Emoji;
      }
      .lds-dual-ring {
        margin-top: 40px;
        display: inline-block;
        width: 80px;
        height: 80px;
        animation: fade-in ease 2s;
        animation-iteration-count: 1;
      }
      @keyframes fade-in {
        0% {
          opacity: 0;
        }
        50% {
          opacity: 0;
        }
        100% {
          opacity: 1;
        }
      }
      .lds-dual-ring:after {
        content: " ";
        display: block;
        width: 64px;
        height: 64px;
        margin: 8px;
        border-radius: 50%;
        border: 6px solid #fff;
        border-color: #fff transparent #fff transparent;
        animation: lds-dual-ring 1.2s linear infinite;
      }

      @keyframes lds-dual-ring {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(360deg);
        }
      }

      [data-tooltip]:before {
        /* needed - do not touch */
        content: attr(data-tooltip);
        position: absolute;
        opacity: 0;

        /* customizable */
        transition: all 0.15s ease;
        padding: 10px;
        color: #ddd;
        border-radius: 10px;

        margin-top: -50px;
        font-weight: normal;
        @media (max-width: 800px) {
          transform: translateX(calc(-50% + 10px));
        }
      }

      [data-tooltip]:hover:before {
        /* needed - do not touch */
        opacity: 1;

        /* customizable */
        background: black;
      }

      [data-tooltip]:not([data-tooltip-persistent]):before {
        pointer-events: none;
      }

      :root {
        --reach-combobox: 1;
      }

      [data-reach-combobox-popover] {
        border: solid 1px hsla(0, 0%, 0%, 0.25);
        background: hsla(0, 100%, 100%, 0.99);
        font-size: 85%;
      }

      [data-reach-combobox-list] {
        list-style: none;
        margin: 0;
        padding: 0;
        user-select: none;
      }

      [data-reach-combobox-option] {
        cursor: pointer;
        margin: 0;
        padding: 0.25rem 0.5rem;
      }

      [data-reach-combobox-option][aria-selected="true"] {
        background: hsl(211, 10%, 95%);
      }

      [data-reach-combobox-option]:hover {
        background: hsl(211, 10%, 92%);
      }

      [data-reach-combobox-option][aria-selected="true"]:hover {
        background: hsl(211, 10%, 90%);
      }

      [data-suggested-value] {
        font-weight: bold;
      }
      :root {
        --reach-dialog: 1;
      }

      [data-reach-dialog-overlay] {
        background: hsla(0, 0%, 0%, 0.33);
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        overflow: auto;
      }

      [data-reach-dialog-content] {
        height: calc(100vh - 40px);
        background: rgb(30, 34, 35);
        color: #eee;
        padding: 20px;
        outline: none;
      }
    `}
  />
);

export default GlobalStyles;
