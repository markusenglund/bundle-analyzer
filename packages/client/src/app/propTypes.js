import PropTypes from "prop-types";

export const intervalPropType = PropTypes.shape({
  start: PropTypes.number.isRequired,
  end: PropTypes.number.isRequired,
  fileName: PropTypes.string.isRequired,
  libraryId: PropTypes.string.isRequired,
  intervalMatchRatio: PropTypes.number,
  intervalSizeFourGrams: PropTypes.number,
  maxIntervalSizeFourGrams: PropTypes.number,
});

export const bundlePropType = PropTypes.shape({
  _id: PropTypes.string.isRequired,
  fullUrl: PropTypes.string.isRequired,
  thirdPartyWebData: PropTypes.shape({
    name: PropTypes.string.isRequired,
    categories: PropTypes.arrayOf(PropTypes.string).isRequired,
  }),
  bundleScan: PropTypes.shape({
    matchedReleases: PropTypes.arrayOf(
      PropTypes.shape({
        _id: PropTypes.string.isRequired,
        libraryId: PropTypes.string.isRequired,
        version: PropTypes.string,
        fileIds: PropTypes.arrayOf(PropTypes.string),
        sourceMapFrequency: PropTypes.number,
        score: PropTypes.number,
        maxIdfScore: PropTypes.number,
        idfScoreRatio: PropTypes.number,
        cumulativeIntervalSize: PropTypes.number.isRequired,
        intervals: PropTypes.arrayOf(intervalPropType).isRequired,
        dependencies: PropTypes.arrayOf(
          PropTypes.shape({
            libraryId: PropTypes.string.isRequired,
            versionRange: PropTypes.string.isRequired,
          })
        ).isRequired,
        bundledDependencies: PropTypes.arrayOf(
          PropTypes.shape({
            libraryId: PropTypes.string.isRequired,
            version: PropTypes.string.isRequired,
          })
        ),
        description: PropTypes.string,
        repositoryUrl: PropTypes.string,
        license: PropTypes.string,
      })
    ).isRequired,
    sourceMapData: PropTypes.shape({
      url: PropTypes.string.isRequired,
      libraryIds: PropTypes.arrayOf(PropTypes.string).isRequired,
      modulePaths: PropTypes.arrayOf(PropTypes.string).isRequired,
    }),
    date: PropTypes.string.isRequired,
    sizeBytes: PropTypes.number.isRequired,
    sizeBytesGzip: PropTypes.number.isRequired,
    numChars: PropTypes.number.isRequired,
  }).isRequired,
  meta: PropTypes.shape({
    wasCached: PropTypes.bool.isRequired,
  }).isRequired,
});

export const websitePropType = PropTypes.shape({
  _id: PropTypes.string.isRequired,
  domain: PropTypes.string.isRequired,
  host: PropTypes.string.isRequired,
  faviconUrl: PropTypes.string.isRequired,
  createdDate: PropTypes.string.isRequired,
  urlScan: PropTypes.shape({
    bundlesById: PropTypes.objectOf(bundlePropType).isRequired,
    totalSizeBytes: PropTypes.number.isRequired,
    totalSizeBytesGzip: PropTypes.number.isRequired,
    firstPartyBundleIds: PropTypes.arrayOf(PropTypes.string).isRequired,
    thirdPartyBundleIds: PropTypes.arrayOf(PropTypes.string).isRequired,
    failedBundleIds: PropTypes.arrayOf(PropTypes.string),
    date: PropTypes.string.isRequired,
  }),
  meta: PropTypes.shape({
    redirectedFrom: PropTypes.string,
    standardizedWebsiteUrl: PropTypes.string.isRequired,
    timeSpent: PropTypes.number.isRequired,
    wasCached: PropTypes.bool.isRequired,
  }).isRequired,
});
