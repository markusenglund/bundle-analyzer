import { useQuery, useQueryClient } from "react-query";
import ky from "../../utils/ky";

const useBundle = ({ encodedBundleUrl, decodedBundleUrl, encodedWebsiteUrl }) => {
  const queryClient = useQueryClient();
  return useQuery(["bundle", encodedBundleUrl], async () => {
    const website = encodedWebsiteUrl
      ? queryClient.getQueryData(["website", encodedWebsiteUrl])
      : null;
    const bundle = website?.urlScan.bundlesById[decodedBundleUrl];
    if (bundle) {
      return bundle;
    }
    const { data } = await ky(`/api/bundle/${encodedBundleUrl}`, { timeout: 20_000 }).json();
    return data;
  });
};

export default useBundle;
