import { useQuery } from "react-query";
import tokenizationWorker from "comlink:../tokenizationWorker.js";

const useTokenPositions = (code, queryId) =>
  useQuery(
    ["tokenPositions", queryId],
    async () => tokenizationWorker.extractTokens({ codeSnippet: code }),
    { enabled: !!code }
  );

export default useTokenPositions;
