import { useQuery } from "react-query";
import ky from "../../utils/ky";

const useWebsite = (encodedWebsiteUrl) =>
  useQuery(["website", encodedWebsiteUrl], async () => {
    const { data } = await ky(`/api/website/${encodedWebsiteUrl}`, { timeout: 30_000 }).json();
    return data;
  });

export default useWebsite;
