import { useQuery } from "react-query";
import ky from "../../utils/ky";

const useBundleCode = (fullBundleUrl) =>
  useQuery(
    ["bundle", fullBundleUrl],
    async () => {
      const code = await ky(`/api/bundle/${encodeURIComponent(fullBundleUrl)}/code`, {
        timeout: 20_000,
      }).text();
      return code;
    },
    { enabled: !!fullBundleUrl }
  );

export default useBundleCode;
