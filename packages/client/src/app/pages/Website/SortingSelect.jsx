import React from "react";
import { css } from "@emotion/react";
import PropTypes from "prop-types";

const SortingSelect = ({ sorting, setSorting }) => {
  const handleSortingChange = (event) => {
    setSorting(event.target.value);
  };
  return (
    <div
      css={css`
        display: flex;
        align-items: center;
        font-weight: bold;
      `}
    >
      <span>Sort by</span>
      <select
        value={sorting}
        onChange={handleSortingChange}
        css={css`
          background: #555;
          border-color: #777;
          border-radius: 6px;
          color: #eee;
          margin-left: 6px;
          padding-left: 4px;
          height: 36px;
          width: 100px;
          font-weight: bold;
          font-size: 16px;
          box-shadow: 0px 1px 4px #1119;
        `}
      >
        <option value="cumulative-interval-size">Footprint</option>
        <option value="alphabetical">Alphabetical</option>
        <option value="score">Match score</option>
        <option value="idf-score-ratio">Match %</option>
      </select>
    </div>
  );
};

SortingSelect.propTypes = {
  sorting: PropTypes.string.isRequired,
  setSorting: PropTypes.func.isRequired,
};

export default SortingSelect;
