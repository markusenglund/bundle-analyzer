import React, { useMemo } from "react";
import PropTypes from "prop-types";
import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { Title } from "react-head";
import ms from "ms";
import { formatDistanceToNow } from "date-fns";
import useWebsite from "../../hooks/useWebsite";
import UrlInput from "../../components/UrlInput";
import WebsiteResults from "./WebsiteResults";

const Error = styled("div")`
  margin-top: 40px;
  white-space: pre-line;
  font-size: 22px;
  text-align: center;
`;

const propTypes = {
  websiteUrl: PropTypes.string.isRequired,
  inputUrl: PropTypes.string.isRequired,
  setInputUrl: PropTypes.func.isRequired,
};

const Website = ({ websiteUrl: encodedWebsiteUrl, inputUrl, setInputUrl }) => {
  const { status, data: website, error } = useWebsite(encodedWebsiteUrl);

  // Memoize to prevent recalculation every time this component rerenders
  const scannedDateDistance = useMemo(
    () =>
      status === "success"
        ? formatDistanceToNow(new Date(website.urlScan.date), { addSuffix: true })
        : null,
    [website?.urlScan.date]
  );

  const decodedWebsiteUrl = decodeURIComponent(encodedWebsiteUrl);

  return (
    <div
      css={css`
        margin-top: 40px;
        margin-left: 20px;
        margin-right: 20px;
        padding-bottom: 50px;
        @media (max-width: 800px) {
          margin-left: 4px;
          margin-right: 4px;
        }
      `}
    >
      <Title>{decodedWebsiteUrl} libraries | Bundle Scanner</Title>
      <div>
        <UrlInput inputUrl={inputUrl} setInputUrl={setInputUrl} status={status} />

        {status === "success" && (
          <div
            css={css`
              display: flex;
              justify-content: space-between;
              font-size: 13px;
              margin-left: 8px;
              margin-top: 5px;
              max-width: 630px;
              margin-right: auto;
              margin-left: auto;
            `}
          >
            <div
              css={css`
                text-align: left;
              `}
            >
              Got results in {ms(website.meta.timeSpent)}
            </div>
            {website.meta.wasCached && website.urlScan.date && (
              <div>Results cached from {scannedDateDistance} </div>
            )}
          </div>
        )}
      </div>
      {status === "loading" && (
        <div
          css={css`
            display: flex;
            flex-direction: column;
            align-items: center;
          `}
        >
          <div
            className="lds-dual-ring"
            css={css`
              margin-right: 12px;
              margin-bottom: 22px;
            `}
          />
          <div>Scanning bundles...</div>
        </div>
      )}
      {status === "error" && <Error>{error.message}</Error>}
      {status === "success" && (
        <WebsiteResults website={website} encodedWebsiteUrl={encodedWebsiteUrl} />
      )}
    </div>
  );
};

Website.propTypes = propTypes;

export default Website;
