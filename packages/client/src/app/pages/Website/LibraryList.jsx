import React from "react";
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import { Link } from "wouter";
import { GoLaw } from "react-icons/go";
import { IoLogoGithub } from "react-icons/io";
import { round, truncate } from "lodash-es";
import npmLogo from "../../../images/npm-logo.svg";
import gitlabLogo from "../../../images/gitlab-logo.svg";
import bitbucketLogo from "../../../images/bitbucket-logo.svg";
import formatSize from "../../../utils/formatSize";

const LibraryList = ({ libraries, websiteUrl, numBundles }) => {
  return (
    <div
      css={css`
        justify-content: center;
        padding: 0 25px 25px 25px;
        width: 100%;
        max-width: 500px;
        @media (max-width: 800px) {
          padding-left: 8px;
          padding-right: 8px;
        }
      `}
    >
      <div
        css={css`
          font-style: italic;
          font-size: 13px;
          margin-bottom: 10px;
        `}
      >
        Showing {libraries.length} libraries from {numBundles} bundles
      </div>
      {libraries.map((library) => (
        <div
          key={library._id}
          css={css`
            padding-bottom: 16px;
            border-bottom: 1px solid #888;
            max-width: 660px;
          `}
        >
          <div
            css={css`
              font-size: 18px;
              font-weight: bold;
            `}
          >
            {library._id}
          </div>
          <div
            css={css`
              font-size: 12px;
              -webkit-line-clamp: 1;
              -webkit-box-orient: vertical;
              display: -webkit-box;
              overflow: hidden;
            `}
          >
            {library.description}
          </div>
          <div
            css={css`
              display: flex;
              justify-content: start;
            `}
          >
            <div
              css={css`
                margin-right: 10px;
              `}
            >
              <a target="_blank" rel="noreferrer" href={`https://npmjs.com/package/${library._id}`}>
                <img src={npmLogo} alt="npm" height="14" />
              </a>
            </div>
            <div
              css={css`
                margin-right: 10px;
              `}
            >
              <RepositoryLink repositoryUrl={library.repositoryUrl} />
            </div>
            <div
              css={css`
                font-size: 14px;
                font-weight: bold;
              `}
            >
              <GoLaw size={13} /> {library.license}
            </div>
          </div>
          {library.bundles.map((bundle) => (
            <div key={bundle._id}>
              <div
                css={css`
                  font-size: 12px;
                  font-style: italic;
                  font-weight: bold;
                  word-break: break-all;
                  -webkit-line-clamp: 1;
                  -webkit-box-orient: vertical;
                  display: -webkit-box;
                  overflow: hidden;
                `}
              >
                {bundle._id}
              </div>
              <div
                css={css`
                  display: flex;
                  flex-wrap: wrap;
                  width: 100%;
                  justify-content: space-between;
                `}
              >
                <div>
                  Footprint:{" "}
                  {(() => {
                    const { unit, roundedSize } = formatSize(bundle.cumulativeIntervalSize);
                    return `${roundedSize} ${unit}`;
                  })()}
                </div>
                {}
                {bundle.analysisMode === "sourcemap" ? (
                  "Sourcemap"
                ) : (
                  <>
                    <div>Match: {`${Math.round(bundle.idfScoreRatio * 100)}%`}</div>
                    <div>Match score: {round(bundle.score, 1)}</div>
                    <div>
                      <Link
                        href={`/bundle/${encodeURIComponent(
                          bundle._id
                        )}/inspect/${encodeURIComponent(library._id)}?${
                          websiteUrl ? `website=${websiteUrl}` : ""
                        }`}
                      >
                        <a
                          css={css`
                            text-decoration: none;
                            border-radius: 4px;
                            background: #161;
                            appearance: button;
                            color: inherit;
                            :hover {
                              background: #050;
                            }
                            padding: 2px 4px;
                            font-size: 11px;
                            display: flex;
                            justify-content: center;
                            align-items: center;
                            text-align: center;
                            margin-left: 6px;
                          `}
                        >
                          Inspect
                        </a>
                      </Link>
                    </div>
                  </>
                )}
              </div>
              {bundle.dependentsChain.length > 0 && (
                <div
                  css={css`
                    font-size: 12px;
                    font-style: italic;
                  `}
                >
                  Dependency of {bundle.dependentsChain.join(" → ")}
                </div>
              )}
            </div>
          ))}
        </div>
      ))}
    </div>
  );
};

LibraryList.propTypes = {
  websiteUrl: PropTypes.string.isRequired,
  numBundles: PropTypes.number,
  libraries: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      description: PropTypes.string,
      repositoryUrl: PropTypes.string,
      license: PropTypes.string,
      bundles: PropTypes.arrayOf(
        PropTypes.shape({
          _id: PropTypes.string.isRequired,
          version: PropTypes.string,
          dependentsChain: PropTypes.arrayOf(PropTypes.string).isRequired,
          analysisMode: PropTypes.string.isRequired,
          cumulativeIntervalSize: PropTypes.number.isRequired,
          score: PropTypes.number,
          idfScoreRatio: PropTypes.number,
        })
      ).isRequired,
    })
  ).isRequired,
};

function RepositoryLink({ repositoryUrl }) {
  if (!repositoryUrl) return null;
  const repositoryHost = new URL(repositoryUrl).host;

  let linkLogo;
  switch (repositoryHost) {
    case "github.com":
      linkLogo = <IoLogoGithub size={18} />;
      break;
    case "gitlab.com":
      linkLogo = <img src={gitlabLogo} alt="Gitlab repo" height="30" />;
      break;
    case "bitbucket.org":
      linkLogo = <img src={bitbucketLogo} alt="Bitbucket repo" height="18" />;
      break;
    default:
      linkLogo = "Repo";
  }
  return (
    <a
      target="_blank"
      rel="noreferrer"
      href={repositoryUrl}
      css={css`
        color: inherit;
      `}
    >
      {linkLogo}
    </a>
  );
}
RepositoryLink.propTypes = { repositoryUrl: PropTypes.string };

export default LibraryList;
