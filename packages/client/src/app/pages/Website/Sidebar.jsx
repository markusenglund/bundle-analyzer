import React from "react";
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import { websitePropType } from "../../propTypes";
import SortingSelect from "./SortingSelect";
import Filters from "./Filters";

const Sidebar = ({ website, filter, setFilter, sorting, setSorting, maxFootprint }) => {
  return (
    <div
      css={css`
        width: 400px;
        padding: 12px 2px 25px 8px;
        margin-top: 10px;
      `}
    >
      <SortingSelect sorting={sorting} setSorting={setSorting} />
      <Filters
        website={website}
        filter={filter}
        setFilter={setFilter}
        maxFootprint={maxFootprint}
      />
    </div>
  );
};

Sidebar.propTypes = {
  website: websitePropType,
  filter: PropTypes.exact({
    bundles: PropTypes.instanceOf(Map).isRequired,
    onlyIncludeTopLevelDependencies: PropTypes.bool.isRequired,
    footprintInterval: PropTypes.exact({
      min: PropTypes.number,
      max: PropTypes.number,
    }).isRequired,
    minIdfScoreRatio: PropTypes.number.isRequired,
    minScore: PropTypes.number.isRequired,
  }).isRequired,
  setFilter: PropTypes.func.isRequired,
  sorting: PropTypes.string.isRequired,
  setSorting: PropTypes.func.isRequired,
  maxFootprint: PropTypes.number.isRequired,
};

export default Sidebar;
