import React, { useEffect, useState, useMemo } from "react";
import { css } from "@emotion/react";
import { useTable, useSortBy } from "react-table";
import { Title } from "react-head";
import ky from "../../utils/ky";

const Library = ({ libraryId }) => {
  const [library, setLibrary] = useState(null);

  // TODO: Error handling
  useEffect(async () => {
    const fetchedLibrary = await ky(`/api/library/${libraryId}`).json();
    setLibrary(fetchedLibrary);
  }, [libraryId]);

  if (!library) {
    return <div className="lds-dual-ring" />;
  }

  return (
    <div
      css={css`
        display: flex;
        flex-direction: column;
        align-items: center;
      `}
    >
      <Title>{libraryId} | Bundle Scanner</Title>
      <h2>List of websites</h2>
      <LibraryTable bundles={library.bundles} />
    </div>
  );
};

const LibraryTable = ({ bundles }) => {
  const columns = useMemo(
    () => [
      {
        Header: "Bundle",
        accessor: "_id",
      },
      {
        Header: "Websites",
        Cell: ({ value: domains }) => {
          const numDomains = domains.length;
          if (numDomains > 3) {
            return `${domains.slice(0, 2).join(", ")} + ${numDomains - 2} more domains`;
          }
          return domains.join(", ");
        },
        accessor: "domains",
      },
    ],
    [bundles]
  );

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable(
    {
      columns,
      data: bundles,
    },
    useSortBy
  );

  return (
    <table {...getTableProps()}>
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => (
              <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                {column.render("Header")}
                <span>
                  {column.canSort &&
                    (column.isSorted ? (column.isSortedDesc ? " ▼" : " ▲") : "  ↕")}
                </span>
              </th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map((cell) => (
                <td
                  css={css`
                    padding: 6px;
                    max-width: 900px;
                    min-width: 130px;
                    text-align: left;
                  `}
                  {...cell.getCellProps()}
                >
                  {cell.render("Cell")}
                </td>
              ))}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default Library;
