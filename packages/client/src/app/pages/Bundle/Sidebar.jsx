import React from "react";
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import SortingSelect from "../Website/SortingSelect";
import Filters from "./Filters";

const Sidebar = ({ filter, setFilter, sorting, setSorting, maxFootprint }) => {
  return (
    <div
      css={css`
        width: 400px;
        padding: 12px 2px 0px 8px;
        margin-top: 10px;
      `}
    >
      <SortingSelect sorting={sorting} setSorting={setSorting} />
      <Filters filter={filter} setFilter={setFilter} maxFootprint={maxFootprint} />
    </div>
  );
};

Sidebar.propTypes = {
  filter: PropTypes.exact({
    onlyIncludeTopLevelDependencies: PropTypes.bool.isRequired,
    footprintInterval: PropTypes.exact({
      min: PropTypes.number,
      max: PropTypes.number,
    }).isRequired,
    minIdfScoreRatio: PropTypes.number.isRequired,
    minScore: PropTypes.number.isRequired,
  }).isRequired,
  setFilter: PropTypes.func.isRequired,
  sorting: PropTypes.string.isRequired,
  setSorting: PropTypes.func.isRequired,
  maxFootprint: PropTypes.number.isRequired,
};

export default Sidebar;
