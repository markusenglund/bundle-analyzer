import React from "react";
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import { Link } from "wouter";
import { GoLaw } from "react-icons/go";
import { IoLogoGithub } from "react-icons/io";
import { round, truncate } from "lodash-es";
import npmLogo from "../../../images/npm-logo.svg";
import gitlabLogo from "../../../images/gitlab-logo.svg";
import bitbucketLogo from "../../../images/bitbucket-logo.svg";
import formatSize from "../../../utils/formatSize";

const LibraryList = ({ releases, analysisMode, bundleId }) => {
  return (
    <div
      css={css`
        justify-content: center;
        padding: 0 25px 25px 25px;
        @media (max-width: 800px) {
          padding-left: 8px;
          padding-right: 8px;
        }
      `}
    >
      <div
        css={css`
          font-style: italic;
          font-size: 13px;
          margin-bottom: 10px;
        `}
      >
        Showing {releases.length} libraries
      </div>
      {releases.map((release) => (
        <div
          key={release._id}
          css={css`
            padding-bottom: 16px;
            border-bottom: 1px solid #888;
            max-width: 660px;
          `}
        >
          <div
            css={css`
              font-size: 18px;
              font-weight: bold;
            `}
          >
            {release.libraryId}
          </div>
          <div
            css={css`
              font-size: 12px;
            `}
          >
            {truncate(release.description, { length: 150 })}
          </div>
          <div
            css={css`
              display: flex;
              justify-content: start;
            `}
          >
            <div
              css={css`
                margin-right: 10px;
              `}
            >
              <a
                target="_blank"
                rel="noreferrer"
                href={`https://npmjs.com/package/${release.libraryId}`}
              >
                <img src={npmLogo} alt="npm" height="14" />
              </a>
            </div>
            <div
              css={css`
                margin-right: 10px;
              `}
            >
              <RepositoryLink repositoryUrl={release.repositoryUrl} />
            </div>
            <div
              css={css`
                font-size: 14px;
                font-weight: bold;
              `}
            >
              <GoLaw size={13} /> {release.license}
            </div>
          </div>
          <div>
            <div
              css={css`
                display: flex;
                flex-wrap: wrap;
                width: 100%;
                justify-content: space-between;
              `}
            >
              <div>
                Footprint:{" "}
                {(() => {
                  const { unit, roundedSize } = formatSize(release.cumulativeIntervalSize);
                  return `${roundedSize} ${unit}`;
                })()}
              </div>
              {analysisMode === "sourcemap" ? (
                "Sourcemap"
              ) : (
                <>
                  <div>Match: {`${Math.round(release.idfScoreRatio * 100)}%`}</div>
                  <div>Match score: {round(release.score, 1)}</div>
                  <div>
                    <Link
                      href={`/bundle/${encodeURIComponent(bundleId)}/inspect/${encodeURIComponent(
                        release.libraryId
                      )}?`}
                    >
                      <a
                        css={css`
                          text-decoration: none;
                          border-radius: 4px;
                          background: #161;
                          appearance: button;
                          color: inherit;
                          :hover {
                            background: #050;
                          }
                          padding: 2px 4px;
                          font-size: 11px;
                          display: flex;
                          justify-content: center;
                          align-items: center;
                          text-align: center;
                          margin-left: 6px;
                        `}
                      >
                        Inspect
                      </a>
                    </Link>
                  </div>
                </>
              )}
            </div>
            {release.dependentsChain.length > 0 && (
              <div
                css={css`
                  font-size: 12px;
                  font-style: italic;
                `}
              >
                Dependency of {release.dependentsChain.join(" → ")}
              </div>
            )}
          </div>
        </div>
      ))}
    </div>
  );
};

LibraryList.propTypes = {
  releases: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      libraryId: PropTypes.string.isRequired,
      version: PropTypes.string,
      fileIds: PropTypes.arrayOf(PropTypes.string),
      sourceMapFrequency: PropTypes.number,
      score: PropTypes.number,
      maxIdfScore: PropTypes.number,
      idfScoreRatio: PropTypes.number,
      cumulativeIntervalSize: PropTypes.number.isRequired,
      dependentsChain: PropTypes.arrayOf(PropTypes.string).isRequired,
      description: PropTypes.string,
      repositoryUrl: PropTypes.string,
      license: PropTypes.string,
    })
  ).isRequired,
  analysisMode: PropTypes.string.isRequired,
  bundleId: PropTypes.string.isRequired,
};

function RepositoryLink({ repositoryUrl }) {
  if (!repositoryUrl) return null;
  const repositoryHost = new URL(repositoryUrl).host;

  let linkLogo;
  switch (repositoryHost) {
    case "github.com":
      linkLogo = <IoLogoGithub size={18} />;
      break;
    case "gitlab.com":
      linkLogo = <img src={gitlabLogo} alt="Gitlab repo" height="30" />;
      break;
    case "bitbucket.org":
      linkLogo = <img src={bitbucketLogo} alt="Bitbucket repo" height="18" />;
      break;
    default:
      linkLogo = "Repo";
  }
  return (
    <a
      target="_blank"
      rel="noreferrer"
      href={repositoryUrl}
      css={css`
        color: inherit;
      `}
    >
      {linkLogo}
    </a>
  );
}
RepositoryLink.propTypes = { repositoryUrl: PropTypes.string };

export default LibraryList;
