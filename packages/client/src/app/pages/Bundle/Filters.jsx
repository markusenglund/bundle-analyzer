import React, { useState } from "react";
import PropTypes from "prop-types";
import { round } from "lodash-es";
import { Range, getTrackBackground } from "react-range";
import { css } from "@emotion/react";

const Filters = ({ filter, setFilter, maxFootprint }) => {
  const [footprintRange, setFootprintRange] = useState([
    filter.footprintInterval.min,
    filter.footprintInterval.max,
  ]);
  const [minScore, setMinScore] = useState(filter.minScore);

  const handleFilterChange = (event) => {
    const onlyIncludeTopLevelDependencies = event.target.checked;
    setFilter({ ...filter, onlyIncludeTopLevelDependencies });
  };
  return (
    <div>
      <h3>Filters</h3>
      <label
        htmlFor="top-level-deps"
        css={css`
          display: flex;
          font-size: 12px;
          overflow-wrap: break-word;
          word-break: break-all;
          margin-bottom: 8px;
          @media (max-width: 800px) {
            font-size: 15px;
          }
        `}
      >
        <input
          name="top-level-deps"
          type="checkbox"
          checked={filter.onlyIncludeTopLevelDependencies}
          onChange={handleFilterChange}
        />
        Only top-level dependencies
      </label>
      <label
        css={css`
          font-size: 12px;
          @media (max-width: 800px) {
            font-size: 15px;
          }
        `}
      >
        Footprint:{" "}
        <div
          css={css`
            display: inline-block;
            min-width: 44px;
            text-align: right;
          `}
        >
          {round(footprintRange[0] / 1000, 1)} kB
        </div>{" "}
        - {round(footprintRange[1] / 1000, 1)} kB
        <div
          css={css`
            margin: 6px 10px 12px 10px;
          `}
        >
          <Range
            step={1000}
            min={0}
            max={maxFootprint}
            values={footprintRange}
            onChange={(values) => setFootprintRange(values)}
            onFinalChange={(values) =>
              setFilter({ ...filter, footprintInterval: { min: values[0], max: values[1] } })
            }
            renderTrack={({ props, children }) => (
              <div
                {...props}
                style={{
                  ...props.style,
                  height: "6px",
                  width: "140px",
                  borderRadius: "2px",
                  background: getTrackBackground({
                    values: footprintRange,
                    colors: ["#ccc", "#104fa1", "#ccc"],
                    min: 0,
                    max: maxFootprint,
                  }),
                }}
              >
                {children}
              </div>
            )}
            renderThumb={({ props }) => (
              <div
                {...props}
                style={{
                  ...props.style,
                  height: "18px",
                  width: "18px",
                  borderRadius: "50%",
                  backgroundColor: "#ccc",
                  border: "1px solid #104FA1",
                }}
              />
            )}
          />
        </div>
      </label>
      <label
        css={css`
          font-size: 12px;
          @media (max-width: 800px) {
            font-size: 15px;
          }
        `}
      >
        Min match score: {minScore}
        <div
          css={css`
            margin: 6px 10px 12px 10px;
          `}
        >
          <Range
            step={1}
            min={3}
            max={50}
            values={[minScore]}
            onChange={(values) => setMinScore(values[0])}
            onFinalChange={(values) => setFilter({ ...filter, minScore: values[0] })}
            renderTrack={({ props, children }) => (
              <div
                {...props}
                style={{
                  ...props.style,
                  height: "6px",
                  width: "140px",
                  borderRadius: "2px",
                  background: getTrackBackground({
                    values: [minScore],
                    colors: ["#ccc", "#104fa1"],
                    min: 3,
                    max: 50,
                  }),
                }}
              >
                {children}
              </div>
            )}
            renderThumb={({ props }) => (
              <div
                {...props}
                style={{
                  ...props.style,
                  height: "18px",
                  width: "18px",
                  borderRadius: "50%",
                  backgroundColor: "#ccc",
                  border: "1px solid #104FA1",
                }}
              />
            )}
          />
        </div>
      </label>
    </div>
  );
};

Filters.propTypes = {
  filter: PropTypes.exact({
    onlyIncludeTopLevelDependencies: PropTypes.bool.isRequired,
    footprintInterval: PropTypes.exact({
      min: PropTypes.number,
      max: PropTypes.number,
    }).isRequired,
    minIdfScoreRatio: PropTypes.number.isRequired,
    minScore: PropTypes.number.isRequired,
  }).isRequired,
  setFilter: PropTypes.func.isRequired,
  maxFootprint: PropTypes.number.isRequired,
};

export default Filters;
