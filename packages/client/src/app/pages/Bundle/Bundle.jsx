import React, { useMemo } from "react";
import PropTypes from "prop-types";
import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { Title } from "react-head";
import { formatDistanceToNow } from "date-fns";
import { useLocation } from "wouter";
import UrlInput from "../../components/UrlInput";
import { standardizeBundleUrl } from "../../../utils/standardizeUrl";
import useBundle from "../../hooks/useBundle";
import BundleResults from "./BundleResults";

const Error = styled("div")`
  margin-top: 40px;
  white-space: pre-line;
  font-size: 28px;
  text-align: center;
`;

const propTypes = {
  bundleUrl: PropTypes.string.isRequired,
  inputUrl: PropTypes.string.isRequired,
  setInputUrl: PropTypes.func.isRequired,
};

const Bundle = ({ bundleUrl: encodedBundleUrl, inputUrl, setInputUrl }) => {
  const decodedBundleUrl = decodeURIComponent(encodedBundleUrl);
  const { status, data: bundle, error } = useBundle({ encodedBundleUrl, decodedBundleUrl });

  // Memoize to prevent recalculation every time this component rerenders
  const scannedDateDistance = useMemo(
    () =>
      status === "success"
        ? formatDistanceToNow(new Date(bundle.bundleScan.date), { addSuffix: true })
        : null,
    [bundle?.bundleScan.date]
  );

  return (
    <div
      css={css`
        margin-top: 40px;
        margin-left: 20px;
        margin-right: 20px;
        padding-bottom: 50px;
        @media (max-width: 800px) {
          margin-left: 4px;
          margin-right: 4px;
        }
      `}
    >
      <Title>{decodedBundleUrl} libraries | Bundle Scanner</Title>
      <div>
        <UrlInput inputUrl={inputUrl} setInputUrl={setInputUrl} status={status} />
        {status === "success" && (
          <div
            css={css`
              display: flex;
              justify-content: space-between;
              font-size: 13px;
              margin-left: 8px;
              margin-top: 5px;
              max-width: 630px;
              margin-right: auto;
              margin-left: auto;
            `}
          >
            {bundle.meta.wasCached && bundle.lastScannedDate && (
              <div>Results cached from {scannedDateDistance} </div>
            )}
          </div>
        )}
      </div>
      {status === "loading" && (
        <div
          css={css`
            display: flex;
            flex-direction: column;
            align-items: center;
          `}
        >
          <div
            className="lds-dual-ring"
            css={css`
              margin-right: 12px;
              margin-bottom: 22px;
            `}
          />
          <div>Scanning bundle...</div>
        </div>
      )}
      {status === "error" && <Error>{error.message}</Error>}
      {status === "success" && <BundleResults bundle={bundle} />}
    </div>
  );
};

Bundle.propTypes = propTypes;

const BundleRedirectContainer = (props) => {
  const { bundleUrl: encodedBundleUrl } = props;
  const decodedBundleUrl = decodeURIComponent(encodedBundleUrl);
  const [, setLocation] = useLocation();
  const standardizedBundleUrl = standardizeBundleUrl(decodedBundleUrl);
  if (standardizedBundleUrl !== decodedBundleUrl) {
    setLocation(`/bundle/${encodeURIComponent(standardizedBundleUrl)}`, { replace: true });
    return null;
  }
  return <Bundle {...props} />;
};
BundleRedirectContainer.propTypes = propTypes;

export default BundleRedirectContainer;
