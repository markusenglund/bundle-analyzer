import React, { useEffect, useState, useMemo } from "react";
import { css } from "@emotion/react";
import { orderBy } from "lodash-es";
import useMedia from "use-media";
import { bundlePropType } from "../../propTypes";
import Overview from "./Overview";
import ShareWidget from "../../components/ShareWidget";
import LibraryList from "./LibraryList";
import Sidebar from "./Sidebar";
import MobileMenu from "./MobileMenu";

const useFilter = (maxFootprint) => {
  const [filter, setFilter] = useState(null);
  useEffect(() => {
    setFilter({
      onlyIncludeTopLevelDependencies: false,
      footprintInterval: { min: 0, max: maxFootprint },
      minIdfScoreRatio: 0,
      minScore: 3,
    });
  }, [maxFootprint]);

  return [filter, setFilter];
};

const BundleResults = ({ bundle }) => {
  const isMobile = useMedia({ maxWidth: "800px" });

  const maxFootprint = Math.max(
    ...bundle.bundleScan.matchedReleases.map((release) => release.cumulativeIntervalSize)
  );

  const [filter, setFilter] = useFilter(maxFootprint);
  const [sorting, setSorting] = useState("cumulative-interval-size");

  const releasesWithDependentsChains = useMemo(
    () => calculateReleases(bundle, filter, sorting),
    [bundle, filter, sorting]
  );

  return (
    <div>
      <h2
        css={css`
          margin-bottom: 10px;

          display: flex;
          justify-content: center;
          align-items: center;
          overflow: hidden;
          text-overflow: ellipsis;
        `}
      >
        {bundle._id}
      </h2>
      <Overview
        bundleSize={bundle.bundleScan.sizeBytes}
        numMatchedLibraries={bundle.bundleScan.matchedReleases.length}
      />
      <p
        css={css`
          text-align: center;
          font-style: italic;
        `}
      >
        {releasesWithDependentsChains.length === 0 && "No libraries were found"}
      </p>
      {isMobile && releasesWithDependentsChains.length > 0 && (
        <MobileMenu
          maxFootprint={maxFootprint}
          filter={filter}
          setFilter={setFilter}
          sorting={sorting}
          setSorting={setSorting}
        />
      )}
      {releasesWithDependentsChains.length > 0 && (
        <>
          <div
            css={css`
              display: flex;
              justify-content: center;
            `}
          >
            <LibraryList
              releases={releasesWithDependentsChains}
              analysisMode={bundle.bundleScan.analysisMode}
              bundleId={bundle._id}
            />
            {filter && !isMobile ? (
              <Sidebar
                maxFootprint={maxFootprint}
                filter={filter}
                setFilter={setFilter}
                sorting={sorting}
                setSorting={setSorting}
              />
            ) : null}
          </div>
          <ShareWidget pageType="bundle" bundleUrl={bundle.fullUrl} />
        </>
      )}
    </div>
  );
};
BundleResults.propTypes = {
  bundle: bundlePropType,
};

function addDependentsChains(releases) {
  const librarySet = new Set(releases.map(({ libraryId }) => libraryId));
  const dependencyDependentsMap = new Map();

  for (const release of releases) {
    const { dependencies = [], libraryId } = release;
    for (const dependency of dependencies) {
      if (librarySet.has(dependency.libraryId)) {
        const dependents = dependencyDependentsMap.get(dependency.libraryId);
        if (dependents) {
          dependents.push(libraryId);
        } else {
          dependencyDependentsMap.set(dependency.libraryId, [libraryId]);
        }
      }
    }
  }

  const releasesWithDependentsChain = releases.map((release) => {
    // Recursively get one chain of dependents
    const dependentsChain = [];
    let curLibraryId = release.libraryId;
    while (curLibraryId != null) {
      curLibraryId = dependencyDependentsMap.get(curLibraryId)?.[0];
      if (curLibraryId) {
        dependentsChain.push(curLibraryId);
      }
    }

    return { ...release, dependentsChain };
  });

  return releasesWithDependentsChain;
}

function calculateReleases(bundle, filter, sorting) {
  if (!filter) {
    return [];
  }
  const { onlyIncludeTopLevelDependencies, footprintInterval, minIdfScoreRatio, minScore } = filter;

  const matchedReleasesWithDependentsChains = addDependentsChains(
    bundle.bundleScan.matchedReleases
  );

  const pickedReleases = matchedReleasesWithDependentsChains.filter((release) => {
    if (onlyIncludeTopLevelDependencies && release.dependentsChain.length > 0) {
      return false;
    }
    if (
      release.cumulativeIntervalSize < footprintInterval.min ||
      release.cumulativeIntervalSize > footprintInterval.max
    ) {
      return false;
    }
    if (release.idfScoreRatio < minIdfScoreRatio) {
      return false;
    }
    if (release.score < minScore) {
      return false;
    }
    return true;
  });

  let sortingKey, order;
  switch (sorting) {
    case "alphabetical": {
      sortingKey = "_id";
      order = "asc";
      break;
    }
    case "cumulative-interval-size": {
      sortingKey = "cumulativeIntervalSize";
      order = "desc";
      break;
    }
    case "score": {
      sortingKey = "score";
      order = "desc";
      break;
    }
    case "idf-score-ratio": {
      sortingKey = "idfScoreRatio";
      order = "desc";
      break;
    }
    default: {
      throw new Error(`Unknown sorting string: '${sorting}'`);
    }
  }

  const sortedReleases = orderBy(pickedReleases, sortingKey, order);

  return sortedReleases;
}

export default React.memo(BundleResults);
