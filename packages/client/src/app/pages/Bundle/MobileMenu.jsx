import React, { useState } from "react";
import PropTypes from "prop-types";
import Dialog from "@reach/dialog";
import { css } from "@emotion/react";
import { FaFilter } from "react-icons/fa";
import SortingSelect from "../Website/SortingSelect";
import Filters from "./Filters";

const MobileMenu = ({ filter, setFilter, sorting, setSorting, maxFootprint }) => {
  const [showMobileDialog, setShowMobileDialog] = useState(false);

  return (
    <div
      css={css`
        display: flex;
        justify-content: space-around;
        margin-bottom: 14px;
      `}
    >
      <button
        onClick={() => setShowMobileDialog(true)}
        css={css`
          background: #555;
          font-family: inherit;
          color: #eee;
          height: 36px;
          width: 100px;
          border: 1px solid #777;
          font-size: 18px;
          cursor: pointer;
          font-weight: bold;
          border-radius: 6px;
          :hover {
            background: #444;
          }
        `}
      >
        <FaFilter size={14} /> Filter
      </button>
      <SortingSelect sorting={sorting} setSorting={setSorting} />
      <Dialog
        aria-label="Filters"
        isOpen={showMobileDialog}
        onDismiss={() => setShowMobileDialog(false)}
      >
        <div>
          <button
            css={css`
              background: #555;
              font-family: inherit;
              color: #eee;
              height: 36px;
              width: 100px;
              border: 1px solid #777;
              font-size: 18px;
              cursor: pointer;
              font-weight: bold;
              border-radius: 6px;
              :hover {
                background: #444;
              }
            `}
            onClick={() => setShowMobileDialog(false)}
          >
            Close
          </button>
        </div>
        <Filters filter={filter} setFilter={setFilter} maxFootprint={maxFootprint} />
      </Dialog>
    </div>
  );
};

MobileMenu.propTypes = {
  filter: PropTypes.exact({
    onlyIncludeTopLevelDependencies: PropTypes.bool.isRequired,
    footprintInterval: PropTypes.exact({
      min: PropTypes.number,
      max: PropTypes.number,
    }).isRequired,
    minIdfScoreRatio: PropTypes.number.isRequired,
    minScore: PropTypes.number.isRequired,
  }).isRequired,
  setFilter: PropTypes.func.isRequired,
  sorting: PropTypes.string.isRequired,
  setSorting: PropTypes.func.isRequired,
  maxFootprint: PropTypes.number.isRequired,
};

export default MobileMenu;
