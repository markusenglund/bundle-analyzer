import React, { useEffect, useState } from "react";
import { css } from "@emotion/react";
import { orderBy } from "lodash-es";
import { useDebounceCallback } from "@react-hook/debounce";
import { useLocation } from "wouter";
import { Title } from "react-head";
import ky from "../../utils/ky";

import {
  Combobox,
  ComboboxInput,
  ComboboxPopover,
  ComboboxList,
  ComboboxOption,
} from "@reach/combobox";

const LibrarySearch = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const libraries = useLibrarySearch(searchTerm);
  const [location, setLocation] = useLocation();

  const handleSearchTermChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleSelect = (item) => {
    setSearchTerm(item);
    setLocation(`/library/${encodeURIComponent(item)}`);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setLocation(`/library/${encodeURIComponent(searchTerm)}`);
  };

  return (
    <div
      css={css`
        text-align: center;
      `}
    >
      <Title>NPM libraries | Bundle Scanner</Title>
      <h3 id="library-search">Find out where a library is used on the web</h3>
      <p>
        This feature is experimental. Results are currently limited to only the 1-2% of websites
        that have public source maps.
      </p>
      <form onSubmit={handleSubmit}>
        <Combobox openOnFocus onSelect={handleSelect}>
          <ComboboxInput aria-labelledby="library-search" onChange={handleSearchTermChange} />
          <ComboboxPopover>
            {libraries.length > 0 ? (
              <ComboboxList aria-labelledby="library-search">
                {libraries.map((library) => {
                  const { name } = library.package;
                  return <ComboboxOption value={name} />;
                })}
              </ComboboxList>
            ) : null}
          </ComboboxPopover>
        </Combobox>
      </form>
    </div>
  );
};

const libraryCache = new Map();
function useLibrarySearch(searchTerm) {
  const [libraries, setLibraries] = useState([]);
  const trimmedSearchTerm = searchTerm.trim();

  const debouncedLibraryFetch = useDebounceCallback(async (q) => {
    if (!q || libraryCache.has(q)) return;
    const queryString = new URLSearchParams({ q, size: 20 }).toString();
    const fetchedLibraries = await ky(
      `https://api.npms.io/v2/search/suggestions?${queryString}`
    ).json();

    const orderedLibraries = orderBy(
      fetchedLibraries,
      (library) => library.searchScore * Math.sqrt(library.score.detail.popularity),
      "desc"
    ).slice(0, 5);

    setLibraries(orderedLibraries);
    libraryCache.set(trimmedSearchTerm, orderedLibraries);
  }, 200);

  useEffect(async () => {
    if (libraryCache.has(trimmedSearchTerm)) {
      setLibraries(libraryCache.get(trimmedSearchTerm));
    }
    debouncedLibraryFetch(trimmedSearchTerm);
  }, [trimmedSearchTerm]);

  return libraries;
}

export default LibrarySearch;
