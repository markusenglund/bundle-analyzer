import React from "react";
import prettyBytes from "pretty-bytes";
import { css } from "@emotion/react";

const LibraryIntervalOverview = ({ release }) => {
  return (
    <div
      css={css`
        display: flex;
        justify-content: center;
        text-align: center;
        margin-top: 30px;
      `}
    >
      <div
        css={css`
          display: flex;
          justify-content: space-around;
          padding: 20px;
          margin: 20px 0 10px 0;
          width: 600px;
          background: #342323;
          border-radius: 10px;
          box-shadow: 0px 1px 6px #111;
        `}
      >
        <div>
          <div>Library files</div>
          <div
            css={css`
              font-size: 40px;
              margin: 8px;
              margin-bottom: 0;
              font-weight: bold;
            `}
          >
            {release.intervals.length}
          </div>
        </div>
        <div>
          <div>Size of matching segments</div>
          <div
            css={css`
              font-size: 40px;
              margin: 8px;
              margin-bottom: 0;
              font-weight: bold;
            `}
          >
            {prettyBytes(release.cumulativeIntervalSize)}
          </div>
        </div>
      </div>
    </div>
  );
};

export default LibraryIntervalOverview;
