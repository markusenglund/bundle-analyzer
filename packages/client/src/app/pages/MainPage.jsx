import React from "react";
import PropTypes from "prop-types";
import { Title } from "react-head";

import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { Link } from "wouter";
import UrlInput from "../components/UrlInput";
import PopularSearches from "../components/PopularSearches";

const Container = styled("div")`
  display: flex;
  justify-content: center;
`;

const Content = styled("div")`
  max-width: 900px;
  margin: 0 100px;
  text-align: center;
  @media (max-width: 800px) {
    margin: 0 16px;
  }
  @media (max-width: 350px) {
    margin: 0 4px;
  }
`;

const H1 = styled("h1")`
  font-size: 34px;
  font-weight: normal;
  margin-top: 150px;
  @media (max-width: 800px) {
    margin-top: 50px;
    font-size: 28px;
  }
`;

const Paragraph = styled("p")`
  font-size: 22px;
  margin: 35px 0;
  line-height: 140%;
  @media (max-width: 800px) {
    font-size: 20px;
  }
`;

const A = styled("a")`
  color: #9ef;
`;

const MainPage = ({ inputUrl, setInputUrl }) => {
  return (
    <div
      css={css`
        display: flex;
        flex-direction: column;
        min-height: calc(100vh - 72px);
        justify-content: space-between;
      `}
    >
      <Title>Bundle Scanner - Identify NPM libraries included in Javascript files</Title>
      <Container>
        <Content>
          <H1>Identify NPM libraries included in Javascript files</H1>
          <Paragraph>
            Enter the URL of a website. Bundle Scanner will fetch every Javascript file from the
            website and search through the files for code that matches any of the 101,962 releases
            it has indexed from 35,003 of the most popular npm libraries on the web.{" "}
            <Link href="/about">
              <A>Learn more.</A>
            </Link>
          </Paragraph>
          <UrlInput inputUrl={inputUrl} setInputUrl={setInputUrl} />
        </Content>
      </Container>
      <PopularSearches />
    </div>
  );
};

MainPage.propTypes = {
  inputUrl: PropTypes.string.isRequired,
  setInputUrl: PropTypes.func.isRequired,
};

export default MainPage;
