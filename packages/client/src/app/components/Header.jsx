import React from "react";
import { css } from "@emotion/react";
import { Link } from "wouter";
import logoSvg from "../../images/bundle-scanner-logo-3.svg";

const Header = () => (
  <div
    css={css`
      display: flex;
      justify-content: center;
      height: 70px;
      border-color: #ddd;
      border-bottom: 1px solid;
    `}
  >
    <div
      css={css`
        display: flex;
        justify-content: space-between;
        align-items: center;
        width: 1200px;
        margin: 0 20px;
      `}
    >
      <div
        css={css`
          display: flex;
          align-items: center;
        `}
      >
        <Link href="/">
          <a
            css={css`
              display: flex;
              align-items: center;
              cursor: pointer;
              text-decoration: none;
              color: inherit;
            `}
          >
            <img
              css={css`
                width: 26px;
                margin: 8px;
              `}
              src={logoSvg}
              alt="Bundle Scanner logo"
            />
            <div
              css={css`
                font-size: 24px;
                margin: 0;
              `}
            >
              Bundle Scanner
              <span
                css={css`
                  font-size: 12px;
                  margin-left: 10px;
                `}
              >
                BETA
              </span>
            </div>
          </a>
        </Link>
      </div>
      <div
        css={css`
          display: flex;
          align-items: center;
        `}
      >
        <Link href="/about">
          <a
            css={css`
              text-decoration: none;
              color: #eee;
              padding: 0 10px;
              margin: 0 4px;
              :hover {
                color: #fff;
              }
            `}
          >
            About
          </a>
        </Link>
        {/* <Link href="/libraries">
          <a
            css={css`
              text-decoration: none;
              color: #eee;
              padding: 0 10px;
              margin: 0 4px;
              :hover {
                color: #fff;
              }
            `}
          >
            Library search
            <div
              css={css`
                color: orange;
                font-size: 12px;
                text-align: center;
              `}
            >
              Experimental
            </div>
          </a>
        </Link> */}
      </div>
    </div>
  </div>
);

export default Header;
