import React, { useState } from "react";
import PropTypes from "prop-types";
import { BiLink } from "react-icons/bi";
import { FaTwitter } from "react-icons/fa";
import { css } from "@emotion/react";

const ShareWidget = ({ pageType, websiteUrl, bundleUrl }) => {
  const [hasCopied, setHasCopied] = useState(false);
  const currentUrl = window?.location.href;

  const copyToClipboard = async () => {
    await navigator.clipboard?.writeText(currentUrl);
    setHasCopied(true);
  };
  let twitterText;
  switch (pageType) {
    case "website": {
      twitterText = `Find out which npm libraries are used on '${websiteUrl}' with the help of Bundle Scanner.`;
      break;
    }
    case "bundle": {
      twitterText = `Find out which npm libraries are bundled into '${bundleUrl}' with the help of Bundle Scanner.`;
      break;
    }
    default: {
      twitterText = "Check out Bundle Scanner.";
    }
  }

  const twitterShareParams = new URLSearchParams({
    text: twitterText,
    url: currentUrl,
  }).toString();

  return (
    <div
      css={css`
        text-align: center;
        padding: 20px;
        background: rgb(23, 27, 28);
        margin: auto;
        width: 240px;
        box-shadow: 0px 1px 6px #111;
      `}
    >
      <p
        css={css`
          margin-top: 0;
          margin-bottom: 20px;
          font-size: 20px;
          font-weight: bold;
        `}
      >
        Share results
      </p>
      <div
        css={css`
          display: flex;
          justify-content: space-evenly;
        `}
      >
        <button
          onClick={copyToClipboard}
          css={css`
            background: inherit;
            border: none;
            cursor: pointer;
            color: inherit;
            font-size: 14px;
            width: 72px;
            height: 72px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            padding: 0;
            :hover {
              background: rgb(30, 34, 35);
            }
          `}
        >
          <BiLink
            size={30}
            css={css`
              margin-bottom: 4px;
            `}
          />
          {hasCopied ? "Copied!" : "Copy link"}
        </button>
        <a
          href={`https://twitter.com/intent/tweet?${twitterShareParams}`}
          target="_blank"
          rel="noreferrer"
          css={css`
            text-decoration: none;
            display: block;
            color: inherit;
            font-size: 14px;
            width: 72px;
            height: 72px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            :hover {
              background: rgb(30, 34, 35);
            }
          `}
        >
          <FaTwitter
            size={28}
            color="rgb(29, 161, 242)"
            css={css`
              margin-bottom: 4px;
            `}
          />
          <span>Tweet</span>
        </a>
      </div>
    </div>
  );
};

ShareWidget.propTypes = {
  pageType: PropTypes.oneOf(["website", "bundle", "interval"]).isRequired,
  websiteUrl: PropTypes.string,
  bundleUrl: PropTypes.string,
};

export default ShareWidget;
