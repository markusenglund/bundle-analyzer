import React, { useEffect, useState } from "react";
import styled from "@emotion/styled";
import { useLocation } from "wouter";
import PropTypes from "prop-types";
import normalizeUrl from "normalize-url";
import { captureMessage } from "@sentry/browser";

const Form = styled("form")`
  display: flex;
  justify-content: center;
  /* box-shadow: 0 1px 6px #111; */
  filter: drop-shadow(0 1px 4px #111);
`;

const Https = styled("div")`
  height: 54px;
  border: 3px solid #111;
  padding: 0 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  font-size: 20px;
  border-radius: 10px 0 0 10px;
  @media (max-width: 800px) {
    display: none;
  }
`;
const Input = styled("input")`
  background: #111;
  color: #eee;
  height: 54px;
  flex-basis: 440px;
  width: 120px;
  padding: 14px;
  border: 0;
  border-radius: 1px;
  font-size: 20px;
  box-sizing: border-box;
  ::placeholder {
    color: #999;
  }
  @media (max-width: 800px) {
    border-radius: 10px 0 0 10px;
  }
`;

const Button = styled("button")`
  background: #247e24;
  font-family: inherit;
  color: #eee;
  height: 54px;
  width: 100px;
  border: 0px;
  font-size: 20px;
  cursor: pointer;
  font-weight: bold;
  border-radius: 0 10px 10px 0;
  :hover {
    background: #050;
  }
`;

const Error = styled("div")`
  margin-top: 40px;
  white-space: pre-line;
  font-size: 28px;
  text-align: center;
`;

const UrlInput = ({ inputUrl, setInputUrl, status }) => {
  const [validationIssue, setValidationIssue] = useState(null);
  const [, setLocation] = useLocation();

  useEffect(() => {
    if (validationIssue) {
      captureMessage(validationIssue);
    }
  }, [validationIssue]);

  const handleWebsiteSubmit = async (e) => {
    e.preventDefault();
    if (!inputUrl) return;
    let isBundleUrl = false;
    try {
      if (!inputUrl.includes(".")) {
        setValidationIssue(`'${inputUrl}' is not a valid URL. Add more dots.`);
        return;
      }
      const normalizedUrl = normalizeUrl(inputUrl, {
        removeQueryParameters: true,
        stripHash: true,
        stripWWW: false,
      });
      isBundleUrl = normalizedUrl.match(/\.(m)?js$/);
    } catch {
      setValidationIssue(`'${inputUrl}' is not a valid URL.`);
      return;
    }

    if (isBundleUrl) {
      setLocation(`/bundle/${encodeURIComponent(inputUrl)}`);
    } else {
      setLocation(`/website/${encodeURIComponent(inputUrl)}`);
    }

    setValidationIssue(null);
  };

  return (
    <>
      <Form onSubmit={handleWebsiteSubmit}>
        <Https>https://</Https>
        <Input
          type="text"
          autoCapitalize="off"
          value={inputUrl}
          onChange={(event) => {
            setInputUrl(event.target.value);
          }}
          placeholder="URL of a website or Javascript file"
        />
        <Button type="submit" disabled={status === "loading"}>
          Scan
        </Button>
      </Form>
      {validationIssue && <Error>{validationIssue}</Error>}
    </>
  );
};

UrlInput.propTypes = {
  inputUrl: PropTypes.string.isRequired,
  setInputUrl: PropTypes.func.isRequired,
  status: PropTypes.string,
};

export default UrlInput;
