import React from "react";
import { css } from "@emotion/react";
import { Link } from "wouter";
import { truncate } from "lodash-es";

const hardCodedPopularSearches = [
  "facebook.com",
  "kentcdodds.com",
  "jajiga.com",
  "bundlescanner.com",
  "twitter.com",
];

const PopularSearches = () => {
  return (
    <div
      css={css`
        justify-content: center;
        align-items: center;
        padding: 30px;
        background: #111;
        margin-top: 120px;
        min-height: 140px;
        display: flex;
        flex-direction: column;
        box-sizing: content-box;
      `}
    >
      <h3
        css={css`
          margin-top: 0;
        `}
      >
        Popular searches
      </h3>
      <div
        css={css`
          display: flex;
          flex-wrap: wrap;
        `}
      >
        {hardCodedPopularSearches.map((url) => (
          <Link href={`/website/${encodeURIComponent(url)}`} key={url}>
            <a
              css={css`
                color: #9ef;
                margin: 10px;
              `}
            >
              {truncate(url, { length: 36 })}
            </a>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default PopularSearches;
