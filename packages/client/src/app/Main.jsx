import React, { Fragment, useState, useLayoutEffect } from "react";
import { Route, Redirect, useLocation, useRoute } from "wouter";

import Website from "./pages/Website/Website";
import Bundle from "./pages/Bundle/Bundle";
import LibrarySearch from "./pages/LibrarySearch";
import Library from "./pages/Library";
import MainPage from "./pages/MainPage";
import About from "./pages/About";
import IntervalInspection from "./pages/IntervalInspection/IntervalInspection";

const Main = () => {
  const [location] = useLocation();
  const [websiteMatch, websiteParams] = useRoute("/website/:websiteUrl");
  // Store inputUrl here to keep it consistent between different views
  const [inputUrl, setInputUrl] = useState(
    websiteMatch ? decodeURIComponent(websiteParams.websiteUrl) : ""
  );

  // Scroll to top on route change
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);

  return (
    <>
      <Route path="/">
        <MainPage inputUrl={inputUrl} setInputUrl={setInputUrl} />
      </Route>
      <Route path="/website/:websiteUrl">
        {(params) => (
          <Website inputUrl={inputUrl} setInputUrl={setInputUrl} websiteUrl={params.websiteUrl} />
        )}
      </Route>
      <Route path="/website/:websiteUrl/:subpage">
        {(params) => <Redirect to={`/website/${params.websiteUrl}`} />}
      </Route>
      <Route path="/about">
        <About />
      </Route>
      <Route path="/bundle/:bundleUrl">
        {({ bundleUrl }) => (
          <Bundle bundleUrl={bundleUrl} inputUrl={inputUrl} setInputUrl={setInputUrl} />
        )}
      </Route>
      <Route path="/bundle/:bundleUrl/inspect/:libraryId">
        {({ bundleUrl, libraryId }) => {
          return (
            <IntervalInspection libraryId={decodeURIComponent(libraryId)} bundleUrl={bundleUrl} />
          );
        }}
      </Route>
      <Route path="/libraries">
        <LibrarySearch />
      </Route>
      <Route path="/library/:libraryId">
        {({ libraryId }) => <Library libraryId={libraryId} />}
      </Route>
    </>
  );
};

export default Main;
