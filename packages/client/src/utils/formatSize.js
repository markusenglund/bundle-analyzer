import { round } from "lodash-es";

const formatSize = (bytes) => {
  let unit;
  let roundedSize;
  if (bytes >= 1_000_000) {
    unit = "MB";
    roundedSize = round(bytes / 1_000_000, 2);
  } else if (bytes >= 1_000) {
    unit = "kB";
    roundedSize = round(bytes / 1_000);
  } else {
    unit = "bytes";
    roundedSize = bytes;
  }

  return { unit, roundedSize };
};

export default formatSize;
