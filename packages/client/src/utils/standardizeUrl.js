import normalizeUrl from "normalize-url";

export const standardizeWebsiteUrl = (url) =>
  normalizeUrl(url, {
    stripHash: true,
    stripProtocol: true,
    removeQueryParameters: true,
    stripWWW: false,
  });

export const standardizeBundleUrl = (url) =>
  normalizeUrl(url, { stripHash: true, stripProtocol: true });
