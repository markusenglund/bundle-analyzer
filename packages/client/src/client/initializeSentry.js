import { init } from "@sentry/react";
import { Integrations } from "@sentry/tracing";

init({
  dsn: "https://4a070e487cd84d849e55aad98fbda3ea@o1040959.ingest.sentry.io/6009931",
  integrations: [new Integrations.BrowserTracing()],
  tracesSampleRate: 1.0,
});
