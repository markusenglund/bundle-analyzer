import React from "react";
import { render } from "react-dom";
import { HeadProvider } from "react-head";
import App from "../app/App";
import "./initializePlausible";
import "./initializeSentry";

render(
  <HeadProvider>
    <App />
  </HeadProvider>,
  document.getElementById("root")
);
