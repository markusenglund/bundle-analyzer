import Plausible from "plausible-tracker";

const { enableAutoPageviews } = Plausible({
  domain: "bundlescanner.com",
});
enableAutoPageviews();
