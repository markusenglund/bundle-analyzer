import { brotliCompressSync } from "zlib";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import image from "@rollup/plugin-image";
import replace from "@rollup/plugin-replace";
import { babel } from "@rollup/plugin-babel";
import { terser } from "rollup-plugin-terser";
import gzip from "rollup-plugin-gzip";
import del from "rollup-plugin-delete";
import copy from "rollup-plugin-copy";
import json from "@rollup/plugin-json";
import html from "@rollup/plugin-html";
import filesize from "rollup-plugin-filesize";
import omt from "@surma/rollup-plugin-off-main-thread";
import comlink from "@surma/rollup-plugin-comlink";
import prerender from "./rollupPlugins/prerenderPlugin";

const getPlugins = () => [
  babel({ babelHelpers: "bundled" }),
  replace({
    preventAssignment: true,
    "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
  }),
  resolve({
    extensions: [".js", ".jsx"],
    preferBuiltins: false,
  }),
  json(),
  commonjs(),
  image(),
  comlink(),
  omt(),
  ...(process.env.NODE_ENV === "production"
    ? [
        copy({
          targets: [
            { src: "src/favicons", dest: "dist/public" },
            {
              src: "src/images",
              dest: "dist/public",
            },
          ],
        }),
        prerender(),
        del({ targets: "dist/*" }),
        filesize(),
        terser(),
        gzip({
          customCompression: (content) => brotliCompressSync(Buffer.from(content)),
          fileName: ".br",
          minSize: 2 * 1024,
        }),
      ]
    : [html({ publicPath: "/static/", template })]),
];

async function template({ files, publicPath }) {
  const scripts = (files.js || [])
    .map(({ fileName }) => `<script src="${publicPath}${fileName}"></script>`)
    .join("\n");

  return /* html */ `
  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta property="og:image" content="https://bundlescanner.com/static/images/bundle-scanner-screenshot.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:height" content="630" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Bundle Scanner" />
    <meta property="og:description" content="Bundle Scanner identifies which npm libraries are used on any website. It downloads every Javascript file from a URL and searches through the files for code that matches one of the 35,000 most popular npm libraries." />
    <meta property="description" content="Bundle Scanner identifies which npm libraries are used on any website. It downloads every Javascript file from a URL and searches through the files for code that matches one of the 35,000 most popular npm libraries." />
    <link rel="apple-touch-icon" sizes="180x180" href="/static/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/static/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/static/favicons/favicon-16x16.png">
    <link rel="manifest" href="/static/favicons/site.webmanifest">
  </head>
  <body>
    <div id="root"></div>
    ${scripts}
  </body>
</html>`;
}

const client = {
  input: "src/client/index.jsx",
  output: {
    dir: "dist/public",
    entryFileNames: "bundle-[hash].js",
    sourcemap: true,
    format: "amd",
  },
  plugins: getPlugins(),
};

export default [client];
