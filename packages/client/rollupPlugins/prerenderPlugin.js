import { rollup } from "rollup";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import { babel } from "@rollup/plugin-babel";
import replace from "@rollup/plugin-replace";
import requireFromString from "require-from-string";
import json from "@rollup/plugin-json";
import image from "@rollup/plugin-image";
import ignore from "rollup-plugin-ignore";

export default function prerenderPlugin() {
  // Run rollup _inside_ this plugin to create the prerender function
  // Then run the function created by rollup to generate html
  return {
    name: "prerender-plugin",
    buildStart() {
      this.addWatchFile("./src/prerender/index.jsx");
    },
    async generateBundle(options, bundle) {
      const prerenderBundle = await rollup({
        input: "src/prerender/index.jsx",
        plugins: [
          ignore(["comlink:../tokenizationWorker.js"]),
          babel({ babelHelpers: "bundled" }),
          replace({
            preventAssignment: true,
            "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
          }),
          resolve({
            extensions: [".js", ".jsx"],
            preferBuiltins: false,
          }),
          json(),
          commonjs(),
          image(),
        ],
      });

      const { output } = await prerenderBundle.generate({
        format: "cjs",
        file: "dist/prerender.js",
        exports: "auto",
      });

      const { code } = output[0];

      const prerender = requireFromString(code);

      const bundleFileName = Object.keys(bundle)[0];

      const pages = [
        {
          route: "/",
          name: "Main page",
          fileName: "main.html",
        },
        {
          route: "/about",
          name: "About",
          fileName: "about.html",
        },
        {
          route: "/libraries",
          name: "Library search",
          fileName: "libraries.html",
        },
        {
          route: "/__EMPTY_ROUTE__",
          name: "Generic page",
          fileName: "index.html",
        },
      ];

      for (const page of pages) {
        const html = prerender({
          bundleUrl: `/static/${bundleFileName}`,
          route: page.route,
        });

        const htmlFile = {
          type: "asset",
          source: html,
          name: page.name,
          fileName: page.fileName,
        };

        this.emitFile(htmlFile);
      }
    },
  };
}
