import { Type, Static } from "@sinclair/typebox";

const MatchedReleases = Type.Array(
  Type.Object({
    _id: Type.String(),
    libraryId: Type.String(),
    sourceMapFrequency: Type.Optional(Type.Number()),
    cumulativeIntervalSize: Type.Optional(Type.Number()),
    intervals: Type.Array(
      Type.Object({
        start: Type.Number(),
        end: Type.Optional(Type.Number()),
        fileName: Type.String(),
        libraryId: Type.String(),
        intervalMatchRatio: Type.Optional(Type.Number()),
        intervalSizeFourGrams: Type.Optional(Type.Number()),
        maxIntervalSizeFourGrams: Type.Optional(Type.Number()),
      })
    ),
    dependencies: Type.Optional(
      Type.Array(Type.Object({ libraryId: Type.String(), versionRange: Type.String() }))
    ),
    description: Type.Optional(Type.String()),
    repositoryUrl: Type.Optional(Type.String()),
    license: Type.Optional(Type.String()),
    bundledDependencies: Type.Optional(
      Type.Array(Type.Object({ libraryId: Type.String(), version: Type.String() }))
    ),
    score: Type.Optional(Type.Number()),
    version: Type.Optional(Type.String()),
    fileIds: Type.Optional(Type.Array(Type.String())),
    maxIdfScore: Type.Optional(Type.Number()),
    idfScoreRatio: Type.Optional(Type.Number()),
  })
);

export const SourceMapData = Type.Object({
  url: Type.Optional(Type.String()),
  libraryIds: Type.Array(Type.String()),
  modulePaths: Type.Array(Type.String()),
});

export const AnalysisMode = Type.Union([Type.Literal("scan"), Type.Literal("sourcemap")]);

export const BundleScan = Type.Object({
  bundleUrl: Type.String(),
  scannerVersion: Type.String(),
  date: Type.String(),
  numChars: Type.Number(),
  sizeBytes: Type.Number(),
  sizeBytesGzip: Type.Number(),
  analysisMode: AnalysisMode,
  sourceMapData: Type.Optional(SourceMapData),
  matchedReleases: MatchedReleases,
  checkSum: Type.Optional(Type.String()),
});

export const BundleWithScanBase = Type.Object({
  _id: Type.String(),
  fullUrl: Type.String(),
  thirdPartyWebData: Type.Optional(
    Type.Object({
      name: Type.String(),
      categories: Type.Array(Type.String()),
    })
  ),
  bundleScan: BundleScan,
});

export const BundleWithScan = Type.Intersect([
  BundleWithScanBase,
  Type.Object({
    domains: Type.Optional(Type.Array(Type.String())),
    hosts: Type.Optional(Type.Array(Type.String())),
    pageUrls: Type.Optional(Type.Array(Type.String())),
  }),
]);

export const BundleWithScanAndMeta = Type.Intersect([
  BundleWithScan,
  Type.Object({ meta: Type.Object({ wasCached: Type.Boolean() }) }),
]);

export const BundleResponse = Type.Object({
  data: BundleWithScanBase,
});

export const Website = Type.Object({
  _id: Type.String(),
  faviconUrl: Type.Optional(Type.String()),
  host: Type.Optional(Type.String()),
  domain: Type.Optional(Type.String()),
  redirectedUrls: Type.Optional(Type.Array(Type.String())),
  createdDate: Type.String(),
  urlScan: Type.Object({
    url: Type.String(),
    date: Type.String(),
    source: Type.Union([Type.Literal("extension"), Type.Literal("puppeteer")]),
    firstPartyBundleIds: Type.Array(Type.String()),
    thirdPartyBundleIds: Type.Array(Type.String()),
    totalSizeBytes: Type.Number(),
    totalSizeBytesGzip: Type.Number(),
    bundlesById: Type.Record(Type.String(), BundleWithScanAndMeta),
    failedBundleIds: Type.Array(Type.String()),
  }),
});

export const WebsiteWithMeta = Type.Intersect([
  Website,
  Type.Object({
    meta: Type.Object({
      redirectedFrom: Type.Optional(Type.String()),
      standardizedWebsiteUrl: Type.String(),
      timeSpent: Type.Number(),
      wasCached: Type.Boolean(),
    }),
  }),
]);

export const WebsiteReply = Type.Object({
  data: Website,
});

export type WebsiteReplyType = Static<typeof WebsiteReply>;
export type WebsiteType = Static<typeof Website>;
export type WebsiteWithMetaType = Static<typeof WebsiteWithMeta>;
export type BundleWithScanBaseType = Static<typeof BundleWithScanBase>;
export type BundleWithScanType = Static<typeof BundleWithScan>;
export type BundleResponseType = Static<typeof BundleResponse>;
export type SourceMapDataType = Static<typeof SourceMapData> | undefined;
export type MatchedReleasesType = Static<typeof MatchedReleases>;
export type AnalysisModeType = Static<typeof AnalysisMode>;
