import { builtinModules as builtInModules } from "module";

const builtInModuleSet = new Set(builtInModules);

const isBuiltInModule = (module: string): boolean =>
  builtInModuleSet.has(module);

export default isBuiltInModule;
