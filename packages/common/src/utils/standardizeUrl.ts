import normalizeUrl from "normalize-url";

export const standardizeWebsiteUrl = (url: string): string => {
  return normalizeUrl(url, {
    stripHash: true,
    stripProtocol: true,
    removeQueryParameters: true,
    stripWWW: false,
  });
};

export const standardizeBundleUrl = (url: string): string =>
  normalizeUrl(url, { stripHash: true, stripProtocol: true, stripWWW: false });
