import winston from "winston";
import LogzioWinstonTransport from "winston-logzio";
import { threadId, isMainThread } from "worker_threads";

const logger = winston.createLogger({
  // "levels" needed for fastify compatibility
  levels: Object.assign({ fatal: 0, warn: 4, trace: 7 }, winston.config.syslog.levels),
  level: process.env.LOG_LEVEL ?? "info",
  format: winston.format.errors({ stack: true }),
  defaultMeta: { threadId, service: process.env.SERVICE_NAME ?? "no-service" },
});

if (process.env.NODE_ENV !== "production") {
  const { printf, combine, timestamp, colorize } = winston.format;

  const format = printf((info) => {
    const { level, message, timestamp, stack } = info;
    const output = stack ?? message;
    if (isMainThread) {
      return `${timestamp} ${level}: ${output}`;
    } else {
      return `${timestamp} - [${threadId}] ${level}: ${output}`;
    }
  });
  logger.add(
    new winston.transports.Console({
      format: combine(colorize(), timestamp(), format),
    })
  );
} else {
  if (!process.env.LOGZIO_TOKEN) {
    throw new Error("logz.io token must be specified");
  }
  const logzioWinstonTransport = new LogzioWinstonTransport({
    level: "info",
    token: process.env.LOGZIO_TOKEN,
    host: "listener-eu.logz.io",
    name: "bundle-scanner",
  });
  logger.add(logzioWinstonTransport);
  logger.add(new winston.transports.Console({ format: winston.format.simple() }));
}

export default logger;
