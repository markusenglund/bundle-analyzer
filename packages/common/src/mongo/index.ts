import mongoDb, { Db } from "mongodb";

const MONGO_URL =
  process.env.NODE_ENV === "production" ? process.env.MONGO_URL : process.env.MONGO_URL_DEV;
if (!MONGO_URL) {
  throw new Error("Mongodb URL env variable must be specified");
}

interface TokenizedLibrary {
  _id: string;
  downloads: number;
  sourceMapFrequency: number;
  dependencies: { libraryId: string; versionRange: string }[];
  description: string;
  isTokenized8: boolean;
  lastModified: Date;
  license: string;
  numErrors: number;
  numVersions: number;
  numSkippedReleases: number;
  repositoryUrl: string;
  tokenizingTimeMs: number;
}

interface NonTokenizedLibrary {
  _id: string;
  downloads: number;
}
export type Library = TokenizedLibrary | NonTokenizedLibrary;

export interface Release {
  _id: string;
  libraryId: string;
  version: string;
  numFourGrams: number;
  numUniqueFourGrams: number;
  numFiles: number;
  size?: number;
  dependencies: {
    libraryId: string;
    versionRange: string;
  }[];
  bundledDependencies: {
    libraryId: string;
    version: string;
  }[];
  sourceMapFrequency: number;
  releaseDate?: Date;
}

export interface File {
  _id: string;
  fileName: string;
  fourGramHash: string;
  libraryId: string;
  numFourGrams: number;
  releaseIds: string[];
  step: number;
  maxIdfScore: number;
}

export interface FourGram {
  _id: string;
  fileIds: string[];
}

export interface Url {
  _id: string;
  host: string;
  domain: string;
  faviconUrl?: string;
  redirectedUrls?: string[];
  createdDate: Date;
}

export interface UrlScan {
  url: string;
  date: Date;
  source: "extension" | "puppeteer";
  firstPartyBundleIds: string[];
  thirdPartyBundleIds: string[];
  failedBundleIds: string[];
  totalSizeBytes: number;
  totalSizeBytesGzip: number;
}

export interface BenchmarkBundle {
  _id: string;
  fullUrl: string;
  bundleDomain: string;
  domain: string;
  sizeBytes: number;
  sizeBytesGzip: number;
  createdDate: Date;
  code: string;
  sourceMapData: {
    url: string;
    libraryIds: string[];
    modulePaths: string[];
    intervals: {
      libraryId: string;
      fileName: string;
      start: number;
      end: number;
    }[];
  };
}

export interface Bundle {
  _id: string;
  fullUrl: string;
  bundleDomain?: string; // TODO
  domains?: string[];
  hosts?: string[];
  pageUrls?: string[];
  createdDate: Date;
  thirdPartyWebData?: {
    name: string;
    categories: string[];
  };
}

type ScanMatchedReleases = {
  _id: string;
  libraryId: string;
  version: string;
  fileIds: string[];
  sourceMapFrequency: number;
  score: number;
  idfScoreRatio: number;
  maxIdfScore: number;
  cumulativeIntervalSize: number;
  intervals: {
    start: number;
    end?: number;
    fileName: string;
    libraryId: string;
    intervalMatchRatio: number;
    intervalSizeFourGrams: number;
    maxIntervalSizeFourGrams: number;
  }[];
  dependencies: {
    libraryId: string;
    versionRange: string;
  }[];
  bundledDependencies: {
    libraryId: string;
    version: string;
  }[];
  description: string;
  repositoryUrl: string;
  license?: string;
}[];

type SourceMapInterval = {
  start: number;
  end?: number;
  fileName: string;
  libraryId: string;
};

type SourceMapMatchedReleases = {
  _id: string;
  libraryId: string;
  cumulativeIntervalSize: number;
  intervals: SourceMapInterval[];
  sourceMapFrequency?: number;
  dependencies?: {
    libraryId: string;
    versionRange: string;
  }[];
  description?: string;
  repositoryUrl?: string;
  license?: string;
}[];

export type MatchedReleases = ScanMatchedReleases | SourceMapMatchedReleases;

export interface BundleScan {
  bundleUrl: string;
  scannerVersion: string;
  date: Date;
  checkSum?: string;
  sizeBytes: number;
  sizeBytesGzip: number;
  numChars: number;
  analysisMode: "scan" | "sourcemap";
  sourceMapData?: {
    url?: string;
    libraryIds: string[];
    modulePaths: string[];
  };
  matchedReleases: MatchedReleases;
}

export interface SourceMapFile {
  _id: string;
  libraryId: string;
  sourceMapFrequency: number;
  sourceMapRatio: number;
}

// export interface Website {
//   _id: mongoDb.ObjectId;
//   url: string;
//   rank?: number;
//   rank2?: number;
//   scripts?: {
//     scriptUrl: string;
//     libraryIds: string[];
//     sourceMapUrl?: string;
//   }[];
//   failedScrape?: boolean;
// }

interface DbProxy extends Db {
  libraries: mongoDb.Collection<Library>;
  releases: mongoDb.Collection<Release>;
  files: mongoDb.Collection<File>;
  fourGrams: mongoDb.Collection<FourGram>;
  // analyzedUrls: mongoDb.Collection<AnalyzedUrl>;
  // analyzedBundles: mongoDb.Collection<AnalyzedBundle>;
  // websites: mongoDb.Collection<Website>;
  sourceMapFiles: mongoDb.Collection<SourceMapFile>;
  urls: mongoDb.Collection<Url>;
  urlScans: mongoDb.Collection<UrlScan>;
  bundles: mongoDb.Collection<Bundle>;
  bundleScans: mongoDb.Collection<BundleScan>;
}

let database: Db | null;
export const connect: () => Promise<void> = async () => {
  const client = await mongoDb.MongoClient.connect(MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    ignoreUndefined: true,
    serverSelectionTimeoutMS: 10 * 1000,
  });
  database = client.db();
};

// @ts-ignore
export const db: DbProxy = new Proxy(
  {},
  {
    get: (obj, key: string) => {
      if (!database) {
        throw new Error("Tried to call MongoDB before initializing connection");
      }
      if (database[key]) {
        return database[key];
      }
      return database.collection(key);
    },
  }
);
