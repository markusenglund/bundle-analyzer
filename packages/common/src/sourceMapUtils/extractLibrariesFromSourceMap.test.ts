import extractLibrariesFromSourceMap from "./extractLibrariesFromSourceMap.js";

const testSourceMap = {
  version: 3,
  sources: [
    "webpack://$jQ111/(webpack)/buildin/global.js",
    "webpack://adme-frontend/./node_modules/lean-intl/locale-data/json|lazy|/^\\.\\/.*\\.json$/|chunkName: lean-intl|groupOptions: {}|namespace object",
    "webpack:///./node_modules/react-use/esm/useClickAway.js",
    "node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/a11y.js",
  ],
  names: [],
  mappings: "",
  sourcesContent: [],
};

describe("extractLibrariesFromSourceMap", () => {
  const { libraryIds } = extractLibrariesFromSourceMap(testSourceMap);
  it("Correctly parses weird webpack string", () => {
    expect(libraryIds).toEqual(["webpack", "lean-intl", "react-use", "@angular/cdk"]);
  });
});
