import getModulePathFromSource from "./getModulePathFromSource.js";

interface RegularSourceMap {
  version: number;
  sources: string[];
  names: string[];
  sourcesContent?: string[];
  mappings: string;
}

interface SectionSourceMap {
  version: number;
  sections: { map: { sources: string[] } }[];
}

export type SourceMap = RegularSourceMap | SectionSourceMap;

const extractLibrariesFromSourceMap = (
  sourceMap: SourceMap
): { libraryIds: string[]; modulePaths: string[] } => {
  const sources =
    "sections" in sourceMap
      ? sourceMap.sections.flatMap((section) => section.map.sources)
      : sourceMap.sources;

  const modulePaths = sources
    .filter((source) => source.includes("node_modules/") || source.includes("(webpack)/"))
    .map(getModulePathFromSource);

  const libraryIds = [
    ...new Set(
      modulePaths.map((path) => {
        const parts = path.split("/");
        if (path.startsWith("@")) {
          const library = `${parts[0]}/${parts[1]}`;
          return library;
        }
        const library = parts[0];
        return library;
      })
    ),
  ];

  return { libraryIds, modulePaths };
};

export default extractLibrariesFromSourceMap;

// const sourceMap = require("fs").readFileSync("../testProject/dist/main.js.map");
// (async () => {
//   const { body: sourceMap } = await require("got")(
//     "https://giphy.com/static/dist/desktopEntry.5fa1076b.bundle.js.map"
//   );
//   const libraryIds = await extractLibraryIdsFromSourceMap(
//     JSON.parse(sourceMap)
//   );
//   console.log(libraryIds);
//   process.exit(0);
// })();
