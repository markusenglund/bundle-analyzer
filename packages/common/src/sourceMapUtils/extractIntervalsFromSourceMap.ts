import { SourceMapConsumer, RawSourceMap } from "source-map";
import { sum } from "lodash-es";
import getModulePathFromSource from "./getModulePathFromSource.js";
import getModulePathParts from "./getModulePathParts.js";
import { SourceMap } from "./extractLibrariesFromSourceMap.js";

function detectEol(content: string): string {
  const LF = "\n";
  const CR_LF = "\r\n";
  return content.includes(CR_LF) ? CR_LF : LF;
}

type Intervals = {
  fileName: string;
  start: number;
  end?: number;
  libraryId?: string;
}[];

const extractIntervalsFromSourceMap = async (
  sourceMap: RawSourceMap | SourceMap,
  code: string
): Promise<Intervals> => {
  // TODO: Is this typing an error?
  const consumer = await new SourceMapConsumer(sourceMap);

  let previousSource: string | undefined;

  const eol = detectEol(code);
  const codeLines = code.split(eol);
  const lineLengths = codeLines.map((line) => line.length + eol.length);
  const lineCharIndices = lineLengths.map((length, lineIndex) =>
    sum(lineLengths.slice(0, lineIndex))
  );

  const intervals: Intervals = [];
  consumer.computeColumnSpans();
  consumer.eachMapping(
    ({
      source,
      generatedLine,
      generatedColumn: startColumnIndex,
      lastGeneratedColumn: endColumnIndex,
    }) => {
      const lineIndex = generatedLine - 1;
      const startCharIndex = lineCharIndices[lineIndex] + startColumnIndex;
      // If the endColumnIndex is null we assume it is the end of the line
      const endCharIndex = lineCharIndices[lineIndex] + (endColumnIndex ?? lineLengths[lineIndex]);
      // Ignore 'dead space' intervals that don't belong to any file and have no source
      if (source != null) {
        // Create a one single interval for each file
        if (previousSource !== source) {
          if (source.includes("node_modules/") || source.includes("(webpack)")) {
            const sourceModulePath = getModulePathFromSource(source);
            const { fileName, libraryId } = getModulePathParts(sourceModulePath);
            intervals.push({ fileName, libraryId, start: startCharIndex });
          } else {
            const fileName = source.replace("webpack://", "");
            intervals.push({ fileName, start: startCharIndex });
          }
        }
        intervals[intervals.length - 1].end = endCharIndex;
      }
      previousSource = source;
    }
  );

  consumer.destroy();

  if (intervals.some((interval) => Number.isNaN(interval.start) || Number.isNaN(interval.end))) {
    throw new Error(
      `Something went wrong when parsing source maps: an interval included NaN in its position.`
    );
  }

  return intervals;
};

export default extractIntervalsFromSourceMap;
