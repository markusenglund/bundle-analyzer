const getModulePathParts = (modulePath: string): { libraryId: string; fileName: string } => {
  const parts = modulePath.split("/");
  let libraryId;
  if (modulePath.startsWith("@")) {
    libraryId = `${parts[0]}/${parts[1]}`;
  } else {
    libraryId = parts[0];
  }
  const fileName = modulePath.replace(libraryId, "");
  return { libraryId, fileName };
};

export default getModulePathParts;
