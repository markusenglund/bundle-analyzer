const getModulePathFromSource = (source: string): string => {
  if (source.includes("(webpack)/")) {
    const parts = source.split("(webpack)/");
    return `webpack/${parts[parts.length - 1]}`;
  }
  const parts = source.split("node_modules/");
  return parts[parts.length - 1];
};

export default getModulePathFromSource;
