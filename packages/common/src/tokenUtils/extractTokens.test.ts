import extractTokens from "./extractTokens.js";

const testSnippet1 =
  '"use strict";var __importDefault=this&&this.__importDefault||function(mod){return mod&&mod.__esModule?mod:{default:mod}};Object.defineProperty(exports,"__esModule",{value:!0}),exports.gstMayApply=exports.isTierTypeSubjectToGST=exports.accountHasGST=exports.GST_RATE_PERCENT=void 0;var lodash_1=require("lodash"),TierType_1=__importDefault(require("./types/TierType"));exports.GST_RATE_PERCENT=15,exports.accountHasGST=function(account){return Boolean(lodash_1.get(account,"settings.GST")||lodash_1.get(account,"parent.settings.GST")||lodash_1.get(account,"parentCollective.settings.GST"))},exports.isTierTypeSubjectToGST=function(tierType){return[TierType_1.default.SUPPORT,TierType_1.default.SERVICE,TierType_1.default.PRODUCT,TierType_1.default.TICKET].includes(tierType)},exports.gstMayApply=function(tierType){return exports.isTierTypeSubjectToGST(tierType)};';

const testSnippetLongArray =
  '"use strict";Object.defineProperty(exports,"__esModule",{value:!0}),exports.isMemberOfTheEuropeanUnion=exports.europeanCountries=void 0,exports.europeanCountries=["AT","BE","BG","HR","CY","CZ","DK","EE","FI","FR","DE","GR","HU","IE","IT","LV","LT","LU","MT","NL","PL","PT","RO","SK","SI","ES","SE","GB"],exports.isMemberOfTheEuropeanUnion=function(countryCode){return Boolean(countryCode)&&exports.europeanCountries.includes(countryCode)};';

describe("extractTokens", () => {
  it("Matches snapshot of regular code snippet 1", () => {
    expect(extractTokens(testSnippet1)).toMatchSnapshot();
  });
  it("Matches snapshot of snippet with long array", () => {
    expect(extractTokens(testSnippetLongArray)).toMatchSnapshot();
  });
});
