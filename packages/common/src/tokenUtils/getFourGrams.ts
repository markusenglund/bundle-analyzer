import extractTokens from "./extractTokens.js";

interface ReturnType {
  fourGrams: string[];
  fourGramIndex: Map<string, number[]>;
  uniqueFourGrams: Set<string>;
  fourGramPositions: { start: number; end: number }[];
}

const getFourGrams = (snippet: string): ReturnType => {
  const { tokens, tokenPositions } = extractTokens(snippet, 500000);

  const fourGrams = [];
  const fourGramPositions = [];
  const fourGramIndex = new Map();

  for (let i = 0; i + 3 < tokens.length; i += 1) {
    let fourGram = tokens[i];

    for (let j = i + 1; j < i + 4; j++) {
      fourGram = `${fourGram}·${tokens[j]}`;
    }
    fourGrams.push(fourGram);
    fourGramPositions.push({
      start: tokenPositions[i].start,
      end: tokenPositions[i + 3].end,
    });

    fourGramIndex.set(
      fourGram,
      fourGramIndex.has(fourGram) ? [...fourGramIndex.get(fourGram), i] : [i]
    );
  }

  const uniqueFourGrams = new Set(fourGrams);

  return { fourGrams, fourGramIndex, uniqueFourGrams, fourGramPositions };
};

export default getFourGrams;
