import * as acorn from "acorn";
import * as acornLoose from "acorn-loose";
import * as walk from "acorn-walk";
import { CustomError } from "../utils/index.js";
import logger from "../logger/index.js";

const MAX_TOKENS = 20000;
// Values which are often removed or added by minification
const literalBlacklist = new Set([true, false, 0, 1, ""]);
const memberExpressionBlacklist = new Set(["exports"]);

interface Node extends acorn.Node {
  value: string;
  computed: boolean;
  property?: {
    type: string;
    name: string;
    start: number;
    end: number;
    value: string;
    raw: string;
  };
  key?: {
    type: string;
    name: string;
    start: number;
    end: number;
    value: string;
    raw: string;
  };
}

function extractTokens(snippet: string, maxTokens = MAX_TOKENS) {
  const tokenPositions: { start: number; end: number; token: string }[] = [];

  const ast = getAst(snippet);

  try {
    walk.ancestor(ast, {
      Literal(node: Node, ancestors?: { type?: string; callee?: { name?: string } }[]) {
        const nextToLastAncestor = ancestors?.[ancestors.length - 2];
        // TODO: This if statement is only necessary for npm packages, not bundles
        if (
          (nextToLastAncestor?.type &&
            ["ImportDeclaration", "ExportNamedDeclaration"].includes(nextToLastAncestor.type)) ||
          nextToLastAncestor?.callee?.name === "require"
        ) {
          return;
        }

        if (
          nextToLastAncestor?.type === "ArrayExpression" &&
          ancestors?.[ancestors.length - 3]?.callee?.name === "define"
        ) {
          return;
        }

        if (literalBlacklist.has(node.value)) {
          return;
        }

        // Only index the first 8 literals from an array to avoid giving to much weight to long arrays in the matching function
        const MAX_ARRAY_INDEX = 8;
        if (
          nextToLastAncestor?.type === "ArrayExpression" &&
          nextToLastAncestor.elements?.length > MAX_ARRAY_INDEX
        ) {
          let numLiterals = 0;
          for (const element of nextToLastAncestor.elements) {
            if (element?.type !== "Literal") continue;
            const currentNodeShouldBeIndexed = element.start === node.start;
            if (currentNodeShouldBeIndexed) break;

            numLiterals += 1;
            if (numLiterals >= MAX_ARRAY_INDEX) {
              return;
            }
          }
        }

        tokenPositions.push({
          start: node.start,
          end: node.end,
          token: node.value,
        });
        if (tokenPositions.length > maxTokens) {
          throw new CustomError(
            "MaxTokensExceeded",
            `Number of tokens exceeded maximum of ${maxTokens}`
          );
        }
      },
      // Add property keys, except computed ones which can be minified
      Property(node: Node) {
        if (node.computed) {
          return;
        }

        if (node.key?.type === "Identifier") {
          tokenPositions.push({
            start: node.key.start,
            end: node.key.end,
            token: node.key.name,
          });
        } else if (node.key?.type === "Literal") {
          tokenPositions.push({
            start: node.key.start,
            end: node.key.end,
            token: node.key.value,
          });
        }
      },
      // Add properties and methods
      MemberExpression(node: Node) {
        if (node.computed) {
          return;
        }
        if (!node.property || memberExpressionBlacklist.has(node.property.name)) {
          return;
        }
        tokenPositions.push({
          start: node.property.start,
          end: node.property.end,
          token: node.property.name,
        });
      },
    });
  } catch (err) {
    if (err.name === "MaxTokensExceeded") {
      logger.info("Number of tokens exceeded maximum, cancelling walk...");
    } else {
      throw err;
    }
  }

  tokenPositions.sort((a, b) => a.start - b.start);

  // undefined and null can't run toString()
  const tokensMaxLength = tokenPositions.map(({ token }) => String(token).slice(0, 15));

  return {
    tokens: tokensMaxLength,
    tokenPositions: tokenPositions.map(({ start, end }) => ({ start, end })),
  };
}

function getAst(snippet: string): acorn.Node {
  const options: acorn.Options = {
    sourceType: "module",
    ecmaVersion: 2021,
  };

  // If the acorn parser throws (usually due to invalid syntax), try with the error resistant loose version
  try {
    const ast = acorn.parse(snippet, options);
    return ast;
  } catch (err) {
    const ast: acorn.Node = acornLoose.parse(snippet, options);
    return ast;
  }
}

export default extractTokens;
