export default {
  verbose: true,
  testTimeout: 100 * 1000,
  testPathIgnorePatterns: ["/src/"],
};
